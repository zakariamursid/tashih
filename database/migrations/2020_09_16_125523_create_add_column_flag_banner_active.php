<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnFlagBannerActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('gallery_mushaf')) {
            Schema::table('gallery_mushaf', function (Blueprint $table) {
                $table->string('flag_showhome', 255)->default('No')->after("id_cms_users");
            });
        }

        if (Schema::hasTable('info_seputar_lajnah')) {
            Schema::table('info_seputar_lajnah', function (Blueprint $table) {
                $table->string('flag_showhome', 255)->default('No')->after("id_cms_users");
            });
        }

        if (Schema::hasTable('list_siaran_pers')) {
            Schema::table('list_siaran_pers', function (Blueprint $table) {
                $table->string('flag_showhome', 255)->default('No')->after("deleted_at");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('gallery_mushaf')) {
            Schema::table('gallery_mushaf', function (Blueprint $table) {
                $table->dropColumn('flag_showhome');
            });
        }

        if (Schema::hasTable('info_seputar_lajnah')) {
            Schema::table('info_seputar_lajnah', function (Blueprint $table) {
                $table->dropColumn('flag_showhome');
            });
        }

        if (Schema::hasTable('list_siaran_pers')) {
            Schema::table('list_siaran_pers', function (Blueprint $table) {
                $table->dropColumn('flag_showhome');
            });
        }
    }
}
