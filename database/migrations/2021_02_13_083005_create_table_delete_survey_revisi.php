<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDeleteSurveyRevisi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_nama', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama', 255)->nullable();
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->string('status', 255)->nullable();
        });
        Schema::create('survey_unsur', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nama', 255)->nullable();
        });
        Schema::create('survey_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('type', 255)->nullable();
            $table->string('nama', 255)->nullable();
        });
        Schema::create('survey_opsi_pertanyaan', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('survey_pertanyaan_id')->nullable()->index();
            $table->string('type', 255)->nullable();
            $table->string('nama', 255)->nullable();
            $table->integer('point')->nullable();
        });
        Schema::create('survey_connect', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('survey_nama_id')->nullable()->index();
            $table->integer('survey_unsur_id')->nullable()->index();
            $table->integer('survey_pertanyaan_id')->nullable()->index();
        });
        Schema::create('survey_answer', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('type_answer', 255)->nullable();
            $table->integer('users_id')->nullable()->index();
            $table->integer('survey_nama_id')->nullable()->index();
            $table->integer('survey_unsur_id')->nullable()->index();
            $table->integer('survey_pertanyaan_id')->nullable()->index();
            $table->text('answer')->nullable();
            $table->integer('point')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_nama');
        Schema::dropIfExists('survey_unsur');
        Schema::dropIfExists('survey_pertanyaan');
        Schema::dropIfExists('survey_opsi_pertanyaan');
        Schema::dropIfExists('survey_connect');
        Schema::dropIfExists('survey_answer');
    }
}
