<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTempCmsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_cms_users', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->integer('id_cms_privileges')->nullable();
            $table->string('status')->nullable();
            $table->integer('id_penerbit')->nullable();
            $table->string('password_value')->nullable();
            $table->string('phone')->nullable();
            $table->string('pic')->nullable();
            $table->text('address')->nullable();
            $table->integer('id_m_jenis_lembaga')->nullable();
            $table->string('surat_permohonan')->nullable();
            $table->string('npwp')->nullable();
            $table->string('akte_notaris')->nullable();
            $table->string('company_profile')->nullable();
            $table->string('siup')->nullable();
            $table->string('tdp')->nullable();
            $table->string('nib')->nullable();
            $table->string('scan_ktp')->nullable();
            $table->integer('send_profile')->nullable();

            $table->integer('id_province')->nullable();
            $table->integer('id_city')->nullable();
            $table->integer('id_district')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('unique_hash')->nullable();

            $table->string('nib_value')->nullable();
            $table->string('npwp_value')->nullable();

            $table->string('pic_jabatan')->nullable();
            $table->string('pic_phone')->nullable();
            $table->string('pic_email')->nullable();
            $table->text('pic_address')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_cms_users');
    }
}
