<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProsesPentashihanAddAtcIzinEdar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->integer('id_countries')->nullable();
            $table->string('nama_penerbit_asal')->nullable();
            $table->string('nama_lembaga_pentashih_asal')->nullable();
            $table->string('bukti_tashih_lembaga_pentashih_asal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->dropColumn('id_countries');
            $table->dropColumn('nama_penerbit_asal');
            $table->dropColumn('nama_lembaga_pentashih_asal');
            $table->dropColumn('bukti_tashih_lembaga_pentashih_asal');
        });
    }
}
