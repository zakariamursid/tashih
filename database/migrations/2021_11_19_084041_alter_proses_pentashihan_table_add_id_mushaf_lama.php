<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProsesPentashihanTableAddIdMushafLama extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_proses_pentashihan', function (Blueprint $table) {
            $table->integer('id_mushaf_lama')->nullable();
        });
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->integer('id_mushaf_lama')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_proses_pentashihan', function (Blueprint $table) {
            $table->dropColumn('id_mushaf_lama');
        });
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->dropColumn('id_mushaf_lama');
        });
    }
}
