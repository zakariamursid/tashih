<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempProsesPentashihanMateriTambahanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_proses_pentashihan_materi_tambahan', function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_proses_pentashihan')->nullable();
            $table->string('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_proses_pentashihan_materi_tambahan');
    }
}
