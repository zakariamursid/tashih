<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUnsurSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_unsur', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_kategori_survey')->nullable();
            $table->string('nama', 255)->nullable();
        });

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function (Blueprint $table) {
                $table->dropColumn('id_kategori_survey');
                $table->integer('id_survey_unsur')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_unsur');

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function (Blueprint $table) {
                $table->integer('id_kategori_survey')->nullable();
                $table->dropColumn('id_survey_unsur');
            });
        }
    }
}
