<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProsesPentashihanLaporanPercetakan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('proses_pentashihan_laporan_percetakan')) {
            Schema::table('proses_pentashihan_laporan_percetakan', function (Blueprint $table) {
                $table->string('cetakan_ke', 255)->nullable()->after('id_proses_pentashihan');
                $table->string('ukuran', 255)->nullable()->after('tanggal_cetak');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('proses_pentashihan_laporan_percetakan')) {
            Schema::table('proses_pentashihan_laporan_percetakan', function (Blueprint $table) {
                $table->dropColumn('cetakan_ke');
                $table->dropColumn('ukuran');
            });
        }
    }
}
