<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('kategori_survey')) {
            Schema::table('kategori_survey', function (Blueprint $table) {
                $table->datetime('to')->nullable()->after('jenis_kategori');
                $table->datetime('from')->nullable()->after('jenis_kategori');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('kategori_survey')) {
            Schema::table('kategori_survey', function (Blueprint $table) {
                $table->dropColumn('to');
                $table->dropColumn('from');
            });
        }
    }
}
