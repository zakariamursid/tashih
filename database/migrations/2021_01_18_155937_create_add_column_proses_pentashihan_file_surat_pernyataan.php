<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnProsesPentashihanFileSuratPernyataan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('proses_pentashihan', 'file_surat_pernyataan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->string('file_surat_pernyataan', 255)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('proses_pentashihan', 'file_surat_pernyataan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->dropColumn('file_surat_pernyataan', 255)->nullable();
            });
        }
    }
}
