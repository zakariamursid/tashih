<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('banner')) {
            Schema::table('banner', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('cms_users')) {
            Schema::table('cms_users', function(Blueprint $table) {
                $table->index('id_cms_privileges');
                $table->index('id_penerbit');
                $table->index('id_m_jenis_lembaga');
            });
        }

        if (Schema::hasTable('download_materi')) {
            Schema::table('download_materi', function(Blueprint $table) {
                $table->index('id_kategori_download');
            });
        }

        if (Schema::hasTable('email_notif')) {
            Schema::table('email_notif', function(Blueprint $table) {
                $table->index(['id_email_notif_template'], 'index_1');
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('faq')) {
            Schema::table('faq', function(Blueprint $table) {
                $table->index('id_cms_user');
            });
        }

        if (Schema::hasTable('faq_isi')) {
            Schema::table('faq_isi', function(Blueprint $table) {
                $table->index('faq_kategori_id');
            });
        }

        if (Schema::hasTable('faq_jawaban')) {
            Schema::table('faq_jawaban', function(Blueprint $table) {
                $table->index('faq_id');
                $table->index('cms_user_id');
            });
        }

        if (Schema::hasTable('forum')) {
            Schema::table('forum', function(Blueprint $table) {
                $table->index('id_cms_user');
            });
        }

        if (Schema::hasTable('forum_jawaban')) {
            Schema::table('forum_jawaban', function(Blueprint $table) {
                $table->index('id_forum');
                $table->index('id_cms_user');
            });
        }

        if (Schema::hasTable('gallery_mushaf')) {
            Schema::table('gallery_mushaf', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('info_seputar_lajnah')) {
            Schema::table('info_seputar_lajnah', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('jawaban_survey')) {
            Schema::table('jawaban_survey', function(Blueprint $table) {
                $table->index('id_cms_users');
                $table->index('id_survey');
                $table->index(['id_survey_opsi_jawaban'], 'index_1');
            });
        }

        if (Schema::hasTable('laporan_mushaf_bermasalah_detail')) {
            Schema::table('laporan_mushaf_bermasalah_detail', function(Blueprint $table) {
                $table->index(['id_laporan_mushaf_bermasalah'], 'index_1');
                $table->index(['id_m_jenis_kesalahan'], 'index_2');
            });
        }

        if (Schema::hasTable('list_siaran_pers')) {
            Schema::table('list_siaran_pers', function(Blueprint $table) {
                $table->index('id_cms_user');
            });
        }

        if (Schema::hasTable('master_pointing_user')) {
            Schema::table('master_pointing_user', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('materi_upload')) {
            Schema::table('materi_upload', function(Blueprint $table) {
                $table->index(['id_download_materi'], 'index_1');
            });
        }

        if (Schema::hasTable('monitoring')) {
            Schema::table('monitoring', function(Blueprint $table) {
                $table->index('id_m_tipe_lokasi');
            });
        }

        if (Schema::hasTable('monitoring_tidak_lanjut')) {
            Schema::table('monitoring_tidak_lanjut', function(Blueprint $table) {
                $table->index(['id_monitoring'], 'index_1');
                $table->index(['id_cms_users'], 'index_2');
                $table->index(['id_m_rekomendasi_aksi'], 'index_3');
            });
        }

        if (Schema::hasTable('monitoring_tidak_lanjut_lampiran')) {
            Schema::table('monitoring_tidak_lanjut_lampiran', function(Blueprint $table) {
                $table->index(['id_monitoring_tidak_lanjut'], 'index_1');
            });
        }

        if (Schema::hasTable('page')) {
            Schema::table('page', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('penerbit')) {
            Schema::table('penerbit', function(Blueprint $table) {
                $table->index('id_m_jenis_lembaga');
            });
        }

        if (Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function(Blueprint $table) {
                $table->index(['id_cms_users'], 'index_1');
                $table->index(['id_m_jenis_naskah'], 'index_2');
                $table->index(['id_m_jenis_mushaf'], 'index_3');
                $table->index(['id_m_materi_tambahan'], 'index_4');
            });
        }

        if (Schema::hasTable('proses_pentashihan_bukti_penerimaan')) {
            Schema::table('proses_pentashihan_bukti_penerimaan', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_histori_status')) {
            Schema::table('proses_pentashihan_histori_status', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_koreksi')) {
            Schema::table('proses_pentashihan_koreksi', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
                $table->index(['id_cms_users'], 'index_2');
            });
        }

        if (Schema::hasTable('proses_pentashihan_laporan_percetakan')) {
            Schema::table('proses_pentashihan_laporan_percetakan', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'],'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_materi_tambahan')) {
            Schema::table('proses_pentashihan_materi_tambahan', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_rating')) {
            Schema::table('proses_pentashihan_rating', function(Blueprint $table) {
                $table->index(['id_cms_users'],'index_1');
                $table->index(['id_proses_pentashihan'], 'index_2');
            });
        }

        if (Schema::hasTable('proses_pentashihan_surat_pernyataan')) {
            Schema::table('proses_pentashihan_surat_pernyataan', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_ukuran')) {
            Schema::table('proses_pentashihan_ukuran', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'], 'index_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_user')) {
            Schema::table('proses_pentashihan_user', function(Blueprint $table) {
                $table->index(['id_proses_pentashihan'],'index_1');
                $table->index(['id_cms_users'], 'index_2');
            });
        }

        if (Schema::hasTable('rating')) {
            Schema::table('rating', function(Blueprint $table) {
                $table->index('id_cms_users');
            });
        }

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function(Blueprint $table) {
                $table->index('id_survey_unsur');
            });
        }

        if (Schema::hasTable('survey_opsi_jawaban')) {
            Schema::table('survey_opsi_jawaban', function(Blueprint $table) {
                $table->index(['id_survey'],'index_1');
            });
        }

        if (Schema::hasTable('survey_unsur')) {
            Schema::table('survey_unsur', function(Blueprint $table) {
                $table->index(['id_kategori_survey'], 'index_1');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('banner')) {
            Schema::table('banner', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('cms_users')) {
            Schema::table('cms_users', function(Blueprint $table) {
                $table->dropIndex('id_cms_privileges');
                $table->dropIndex('id_penerbit');
                $table->dropIndex('id_m_jenis_lembaga');
            });
        }

        if (Schema::hasTable('download_materi')) {
            Schema::table('download_materi', function(Blueprint $table) {
                $table->dropIndex('id_kategori_download');
            });
        }

        if (Schema::hasTable('email_notif')) {
            Schema::table('email_notif', function(Blueprint $table) {
                $table->dropIndex(['id_email_notif_template'], 'dropIndex_1');
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('faq')) {
            Schema::table('faq', function(Blueprint $table) {
                $table->dropIndex('id_cms_user');
            });
        }

        if (Schema::hasTable('faq_isi')) {
            Schema::table('faq_isi', function(Blueprint $table) {
                $table->dropIndex('faq_kategori_id');
            });
        }

        if (Schema::hasTable('faq_jawaban')) {
            Schema::table('faq_jawaban', function(Blueprint $table) {
                $table->dropIndex('faq_id');
                $table->dropIndex('cms_user_id');
            });
        }

        if (Schema::hasTable('forum')) {
            Schema::table('forum', function(Blueprint $table) {
                $table->dropIndex('id_cms_user');
            });
        }

        if (Schema::hasTable('forum_jawaban')) {
            Schema::table('forum_jawaban', function(Blueprint $table) {
                $table->dropIndex('id_forum');
                $table->dropIndex('id_cms_user');
            });
        }

        if (Schema::hasTable('gallery_mushaf')) {
            Schema::table('gallery_mushaf', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('info_seputar_lajnah')) {
            Schema::table('info_seputar_lajnah', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('jawaban_survey')) {
            Schema::table('jawaban_survey', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
                $table->dropIndex('id_survey');
                $table->dropIndex(['id_survey_opsi_jawaban'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('laporan_mushaf_bermasalah_detail')) {
            Schema::table('laporan_mushaf_bermasalah_detail', function(Blueprint $table) {
                $table->dropIndex(['id_laporan_mushaf_bermasalah'], 'dropIndex_1');
                $table->dropIndex(['id_m_jenis_kesalahan'], 'dropIndex_2');
            });
        }

        if (Schema::hasTable('list_siaran_pers')) {
            Schema::table('list_siaran_pers', function(Blueprint $table) {
                $table->dropIndex('id_cms_user');
            });
        }

        if (Schema::hasTable('master_pointing_user')) {
            Schema::table('master_pointing_user', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('materi_upload')) {
            Schema::table('materi_upload', function(Blueprint $table) {
                $table->dropIndex(['id_download_materi'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('monitoring')) {
            Schema::table('monitoring', function(Blueprint $table) {
                $table->dropIndex('id_m_tipe_lokasi');
            });
        }

        if (Schema::hasTable('monitoring_tidak_lanjut')) {
            Schema::table('monitoring_tidak_lanjut', function(Blueprint $table) {
                $table->dropIndex(['id_monitoring'], 'dropIndex_1');
                $table->dropIndex(['id_cms_users'], 'dropIndex_2');
                $table->dropIndex(['id_m_rekomendasi_aksi'], 'dropIndex_3');
            });
        }

        if (Schema::hasTable('monitoring_tidak_lanjut_lampiran')) {
            Schema::table('monitoring_tidak_lanjut_lampiran', function(Blueprint $table) {
                $table->dropIndex(['id_monitoring_tidak_lanjut'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('page')) {
            Schema::table('page', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('penerbit')) {
            Schema::table('penerbit', function(Blueprint $table) {
                $table->dropIndex('id_m_jenis_lembaga');
            });
        }

        if (Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function(Blueprint $table) {
                $table->dropIndex(['id_cms_users'], 'dropIndex_1');
                $table->dropIndex(['id_m_jenis_naskah'], 'dropIndex_2');
                $table->dropIndex(['id_m_jenis_mushaf'], 'dropIndex_3');
                $table->dropIndex(['id_m_materi_tambahan'], 'dropIndex_4');
            });
        }

        if (Schema::hasTable('proses_pentashihan_bukti_penerimaan')) {
            Schema::table('proses_pentashihan_bukti_penerimaan', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_histori_status')) {
            Schema::table('proses_pentashihan_histori_status', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_koreksi')) {
            Schema::table('proses_pentashihan_koreksi', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
                $table->dropIndex(['id_cms_users'], 'dropIndex_2');
            });
        }

        if (Schema::hasTable('proses_pentashihan_laporan_percetakan')) {
            Schema::table('proses_pentashihan_laporan_percetakan', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'],'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_materi_tambahan')) {
            Schema::table('proses_pentashihan_materi_tambahan', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_rating')) {
            Schema::table('proses_pentashihan_rating', function(Blueprint $table) {
                $table->dropIndex(['id_cms_users'],'dropIndex_1');
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_2');
            });
        }

        if (Schema::hasTable('proses_pentashihan_surat_pernyataan')) {
            Schema::table('proses_pentashihan_surat_pernyataan', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_ukuran')) {
            Schema::table('proses_pentashihan_ukuran', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'], 'dropIndex_1');
            });
        }

        if (Schema::hasTable('proses_pentashihan_user')) {
            Schema::table('proses_pentashihan_user', function(Blueprint $table) {
                $table->dropIndex(['id_proses_pentashihan'],'dropIndex_1');
                $table->dropIndex(['id_cms_users'], 'dropIndex_2');
            });
        }

        if (Schema::hasTable('rating')) {
            Schema::table('rating', function(Blueprint $table) {
                $table->dropIndex('id_cms_users');
            });
        }

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function(Blueprint $table) {
                $table->dropIndex('id_survey_unsur');
            });
        }

        if (Schema::hasTable('survey_opsi_jawaban')) {
            Schema::table('survey_opsi_jawaban', function(Blueprint $table) {
                $table->dropIndex(['id_survey'],'dropIndex_1');
            });
        }

        if (Schema::hasTable('survey_unsur')) {
            Schema::table('survey_unsur', function(Blueprint $table) {
                $table->dropIndex(['id_kategori_survey'], 'dropIndex_1');
            });
        }
    }
}
