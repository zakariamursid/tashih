<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTempProsesPentashihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_proses_pentashihan', function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->string('nomor_registrasi')->nullable();
            $table->string('jenis_permohonan')->nullable();
            $table->integer('id_cms_users')->nullable();
            $table->string('id_m_jenis_naskah')->nullable();
            $table->string('file_aplikasi')->nullable();
            $table->integer('id_m_jenis_mushaf')->nullable();
            $table->string('id_m_materi_tambahan')->nullable();
            $table->text('materi_lainnya')->nullable();
            $table->string('penanggung_jawab_produk')->nullable();
            $table->string('nama_produk')->nullable();
            $table->string('ukuran')->nullable();
            $table->string('oplah')->nullable();
            $table->string('nama_percetakan')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('tashih_internal')->defaut('Belum');
            $table->text('penanggung_jawab_materi_tambahan')->nullable();
            $table->string('surat_permohonan')->nullable();
            $table->string('gambar_cover')->nullable();
            $table->string('dokumen_naskah')->nullable();
            $table->string('bukti_tashih_internal')->nullable();
            $table->string('status')->defaut('Registrasi Berhasil');
            $table->string('disposisi_nomor_agenda')->nullable();
            $table->string('disposisi_file')->nullable();
            $table->string('file_naskah')->nullable();
            $table->string('bukti_penerimaan')->nullable();
            $table->string('nama_penerima')->nullable();
            $table->date('tanggal_diterima')->nullable();
            $table->integer('status_penerimaan')->nullable();
            $table->datetime('tanggal_verifikasi')->nullable();
            $table->datetime('tanggal_deadline')->nullable();
            $table->integer('estimasi')->nullable();
            $table->string('bukti_verifikasi')->nullable();
            $table->string('nomor_tanda_tashih')->nullable();
            $table->string('kode_tanda_tashih')->nullable();
            $table->string('surat_penerbitan')->nullable();
            $table->string('scan_tanda_tashih')->nullable();
            $table->string('no_resi')->nullable();
            $table->integer('surat_pernyataan')->nullable();
            $table->string('submit')->nullable();
            $table->string('PNBP')->nullable();
            $table->text('bukti_pembayaran')->bukti_pembayaran();
            $table->integer('nominal_PNBP')->nullable();
            $table->text('bukti_PNBP')->nullable();
            $table->string('status_PNBP')->nullable();
            $table->string('file_surat_pernyataan')->nullable();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_proses_pentashihan');
    }
}
