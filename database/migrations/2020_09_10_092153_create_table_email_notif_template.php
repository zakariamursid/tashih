<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmailNotifTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_notif_template', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("name", 255)->nullable();
            $table->string("slug", 255)->nullable();
            $table->string("subject", 255)->nullable();
            $table->text("content")->nullable();
            $table->string("file", 255)->nullable();
        });

        Schema::create('email_notif', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer("id_email_notif_template")->nullable();
            $table->integer("id_cms_users")->nullable();
            $table->datetime("date_send")->nullable();
            $table->integer("is_send")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_notif_template');

        Schema::dropIfExists('email_notif');
    }
}
