<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnTableProsesPentashihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->string('jenis_permohonan', 255)->nullable()->after('nomor_registrasi');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->dropColumn('jenis_permohonan');
            });
        }
    }
}
