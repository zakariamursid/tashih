<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSurveyOpsiJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_opsi_jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_survey')->nullable();
            $table->string('nama', 255)->nullable();
            $table->integer('nilai')->nullable();
        });

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function (Blueprint $table) {
                $table->string('type', 255)->nullable()->after('id_kategori_survey');
                $table->dropColumn('status');
            });
        }

        if (Schema::hasTable('jawaban_survey')) {
            Schema::table('jawaban_survey', function (Blueprint $table) {
                $table->integer('id_survey_opsi_jawaban')->nullable()->after('id_survey');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_opsi_jawaban');

        if (Schema::hasTable('survey')) {
            Schema::table('survey', function (Blueprint $table) {
                $table->dropColumn('type');
                $table->string('status', 255)->nullable()->after('pertanyaan_survey');
            });
        }

        if (Schema::hasTable('jawaban_survey')) {
            Schema::table('jawaban_survey', function (Blueprint $table) {
                $table->dropColumn('id_survey_opsi_jawaban');
            });
        }
    }
}
