<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDeleteSurveyOld extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('kategori_survey');
        Schema::dropIfExists('survey');
        Schema::dropIfExists('survey_opsi_jawaban');
        Schema::dropIfExists('survey_unsur');
        Schema::dropIfExists('jawaban_survey');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('kategori_survey')) {
            Schema::create('kategori_survey', function (Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->softDeletes();
                $table->string('jenis_kategori', 255)->nullable();
                $table->dateTime('from')->nullable();
                $table->dateTime('to')->nullable();
                $table->string('status', 255)->nullable();
            });
        }
        if (!Schema::hasTable('survey')) {
            Schema::create('survey', function (Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->softDeletes();
                $table->string('type', 255)->nullable();
                $table->text('pertanyaan_survey')->nullable();
                $table->integer('id_survey_unsur')->nullable()->index();
            });
        }
        if (!Schema::hasTable('survey_opsi_jawaban')) {
            Schema::create('survey_opsi_jawaban', function (Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->softDeletes();
                $table->integer('id_survey')->nullable()->index();
                $table->string('nama', 255)->nullable();
                $table->string('type_option', 255)->nullable();
                $table->integer('nilai')->nullable();
            });
        }
        if (!Schema::hasTable('survey_unsur')) {
            Schema::create('survey_unsur', function (Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->softDeletes();
                $table->integer('id_kategori_survey')->nullable()->index();
                $table->string('nama', 255)->nullable();
            });
        }
        if (!Schema::hasTable('jawaban_survey')) {
            Schema::create('jawaban_survey', function (Blueprint $table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->softDeletes();
                $table->integer('id_cms_users')->nullable()->index();
                $table->integer('id_survey')->nullable()->index();
                $table->integer('id_survey_opsi_jawaban')->nullable()->index();
                $table->string('jawaban', 255)->nullable();
            });
        }
    }
}
