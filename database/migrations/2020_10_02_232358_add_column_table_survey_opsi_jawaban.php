<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTableSurveyOpsiJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('survey_opsi_jawaban')) {
            Schema::table('survey_opsi_jawaban', function (Blueprint $table) {
                $table->string('type_option', 255)->nullable()->after('nama');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('survey_opsi_jawaban')) {
            Schema::table('survey_opsi_jawaban', function (Blueprint $table) {
                $table->dropColumn('type_option');
            });
        }
    }
}
