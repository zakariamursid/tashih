<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCmsUsersAddProvinceInfoPenanggungJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->string('id_province')->nullable();
            $table->string('id_city')->nullable();
            $table->integer('id_district')->nullable();

            $table->string('kode_pos')->nullable();
            $table->string('unique_hash')->nullable();

            $table->string('nib_value')->nullable();
            $table->string('npwp_value')->nullable();

            $table->string('pic_jabatan')->nullable();
            $table->string('pic_phone')->nullable();
            $table->string('pic_email')->nullable();
            $table->text('pic_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->dropColumn('id_province')->nullable();
            $table->dropColumn('id_city')->nullable();
            $table->dropColumn('id_district')->nullable();

            $table->dropColumn('kode_pos')->nullable();
            $table->dropColumn('unique_hash')->nullable();

            $table->dropColumn('nib_value')->nullable();
            $table->dropColumn('npwp_value')->nullable();

            $table->dropColumn('pic_jabatan')->nullable();
            $table->dropColumn('pic_phone')->nullable();
            $table->dropColumn('pic_email')->nullable();
            $table->dropColumn('pic_address')->nullable();
        });
    }
}
