<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnEmailNotif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('email_notif')) {
            Schema::table('email_notif', function (Blueprint $table) {
                $table->integer('is_read')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('email_notif')) {
            Schema::table('email_notif', function (Blueprint $table) {
                $table->dropColumn('is_read');
            });
        }
    }
}
