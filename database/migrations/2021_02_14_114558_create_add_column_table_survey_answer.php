<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnTableSurveyAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('survey_answer')) {
            Schema::table('survey_answer', function (Blueprint $table) {
                $table->string('option', 255)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('survey_answer')) {
            Schema::table('survey_answer', function (Blueprint $table) {
                $table->dropColumn('option');
            });
        }
    }
}
