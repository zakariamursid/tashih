<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnProsesPentashihanTerimaNaskah extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->date('tanggal_diterima')->nullable()->after('bukti_penerimaan');
                $table->string('nama_penerima', 255)->nullable()->after('bukti_penerimaan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('proses_pentashihan')) {
            Schema::table('proses_pentashihan', function (Blueprint $table) {
                $table->dropColumn('nama_penerima');
                $table->dropColumn('tanggal_diterima');
            });
        }
    }
}
