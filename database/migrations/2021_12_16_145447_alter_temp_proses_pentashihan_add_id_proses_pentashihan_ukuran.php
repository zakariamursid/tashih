<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTempProsesPentashihanAddIdProsesPentashihanUkuran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_proses_pentashihan', function (Blueprint $table) {
            $table->integer('id_proses_pentashihan_ukuran')->nullable();
        });
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->integer('id_proses_pentashihan_ukuran')->nullable();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_proses_pentashihan', function (Blueprint $table) {
            $table->dropColumn('id_proses_pentashihan_ukuran');
        });
        Schema::table('proses_pentashihan', function (Blueprint $table) {
            $table->dropColumn('id_proses_pentashihan_ukuran');
        });
    }
}
