<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnProsesPentashihanBuktiPenerimaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('proses_pentashihan_bukti_penerimaan')) {
            Schema::table('proses_pentashihan_bukti_penerimaan', function (Blueprint $table) {
                $table->string('nama_penerima', 255)->nullable()->after('bukti_penerimaan');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('proses_pentashihan_bukti_penerimaan')) {
            Schema::table('proses_pentashihan_bukti_penerimaan', function (Blueprint $table) {
                $table->dropColumn('nama_penerima');
            });
        }
    }
}
