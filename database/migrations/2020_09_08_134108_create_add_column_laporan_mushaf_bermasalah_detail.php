<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddColumnLaporanMushafBermasalahDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('laporan_mushaf_bermasalah_detail')) {
            Schema::table('laporan_mushaf_bermasalah_detail', function (Blueprint $table) {
                $table->text('desc_masalah')->nullable()->after('gambar');
                $table->string('halaman', 255)->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('laporan_mushaf_bermasalah_detail')) {
            Schema::table('laporan_mushaf_bermasalah_detail', function (Blueprint $table) {
                $table->dropColumn('desc_masalah');
                $table->integer('halaman')->nullable()->change();
            });
        }
    }
}
