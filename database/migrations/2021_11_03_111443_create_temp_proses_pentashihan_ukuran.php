<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempProsesPentashihanUkuran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_proses_pentashihan_ukuran', function (Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->integer('id_proses_pentashihan')->nullable();
            $table->string('ukuran')->nullable();
            $table->string('oplah')->nullable();
            $table->string('nomor')->nullable();
            $table->string('kode')->nullable();
            $table->date('tanggal_penetapan')->nullable();
            $table->string('surat_penerbitan')->nullable();
            $table->string('scan_tanda_tashih')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_proses_pentashihan_ukuran');
    }
}
