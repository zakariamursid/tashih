<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTempCmsUsersProvinceEtc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_cms_users', function (Blueprint $table) {
            $table->string('id_province')->change();
            $table->string('id_city')->change();
            $table->string('id_district')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_cms_users', function (Blueprint $table) {
            $table->integer('id_province')->change();
            $table->integer('id_city')->change();
            $table->integer('id_district')->change();
        });
    }
}
