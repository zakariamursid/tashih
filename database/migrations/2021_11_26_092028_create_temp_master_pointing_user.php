<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempMasterPointingUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_master_pointing_user', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_cms_users');

            $table->string('lat')->nullable();
            $table->string('lng')->nullable();

            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_master_pointing_user');
    }
}
