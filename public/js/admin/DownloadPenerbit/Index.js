
Vue.prototype.$http = axios

Vue.component('vue-pagination', {
    data: function () {
        return {
            
        }
    },
    props: {
        pagination: {
            type: Object,
            required: true
        },
        offset: {
            type: Number,
            default: 4
        }
    },
    computed: {
        pagesNumber() {
            if (!this.pagination.to) {
                return [];
            }
            let from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            let to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            let pagesArray = [];
            for (let page = from; page <= to; page++) {
                pagesArray.push(page);
            }
            return pagesArray;
        }
    },
    methods : {
        changePage(page) {
            this.pagination.current_page = page;
            this.$emit('paginate');
        },
    },
    template: `
    <nav class="paging text-center">
        <label class="pagination-label">Showing {{ pagination.from }} from {{ pagination.total }} results</label>
        <ul class="pagination">
            <li class="page-item" :class="pagination.current_page < pagination.last_page && pagination.current_page !== pagination.last_page || pagination.current_page === 1 ? 'disabled' : ''">
                <a v-on:click.prevent="changePage(pagination.current_page - 1)" class="page-link" href="#" aria-label="prev">
                    <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                    <span class="sr-only">prev</span>
                </a>
            </li>
            <li v-for="page in pagesNumber" :class="{'active': page == pagination.current_page}" class="page-item">
                <a v-on:click.prevent="changePage(page)" class="page-link" href="#">{{ page }}</a>
            </li>
            <li :class="pagination.current_page >= pagination.last_page && pagination.current_page === pagination.last_page ? 'disabled' : ''" class="page-item">
                <a v-on:click.prevent="changePage(pagination.current_page + 1)" class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
    `
})

new Vue({
    el: '#appForm',
    data: {
        title : null,
        caption : null,
        data: {
            data: [],
            total: 0,
            per_page: 6,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: null,
        base_url: conf.base_url,
        base_asset: conf.base_asset,
        dataModal : {
            id : null,
            title : null,
            content: null,
            file: null,
            list_materi: [],
        }
    },
    mounted() {
        
    },
    created() {
        this.listData();
    },
    methods: {
        listData() {
            this.$http.post( conf.base_url + '/list-data' , {
                _token : conf.token,
                page : this.data.current_page,
                totalPage : this.data.total,
                totalRecord : this.data.per_page,
                search: this.search,
            })
            .then(res => {
                getData = res.data;

                if (getData.status == 1) {
                    this.data = getData.item;                  
                }
            })
            .catch(res => {
                console.log(res);
            })
        },
        searchInput() {
            vm = this;

            //setup width and length
            var typingTimer;
            var doneTypingInterval = 1500;

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                console.log(vm.search);

                vm.listData();
            }, doneTypingInterval);
        },
        listMateriDownload(id_materi, i) {
            find = this.data.data[i];

            id = find.id;
            title = find.judul;
            deskripsi = find.deskripsi;
            file = conf.base_asset + find.file;

            this.$http.post( conf.base_url +'/list-materi' , {
                _token : conf.token,
                id_materi : id_materi
            })
            .then(res => {
                getData = res.data;

                this.dataModal.list_materi = getData.item; 
                let materi_upload_list = {
                    id: null,
                    file: file,
                    name_file: title,
                }
                this.dataModal.list_materi.push(materi_upload_list);
            })
            .catch(res => {
                console.log(res);
            })
        },
        findMateri(i) {
            find = this.data.data[i];

            if (find != null) {
                this.emptyModal();

                id = find.id;
                title = find.judul;
                deskripsi = find.deskripsi;
                file = conf.base_asset + find.file;

                this.listMateriDownload(id, i);

                this.dataModal.id = id;
                this.dataModal.title = title;
                this.dataModal.content = deskripsi;
                
                urlFile = this.doesFileExist(find.file);
                if (urlFile === true) {
                    this.dataModal.file = file;                    
                } else if (urlFile == 'pdf') {
                    this.dataModal.file = file;                    
                } else if (urlFile == 'other') {
                    this.viewIframe("File format is not pdf cannnot view file");  
                } else {
                    this.viewIframe("File is not Found");                    
                }

                this.showModal();
            }
        },
        showModal() {
            $(function () {
                $("#modalDownload").modal("show");
            });
        },
        closeModal() {
            $(function () {
                $("#modalDownload").modal("hide");
            });
            this.emptyModal();
        },
        emptyModal() {
            this.dataModal.id = null;
            this.dataModal.title = null;
            this.dataModal.content = null;
            this.dataModal.file = '';

            this.dataModal.list_materi = [];
        },
        doesFileExist(urlToFile) {
            var xhr = new XMLHttpRequest();
            xhr.open('HEAD', urlToFile, false);
            xhr.send();

            if (xhr.status == "404" || xhr.status == "401") {
                return false;
            } else if(xhr.status == "403") {
                if (this.ValidateExtension(urlToFile) === true) {
                    return 'pdf';
                } else {
                    return 'other';
                }
            } else {
                return true;
            }
        },
        ValidateExtension(value) {
            let allowedFiles = [".pdf"];
            let regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(value.toLowerCase())) {
                return false;
            }
            return true;
        },
        viewIframe(text) {
            iframe.contentDocument.body.innerHTML  = text;
            // iframe.contentDocument.body.prepend(text);
        }
    }
});