Vue.prototype.$http = axios

new Vue({
    el: '#appForm',
    data: {
        page: 1,
        totalPage: 1,
        totalRecord: 5,
        search: null,
        isNull: false,
        data: [],
        listPenerbit: [],
        listpenerbit_temp: [],
        select_template: null,
    },
    mounted() {
        this.getList();
        this.searchInput();

        $(function () {
            // $("#select_template").select2();

            $("#checkall").click(function () {
                var is_checked = $(this).is(":checked");
                $("#modalPenerbit table input[type='checkbox']").prop("checked", !is_checked).trigger("click");
            });
        });
    },
    created() {

    },
    methods: {
        showModal: function (val) {
            this.empty();
            this.getList();

            $(function () {
                $("#modalPenerbit").modal("show");
            });
        },
        closeModal: function (val) {
            this.empty();

            $(function () {
                $("#modalPenerbit").modal("hide");
            });
        },
        empty() {
            this.search = null;
        },

        next: function (val) {
            //get next page
            if ((this.page + 1) > this.totalPage) {
                this.page = this.totalPage;
            } else {
                this.page += 1;
                this.getList();
            }

            targetId = event.currentTarget.id;
            $('#'+targetId).blur();
        },
        prev: function (val) {
            //get prev page
            if ((this.page - 1) < 1) {
                this.page = 1;
            } else {
                this.page -= 1;
                this.getList();
            }

            targetId = event.currentTarget.id;
            $('#'+targetId).blur();
        },
        changePage() {
            const self = this;          
            setTimeout(function(){
                //validation next and prev page
                if (self.page < 1 || isNaN(self.page)) {
                    self.page = 1;
                } else if (self.page > self.totalPage) {
                    self.page = self.totalPage;
                }

                self.getList();
            }, 1800);
        },
        getList() {
            this.data = [];
            this.isNull = true;

            this.$http.post( conf.base_url + '/list-data' , {
                _token : conf.token,
                page : this.page,
                totalPage : this.totalPage,
                totalRecord : this.totalRecord,
                search: this.search,
            })
            .then(res => {
                getData = res.data.data;
                
                this.isNull = false;

                this.page = getData.current_page;
                this.data = getData.data;
                this.totalPage = getData.last_page;
                this.totalRecord = getData.per_page;
            })
            .catch(res => {
                console.log(res);
            })
        },
        searchInput() {
            vm = this;

            $(function () {
                //setup width and length
                var typingTimer;
                var doneTypingInterval = 1500;

                $("#formSearch").keyup(function () {
                    val = $(this).val();

                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(function () {
                        if (val) {
                            vm.search = val;
                        }
                        vm.getList();
                    }, doneTypingInterval);
                });
            });
        },
        checkId() {
            var id_bulk = this.listpenerbit_temp;
            self = this;

            $("input[type='checkbox']:checked").each(function () {
                i = $(this).data("id");
                getData = self.data[i];
                if (getData != null) {
                    id = getData.id;
                    name = getData.name;
                    email = getData.email;
                    image = (getData.photo != null ?conf.base_asset + getData.photo:'https://via.placeholder.com/430x300?text=Not+Found');

                    let penerbit = {
                        id : id,
                        name : name,
                        email : email,
                        image : image,
                    };

                    id_bulk.push(penerbit);
                }
            });
        
            if (id_bulk.length <= 0) {
                return false;
            }else{
                return id_bulk;
            }
        },
        selectPenerbitAll() {
            let id = this.checkId();

            if (id === false) {
                return swal('Opps, ada sesuatu yang salah!','Silahkan pilih setidaknya satu data!','warning');
            }else{
                this.listPenerbit.push.apply(this.listPenerbit, id);
                this.closeModal();

                $("#checkall").attr("checked", false);
            }
        },
        selectPenerbit(i) {
            getData = this.data[i];
            if (getData != null) {
                id = getData.id;
                name = getData.name;
                email = getData.email;
                image = (getData.photo != null ?conf.base_asset + getData.photo:'https://via.placeholder.com/430x300?text=Not+Found');

                let penerbit = {
                    id : id,
                    name : name,
                    email : email,
                    image : image,
                };

                check = this.checkSamePenerbit(id);
                if (check === false) {
                    this.listPenerbit.push(penerbit);
                }

                this.closeModal();
            }else{
                return swal('Opps, ada sesuatu yang salah!','Anda sudah memilih penerbit ini!','warning');
            }
        },
        checkSamePenerbit(id) {
            same = false;
            array = this.listPenerbit;
            if (this.listPenerbit.length > 0) {
                for (let i = 0; i < array.length; i++) {
                    idList = array[i].id;
                    
                    if (id == idList) {
                        same = true;
                        break;
                    }
                }
            }

            return same;
        },
        remove(i) {
            // show alert delete
            let confirmation = confirm('Apakah anda yakin menghapus penerbit ini ?');
            if (confirmation) {
                // delete data
                this.listPenerbit.splice(i, 1);
            }
        },
        saveData() {
            $("#appForm").addClass("disabled-form");

            if (this.listPenerbit.length <= 0) {
                return swal('Opps, ada sesuatu yang salah!','Anda belum memilih penerbit','warning');
            }else{
                this.$http.post(conf.base_url + '/save-data', {
                    _token : conf.token,
                    penerbit : this.listPenerbit,
                    template : this.select_template,
                })
                .then(res => {
                    $("#appForm").removeClass("disabled-form");

                    getData = res.data;
                    if (getData.status == 1) {
                        swal('Berhasil',getData.message,'success');

                        setTimeout(() => {
                            location.href = conf.base_url;
                        }, 2000);
                    }else{
                        return swal('Opps, ada sesuatu yang salah!',getData.message,'error');
                    }
                })
                .catch(res => {
                    $("#appForm").removeClass("disabled-form");
                })
            }
        }
    }
});