/**
 * DISPOSISI MODALS ACTION
 */
	let loading = '';
	loading += '<div id="loading" class="sk-fading-circle">';
	loading += '<div class="sk-circle1 sk-circle"></div>';
	loading += '<div class="sk-circle2 sk-circle"></div>';
	loading += '<div class="sk-circle3 sk-circle"></div>';
	loading += '<div class="sk-circle4 sk-circle"></div>';
	loading += '<div class="sk-circle5 sk-circle"></div>';
	loading += '<div class="sk-circle6 sk-circle"></div>';
	loading += '<div class="sk-circle7 sk-circle"></div>';
	loading += '<div class="sk-circle8 sk-circle"></div>';
	loading += '<div class="sk-circle9 sk-circle"></div>';
	loading += '<div class="sk-circle10 sk-circle"></div>';
	loading += '<div class="sk-circle11 sk-circle"></div>';
	loading += '<div class="sk-circle12 sk-circle"></div>';
	loading += '</div>';

	let modalDisposisi = '';
	modalDisposisi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/update-disposisi">';
	modalDisposisi += '<div id="modalDisposisi" class="modal fade" role="dialog">';
	modalDisposisi += '<div class="modal-dialog">';
	modalDisposisi += '<div class="modal-content">';
	modalDisposisi += '<div class="modal-header">';
	modalDisposisi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalDisposisi += '<h4 class="modal-title">Disposisi</h4>';
	modalDisposisi += '</div>';
	modalDisposisi += '<div class="modal-body">';
	modalDisposisi += loading;
	modalDisposisi += '<div class="form-group">';
	modalDisposisi += '<label for="nomorAgenda">Nomor Agenda:</label>';
	modalDisposisi += '<input type="text" name="disposisi_nomor_agenda" class="form-control" id="nomorAgenda" required="">';
	modalDisposisi += '</div>';
	// modalDisposisi += '<div class="form-group">';
	// modalDisposisi += '<label for="fileDisposisi">File:</label>';
	// modalDisposisi += '<embed width="100%" height="100%" name="plugin" class="embed" src="">';
	// modalDisposisi += '<input type="file" name="disposisi_file" class="form-control" id="fileDisposisi">';
	// modalDisposisi += '<p class="help"><i>*Abaikan jika tidak ingin diganti</i></p>';
	// modalDisposisi += '</div>';
	modalDisposisi += '</div>';
	modalDisposisi += '<div class="modal-footer">';
	modalDisposisi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalDisposisi += '<button id="submitDisposisi" type="submit" class="btn btn-primary">Submit</button>';
	modalDisposisi += '</div>';
	modalDisposisi += '</div>';
	modalDisposisi += '</div>';
	modalDisposisi += '</div>';
	modalDisposisi += '<input type="hidden" name="_token" value="'+token+'">';
	modalDisposisi += '<input type="hidden" name="id" id="idDisposisi">';
	modalDisposisi += '</form>';

	$('body').append(modalDisposisi);

	$('.btn-disposisi').each(function(){
		$(this).on('click',function(){
			let id = $(this).data('id');

			$('#idDisposisi').val(id);
			$("#modalDisposisi").find("#submitDisposisi").hide();
			$("#modalDisposisi").find(".form-group").hide();
			$("#modalDisposisi").find("#loading").show();

			$('#modalDisposisi').modal('show');

			var request = $.ajax({
			  url: mainpath+'/load-disposisi',
			  type: "GET",
			  data: {
			  	id : id
			  }
			});

			request.done(function(msg) {
				let data    = msg.data;
				let status  = msg.api_status;
				let message = msg.api_message;
				$('#fileDisposisi').val("");

				if (status === 1) {
					let embed       = $('#modalDisposisi').find('.embed');
					let extension   = data.extension;
					let file        = data.disposisi_file;
					let nomorAgenda = data.disposisi_nomor_agenda;

					if (file == '') {
						embed.hide();
					}else{
						embed.show();
					}
					let newEmbed = '<embed width="100%" height="100%" name="plugin" class="embed" src="'+file+'">';
				  embed.replaceWith(newEmbed);

				  if (file == '') {
				  	$('embed').hide()
				  }else{
				  	$('embed').show()
				  }
				  
					$('#nomorAgenda').val(nomorAgenda);
					$("#modalDisposisi").find("#loading").hide();
					$("#modalDisposisi").find("#submitDisposisi").show();
					$("#modalDisposisi").find(".form-group").show();

					$('#fileDisposisi').on('change',function(){
						let embed = $('#modalDisposisi').find('.embed');
						let input = this;
						let url   = $(this).val();
						let ext   = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

					  if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "pdf")){
				      let reader = new FileReader();

				      reader.onload = function (e) {
				      	let result = e.target.result;
				      	let newEmbed = '<embed width="100%" height="100%" name="plugin" class="embed" src="'+result+'">';
				        embed.replaceWith(newEmbed);
				      }
				     	reader.readAsDataURL(input.files[0]);
					  }else{
					  	if (file == '') {
					  		embed.hide(500)
					  	}else{
					  		embed.show();
					    	embed.attr('src', file);
					  	}
					  }
					})

				}else{
					$('#modalDisposisi').modal('hide');
					alert('Error : data tidak ditemukan');
				}

			});

			request.fail(function(jqXHR, textStatus) {
			  alert( "Request failed: " + textStatus );
			});
		})
	})

	$('body').on('click','embed',function(){
		let href = $(this).attr('src');

		console.log(href);
	})




/**
 * KOREKSI MODAL ACTION
 */
	let loading2 = '';
	loading2 += '<div id="loading2" class="sk-fading-circle">';
	loading2 += '<div class="sk-circle1 sk-circle"></div>';
	loading2 += '<div class="sk-circle2 sk-circle"></div>';
	loading2 += '<div class="sk-circle3 sk-circle"></div>';
	loading2 += '<div class="sk-circle4 sk-circle"></div>';
	loading2 += '<div class="sk-circle5 sk-circle"></div>';
	loading2 += '<div class="sk-circle6 sk-circle"></div>';
	loading2 += '<div class="sk-circle7 sk-circle"></div>';
	loading2 += '<div class="sk-circle8 sk-circle"></div>';
	loading2 += '<div class="sk-circle9 sk-circle"></div>';
	loading2 += '<div class="sk-circle10 sk-circle"></div>';
	loading2 += '<div class="sk-circle11 sk-circle"></div>';
	loading2 += '<div class="sk-circle12 sk-circle"></div>';
	loading2 += '</div>';

	let timeline = '';
	timeline += '<ul class="timeline">';;
	timeline += '</ul>';

	let modalKoreksi = '';
	modalKoreksi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/koreksi">';
	modalKoreksi += '<div id="modalKoreksi" class="modal fade" role="dialog">';
	modalKoreksi += '<div class="modal-dialog modal-lg">';
	modalKoreksi += '<div class="modal-content">';
	modalKoreksi += '<div class="modal-header">';
	modalKoreksi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalKoreksi += '<h4 class="modal-title">Koreksi dan Revisi</h4>';
	modalKoreksi += '</div>';
	modalKoreksi += '<div class="modal-body">';
	modalKoreksi += loading2;
	modalKoreksi += '<div class="form-group">';
	modalKoreksi += '<label for="pesanKoreksi">*Pesan:</label>';
	modalKoreksi += '<textarea name="pesan" class="form-control" id="pesanKoreksi" required="" rows="5"></textarea>';
	modalKoreksi += '</div>';
	modalKoreksi += '<div class="form-group">';
	modalKoreksi += '<label for="fileLampiran">File:</label>';
	modalKoreksi += '<input type="file" name="lampiran" class="form-control" id="fileLampiran">';
	modalKoreksi += '</div>';
	modalKoreksi += '<div class="row">';
	modalKoreksi += '<div class="col-sm-12">';
	modalKoreksi += '<div class="box-group">';
	modalKoreksi += '<div class="panel box box-success">';
	modalKoreksi += '<div class="box-header with-border">';
	modalKoreksi += '<h4 class="box-title">';
	modalKoreksi += '<a data-toggle="collapse" data-parent="#accordion" href="#collapseRevisi" aria-expanded="false" class="collapsed">';
	modalKoreksi += 'Riwayat Revisi';
	modalKoreksi += '</a>';
	modalKoreksi += '</h4>';
	modalKoreksi += '</div>';
	modalKoreksi += '<div id="collapseRevisi" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">';
	modalKoreksi += '<div class="box-body">';
	modalKoreksi += timeline;
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '<div class="modal-footer">';
	modalKoreksi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalKoreksi += '<button id="submitKoreksi" type="submit" class="btn btn-primary">Submit</button>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '</div>';
	modalKoreksi += '<input id="id_proses_pentashihan_koreksi" type="hidden" name="id_proses_pentashihan" value="'+token+'">';
	modalKoreksi += '<input type="hidden" name="_token" value="'+token+'">';
	modalKoreksi += '</form>';

	$('body').append(modalKoreksi);

	$('.btn-koreksi').each(function(){
		$(this).on('click',function(){
			$('#modalKoreksi').find('.modal-body').children('div').hide();
			$('#loading2').show();
			$('#modalKoreksi').modal('show');

			let id = $(this).data('id');
			$('#id_proses_pentashihan_koreksi').val(id);

			var request = $.ajax({
			  url: mainpath+'/load-koreksi',
			  type: "GET",
			  data: {
			  	id : id
			  }
			});

			request.done(function(msg) {
				let data    = msg.data;
				let status  = msg.api_status;
				let message = msg.api_message;
				let submit  = msg.submit;

				if (status === 1) {
					$('#modalKoreksi').find('.modal-body').children('div').show(700);
					$('#loading2').hide(700);

					removeTimeline();

					$.each(data, function(i, item) {
						addTimeline(item);
					});

					if(data.length == 0){
						$('.timeline').append('<p align="center">Belum Ada Revisi</p>');
					}

					if (submit == 1) {
						console.log('show');
						$('#submitKoreksi').show();
						$('#pesanKoreksi').parent().show();
						$('#fileLampiran').parent().show();
					}else{
						console.log('hide');
						$('#submitKoreksi').hide();
						$('#pesanKoreksi').parent().hide();
						$('#fileLampiran').parent().hide();
					}
				}else{
					$('#modalKoreksi').modal('hide');
					alert('Error : data tidak ditemukan');
				}
			});

			request.fail(function(jqXHR, textStatus) {
			  alert( "Request failed: " + textStatus );
			});
		})
	})

	$('.timeline').on('click','.delete-data',function(){
		let id = $(this).data('id');

		let konfirmasi = confirm('apakah anda yakin menghapus data ini ?')
		if (konfirmasi) {
			window.location.href = mainpath+'/hapus-revisi/'+id;
		}
	})

	function removeTimeline(){
		$('.timeline').html("");
	}

	function addTimeline(data){
		let lampiran = '';
		if (data.lampiran == '') {
			lampiran += '-';
		}else{
			lampiran += '<a href="'+data.lampiran+'" target="_blank" class="btn btn-primary btn-xs">Lihat</a>';
		}

		let revisi_lampiran = '';
		if (data.revisi_lampiran == '') {
			revisi_lampiran += '-';
		}else{
			revisi_lampiran += '<a href="'+data.revisi_lampiran+'" target="_blank" class="btn btn-primary btn-xs">Lihat</a>';
		}

		let revisi_date = '';
		if (data.revisi_date == '') {
			revisi_date += '-';
		}else{
			revisi_date += '<i class="fa fa-clock-o"></i> '+data.revisi_date;
		}

		let bg = '';
		if (data.revisi_lampiran == '' || data.revisi == '') {
			bg += 'yellow';
		}else{
			bg += 'green';
		}

		let appendTimeline = '';
		appendTimeline += '<li class="time-label"> <span class="bg-'+bg+'"> '+data.admin_name+' </span> </li>';
		appendTimeline += '<li> <i class="fa fa-trash bg-red delete-data" data-id="'+data.id+'"></i>';

		appendTimeline += '<div class="timeline-item">';
		appendTimeline += '<span class="time"><i class="fa fa-clock-o"></i> '+data.created_at+'</span>';
		appendTimeline += '<h3 class="timeline-header">Koreksi</h3>';
		appendTimeline += '<div class="timeline-body">'; 
		appendTimeline += '<div class="timeline-content">'+data.pesan+'</div>'
		appendTimeline += '<div class="timeline-lampiran">Lampiran : '+lampiran+'</div>';
		appendTimeline += '</div>';
		appendTimeline += '</div>';

		appendTimeline += '<div class="timeline-item">';
		appendTimeline += '<span class="time">'+revisi_date+'</span>';
		appendTimeline += '<h3 class="timeline-header">Revisi</h3>';
		if (data.revisi_lampiran == '' && data.revisi == '') {
			appendTimeline += '<div class="timeline-body"><center>Belum ada revisi</center></div>';
		}else{
			appendTimeline += '<div class="timeline-body">'; 
			appendTimeline += '<div class="timeline-content">'+data.revisi+'</div>'
			appendTimeline += '<div class="timeline-lampiran">Lampiran : '+revisi_lampiran+'</div>';
			appendTimeline += '</div>';
		}
		appendTimeline += '</div>';
		appendTimeline += '</div>';

		appendTimeline += '</li>';
		$('.timeline').append(appendTimeline);
	}





/**
 * PENERIMAAN NASKAH
 */
let modalPenerimaan = '';
	modalPenerimaan += `
	<form method="post" enctype="multipart/form-data" action="`+mainpath+`/penerimaan-naskah">
		<div id="modalPenerimaan" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Penerimaan Naskah</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="namaPenerimaan">Nama Penerima :</label>
							<!--input type="text" name="nama_penerima" class="form-control" id="namaPenerimaan" required=""-->
							<select name="nama_penerima" class="form-control" id="namaPenerimaan" required="">
								<option value="">Pilih Pentashih**</option>
							</select>
						</div>
						<div class="form-group">
							<label for="tanggalDiterima">Tanggal Diterima :</label>
							<input type="text" name="tanggal_diterima" class="form-control" id="tanggalDiterima" required="">
						</div>
						<!--div class="form-group">
							<label for="buktiPenerimaan">Bukti Penerimaan :</label>
							<input type="file" name="bukti_penerimaan" class="form-control" id="buktiPenerimaan" required="">
						</div-->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="_token" value="`+token+`">
		<input type="hidden" name="id" id="idPenerimaan">
	</form>
	`;

$('body').append(modalPenerimaan);
$('.btn-penerimaan').each(function(){
	$(this).on('click',function(){
		listOptionTashih();

		let id = $(this).data('id');

		$('#idPenerimaan').val(id);
		$('#modalPenerimaan').modal('show');
	})
});

function listOptionTashih()
{
    var selectNamaPenerima = $('#namaPenerimaan');
    var selectNama = $('#namaPenerima');

	selectNamaPenerima.find('option').remove().end();
	selectNama.find('option').remove().end();

	$.ajax({
        url : mainpath + "/list-users-tashih",
        method : "GET",
        data : {
			_token : token
        },
        beforeSend: function() {
        },
        complete: function () {
        },
        success: function(response){
            if (response.message == "success") {
				selectNamaPenerima.append($('<option>', { 
					value: "",
					text : "Pilih Pentashih**" 
				}));
				selectNama.append($('<option>', { 
					value: "",
					text : "Pilih Pentashih**" 
				}));

				$.each(response.item, function (key, value) {
					selectNamaPenerima
						.append($('<option>', { value : value.name })
						.text(value.name));
					selectNama
						.append($('<option>', { value : value.name })
						.text(value.name));
				});
            }
        },
    })
}


/**
 * Rating
 */

  function submitRating() {
	if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
		swal("Perhatian","Pilih Rating dulu","warning");
			// $(".text-rating").text("Pilih Rating dulu");
	  }else{
		  $("#form-rating").submit();
	  }
  }

let modalRating = '';
modalRating += '<form method="post" id="form-rating" enctype="multipart/form-data" action="'+mainpath+'/rating">';
modalRating += '<div id="modalRating" class="modal fade" role="dialog">';
modalRating += '<div class="modal-dialog">';
modalRating += '<div class="modal-content">';
modalRating += '<div class="modal-header">';
modalRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalRating += '<h4 class="modal-title">Beri Rating</h4>';
modalRating += '</div>';
modalRating += '<div class="modal-body" style="text-align:center">';


$(function () {
	$(".my-rating-9").starRating({
	  starShape: 'rounded',
	  starSize: 40,
	  initialRating: 0,
	  disableAfterRate: false,
	  emptyColor: '#DEDEDE',
	  hoverColor: '#EDBD19',
	  strokeColor: '#EDBD19',
	  activeColor: '#EDBD19',
	  disableAfterRate: false,
	  onHover: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentIndex);
		if (currentIndex < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentIndex >2 &&  currentIndex <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentIndex > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentIndex);
	  },
	  onLeave: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentRating);
		if (currentRating < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentRating >2 &&  currentRating <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentRating > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentRating);

	  }
	});
  });
modalRating += '<div class="form-group">';
modalRating += '<span class="text-rating text-danger">Tidak Puas</span>';
modalRating += '<br><br>';
modalRating += '<span class="my-rating-9"></span><br>';
modalRating += '<input type="hidden" name="jumlah_rating" id="jumlah_rating" required>';
modalRating += '</div>';
modalRating += '<div class="form-group">';
modalRating += '<label>Kosongkan jika tidak ada komentar<code>*</code></label>';
modalRating += '<textarea name="komentar" class="form-control id="komentar-rating" cols="30" rows="10" placeholder="Masukkan Komentar anda"></textarea>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<div class="modal-footer">';
modalRating += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalRating += '<button type="button" onclick="submitRating()"  class="btn btn-primary" id="simpan_rating_fdsgdh">Submit</button>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<input type="hidden" name="_token" value="'+token+'">';
modalRating += '<input type="hidden" name="id" id="idRating">';
modalRating += '</form>';

$('body').append(modalRating);
$('.btn-rating').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');

		$('#idRating').val(id);
		$('#modalRating').modal('show');
	})
})

/**
 * FINALISASI NASKAH
 */
let modallokkRating = '';
modallokkRating += '<div id="modallokkRating" class="modal fade" role="dialog">';
modallokkRating += '<div class="modal-dialog">';
modallokkRating += '<div class="modal-content">';
modallokkRating += '<div class="modal-header">';
modallokkRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modallokkRating += '<h4 class="modal-title">Rating</h4>';
modallokkRating += '</div>';
modallokkRating += '<div class="modal-body">';

modallokkRating += '<ul class="nav nav-tabs nav-justified">';
modallokkRating += '</ul>';

modallokkRating += '<div class="tab-content">';
modallokkRating += '</div>';

modallokkRating += '</div>';
modallokkRating += '<div class="modal-footer">';
modallokkRating += '</div>';
modallokkRating += '</div>';
modallokkRating += '</div>';
modallokkRating += '</div>';

$('body').append(modallokkRating);
$('.btn-rating-look').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');

		var request = $.ajax({
		  url: mainpath+'/rating-us/'+id,
		  type: "GET",
		  data: {
			  id : id
		  }
		});

		$("#modallokkRating").find(".nav-justified").html("");
		$("#modallokkRating").find(".tab-content").html("");

		request.done(function(msg) {
			let item    = msg.item;
			let menu    = '';
			let content = '';

			$.each(item,function(key, value){
				content += '<div class="form-group" style="text-align:center">';
				content += '<span class="text-rating_s text-danger">Tidak Puas</span>';
				content += '<br><br>';
				content += '<span class="my-rating-7"></span><br>';
				content += '<input type="hidden" name="jumlah_rating" id="jumlah_rating_s" value="'+value.jumlah_rating+'">';
				content += '</div>';
				content += '<div class="form-group" style="text-align:center">';
				content += '<label>Komentar </label>';
				content += '"'+value.komentar+'"';
				content += '</div>';
			})

			$("#modallokkRating").find(".nav-justified").html(menu);
			$("#modallokkRating").find(".tab-content").html(content);
			$(".my-rating-7").starRating({
				starShape: 'rounded',
				starSize: 40,
				initialRating: $("#jumlah_rating_s").val(),
				disableAfterRate: false,
				readOnly: true,
				emptyColor: '#DEDEDE',
				hoverColor: '#EDBD19',
				strokeColor: '#EDBD19',
				activeColor: '#EDBD19',
			});
			if ($("#jumlah_rating_s").val() < 1) {
				$(".text-rating_s").text("Tidak Puas");
			}else if($("#jumlah_rating_s").val() >2 &&  $("#jumlah_rating_s").val() <=3){
				$(".text-rating_s").text("Kurang Puas");
			}else if($("#jumlah_rating_s").val() > 3){
				$(".text-rating_s").text("Puas");
			}
		});

		request.fail(function(jqXHR, textStatus) {
		  alert( "Request failed: " + textStatus );
		});

		$('#modallokkRating').modal('show');
	})
});


/**
 * VERIFIKASI NASKAH
 */
	let modalVerifikasi = '';
	modalVerifikasi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/verifikasi-naskah">';
	modalVerifikasi += '<div id="modalVerifikasi" class="modal fade" role="dialog">';
	modalVerifikasi += '<div class="modal-dialog">';
	modalVerifikasi += '<div class="modal-content">';
	modalVerifikasi += '<div class="modal-header">';
	modalVerifikasi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalVerifikasi += '<h4 class="modal-title">Verifikasi</h4>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<div class="modal-body">';
	modalVerifikasi += '<div class="row">';
	modalVerifikasi += '<div class="col-md-12" style="margin-bottom:15px;">';
	modalVerifikasi += '<label for="buktiVerifikasi">Status :</label>';
	modalVerifikasi += '<br>';
	modalVerifikasi += '<label class="radio-inline">';
	modalVerifikasi += '<input type="radio" name="status" value="Lolos Verifikasi" checked="true">Lolos Verifikasi';
	modalVerifikasi += '</label>';
	modalVerifikasi += '<label class="radio-inline">';
	modalVerifikasi += '<input type="radio" name="status" value="Tidak Lolos Verifikasi">Tidak Lolos Verifikasi';
	modalVerifikasi += '</label>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '</div>';
// 	modalVerifikasi += '<div class="form-group">';
// 	modalVerifikasi += '<label for="buktiVerifikasi">PNBP :</label>';
// 	modalVerifikasi += '<input type="text" name="PNBP" class="form-control" id="PNBP" required>';
// 	modalVerifikasi += '</div>';
// 	modalVerifikasi += '<div class="form-group">';
// 	modalVerifikasi += '<label for="buktiPembayaran">Upload PNBP :</label>';
// 	modalVerifikasi += '<input type="file" name="bukti_pembayaran" class="form-control" id="buktiPembayaran" required>';
// 	modalVerifikasi += '</div>';
	modalVerifikasi += '<div class="form-group">';
	modalVerifikasi += '<label for="buktiVerifikasi">Dokumen :</label>';
	modalVerifikasi += '<input type="file" name="bukti_verifikasi" class="form-control" id="buktiVerifikasi">';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<div class="form-group">';
	modalVerifikasi += '<label>Tanggal Awal :</label>';
	modalVerifikasi += '<div class="input-group">';
	modalVerifikasi += '<div class="input-group-addon">';
	modalVerifikasi += '<i class="fa fa-calendar"></i>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<input type="text" class="form-control pull-right" name="datepicker" id="datepicker">';
	modalVerifikasi += '</div>';
	modalVerifikasi += '</div>';

	modalVerifikasi += '<div class="form-group">';
	modalVerifikasi += '<label>Tanggal Akhir :</label>';
	modalVerifikasi += '<div class="input-group">';
	modalVerifikasi += '<div class="input-group-addon">';
	modalVerifikasi += '<i class="fa fa-calendar"></i>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<input type="text" class="form-control pull-right" name="datepicker_akhir" id="datepicker_akhir">';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<span class="label">Hari minggu dan sabtu tidak akan dihitung</span>';
	modalVerifikasi += '</div>';

	modalVerifikasi += '<div class="form-group">';
	modalVerifikasi += '<label for="estimasi">Estimasi (hari) :</label>';
	modalVerifikasi += '<input type="number" name="estimasi" class="form-control" id="estimasi" readonly>';
	modalVerifikasi += '</div>';

	modalVerifikasi += '</div>';
	modalVerifikasi += '<div class="modal-footer">';
	modalVerifikasi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalVerifikasi += '<button type="submit" class="btn btn-primary">Submit</button>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '</div>';
	modalVerifikasi += '<input type="hidden" name="_token" value="'+token+'">';
	modalVerifikasi += '<input type="hidden" name="id" id="idVerifikasi">';
	modalVerifikasi += '</form>';
    
     $(function () {
        $('#PNBP').priceFormat({
            prefix:'',thousandsSeparator:'.',centsLimit: 0,clearOnEmpty:false
        });
    })
	$('body').append(modalVerifikasi);
	$('.btn-verifikasi').each(function(){
		$(this).on('click',function(){
			let id = $(this).data('id');

			$('#idVerifikasi').val(id);
			$('#modalVerifikasi').modal('show');
		});
	})

/**
 * FINALISASI NASKAH
 */
	let modalFinalisasi = '';
	modalFinalisasi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/finalisasi-naskah">';
	modalFinalisasi += '<div id="modalFinalisasi" class="modal fade" role="dialog">';
	modalFinalisasi += '<div class="modal-dialog">';
	modalFinalisasi += '<div class="modal-content">';
	modalFinalisasi += '<div class="modal-header">';
	modalFinalisasi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalFinalisasi += '<h4 class="modal-title">Finalisasi</h4>';
	modalFinalisasi += '</div>';
	modalFinalisasi += '<div class="modal-body">';

	modalFinalisasi += '<ul class="nav nav-tabs nav-justified">';
	modalFinalisasi += '</ul>';

	modalFinalisasi += '<div class="tab-content">';
	modalFinalisasi += '</div>';

	modalFinalisasi += '</div>';
	modalFinalisasi += '<div class="modal-footer">';
	modalFinalisasi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalFinalisasi += '<button type="submit" class="btn btn-primary">Submit</button>';
	modalFinalisasi += '</div>';
	modalFinalisasi += '</div>';
	modalFinalisasi += '</div>';
	modalFinalisasi += '</div>';
	modalFinalisasi += '<input type="hidden" name="_token" value="'+token+'">';
	modalFinalisasi += '<input type="hidden" name="id" id="idFinalisasi">';
	modalFinalisasi += '</form>';

	$('body').append(modalFinalisasi);
	$('.btn-finalisasi').each(function(){
		$(this).on('click',function(){
			let id = $(this).data('id');

			var request = $.ajax({
			  url: mainpath+'/ukuran/'+id,
			  type: "GET",
			  data: {
			  	id : id
			  }
			});

			$("#modalFinalisasi").find(".nav-justified").html("");
			$("#modalFinalisasi").find(".tab-content").html("");

			request.done(function(msg) {
				let item    = msg.item;
				let menu    = '';
				let content = '';
				let active;
				let content_active;

				$.each(item,function(key, value){
					if (key == 0) {
						active = 'active';
						content_active = 'in active';
					}else{
						active = '';
						content_active = '';
					}

					menu += '<li class="'+active+'"><a data-toggle="tab" href="#'+value.id+'">Ukuran '+value.ukuran+'</a></li>';

					content += '<div id="'+value.id+'" class="tab-pane fade '+content_active+'">';
					content += '<div class="row">';
					content += '<div class="col-sm-6 col-sm-offset-3">';
					content += '<img src="'+value.qrcode+'" class="img-responsive">';
					content += '</div>';
					content += '</div>';
					content += '<div class="form-group">';
					content += '<label for="nomorTandaTashih">Nomor Tanda Tashih :</label>';
					content += '<input value="'+value.nomor+'" type="text" name="nomor_tanda_tashih[]" class="form-control" id="nomorTandaTashih" required="">';
					content += '</div>'; 
					content += '<div class="form-group">';
					content += '<label for="tanggalTandaTashih">Kode Tanda Tashih :</label>';
					content += '<input value="'+value.kode+'" type="text" name="kode_tanda_tashih[]" class="form-control" id="tanggalTandaTashih" required="">';
					content += '</div>';
					content += '<div class="form-group">';
					content += '<label for="tanggalPenetapan">Tanggal Penetapan :</label>';
					content += '<input value="'+value.tanggal_penetapan+'" type="text" name="tanggal_penetapan[]" class="form-control date-picker" readonly="" id="tanggalPenetapan" required="">';
					content += '</div>';
					content += '<div class="form-group">';
					content += '<label for="suratPenerbitan">Surat Penerbitan :</label>';
					if (value.surat_penerbitan != '') {
						content += '<embed width="100%" height="250px" class="embed" src="'+value.surat_penerbitan+'">';
					}
					content += '<input type="file" name="surat_penerbitan[]" class="form-control" id="suratPenerbitan" required="">';
					content += '</div>';
					content += '<div class="form-group">';
					content += '<label for="scanTandaTashih">Scan Tanda Tashih :</label>';
					if (value.scan_tanda_tashih != '') {
						content += '<embed width="100%" height="250px" class="embed" src="'+value.scan_tanda_tashih+'">';
					}
					content += '<input type="file" name="scan_tanda_tashih[]" class="form-control" id="scanTandaTashih" required="">';
					content += '</div>';

					content += '<input type="hidden" name="id_ukuran[]" value="'+value.id+'">';
					content += '</div>';
				})

				$("#modalFinalisasi").find(".nav-justified").html(menu);
				$("#modalFinalisasi").find(".tab-content").html(content);
				$(".date-picker").datepicker({
					format: 'yyyy-mm-dd',
					autoclose: true
				});
			});

			request.fail(function(jqXHR, textStatus) {
			  alert( "Request failed: " + textStatus );
			});

			// $('#qrCode').attr('src','https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl='+id+'&choe=UTF-8');
			$('#idFinalisasi').val(id);
			$('#modalFinalisasi').modal('show');
		})
	})

/**
 * TASHIH USER
 */
 	let optionAdmin = '<option value="">Please select a Pentashih</option>';
 	$.each(admin, function(i, item) {
		optionAdmin += '<option value="'+item.id+'">'+item.nama+'</option>'
	});

	let modalPentashihan = '';
	modalPentashihan += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/tashih-user">';
	modalPentashihan += '<div id="modalPentashihan" class="modal fade" role="dialog">';
	modalPentashihan += '<div class="modal-dialog">';
	modalPentashihan += '<div class="modal-content">';
	modalPentashihan += '<div class="modal-header">';
	modalPentashihan += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalPentashihan += '<h4 class="modal-title">Pentashihan</h4>';
	modalPentashihan += '</div>';
	modalPentashihan += '<div class="modal-body">';
	modalPentashihan += '<div class="form-group">';
	modalPentashihan += '<label for="pentashih">Pentashih :</label>';
	modalPentashihan += '<select class="form-control" name="id_cms_users" id="pentashih" required>'+optionAdmin+'</select>';
	modalPentashihan += '</div>';
	modalPentashihan += '<div class="form-group">';
	modalPentashihan += '<label for="juz">Juz :</label>';
	modalPentashihan += '<input type="text" name="juz" class="form-control" id="juz" required="" placeholder="ex : 1 - 5">';
	modalPentashihan += '</div>';
	modalPentashihan += '</div>';
	modalPentashihan += '<div class="modal-footer">';
	modalPentashihan += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalPentashihan += '<button type="submit" class="btn btn-primary">Submit</button>';
	modalPentashihan += '</div>';
	modalPentashihan += '</div>';
	modalPentashihan += '</div>';
	modalPentashihan += '</div>';
	modalPentashihan += '<input type="hidden" name="_token" value="'+token+'">';
	modalPentashihan += '<input type="hidden" name="id" id="idPentashihan">';
	modalPentashihan += '</form>';

	$('body').append(modalPentashihan);
	$('.btn-pentashihan').each(function(){
		$(this).on('click',function(){
			let id = $(this).data('id');

			$('#idPentashihan').val(id);
			$('#modalPentashihan').modal('show');
		});
	});



let penerimaanEstimasi = '';
penerimaanEstimasi += `
<form method="post" enctype="multipart/form-data" action="`+mainpath+`/bukti-penerimaan">
    <div id="penerimaanEstimasi" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bukti Penerimaan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="cTanggal">Tanggal Awal *:</label>
                        <input required="" type="text" name="tanggal_penerimaan" class="form-control" id="datepicker_awal" readonly>
                    </div>

                    <div class="form-group">
                        <label for="cTanggal">Tanggal Akhir *:</label>
                        <input required="" type="text" name="tanggal_deadline_saka" class="form-control" id="datepicker_aks" readonly>
                    </div>

                    <!--div class="form-group">
                        <label for="cPenerimaan">Bukti Penerimaan * :</label>
                        <input required="" type="file" name="bukti_penerimaan" class="form-control" id="cPenerimaan">
					</div-->
					<div class="form-group">
						<label for="namaPenerima">Nama Penerima :</label>
						<select name="nama_penerima" class="form-control" id="namaPenerima" required="">
							<option value="">Pilih Pentashih**</option>
						</select>
					</div>

                    <div class="form-group">
                        <label for="cEstimasi">Estimasi (hari) * :</label>
                        <input required="" type="number" name="estimasi" class="form-control" id="estimasi_satu" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="submitEstimasi" type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <input id="id_proses_pentashihan_estimasi" type="hidden" name="id_proses_pentashihan" value="`+token+`">
    <input type="hidden" name="_token" value="`+token+`">
</form>`;

$('body').append(penerimaanEstimasi);
$('#cTanggal').datepicker({
	format: "yyyy-mm-dd",
	autoclose: true
});
$('.btn-penerimaan-estimasi').each(function(){
	$(this).on('click',function(){
		listOptionTashih();

		let id = $(this).data('id');

		$("#id_proses_pentashihan_estimasi").val(id);

		$("#penerimaanEstimasi").modal("show");
	})
});
	
let modalUploadBayar = '';
modalUploadBayar += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/upload-bukti">';
modalUploadBayar += '<div id="modalUploadBayar" class="modal fade" role="dialog">';
modalUploadBayar += '<div class="modal-dialog">';
modalUploadBayar += '<div class="modal-content">';

modalUploadBayar += '<div class="modal-header">';
modalUploadBayar += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalUploadBayar += '<h4 class="modal-title">Bukti Pembayaran</h4>';
modalUploadBayar += '</div>';
modalUploadBayar += '<div class="modal-body">';

modalUploadBayar +='<div id="tabelUp"></div>'

modalUploadBayar += '</div>';
modalUploadBayar += '<div class="modal-footer">';

modalUploadBayar += '</div>';

modalUploadBayar += '</div>';
modalUploadBayar += '</div>';
modalUploadBayar += '</div>';

modalUploadBayar += '</form>';

$('body').append(modalUploadBayar);

$('.btn-lihat-bukti').each(function(){
    // $("#imageBukti").hide();
    // $("#pdfBukti").hide();

	$(this).on('click',function(){
	    id = $(this).data("id")
        
        $.ajax({
            url : mainpath +"/pembayaran/"+id,
            method : "GET",
            data : {
                "id" : id,
            },
            success:function(data){
                // alert(data.bukti_pembayaran);
                if(data.api_message=="success"){
                    
                    let html = '';
                    html += '<div class="form-group">';
                    html += '<label for="text-PNBP">PNBP :</label>';
                    html += 'Rp. '+data.pnbp;
                    html += '</div>';
                    
                    // $("#text-PNBP").html('Rp. '+data.pnbp);
                    if(data.extension == 'pdf' || data.extension == 'PDF'){
                        $("#pdfBukti").show(200);
                        // $("#pdfBukti").attr("src",data.bukti_pembayaran);
                        
                        html += '<div class="form-group">';
                        html += '<label for="buktiPembayaran">Tanda PNBP :</label>';
                        if(data.bukti_pembayaran==""){
                            html += '---';
                        }else{
                            html += '<iframe id="pdfBukti" src="'+data.bukti_pembayaran+'" style="width:100%;height:400px"></iframe>';
                        }
                        html += '</div>';
                    }else{
                        $("#imageBukti").show(200);
                        // $("#imageBukti").attr("src",data.bukti_pembayaran);
                        
                        html += '<div class="form-group">';
                        html += '<label for="buktiPembayaran">Tanda PNBP :</label>';
                        if(data.bukti_pembayaran==""){
                            html += '---';
                        }else{
                            html += '<img id="imageBukti" src="'+data.bukti_pembayaran+'" style="width:100%">';   
                        }
                        html += '</div>';
                    }
                    
                    
                    if(data.extension_bukti_PNBP == 'pdf' || data.extension_bukti_PNBP == 'PDF'){
                        html += '<div class="form-group">';
                        html += '<label>Upload Bukti Pembayaran :</label>';
                        if(data.bukti_pembayaran_bukti_PNB==""){
                             html += '---';
                        }else{
                            html += '<iframe src="'+data.bukti_pembayaran_bukti_PNBP+'" style="width:100%;height:400px"></iframe>';   
                        }
                        html += '</div>';
                    }else{
                        html += '<div class="form-group">';
                        html += '<label>Upload Bukti Pembayaran  :</label>';
                        if(data.bukti_pembayaran_bukti_PNB==""){
                            html += '---';
                        }else{
                            html += '<img src="'+data.bukti_pembayaran_bukti_PNBP+'" style="width:100%">';   
                        }
                        html += '</div>';
                    }
                    
                    $("#tabelUp").empty().append(html);
                }
            },
            error:function(data){
                alert(data);
            }
        })
	    
		$('#modalUploadBayar').modal('show');
	});
});


let modalsetPNBP = '';
modalsetPNBP += '<div id="modalsetPNBP" class="modal fade" role="dialog">';
modalsetPNBP += '<div class="modal-dialog">';
modalsetPNBP += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/set-pnbp">';
modalsetPNBP += '<input type="hidden" name="id" id="id_proses_tashih">';
modalsetPNBP += '<input type="hidden" name="_token" value="'+token+'">';
modalsetPNBP += '<div class="modal-content">';

modalsetPNBP += '<div class="modal-header">';
modalsetPNBP += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalsetPNBP += '<h4 class="modal-title"><b>Tanda PNBP</b></h4>';
modalsetPNBP += '</div>';
modalsetPNBP += '<div class="modal-body">';

modalsetPNBP += '<div class="form-group">';
modalsetPNBP += '<label for="buktiVerifikasi">PNBP :</label>';
modalsetPNBP += '<input type="text" name="PNBP" class="form-control" id="PNBP" required>';
modalsetPNBP += '</div>';
modalsetPNBP += '<div class="form-group">';
modalsetPNBP += '<label for="buktiPembayaran">Upload PNBP :</label>';
modalsetPNBP += '<input type="file" name="bukti_pembayaran" class="form-control" id="buktiPembayaran" required>';
modalsetPNBP += '</div>';

modalsetPNBP += '</div>';
modalsetPNBP += '<div class="modal-footer">';
modalsetPNBP += '<button type="submit" class="btn btn-sm btn-primary">Submit</button>';
modalsetPNBP += '</div>';
modalsetPNBP += '</div>';
modalsetPNBP += '</form>';

modalsetPNBP += '</div>';
modalsetPNBP += '</div>';

$('body').append(modalsetPNBP);

$('.btn-upload-pnbp').each(function(){
    $(this).click(function(){
        let id = $(this).data("id");
        
        $("#id_proses_tashih").val(id);
        $("#modalsetPNBP").modal("show");
    });
});