function replaceOpenModal(){
	$('.open-modal').each(function(){
		let id = $(this).attr("href");
		$(this).attr("href","javascript:void(0)");

		$(this).on('click',function(){
			$('#idMonitoring').val(id);

			$("#tindakLanjut").modal("show");
		})
	})
}
replaceOpenModal();

let pathname = window.location.pathname;
pathname = pathname.split('/');

if (pathname[5] == 'detail') {
	let modalViewer = '';
	modalViewer +=' <div id="modalViewer" class="modal fade" role="dialog">';
	modalViewer +=' <div class="modal-dialog">';
	modalViewer +=' <div class="modal-content">';
	modalViewer +=' <div class="modal-header">';
	modalViewer +=' <h4 class="modal-title"></h4>';
	modalViewer +=' <button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalViewer +=' </div>';
	modalViewer +=' <div class="modal-body">';
	modalViewer +=' <center><img src="" class="img-thumbnail img-responsive"></center>';
	modalViewer +=' <iframe src="" style="height: 450px;width: 100%;"></iframe>';
	modalViewer +=' </div>';
	modalViewer +=' <div class="modal-footer">';
	modalViewer +=' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalViewer +=' </div>';
	modalViewer +=' </div>';
	modalViewer +=' </div>';
	modalViewer +=' </div>';

	$('body').append(modalViewer);

	$('td.lampiran').each(function(){
		let link      = $(this).find('a');
		let pathFile  = $(this).find('input').val();
		let extension = pathFile.split('.');
		extension     = extension[extension.length-1];

		link.attr('href','javascript:void(0)');
		if (pathFile == '' || pathFile == '#') {
			link.html('-');
		}else{
			link.html('LIHAT');
		}

		link.on('click',function(){
			if (extension == 'pdf') {
				$('iframe').show();
				$('#modalViewer').find('img').hide();

				$('iframe').attr('src',ASSET_URL+''+pathFile);
			}else{
				$('iframe').hide();
				$('#modalViewer').find('img').show();

				$('#modalViewer').find('img').attr('src',ASSET_URL+''+pathFile);
			}
			$('#modalViewer').modal('show');
		})
		
	})
}

$(document).ready(function(){
	$('#wrapperLampiran').on('click','.btn-remove',function(){
		$(this).parent().remove();
	})

	$('#buttonLampiran').on('click',function(){
		let html = '';
		html += '<div class="col-sm-6" class="lampiran">'
		html += '<input type="file" name="lampiran[]" class="form-control">'
		html += '<button type="button" class="btn btn-warning btn-remove btn-xs"><i class="fa fa-times"></i></button>'
		html += '</div>';
		$('#wrapperLampiran').find('.row').append(html);
	})
})

let modalLampiran = '';
	modalLampiran +=' <div id="modalLampiran" class="modal fade" role="dialog">';
	modalLampiran +=' <div class="modal-dialog modal-lg">';
	modalLampiran +=' <div class="modal-content">';
	modalLampiran +=' <div class="modal-header">';
	modalLampiran +=' <button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalLampiran +=' <h4 class="modal-title">Lampiran</h4>';
	modalLampiran +=' </div>';
	modalLampiran +=' <div class="modal-body">';
	modalLampiran +=' </div>';
	modalLampiran +=' <div class="modal-footer">';
	modalLampiran +=' <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalLampiran +=' </div>';
	modalLampiran +=' </div>';
	modalLampiran +=' </div>';
	modalLampiran +=' </div>';
$('body').append(modalLampiran);

$("#table-monitoring_tidak_lanjut").find('.id').each(function(){
	let label = $(this).find('.td-label');
	let id    = label.next().val();
	label.replaceWith('<button type="button" class="btn btn-xs btn-info btn-lampiran" data-id="'+id+'">Lihat Lampiran</button>');
})

$('#table-monitoring_tidak_lanjut').on('click','.btn-lampiran',function(){
	id = $(this).data('id');
	$('#modalLampiran').find('.modal-body').html('<p align="center">Please Wait...</p>')
	$('#modalLampiran').modal('show');

	var request = $.ajax({
	  url: ADMIN_PATH+'/pengawasan-peredaran/load-lampiran',
	  type: "GET",
	  data: {
	  	id : id
	  }
	});

	request.done(function(msg) {
		$('#modalLampiran').find('.modal-body').html(msg);
	});

	request.fail(function(jqXHR, textStatus) {
	  alert( "Request failed: " + textStatus );
	  $('#modalLampiran').modal('hide');
	});
})