modalCombinePentasihan = '';
modalCombinePentasihan += `
<div class="modal fade" id="modalCombinePentasihan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      
      <div class="modal-body">
        <button type="button" class="close" onclick="closeModalCombinePentasihan()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="javascript:void(0)">Pernyataan</a></li>
          <li><a href="javascript:void(0)">Isi Survei</a></li>
          <li><a href="javascript:void(0)">Beri Rating</a></li>
          <li><a href="javascript:void(0)">Unduh Tanda Tashih</a></li>
        </ul>

        <div class="tab-content">

          <div class="tab-pane active" id="PernyataanForm">
            <div class="alert alert-danger" style="padding: 5px 10px;display: none;"></div>
            <input type="hidden" name="id_proses_pentashihan_pernyataan" class="id_proses_pentashihan" id="id_proses_pentashihan_pernyataan" value="">

            <div id="form-pernyataan"></div>
          </div>

          <div class="tab-pane" id="IsiSurvei">

          </div>

          <div class="tab-pane" id="BeriRating">

          </div>
          <div class="tab-pane" id="UnduhTandaTashih">

          </div>

        </div>
      </div>

      </div>
    </div>
  </div>
</div>
`;
$('body').append(modalCombinePentasihan);

window.document.addEventListener('myCustomEvent', handleEvent, false)
function handleEvent(e) {
  console.log(e.detail) // outputs: {foo: 'bar'}
    // alert(e.detail.foo)
	$("#btnBeriRating").removeAttr("disabled");
}

$(document).on("click", ".btn-combinepentasihan", function () {
    let id_proses_pentashihan = $(this).data("id");
    $(".id_proses_pentashihan").val(id_proses_pentashihan);
    $("#penerbit").val(name);

    checkSuratPernyataan(id_proses_pentashihan);
    checkRating(id_proses_pentashihan);
    checkTandaTansih(id_proses_pentashihan);
    checkSurvey(id_proses_pentashihan);

    $("#modalCombinePentasihan").modal("show");
});

const closeModalCombinePentasihan = () => {
    $(".id_proses_pentashihan").val("");
    $("#penerbit").val("");
    $("#modalCombinePentasihan").modal("hide");
}

const goToPage = (page) => {
    $(document).find('#modalCombinePentasihan .tab-pane').removeClass('active');
    $(document).find('#modalCombinePentasihan #'+page).addClass('active');

    $(document).find('#modalCombinePentasihan .nav-tabs li').removeClass('active');
    if (page == 'PernyataanForm') {
        to = '#modalCombinePentasihan .nav-tabs li:nth-child(1)';
    } else if (page == 'IsiSurvei') {
        to = '#modalCombinePentasihan .nav-tabs li:nth-child(2)';
    }  else if (page == 'BeriRating') {
        to = '#modalCombinePentasihan .nav-tabs li:nth-child(3)';
    }else if (page == 'UnduhTandaTashih') {
        to = '#modalCombinePentasihan .nav-tabs li:nth-child(4)';
    }
    $(document).find(to).addClass('active');
}

const runValidation = () => {
    let input = $('.validation');
    for (var i = 0; i < input.length; i++) {
        if (checkValidation(input[i]) === false) {
            $(input[i]).parent().addClass('has-error');
            $(input[i]).focus();
            return false;
        }
    }

    return true;
}

const checkValidation = (input) => {
    let inputHtml = $(input);
    let nameAttr = inputHtml.attr('placeholder');

    inputHtml.parent().removeClass('has-error');
    if (inputHtml.val().trim() === '') {
        addValidation('block', 'Kolom '+nameAttr+' harus diisi.');
        return false;
    } else if ($(input).attr('type') === 'email' || $(input).attr('name') === 'email') {
        if (validEmail(inputHtml.val()) !== true) {
            addValidation('block', 'Kolom '+nameAttr+' berisi alamat email yang tidak valid.');
            return false;
        }
    } else if ($(input).attr('type') === 'number' || $(input).attr('name') === 'phone') {
        if (validNumeric(inputHtml.val()) !== true) {
            addValidation('block', 'Kolom '+nameAttr+' berisi selain angka.');
            return false;
        }
    }
    return true;
}

const validEmail = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const validNumeric = (value) => {
    var re = /^[0-9]+$/;
    return re.test(value);
}

const addValidation = (show, msg) => {
    $(document).find('#modalCombinePentasihan .alert-danger').css('display', show);
    $(document).find('#modalCombinePentasihan .alert-danger').empty().html(msg);
}

const nextPage = (page) => {
    addValidation('none', '');

    if (page == 'IsiSurvei') {
        valid = runValidation();

        if (valid) {
            if ($('#checkRedaksi:checked').length > 0) {
                saveSuratPernyataan(page);
            }else{
                alert('Redaksi kesanggupan harus disetujui');
            }
        }
    } else {
        goToPage(page);
    }
}

const prevPage = (page) => {
    goToPage(page);
}

const saveSuratPernyataan = (page) => {
    const headersForm = {
        headers: { 'content-type': 'multipart/form-data' }
    }

    $.ajax({
        url : mainpath + "/pernyataan",
        method : "POST",
        headersForm, 
        data : {
            '_token': token,
            'id_proses_pentashihan': $("#id_proses_pentashihan_pernyataan").val(),
            'nama': $("#nama").val(),
            'jabatan': $("#jabatan").val(),
            // 'penerbit': $("#penerbit").val(),
            'alamat': $("#alamat").val(),
            'phone': $("#phone").val(),
            'email': $("#email").val(),
            'nama_percetakan': $("#nama_percetakan").val(),
            'alamat_percetakan': $("#alamat_percetakan").val(),
            'rencana_pelaksanaan': $("#rencana_pelaksanaan").val()
        },
        beforeSend: function() {
            $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').attr('disabled', 'disabled');
        },
        complete: function () {
            $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').removeAttr('disabled');
        },
        success: function(res){
            if (res.status == 1) {
                goToPage(page);
            } else {
                $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').removeAttr('disabled');
                alert('Maaf sedang ada gangguan anda bisa mencoba untuk beberapa saat lagi.')
            }
        },
        errors: function(res) {
            $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').removeAttr('disabled');
            alert('Maaf sedang ada gangguan anda bisa mencoba untuk beberapa saat lagi.')
        }
    });
}

const checkSuratPernyataan = (id_proses_pentashihan) => {
    $.ajax({
        url : mainpath + "/check-pernyataan",
        method : "POST",
        data : {
            '_token': token,
            'id_proses_pentashihan': id_proses_pentashihan
        },
        beforeSend: function() {
            $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').attr('disabled', 'disabled');
        },
        complete: function () {
            $(document).find('#modalCombinePentasihan #PernyataanForm .btn-primary').removeAttr('disabled');
        },
        success: function(res){
            if (res.status == 1) {
                // $("#nama").val(res.item.nama);
                // $("#jabatan").val(res.item.jabatan);
                // $("#penerbit").val(name);
                // $("#alamat").val(res.item.alamat);
                // $("#phone").val(res.item.phone);
                // $("#email").val(res.item.email);
                // $("#nama_percetakan").val(res.item.nama_percetakan);
                // $("#alamat_percetakan").val(res.item.alamat_percetakan);
                // $("#rencana_pelaksanaan").val(res.item.rencana_pelaksanaan);

                htmlFormPernyataan(res.item);
            } else {
                checkSuratPernyataan(id_proses_pentashihan);
            }
        },
        errors: function(res) {
            checkSuratPernyataan(id_proses_pentashihan);
        }
    });
}

const htmlFormPernyataan = (data) => {
    html = '';
    if (data.nama == '' || data.nama == null) {
        html += `
            <div class="form-group">
                <label for="nama">Nama * :</label>
                <input type="text" name="nama" class="form-control validation" id="nama" placeholder="Nama">
            </div>

            <div class="form-group">
                <label for="jabatan">Jabatan * :</label>
                <input type="text" name="jabatan" class="form-control validation" id="jabatan" placeholder="Jabatan">
            </div>

            <div class="form-group">
                <label for="penerbit">Penerbit * :</label>
                <input type="text" class="form-control" value="`+name+`" id="penerbit" disabled="" placeholder="Penerbit">
            </div>

            <div class="form-group">
                <label for="alamat">Alamat * :</label>
                <textarea name="alamat" class="form-control validation" id="alamat" placeholder="Alamat" style="resize:none;" rows="5"></textarea>
            </div>

            <div class="form-group">
                <label for="phone">Telp./HP. * :</label>
                <input type="number" name="phone" class="form-control validation" id="phone" placeholder="Telp/HP">
            </div>

            <div class="form-group">
                <label for="email">Alamat Email * :</label>
                <input type="email" name="email" class="form-control validation" id="email" placeholder="Alamat Email">
            </div>

            <div class="form-group">
                <label for="nama_percetakan">Nama Percetakan * :</label>
                <input type="text" name="nama_percetakan" class="form-control validation" id="nama_percetakan" placeholder="Nama Percetakan">
            </div>

            <div class="form-group">
                <label for="alamat_percetakan">Alamat * :</label>
                <textarea name="alamat_percetakan" class="form-control validation" id="alamat_percetakan" placeholder="Alamat" style="resize:none;" rows="5"></textarea>
            </div>

            <div class="form-group">
                <label for="rencana_pelaksanaan">Rencana Pelaksanaan * :</label>
                <input type="text" name="rencana_pelaksanaan" class="form-control validation" id="rencana_pelaksanaan" placeholder="Rencana Pelaksanaan">
            </div>

            <div class="form-group">
                <label class="checkbox-inline">
                <input id="checkRedaksi" type="checkbox" value="">
                <a href="#redaksi" data-toggle="collapse">Redaksi Kesanggupan</a>
                </label>  
                <div id="redaksi" class="collapse" style="text-align:justify;border:1px solid #CCC;padding:5px 10px;border-radius:5px;margin-top:10px">
                Dengan ini menyatakan bahwa sesuai ketentuan Lajnah Pentashihan Mushaf Al-Qur’an (LPMQ) dan sebagai kewajiban selaku penerbit, saya akan mengirimkan contoh hasil cetak Mushaf Al-Qur\'an yang kami terbitkan sebagai berikut Selambat-lambatnya 2 (dua) minggu setelah dicetak, dengan jumlah 5 (lima) eksemplar, sebagai bukti penerbitan dan dokumentasi untuk Lajnah Pentashihan Mushaf Al-Qur’an.

                <br>Jika saya belum mengirimkan hasil cetak Mushaf yang telah mendapatkan Tanda Tashih, saya tidak akan mengajukan permohonan pentashihan untuk naskah mushaf yang lainnya, dan LPMQ berhak untuk tidak memproses pengajuan tersebut.
                
                <br>Sedangkan jika di kemudian hari ada pihak-pihak yang mempersoalkan terkait hak cipta produk ini, saya sebagai penerbit akan bertanggung jawab sepenuhnya, dan tidak akan melibatkan Lajnah Pentashihan Mushaf Al-Qur’an.
                </div>
            </div>

            <div class="form-group" style="display: table;margin-left: auto;">
                <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="nextPage('IsiSurvei')">Selanjutnya <i class="fa fa-chevron-right"></i></a>
            </div>
        `;
    } else {
        goToPage('IsiSurvei')

        html += `
            <p style="margin-top: 15px">
                Trima kasih karna telah menginputkan pernyataan, pernyataan anda sedang kami proses.
            </p>
            <div class="form-group" style="display: table;margin-left: auto;">
                <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="goToPage('IsiSurvei')">Selanjutnya <i class="fa fa-chevron-right"></i></a>
            </div>
        `;
    }

    $(document).find("#form-pernyataan").empty().html(html);
}

const checkSurvey = (id_proses_pentashihan) => {
    $.ajax({
        url : mainpath + "/check-survey",
        method : "POST",
        data : {
            '_token': token,
            'id_proses_pentashihan': id_proses_pentashihan
        },
        beforeSend: function() {
            $(document).find('#modalCombinePentasihan #IsiSurvei').empty().html('Load Content ..');
        },
        complete: function () {
        },
        success: function(res){
            if (res.status == 1) {
                htmlSurvei(id_proses_pentashihan, res.item.isUserAnswer);
            } else {
                checkSurvey(id_proses_pentashihan);
            }
        },
        errors: function(res) {
            checkSurvey(id_proses_pentashihan);
        }
    });
}

const htmlSurvei = (id_proses_pentashihan, isSurvey) => {
    html = '';
    if (isSurvey != 'Yes') {
        html += `
        <body style="margin:0px;padding:0px;overflow:hidden">
            <iframe src="`+pathSurvey+`?redirect_link=`+mainpath+`&type=Survey+Download+`+id_proses_pentashihan+`" frameborder="0" style="overflow:hidden;height:500px;width:100%" height="500px;" width="100%"></iframe>
        </body>
        `;
    } else {
        goToPage('BeriRating')

        html += `
            <p style="margin-top: 15px">
                Terima kasih karna telah mengikuti survei, sebagai responden dalam survei ini.
            </p>
        `;
    }

    html += `
        <div class="form-group" style="display: table;margin-left: auto;">
            <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="prevPage('PernyataanForm')" style="margin-right: 10px;"><i class="fa fa-chevron-left"></i> Sebelumnya</a>
            <button type="button" href="javascript:void(0)" id="btnBeriRating" class="btn btn-md btn-primary" `+(isSurvey != 'Yes' ? 'disabled' : '')+` onclick="nextPage('BeriRating')">Selanjutnya <i class="fa fa-chevron-right"></i></button>
        </div>
    `;

    $(document).find('#IsiSurvei').empty().html(html);
}

const checkRating = (id_proses_pentashihan) => {
    var request = $.ajax({
        url: mainpath + '/rating-us/' + id_proses_pentashihan,
        type: "GET",
        data: {
            id : id_proses_pentashihan
        }
    });

    request.done(function(msg) {
        console.log(msg);
        let item    = msg.item;
        let content = '';
        rating = 0;
        
        $.each(item, function(key, value){
            rating += Number(value.jumlah_rating);

            if (value.jumlah_rating < 1) {
                textRating = "Tidak Puas";
            }else if(value.jumlah_rating >2 &&  value.jumlah_rating <=3){
                textRating = "Kurang Puas";
            }else if(value.jumlah_rating > 3){
                textRating = "Puas";
            }
            content += `
                <div class="form-group" style="display: block;text-align: center;margin: 30px auto;">
                    <span class="text-rating text-danger">`+textRating+`</span>
                    <br><br>
                    <span class="my-rating-7"></span><br>
                </div>
                <div class="form-group" style="display: block;text-align: center;margin: 30px auto;">
                    <label>Komentar </label>
                    `+value.komentar+`
                </div>
            `;
        });
        htmlBeriRating(item, content, rating, id_proses_pentashihan);
    });

    request.fail(function(jqXHR, textStatus) {
        alert("Opps ada yang salah silahkan coba beberapa saat kemudian.");
        checkRating(id_proses_pentashihan);
    });
}

const checkTandaTansih = (id_proses_pentashihan) => {
    $.ajax({
        url : mainpath + "/check-tanda-tansih",
        method : "POST",
        data : {
            '_token': token,
            'id_proses_pentashihan': id_proses_pentashihan
        },
        beforeSend: function() {
            $(document).find('#modalCombinePentasihan #contentTandaTansih').empty().html('Load Content ..');
        },
        complete: function () {
        },
        success: function(res){
            if (res.status == 1) {
                html = '';
                html += `
                    <table class="table table-striped">
                        <tr>
                            <th>Surat Penerbitan</th>
                            <th>Tanda Tashih</th>
                        </tr>
                `;
                $.each(res.item, function (key, val) {
                    html += `
                        <tr>
                            <td>
                                <a href="`+val.surat_penerbitan+`" target="_blank" class="btn btn-xs btn-success"><span class="fa fa-download"></span>  Download File</a>
                            </td>
                            <td>
                                <a href="`+val.scan_tandfa_tashih+`" target="_blank" class="btn btn-xs btn-success"><span class="fa fa-download"></span>  Download File</a>
                            </td>
                        </tr>
                    `;
                });
                html += `
                    </table>
                `;
                $(document).find('#contentTandaTansih').empty().html(html);
                htmlUnduhTandaTashih(id_proses_pentashihan,html);
            } else {
                checkTandaTansih(id_proses_pentashihan);
            }
        },
        errors: function(res) {
            checkTandaTansih(id_proses_pentashihan);
        }
    });
}

const htmlUnduhTandaTashih = (id_proses_pentashihan,content) =>
{
    html = '';
    html += content;
    html += `<div id="contentTandaTansih"></div>`
    html += `
    <div class="form-group" style="display: table;margin-left: auto;">
        <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="prevPage('BeriRating')" style="margin-right: 10px;"><i class="fa fa-chevron-left"></i> Sebelumnya</a>
    </div>
    `;
    $(document).find('#UnduhTandaTashih').empty().html(html);
}



const htmlBeriRating = (data, ratingContent, rating,id_proses_pentashihan) => {
    html = '';
    if (data.length > 0) {
        
        html += ratingContent;

        html += `
            <div class="form-group" style="display: table;margin-left: auto;">
                <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="prevPage('IsiSurvei')" style="margin-right: 10px;"><i class="fa fa-chevron-left"></i> Sebelumnya</a>
                <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="nextPage('UnduhTandaTashih')" style="margin-right: 10px;"><i class="fa fa-chevron-right"></i> Selanjutnya</a>
            </div>
        `;
    } else {
        html += `
            <form method="post" id="form-rating" enctype="multipart/form-data" action="`+mainpath+`/rating">
                <input type="hidden" name="id_proses_pentashihan" class="id_proses_pentashihan" value="`+id_proses_pentashihan+`">
                <input type="hidden" name="_token" value="`+token+`">

                <div class="form-group" style="display: block;text-align: center;margin: 30px auto;">
                    <span class="text-rating text-danger">Tidak Puas</span>
                    <br><br>
                    <span class="my-rating-9"></span><br>
                    <input type="hidden" name="jumlah_rating" id="jumlah_rating" required>
                </div>

                <div class="form-group">
                    <label>Kosongkan jika tidak ada komentar<code>*</code></label>
                    <textarea name="komentar" class="form-control" id="komentar-rating" cols="30" rows="10" placeholder="Masukkan Komentar anda"></textarea>
                </div>

                <div class="form-group" style="display: table;margin-left: auto;">
                    <a href="javascript:void(0)" class="btn btn-md btn-primary" onclick="prevPage('IsiSurvei')" style="margin-right: 10px;"><i class="fa fa-chevron-left"></i> Sebelumnya</a>
                    <button disabled id="submitIsiSurvey" type="button" class="btn btn-md btn-primary"><i class="fa fa-save"></i> Selanjutnya</button>
                </div>
            </form>
        `;
    }

    // html += `<div id="contentTandaTansih"></div>`

    $(document).find('#BeriRating').empty().html(html);

    $(document).find(".my-rating-7").starRating({
        starShape: 'rounded',
        starSize: 40,
        initialRating: rating,
        disableAfterRate: false,
        readOnly: true,
        emptyColor: '#DEDEDE',
        hoverColor: '#EDBD19',
        strokeColor: '#EDBD19',
        activeColor: '#EDBD19',
    });

    $(document).find(".my-rating-9").starRating({
        starShape: 'rounded',
        starSize: 40,
        initialRating: 0,
        disableAfterRate: false,
        emptyColor: '#DEDEDE',
        hoverColor: '#EDBD19',
        strokeColor: '#EDBD19',
        activeColor: '#EDBD19',
        disableAfterRate: false,
        onHover: function(currentIndex, currentRating, $el){
            if (currentIndex < 1) {
                $(".text-rating").text("Tidak Puas");
            }else if(currentIndex >2 &&  currentIndex <=3){
                $(".text-rating").text("Kurang Puas");
            }else if(currentIndex > 3){
                $(".text-rating").text("Puas");
            }
            $("#jumlah_rating").val(currentIndex);
            $("#submitIsiSurvey").removeAttr("disabled");
            document.getElementById("submitIsiSurvey").type = "submit";
        },
        onLeave: function(currentIndex, currentRating, $el){
            if (currentRating < 1) {
                $(".text-rating").text("Tidak Puas");
            }else if(currentRating >2 &&  currentRating <=3){
                $(".text-rating").text("Kurang Puas");
            }else if(currentRating > 3){
                $(".text-rating").text("Puas");
            }
            $("#jumlah_rating").val(currentRating);
            $("#submitIsiSurvey").removeAttr("disabled");
            document.getElementById("submitIsiSurvey").type = "submit";
        }
    });
}

modalLaporanPencetakan = '';
modalLaporanPencetakan += `
<form id="formPernyataan" method="post" enctype="multipart/form-data" action="`+mainpath+`/laporan-pencetakan">
	<div id="modalLaporanPencetakan" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Laporan Pencetakan</h4>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<label>Pencetakan ke</label>
						<input type="text" name="pencetakan_ke" id="pencetakan_ke" class="form-control">
					</div>

					<div class="form-group">
						<label for="tanggal_laporan">Tanggal Cetak * :</label>
						<input type="text" required="" readonly name="tanggal_laporan" class="form-control datepicker" id="tanggal_laporan">
					</div>

					<div id="listFormLaporanPencetakan"></div>

					<div class="form-group">
						<label for="nama_percetakan_laporan">Nama Percetakan * :</label>
						<input type="text" required="" name="nama_percetakan_laporan" class="form-control" id="nama_percetakan_laporan">
					</div>

					<div class="form-group">
						<label for="alamat_percetakan_laporan">Alamat Percetakan * :</label>
						<textarea required="" name="alamat_percetakan_laporan" class="form-control" id="alamat_percetakan_laporan" style="resize:none;" rows="5"></textarea>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
	</div>
	
	<input id="id_proses_pentashihan_laporan" type="hidden" name="id_proses_pentashihan_laporan" value="">
	<input type="hidden" name="_token" value="`+token+`">
</form>
`;

$('body').append(modalLaporanPencetakan);
$('.datepicker').datepicker({
	format:'yy-mm-dd'
});

$('.btn-laporan-pencetakan').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');
		
		htmlListOplah(id);

		$('#id_proses_pentashihan_laporan').val(id);
		$('#modalLaporanPencetakan').modal('show');
	})
});

const htmlListOplah = (id_proses_pentashihan) => {
	$.ajax({
        url : mainpath + "/list-ukuran-oplah",
        method : "POST",
        data : {
			"_token" : token,
			"id_proses_pentashihan" : id_proses_pentashihan,
        },
        beforeSend: function(res) {
			$("#listFormLaporanPencetakan").empty();
			$("#pencetakan_ke").val('');
        },
        complete: function (res) {
        },
        success: function(res){
			$("#pencetakan_ke").val(res.count_cetak_tashih);

			html = '';
			if (res.status == 1) {
				$.each(res.item, function (key, val) {
					html += `
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Ukuran</label>
									<input type="text" name="ukuran_cetak[]" id="ukuran_cetak" value="`+val.ukuran+`" class="form-control" readonly>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Oplah</label>
									<input type="text" name="oplah_cetak[]" id="oplah_cetak" class="form-control price-format" required>
								</div>
							</div>
						</div>
					</div>
					`;
				});
			}

			$("#listFormLaporanPencetakan").empty().html(html);
			

			$(function () {
				$(document).find('.price-format').priceFormat({
					prefix:'', thousandsSeparator:'.', centsLimit: 0, clearOnEmpty:false
				});
			});
		},
		error: function(res){
        }
    })
}