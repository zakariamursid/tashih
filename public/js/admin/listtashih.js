let modalDownloadKoreksi = '';
modalDownloadKoreksi += '<div id="modalDownloadKoreksi" class="modal fade" role="dialog">';
modalDownloadKoreksi += '<div class="modal-dialog">';
modalDownloadKoreksi += '<div class="modal-content">';

modalDownloadKoreksi += '<div class="modal-header">';
modalDownloadKoreksi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalDownloadKoreksi += '<h4 class="modal-title">Download Koreksi</h4>';
modalDownloadKoreksi += '</div>';

modalDownloadKoreksi += '<div class="modal-body">';
modalDownloadKoreksi += '<div class="table-responsive">';

modalDownloadKoreksi += '<div id="table-download-koreksi">';
modalDownloadKoreksi += '<div>';

modalDownloadKoreksi += '</div>';
modalDownloadKoreksi += '</div>';

modalDownloadKoreksi += '<div class="modal-footer">';
modalDownloadKoreksi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalDownloadKoreksi += '</div>';

modalDownloadKoreksi += '</div>';
modalDownloadKoreksi += '</div>';
modalDownloadKoreksi += '</div>';

$('body').append(modalDownloadKoreksi);

$(".btn-download-koreksi-satu").each(function(){
    $(this).click(function(){
        let id = $(this).data('idtashih');

			var request = $.ajax({
			  url: mainpath+'/koreksi-tashih/'+id,
			  type: "GET",
			  data: {
			  	id : id
			  }
			});

// 			$("#modalDownloadKoreksi").find("#table-download-koreksi").html("");

			request.done(function(msg) {
			    let item = msg.item;
			    let content = '';
			    let i=0;
			    content += '<div class="form-group">';
			        content += '<table class="table">';
			        content += '<tr>';
			        content += '<th>Pesan Koreksi</th>';
			        content += '<th>Isi Koreksi</th>';
			        content += '<th>Lapiran Koreksi</th>';
			        content += '</tr>';
			    $.each(item,function(key, value){
			        i = i++;
			        op=value.pesan;
			        opw=value.revisi;
			        
			        if(op==null){
			            op='-';
			        }else{
 			            op=op;
			        }
			        
			        if(opw==null){
			            opw='-';
			        }else{
			            opw=opw;
			        }
			        
    			    content += '<tr>';
    			    
    			    content += '<td>'+op+'</td>';
    			    content += '<td>'+opw+'</td>';
    			    content += '<td>';
    			    content += '<a href="'+mainpath+'/download-revisi/'+value.id+'" class="btn btn-xs btn-warning"><i class="fa fa-download"></i> Download Koreksi</a>';
    			    content += '</td>';
    			    
    			    content += '</tr>';
    			    
			    });
			    content += '</table>';
    			    
    			    content += '</div>';
			     $("#modalDownloadKoreksi").find("#table-download-koreksi").html(content);
			   
			});
			request.fail(function(msg){
			    alert(msg);
			});
			
			
			 //   $("#modalDownloadKoreksi").modal("show");
    }); 
});



/**
 * KOREKSI MODAL ACTION
 */
let loading2 = '';
loading2 += '<div id="loading2" class="sk-fading-circle">';
loading2 += '<div class="sk-circle1 sk-circle"></div>';
loading2 += '<div class="sk-circle2 sk-circle"></div>';
loading2 += '<div class="sk-circle3 sk-circle"></div>';
loading2 += '<div class="sk-circle4 sk-circle"></div>';
loading2 += '<div class="sk-circle5 sk-circle"></div>';
loading2 += '<div class="sk-circle6 sk-circle"></div>';
loading2 += '<div class="sk-circle7 sk-circle"></div>';
loading2 += '<div class="sk-circle8 sk-circle"></div>';
loading2 += '<div class="sk-circle9 sk-circle"></div>';
loading2 += '<div class="sk-circle10 sk-circle"></div>';
loading2 += '<div class="sk-circle11 sk-circle"></div>';
loading2 += '<div class="sk-circle12 sk-circle"></div>';
loading2 += '</div>';

let timeline = '';
timeline += '<ul class="timeline">';;
timeline += '</ul>';

let modalRevisi = '';
modalRevisi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/koreksi">';
modalRevisi += '<div id="modalRevisi" class="modal fade" role="dialog">';
modalRevisi += '<div class="modal-dialog modal-lg">';
modalRevisi += '<div class="modal-content">';
modalRevisi += '<div class="modal-header">';
modalRevisi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalRevisi += '<h4 class="modal-title">Revisi</h4>';
modalRevisi += '</div>';
modalRevisi += '<div class="modal-body">';
modalRevisi += loading2;
modalRevisi += '<div class="form-group">';
modalRevisi += '<label for="pesanKoreksi">*Pesan:</label>';
modalRevisi += '<textarea name="pesan" class="form-control" id="pesanKoreksi" required="" rows="5"></textarea>';
modalRevisi += '</div>';
modalRevisi += '<div class="form-group">';
modalRevisi += '<label for="fileLampiran">File:</label>';
modalRevisi += '<input type="file" name="lampiran" class="form-control" id="fileLampiran">';
modalRevisi += '</div>';
modalRevisi += '<div class="row">';
modalRevisi += '<div class="col-sm-12">';
modalRevisi += '<div class="box-group">';
modalRevisi += '<div class="panel box box-success">';
modalRevisi += '<div class="box-header with-border">';
modalRevisi += '<h4 class="box-title">';
modalRevisi += '<a data-toggle="collapse" data-parent="#accordion" href="#collapseRevisi" aria-expanded="false" class="collapsed">';
modalRevisi += 'Riwayat Revisi';
modalRevisi += '</a>';
modalRevisi += '</h4>';
modalRevisi += '</div>';
modalRevisi += '<div id="collapseRevisi" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">';
modalRevisi += '<div class="box-body">';
modalRevisi += timeline;
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '<div class="modal-footer">';
modalRevisi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalRevisi += '<button id="submitKoreksi" type="submit" class="btn btn-primary">Submit</button>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '</div>';
modalRevisi += '<input id="id_proses_pentashihan_koreksi" type="hidden" name="id_proses_pentashihan" value="">';
modalRevisi += '<input type="hidden" name="_token" value="'+token+'">';
modalRevisi += '</form>';

$('body').append(modalRevisi);

$('.btn-revisi').each(function(){
	$(this).on('click',function(){
		$('#modalRevisi').find('.modal-body').children('div').hide();
		$('#loading2').show();
		$('#modalRevisi').modal('show');

		let id = $(this).data('id');
		$('#id_proses_pentashihan_koreksi').val(id);

		var request = $.ajax({
		  url: mainpath+'/load-koreksi',
		  type: "GET",
		  data: {
		  	id : id
		  }
		});

		request.done(function(msg) {
			let data    = msg.data;
			let status  = msg.api_status;
			let message = msg.api_message;

			if (status === 1) {
				$('#modalRevisi').find('.modal-body').children('div').show(700);
				$('#loading2').hide(700);

				removeTimeline();

				$.each(data, function(i, item) {
					addTimeline(item);
				});

			}else{
				$('#modalRevisi').modal('hide');
				alert('Error : data tidak ditemukan');
			}
		});

		request.fail(function(jqXHR, textStatus) {
		  alert( "Request failed: " + textStatus );
		});
	})
})

$('.timeline').on('click','.delete-data',function(){
	let id = $(this).data('id');

	let konfirmasi = confirm('apakah anda yakin menghapus data ini ?')
	if (konfirmasi) {
		window.location.href = mainpath+'/hapus-revisi/'+id;
	}
})

function removeTimeline(){
	$('.timeline').html("");
}
function addTimeline(data){

	let lampiran = '';
	if (data.lampiran == '') {
		lampiran += '-';
	}else{
		lampiran += '<a href="'+data.lampiran+'" target="_blank" class="btn btn-primary btn-xs">Lihat</a>';
	}

	let revisi_lampiran = '';
	if (data.revisi_lampiran == '') {
		revisi_lampiran += '-';
	}else{
		revisi_lampiran += '<a href="'+data.revisi_lampiran+'" target="_blank" class="btn btn-primary btn-xs">Lihat</a>';
	}

	let revisi_date = '';
	if (data.revisi_date == '') {
		revisi_date += '-';
	}else{
		revisi_date += '<i class="fa fa-clock-o"></i> '+data.revisi_date;
	}

	let bg = '';
	let ic = '';
	if (data.revisi_lampiran == '' || data.revisi == '') {
		bg += 'yellow';
		ic += 'times';
	}else{
		bg += 'green';
		ic += 'check';
	}

	let appendTimeline = '';
	appendTimeline += '<li class="time-label"> <span class="bg-'+bg+'"> '+data.admin_name+' </span> </li>';
	appendTimeline += '<li> <i class="fa fa-'+ic+' bg-'+bg+'" data-id="'+data.id+'"></i>';

	appendTimeline += '<div class="timeline-item">';
	appendTimeline += '<span class="time"><i class="fa fa-clock-o"></i> '+data.created_at+'</span>';
	appendTimeline += '<h3 class="timeline-header">Koreksi</h3>';
	appendTimeline += '<div class="timeline-body">'; 
	appendTimeline += '<div class="timeline-content">'+data.pesan+'</div>'
	appendTimeline += '<div class="timeline-lampiran">Lampiran : '+lampiran+'</div>';
	appendTimeline += '</div>';
	appendTimeline += '</div>';

	appendTimeline += '<div class="timeline-item">';
	appendTimeline += '<span class="time">'+revisi_date+'</span>';
	appendTimeline += '<h3 class="timeline-header">Revisi</h3>';
	if (data.revisi_lampiran == '' && data.revisi == '') {
		appendTimeline += '<div class="timeline-body "><center>Belum ada revisi</center></div>';
	}else{
		appendTimeline += '<div class="timeline-body">'; 
		appendTimeline += '<div class="timeline-content">'+data.revisi+'</div>'
		appendTimeline += '<div class="timeline-lampiran">Lampiran : '+revisi_lampiran+'</div>';
		appendTimeline += '</div>';
	}
	appendTimeline += '</div>';
	appendTimeline += '</div>';

	appendTimeline += '</li>';
	$('.timeline').append(appendTimeline);
}

$("#btnOplah").on("click",function(){
	let myOplah = '';
	myOplah += '<div class="col-sm-6" style="margin-top:10px;">'
	myOplah += '<label class="verysmall-green-default">'
	myOplah += 'Ukuran (cm)  *'
	myOplah += '<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dalam ukuran centimeter, dan format panjang X tinggi. misal:15 X 20"></i>'
	myOplah += '</label>'
	myOplah += '<input type="text" name="ukuran[]" placeholder="Tulis ukuran" required="" class="form-control" value="">'
	myOplah += '</div>'
	myOplah += '<div class="col-sm-6" style="margin-top:10px;">'
	myOplah += '<label class="verysmall-green-default">'
	myOplah += 'Oplah *'
	myOplah += '<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dengan angka dan jml unit. misal:100000 eksemplar"></i>'
	myOplah += '</label>'
	myOplah += '<button type="button" class="btn btn-xs btn-warning pull-right btn-remove-oplah"><i class="fa fa-times"></i></button>'
	myOplah += '<input type="text" name="oplah[]" placeholder="Tulis oplah" required="" class="form-control" value="">'
	myOplah += '</div>';

	$("#wrapOplah").append(myOplah);
})

$("#wrapOplah").on("click",".btn-remove-oplah",function(){
	if (segment == 'edit') {
		let confirmRemove = confirm('apakah anda yakin ingin menghapus ukuran ini ?');
		if (confirmRemove) {
			$(this).parent().prev().remove();
			$(this).parent().remove();
		}
	}else{
		$(this).parent().prev().remove();
		$(this).parent().remove();
	}
})

let modalResi = '';
modalResi += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/resi">';
modalResi += '<div id="modalResi" class="modal fade" role="dialog">';
modalResi += '<div class="modal-dialog">';
modalResi += '<div class="modal-content">';
modalResi += '<div class="modal-header">';
modalResi += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalResi += '<h4 class="modal-title">Resi</h4>';
modalResi += '</div>';
modalResi += '<div class="modal-body">';
modalResi += '<div class="form-group">';
modalResi += '<label for="noResi">No Resi :</label>';
modalResi += '<input type="text" name="no_resi" class="form-control" id="noResi">';
modalResi += '</div>';
// modalResi += '<div class="form-group">';
// modalResi += '<label for="tandaTerima">Tanda Terima :</label>';
// modalResi += '<input type="file" name="tanda_terima" class="form-control" id="tandaTerima">';
// modalResi += '</div>';
modalResi += '</div>';
modalResi += '<div class="modal-footer">';
modalResi += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalResi += '<button id="submitKoreksi" type="submit" class="btn btn-primary">Submit</button>';
modalResi += '</div>';
modalResi += '</div>';
modalResi += '</div>';
modalResi += '</div>';
modalResi += '<input id="id_proses_pentashihan" type="hidden" name="id_proses_pentashihan" value="">';
modalResi += '<input type="hidden" name="_token" value="'+token+'">';
modalResi += '</form>';
$('body').append(modalResi);

$('.kirim-resi').each(function(){
	console.log($(this).data('id'));
	$(this).on('click',function(){
		let id = $(this).data('id');

		$('#id_proses_pentashihan').val(id);
		$('#modalResi').modal('show');
	})
})

$('.show-file').on('click',function(){
	let show = false;
	$('.show-file').each(function(){
		if ($(this).is(':checked')) {
			show = true;
		}
	})

	if (show) {
		$('#fileNaskah').parent().show(600);
		$('#fileNaskah').attr('required',true);
	}else{
		$('#fileNaskah').parent().hide(600);
		$('#fileNaskah').attr('required',false);
		$('#fileNaskah').val('');
	}
})

/**
 * Rating
 */

function submitRating() {
	if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
		swal("Perhatian","Pilih Rating dulu","warning");
			// $(".text-rating").text("Pilih Rating dulu");
	  }else{
		  $("#form-rating").submit();
		  $("#modalRating").modal("hide");
	  }
  }

let modalRating = '';
modalRating += '<form method="post" id="form-rating" enctype="multipart/form-data" action="'+mainpath+'/download-tansih">';
modalRating += '<div id="modalRating" class="modal fade" role="dialog">';
modalRating += '<div class="modal-dialog">';
modalRating += '<div class="modal-content">';
modalRating += '<div class="modal-header">';
modalRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalRating += '<h4 class="modal-title">Beri Rating</h4>';
modalRating += '</div>';
modalRating += '<div class="modal-body" style="text-align:center">';


$(function () {
	$(".my-rating-9").starRating({
	  starShape: 'rounded',
	  starSize: 40,
	  initialRating: 0,
	  disableAfterRate: false,
	  emptyColor: '#DEDEDE',
	  hoverColor: '#EDBD19',
	  strokeColor: '#EDBD19',
	  activeColor: '#EDBD19',
	  disableAfterRate: false,
	  onHover: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentIndex);
		if (currentIndex < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentIndex >2 &&  currentIndex <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentIndex > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentIndex);
	  },
	  onLeave: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentRating);
		if (currentRating < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentRating >2 &&  currentRating <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentRating > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentRating);

	  }
	});
  });
modalRating += '<input type="hidden" name="id" id="idUserCms">';
modalRating += '<input type="hidden" name="idtashih" id="idTashih">';
modalRating += '<div class="form-group">';
modalRating += '<span class="text-rating text-danger">Tidak Puas</span>';
modalRating += '<br><br>';
modalRating += '<span class="my-rating-9"></span><br>';
modalRating += '<input type="hidden" name="jumlah_rating" id="jumlah_rating" required>';
modalRating += '</div>';
modalRating += '<div class="modal-footer">';
modalRating += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalRating += '<button type="button" onclick="submitRating()"  class="btn btn-primary" id="simpan_rating_fdsgdh">Submit</button>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<input type="hidden" name="_token" value="'+token+'">';
modalRating += '</form>';

$('body').append(modalRating);
$('.btn-download-tashih').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');
		let idt = $(this).data('idtashih');

		$('#idUserCms').val(id);
		$('#idTashih').val(idt);
		$('#modalRating').modal('show');
	})
})


let modalUploadBukti = '';
modalUploadBukti += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/upload-bukti">';
modalUploadBukti += '<div id="modalUploadBukti" class="modal fade" role="dialog">';
modalUploadBukti += '<div class="modal-dialog">';
modalUploadBukti += '<div class="modal-content">';
modalUploadBukti += '<div class="modal-header">';
modalUploadBukti += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalUploadBukti += '<h4 class="modal-title">Unggah Bukti Pembayaran</h4>';
modalUploadBukti += '</div>';
modalUploadBukti += '<div class="modal-body">';

modalUploadBukti += '<div class="form-group">';
modalUploadBukti += '<div id="divKotakBukti"></div>';
modalUploadBukti += '</div>';
modalUploadBukti += '<div class="form-group">';
modalUploadBukti += '<label for="buktiPembayaran">Bukti Pembayaran :</label>';
modalUploadBukti += '<input type="file" name="bukti_pembayaran" class="form-control" id="buktiPembayaran" required>';
modalUploadBukti += '</div>';

modalUploadBukti += '</div>';
modalUploadBukti += '<div class="modal-footer">';
modalUploadBukti += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalUploadBukti += '<button type="submit" class="btn btn-primary">Submit</button>';
modalUploadBukti += '</div>';
modalUploadBukti += '</div>';
modalUploadBukti += '</div>';
modalUploadBukti += '</div>';

modalUploadBukti += '<input id="id_proses_pentashihan_lima" type="hidden" name="id_proses_pentashihan">';
modalUploadBukti += '<input type="hidden" name="_token" value="'+token+'">';
modalUploadBukti += '</form>';

$('body').append(modalUploadBukti);

$(function () {
    $('#PNBP').priceFormat({
        prefix:'',thousandsSeparator:'.',centsLimit: 0,clearOnEmpty:false
    });
})

$('.upload-bukti').each(function(){
	$(this).on('click',function(){
	    id = $(this).data("id")
	    
	    $.ajax({
            url : mainpath+"/cari-pnbp/"+id,
            method:"GET",
            data: {
                "id":id
            },
            success:function(data){
                if(data.message=="success"){
                    let html = HTMLpnbp(data,id);
                    $("#divKotakBukti").empty().append(html);
                }
            },
            error:function(data){
                alert(data);
            }
        });
	    
	    $("#id_proses_pentashihan_lima").val(id);
		$('#modalUploadBukti').modal('show');
	})
});


let modalListPNBP = '';
modalListPNBP += '<div id="modalListPNBP" class="modal fade" role="dialog">';
modalListPNBP += '<div class="modal-dialog">';

modalListPNBP += '<div class="modal-content">';

modalListPNBP += '<div class="modal-header">';
modalListPNBP += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalListPNBP += '<h4 class="modal-title"><b>Tanda PNBP</b></h4>';
modalListPNBP += '</div>';
modalListPNBP += '<div class="modal-body">';

modalListPNBP += '<div class="form-group">';
modalListPNBP += '<div id="tabelListPNBP">';
modalListPNBP += '</div>';

modalListPNBP += '</div>';
modalListPNBP += '<div class="modal-footer">';

modalListPNBP += '</div>';
modalListPNBP += '</div>';

modalListPNBP += '</div>';
modalListPNBP += '</div>';

$('body').append(modalListPNBP);

$('.look-pnbp-list-satu').each(function(){
    $(this).click(function(){
        let id = $(this).data("id");

        $.ajax({
            url : mainpath+"/cari-pnbp/"+id,
            method:"GET",
            data: {
                "id":id
            },
            success:function(data){
                if(data.message=="success"){
                    let html = HTMLpnbp(data,id);
                    
                    html +='<div class="form-group">';
                    html +='<label>Upload Pembayaran</label>';
                    
                    if(data.link_file_cek==""){
                        html +='---';
                    }else{
                        if(data.eksten_cek == 'pdf' || data.eksten_cek == 'PDF'){
                            html +='<iframe src="'+data.link_file_cek+'" style="width:100%;height:350px"></iframe>';; 
                        }else{
                            html +='<img src="'+data.link_file_cek+'" style="width:100%" alt="Responsive image">';
                        }   
                    }
                
                    html +='</div>';
                    $("#tabelListPNBP").empty().append(html);
                }
            },
            error:function(data){
                alert(data);
            }
        });

        $("#modalListPNBP").modal("show");
    });
});

function HTMLpnbp(data,id){
    let html = '';
    html +='<div class="form-group">';
    html +='<label>Nominal PNBP</label>';
    html +='<b><h4>Rp. '+data.nominal+'</h4></b>';
    html +='</div>';

    html +='<div class="form-group">';
    html +='<a href="'+mainpath+'/download-pnbp/'+id+'" class="pull-right btn btn-xs btn-danger"><span class="fa fa-download"></span> Download File</a>';
    html +='<label>Bukti PNBP</label>';
    
    if(data.link_file==""){
        html +='---';
    }else{
        if(data.eksten == 'pdf' || data.eksten == 'PDF'){
            html +='<iframe src="'+data.link_file+'" style="width:100%;height:350px"></iframe>';; 
        }else{
            html +='<img src="'+data.link_file+'" style="width:100%" alt="Responsive image">';
        }   
    }

    html +='</div>';

    return html;
}