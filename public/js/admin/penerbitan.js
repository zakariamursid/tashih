let modalPernyataan = '';
modalPernyataan += '<form id="formPernyataan" method="post" enctype="multipart/form-data" action="'+mainpath+'/pernyataan">';
modalPernyataan += '<div id="modalPernyataan" class="modal fade" role="dialog">';
modalPernyataan += '<div class="modal-dialog">';
modalPernyataan += '<div class="modal-content">';
modalPernyataan += '<div class="modal-header">';
modalPernyataan += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalPernyataan += '<h4 class="modal-title">Pernyataan</h4>';
modalPernyataan += '</div>';
modalPernyataan += '<div class="modal-body">';
modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="nama">Nama * :</label>';
modalPernyataan += '<input type="text" required="" name="nama" class="form-control" id="nama">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="jabatan">Jabatan * :</label>';
modalPernyataan += '<input type="text" required="" name="jabatan" class="form-control" id="jabatan">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="penerbit">Penerbit * :</label>';
modalPernyataan += '<input type="text" class="form-control" value="" id="penerbit" disabled="">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="alamat">Alamat * :</label>';
modalPernyataan += '<textarea required="" name="alamat" class="form-control" id="alamat" style="resize:none;" rows="5"></textarea>';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="phone">Telp./HP. * :</label>';
modalPernyataan += '<input type="text" required="" name="phone" class="form-control" id="phone">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="email">Alamat Email * :</label>';
modalPernyataan += '<input type="email" required="" name="email" class="form-control" id="email">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="nama_percetakan">Nama Percetakan * :</label>';
modalPernyataan += '<input type="text" required="" name="nama_percetakan" class="form-control" id="nama_percetakan">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="alamat_percetakan">Alamat * :</label>';
modalPernyataan += '<textarea required="" name="alamat_percetakan" class="form-control" id="alamat_percetakan" style="resize:none;" rows="5"></textarea>';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label for="rencana_pelaksanaan">Rencana Pelaksanaan * :</label>';
modalPernyataan += '<input type="text" required="" name="rencana_pelaksanaan" class="form-control" id="rencana_pelaksanaan">';
modalPernyataan += '</div>';

modalPernyataan += '<div class="form-group">';
modalPernyataan += '<label class="checkbox-inline"><input id="checkRedaksi" type="checkbox" value="">';
modalPernyataan += '<a href="#redaksi" data-toggle="collapse">'
modalPernyataan += 'Redaksi Kesanggupan</label>';					
modalPernyataan += '</a>';
modalPernyataan += '<div id="redaksi" class="collapse" style="text-align:justify;border:1px solid #CCC;padding:5px 10px;border-radius:5px;">';
modalPernyataan += 'Dengan ini menyatakan bahwa sesuai ketentuan Lajnah Pentashihan Mushaf Al-Qur’an (LPMQ) dan sebagai ';
modalPernyataan += 'kewajiban selaku penerbit, saya akan mengirimkan contoh hasil cetak Mushaf Al-Qur\'an yang kami terbitkan ';
modalPernyataan += 'sebagai berikut Selambat-lambatnya 2 (dua) minggu setelah dicetak, dengan jumlah 5 (lima) eksemplar, ';
modalPernyataan += 'sebagai bukti penerbitan dan dokumentasi untuk Lajnah Pentashihan Mushaf Al-Qur’an.';
modalPernyataan += '<br>Jika saya belum mengirimkan hasil cetak Mushaf yang telah mendapatkan Tanda Tashih, saya tidak akan ';
modalPernyataan += 'mengajukan permohonan pentashihan untuk naskah mushaf yang lainnya, dan LPMQ berhak untuk tidak memproses pengajuan tersebut. ';
modalPernyataan += '<br>Sedangkan jika di kemudian hari ada pihak-pihak yang mempersoalkan terkait hak cipta produk ini, ';
modalPernyataan += 'saya sebagai penerbit akan bertanggung jawab sepenuhnya, dan tidak akan melibatkan Lajnah Pentashihan Mushaf Al-Qur’an.';
modalPernyataan += '</div>';
modalPernyataan += '</div>';

modalPernyataan += '</div>';
modalPernyataan += '<div class="modal-footer">';
modalPernyataan += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalPernyataan += '<button id="confirmSubmit" type="button" class="btn btn-primary">Submit</button>';
modalPernyataan += '<button id="submit" type="submit" style="display:none;">Submit</button>';
modalPernyataan += '</div>';
modalPernyataan += '</div>';
modalPernyataan += '</div>';
modalPernyataan += '</div>';
modalPernyataan += '<input id="id_proses_pentashihan" type="hidden" name="id_proses_pentashihan" value="">';
modalPernyataan += '<input type="hidden" name="_token" value="'+token+'">';
modalPernyataan += '</form>';
$('body').append(modalPernyataan);

$('#penerbit').val(name);

$('.btn-pernyataan').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');
		$('#id_proses_pentashihan').val(id);

		$('#modalPernyataan').modal('show');
	})
})
$('#confirmSubmit').on('click',function(){
	
	let confirmation = confirm('Apakah anda yakin dengan data yang akan dikirim?');

	if (confirm) {
		if ($('#checkRedaksi:checked').length > 0) {
			$('#submit').click();
		}else{
			alert('Redaksi kesanggupan harus disetujui');
		}
		// $("#formPernyataan").submit();
	}
	
	
})


let modalTandaTashih = '';
modalTandaTashih += `
<div id="modalTandaTashih" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Download Tanda Tashih</h4>
            </div>

			<div class="modal-body">`;
			if (isUserAnswerSurvey == 'Yes') {
modalTandaTashih +=`
				<ul class="nav nav-tabs nav-justified">
                </ul>
                <div class="tab-content">
                </div>`;
			}else{
modalTandaTashih +=`
				<p>Isi Survey Dahulu <a href="`+pathSurvey+`?redirect_link=`+mainpath+`" class="btn btn-xs btn-primary"><span class="fa fa-send"></span> &nbsp;Lakukan Survey</a></p>`;
			}
modalTandaTashih+=`
			</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>`;

$('body').append(modalTandaTashih);

$('.btn-tanda-tashih').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');

		checkrate = getRatingUs(id);

		var request = $.ajax({
			url: mainpath+'/tanda-tashih/'+id,
			type: "GET"
		});

		$("#modalTandaTashih").find(".nav-justified").html("");
		$("#modalTandaTashih").find(".tab-content").html("");

		request.done(function(msg) {
			let item    = msg.item;
			let menu    = '';
			let content = '';
			let active;
			let content_active;

			if (checkrate > 0) {
				content +=`<p>Isi Rating Dahulu</p>`;
			}else{
				$.each(item,function(key, value){
					if (key == 0) {
						active = 'active';
						content_active = 'in active';
					}else{
						active = '';
						content_active = '';
					}
	
					menu += '<li class="'+active+'"><a data-toggle="tab" href="#'+value.id+'">Ukuran '+value.ukuran+'</a></li>';
					
					content += `
						<div id="`+value.id+`" class="tab-pane fade `+content_active+`">
							<div class="row" style="margin-top:25px;">
								<div class="col-sm-6" align="center">
									<a class="btn btn-primary" href="`+value.surat_penerbitan+`" target="_blank"> <i class="fa fa-download"></i> Surat Penerbitan</a>
								</div>
								<div class="col-sm-6" align="center">
									<a class="btn btn-primary" href="`+value.scan_tanda_tashih+`" target="_blank"> <i class="fa fa-download"></i> Scan Tanda Tashih</a>
								</div>
							</div>
						</div>`;
				});
			}

			$("#modalTandaTashih").find(".nav-justified").html(menu);
			$("#modalTandaTashih").find(".tab-content").html(content);
		});

		request.fail(function(jqXHR, textStatus) {
			alert( "Request failed: " + textStatus );
		});

		$('#modalTandaTashih').modal('show');
	})
})

function getRatingUs(id_tashih) {
	isRate = 0;

	$.ajax({
		url: mainpath+'/rating-us/'+id_tashih,
		type: "GET",
		data: {
			id : id_tashih
		},
	})
	.done(function(response, text) {
		isRate = response.item.length;
	})
	.fail(function(jqXHR, textStatus) {
		alert( "Request failed: " + textStatus );
	});

	return isRate;
}

let modalLaporanPencetakan = '';
modalLaporanPencetakan += '<form id="formPernyataan" method="post" enctype="multipart/form-data" action="'+mainpath+'/laporan-pencetakan">';
modalLaporanPencetakan += '<div id="modalLaporanPencetakan" class="modal fade" role="dialog">';
modalLaporanPencetakan += '<div class="modal-dialog">';
modalLaporanPencetakan += '<div class="modal-content">';
modalLaporanPencetakan += '<div class="modal-header">';
modalLaporanPencetakan += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalLaporanPencetakan += '<h4 class="modal-title">Laporan Pencetakan</h4>';
modalLaporanPencetakan += '</div>';

modalLaporanPencetakan += '<div class="modal-body">';

modalLaporanPencetakan += `
	<div class="form-group">
		<label>Pencetakan ke</label>
		<input type="text" name="pencetakan_ke" id="pencetakan_ke" class="form-control">
	</div>
`;

modalLaporanPencetakan += '<div class="form-group">';
modalLaporanPencetakan += '<label for="tanggal_laporan">Tanggal Cetak * :</label>';
modalLaporanPencetakan += '<input type="text" required="" readonly name="tanggal_laporan" class="form-control datepicker" id="tanggal_laporan">';
modalLaporanPencetakan += '</div>';

modalLaporanPencetakan += `
	<div id="listFormLaporanPencetakan"></div>
`;
// modalLaporanPencetakan += '<div class="form-group">';
// modalLaporanPencetakan += '<label for="oplah_laporan">Oplah * :</label>';
// modalLaporanPencetakan += '<input type="text" required="" name="oplah_laporan" class="form-control" id="oplah_laporan">';
// modalLaporanPencetakan += '</div>';

modalLaporanPencetakan += '<div class="form-group">';
modalLaporanPencetakan += '<label for="nama_percetakan_laporan">Nama Percetakan * :</label>';
modalLaporanPencetakan += '<input type="text" required="" name="nama_percetakan_laporan" class="form-control" id="nama_percetakan_laporan">';
modalLaporanPencetakan += '</div>';

modalLaporanPencetakan += '<div class="form-group">';
modalLaporanPencetakan += '<label for="alamat_percetakan_laporan">Alamat Percetakan * :</label>';
modalLaporanPencetakan += '<textarea required="" name="alamat_percetakan_laporan" class="form-control" id="alamat_percetakan_laporan" style="resize:none;" rows="5"></textarea>';
modalLaporanPencetakan += '</div>';
modalLaporanPencetakan += '</div>';

modalLaporanPencetakan += '<div class="modal-footer">';
modalLaporanPencetakan += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalLaporanPencetakan += '<button type="submit" class="btn btn-primary">Submit</button>';
modalLaporanPencetakan += '</div>';
modalLaporanPencetakan += '</div>';
modalLaporanPencetakan += '</div>';
modalLaporanPencetakan += '</div>';
modalLaporanPencetakan += '<input id="id_proses_pentashihan_laporan" type="hidden" name="id_proses_pentashihan_laporan" value="">';
modalLaporanPencetakan += '<input type="hidden" name="_token" value="'+token+'">';
modalLaporanPencetakan += '</form>';
$('body').append(modalLaporanPencetakan);
$('.datepicker').datepicker({
	format:'yy-mm-dd'
});

$('.btn-laporan-pencetakan').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');
		
		htmlListOplah(id);

		$('#id_proses_pentashihan_laporan').val(id);
		$('#modalLaporanPencetakan').modal('show');
	})
});

const htmlListOplah = (id) => {
	$.ajax({
        url : mainpath + "/list-ukuran-oplah",
        method : "POST",
        data : {
			"_token" : token,
			"id_proses_pentashihan" : id,
        },
        beforeSend: function(res) {
			$("#listFormLaporanPencetakan").empty();
			$("#pencetakan_ke").val('');
        },
        complete: function (res) {
        },
        success: function(res){
			$("#pencetakan_ke").val(res.count_cetak_tashih);

			html = '';
			if (res.status == 1) {
				$.each(res.item, function (key, val) {
					html += `
					<div class="form-group">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Ukuran</label>
									<input type="text" name="ukuran_cetak[]" id="ukuran_cetak" value="`+val.ukuran+`" class="form-control" readonly>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Oplah</label>
									<input type="text" name="oplah_cetak[]" id="oplah_cetak" class="form-control price-format" required>
								</div>
							</div>
						</div>
					</div>
					`;
				});
			}

			$("#listFormLaporanPencetakan").empty().html(html);
			

			$(function () {
				$(document).find('.price-format').priceFormat({
					prefix:'', thousandsSeparator:'.', centsLimit: 0, clearOnEmpty:false
				});
			});
		},
		error: function(res){
        }
    })
}

function submitRating() {
    if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
		swal("Perhatian","Pilih Rating dulu","warning");
	}else{
		$("#form-rating").submit();
    }
}

let modalRating = '';
modalRating += '<form method="post" id="form-rating" enctype="multipart/form-data" action="'+mainpath+'/rating">';
modalRating += '<div id="modalRating" class="modal fade" role="dialog">';
modalRating += '<div class="modal-dialog">';
modalRating += '<div class="modal-content">';
modalRating += '<div class="modal-header">';
modalRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalRating += '<h4 class="modal-title">Beri Rating</h4>';
modalRating += '</div>';
modalRating += '<div class="modal-body" style="text-align:center">';


$(function () {
	$(".my-rating-9").starRating({
	  starShape: 'rounded',
	  starSize: 40,
	  initialRating: 0,
	  disableAfterRate: false,
	  emptyColor: '#DEDEDE',
	  hoverColor: '#EDBD19',
	  strokeColor: '#EDBD19',
	  activeColor: '#EDBD19',
	  disableAfterRate: false,
	  onHover: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentIndex);
		if (currentIndex < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentIndex >2 &&  currentIndex <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentIndex > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentIndex);
	  },
	  onLeave: function(currentIndex, currentRating, $el){
		// $('.live-rating').text(currentRating);
		if (currentRating < 1) {
		  $(".text-rating").text("Tidak Puas");
		}else if(currentRating >2 &&  currentRating <=3){
		  $(".text-rating").text("Kurang Puas");
		}else if(currentRating > 3){
		  $(".text-rating").text("Puas");
		}
		$("#jumlah_rating").val(currentRating);

	  }
	});
});

modalRating += '<div class="form-group">';
modalRating += '<span class="text-rating text-danger">Tidak Puas</span>';
modalRating += '<br><br>';
modalRating += '<span class="my-rating-9"></span><br>';
modalRating += '<input type="hidden" name="jumlah_rating" id="jumlah_rating" required>';
modalRating += '</div>';
modalRating += '<div class="form-group">';
modalRating += '<label>Kosongkan jika tidak ada komentar<code>*</code></label>';
modalRating += '<textarea name="komentar" class="form-control id="komentar-rating" cols="30" rows="10" placeholder="Masukkan Komentar anda"></textarea>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<div class="modal-footer">';
modalRating += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
modalRating += '<button type="button" onclick="submitRating()"  class="btn btn-primary" id="simpan_rating_fdsgdh">Submit</button>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<input type="hidden" name="_token" value="'+token+'">';
modalRating += '<input type="hidden" name="id" id="idRating">';
modalRating += '</form>';
	
$('body').append(modalRating);
$('.btn-rating').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');

		$('#idRating').val(id);
		$('#modalRating').modal('show');
	})
});


let modallokkRating = '';
modallokkRating += '<div id="modallokkRating" class="modal fade" role="dialog">';
modallokkRating += '<div class="modal-dialog">';
modallokkRating += '<div class="modal-content">';
modallokkRating += '<div class="modal-header">';
modallokkRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modallokkRating += '<h4 class="modal-title">Rating</h4>';
modallokkRating += '</div>';
modallokkRating += '<div class="modal-body">';

modallokkRating += '<ul class="nav nav-tabs nav-justified">';
modallokkRating += '</ul>';

modallokkRating += '<div class="tab-content">';
modallokkRating += '</div>';

modallokkRating += '</div>';
modallokkRating += '<div class="modal-footer">';
modallokkRating += '</div>';
modallokkRating += '</div>';
modallokkRating += '</div>';
modallokkRating += '</div>';

$('body').append(modallokkRating);
$('.btn-rating-look').each(function(){
	$(this).on('click',function(){
		let id = $(this).data('id');

		var request = $.ajax({
		  url: mainpath+'/rating-us/'+id,
		  type: "GET",
		  data: {
			  id : id
		  }
		});

		$("#modallokkRating").find(".nav-justified").html("");
		$("#modallokkRating").find(".tab-content").html("");

		request.done(function(msg) {
			let item    = msg.item;
			let menu    = '';
			let content = '';

			$.each(item,function(key, value){
				content += '<div class="form-group" style="text-align:center">';
				content += '<span class="text-rating_s text-danger">Tidak Puas</span>';
				content += '<br><br>';
				content += '<span class="my-rating-7"></span><br>';
				content += '<input type="hidden" name="jumlah_rating" id="jumlah_rating_s" value="'+value.jumlah_rating+'">';
				content += '</div>';
				content += '<div class="form-group" style="text-align:center">';
				content += '<label>Komentar </label>';
				content += '"'+value.komentar+'"';
				content += '</div>';
			})

			$("#modallokkRating").find(".nav-justified").html(menu);
			$("#modallokkRating").find(".tab-content").html(content);
			$(".my-rating-7").starRating({
				starShape: 'rounded',
				starSize: 40,
				initialRating: $("#jumlah_rating_s").val(),
				disableAfterRate: false,
				readOnly: true,
				emptyColor: '#DEDEDE',
				hoverColor: '#EDBD19',
				strokeColor: '#EDBD19',
				activeColor: '#EDBD19',
			});
			if ($("#jumlah_rating_s").val() < 1) {
				$(".text-rating_s").text("Tidak Puas");
			}else if($("#jumlah_rating_s").val() >2 &&  $("#jumlah_rating_s").val() <=3){
				$(".text-rating_s").text("Kurang Puas");
			}else if($("#jumlah_rating_s").val() > 3){
				$(".text-rating_s").text("Puas");
			}
		});

		request.fail(function(jqXHR, textStatus) {
		  alert( "Request failed: " + textStatus );
		});

		$('#modallokkRating').modal('show');
	})
});
