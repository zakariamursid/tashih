
Vue.prototype.$http = axios

new Vue({
    el: '#appMain',
    data: {
        config: {
            base: conf.base_url,
            token: conf.token,
            asset: conf.base_asset,
        },

        form: {
            type: (conf.data.type !== null ? conf.data.type : null),
            nama: (conf.data.type !== null ? conf.data.nama : null),

            type_form_pilihan: "Add",
            text_btn_pilihan: "Add",
            row_pilihan: null,
            opsi_pilihan: "",
            nama_pilihan: null,
            list_pilihan:  conf.list
        },
    },
    mounted() {
        
    },
    created() {

    },
    methods: {
        emptyPilihan() {
            this.form.type_form_pilihan = "Add";
            this.form.text_btn_pilihan = "Add";
            this.form.row_pilihan = null;
            this.form.opsi_pilihan = "";
            this.form.nama_pilihan = null;
        },
        addPilihan() {
            
            if (this.form.opsi_pilihan != null && this.form.nama_pilihan != null) {
                let data = {
                    pilihan_id: null,
                    opsi: this.form.opsi_pilihan,
                    nama: this.form.nama_pilihan,
                }

                if (this.findTypeSame(this.form.opsi_pilihan) == true) {
                    swal('Opsi Sama', 'Opsi sudah dipilih !', 'warning');
                    this.emptyPilihan();
                } else {
                    this.form.list_pilihan.push(data);
                    this.emptyPilihan();
                }
            } else {
                swal('Kolom masih kosong', 'Kolom Opsi dan nama plihan belum diisi !', 'warning');
            }
        },
        editRowPilihan(row, i){
            this.form.type_form_pilihan = "Edit";
            this.form.text_btn_pilihan = "Save";
            this.form.row_pilihan = i;
            this.form.opsi_pilihan = row.opsi;
            this.form.nama_pilihan = row.nama;
        },
        editPilihan() {
            let form = this.form;
            if (form.opsi_pilihan != null && form.nama_pilihan != null) {
                // if (this.findTypeSame(this.form.opsi_pilihan) == true) {
                //     swal('Opsi Sama', 'Opsi sudah dipilih !', 'warning');
                //     this.emptyPilihan();
                // } else {
                    form.list_pilihan[form.row_pilihan].opsi = form.opsi_pilihan;
                    form.list_pilihan[form.row_pilihan].nama = form.nama_pilihan;
                    this.emptyPilihan();
                // }
            } else {
                swal('Kolom masih kosong', 'Kolom Opsi dan nama plihan belum diisi !', 'warning');
            }
        },
        deletePilihan(i) {
            let msg = confirm('Apakah anda yakin ingin menghapus data ini ?');
            if (msg) {
                this.form.list_pilihan.splice(i, 1);
            }
        },
        findTypeSame(type) {
            let list = this.form.list_pilihan;
            if (list.length > 0) {
                for (let i = 0; i < list.length; i++) {
                    let opsi = list[i].opsi;
                    if (opsi === type) {
                        return true;
                        break;
                    }
                }
            }

            return false;
        }
    }
});