
Vue.prototype.$http = axios

Vue.component('vue-pagination', {
    data: function () {
        return {
            
        }
    },
    props: {
        pagination: {
            type: Object,
            required: true
        },
        offset: {
            type: Number,
            default: 4
        }
    },
    computed: {
        pagesNumber() {
            if (!this.pagination.to) {
                return [];
            }
            let from = this.pagination.current_page - this.offset;
            if (from < 1) {
                from = 1;
            }
            let to = from + (this.offset * 2);
            if (to >= this.pagination.last_page) {
                to = this.pagination.last_page;
            }
            let pagesArray = [];
            for (let page = from; page <= to; page++) {
                pagesArray.push(page);
            }
            return pagesArray;
        }
    },
    methods : {
        changePage(page) {
            this.pagination.current_page = page;
            this.$emit('paginate');
        },
    },
    template: `
    <nav class="paging text-center">
        <label class="pagination-label">Showing {{ pagination.from }} from {{ pagination.total }} results</label>
        <ul class="pagination">
            <li class="page-item" :class="pagination.current_page < pagination.last_page && pagination.current_page !== pagination.last_page || pagination.current_page === 1 ? 'disabled' : ''">
                <a v-on:click.prevent="changePage(pagination.current_page - 1)" class="page-link" href="#" aria-label="prev">
                    <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                    <span class="sr-only">prev</span>
                </a>
            </li>
            <li v-for="page in pagesNumber" :class="{'active': page == pagination.current_page}" class="page-item">
                <a v-on:click.prevent="changePage(page)" class="page-link" href="#">{{ page }}</a>
            </li>
            <li :class="pagination.current_page >= pagination.last_page && pagination.current_page === pagination.last_page ? 'disabled' : ''" class="page-item">
                <a v-on:click.prevent="changePage(pagination.current_page + 1)" class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </nav>
    `
})

new Vue({
    el: '#appForm',
    data: {
        title : null,
        caption : null,
        data: {
            data: [],
            total: 0,
            per_page: 6,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
        search: null,
        base_url: conf.base_url,
        base_asset: conf.base_asset,
    },
    mounted() {
        
    },
    created() {
        this.listData();
    },
    methods: {
        listData() {
            this.$http.post( conf.base_url + '/list-data' , {
                _token : conf.token,
                page : this.data.current_page,
                totalPage : this.data.total,
                totalRecord : this.data.per_page,
                search: this.search,
            })
            .then(res => {
                getData = res.data;

                // console.log(getData.item);
                if (getData.status == 1) {
                    this.data = getData.item;                  
                }
            })
            .catch(res => {
                console.log(res);
            })
        },
        searchInput() {
            vm = this;

            //setup width and length
            var typingTimer;
            var doneTypingInterval = 1500;

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                console.log(vm.search);

                vm.listData();
            }, doneTypingInterval);
        },
    }
});