let modalRating = '';
modalRating += '<form method="post" id="form-rating" enctype="multipart/form-data" action="'+mainpath+'/beri-rating">';
modalRating += '<div id="modalRating" class="modal fade" role="dialog">';
modalRating += '<div class="modal-dialog">';
modalRating += '<div class="modal-content">';
modalRating += '<div class="modal-header">';
modalRating += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
modalRating += '<h4 class="modal-title">Beri Rating</h4>';
modalRating += '</div>';
modalRating += '<div class="modal-body" style="text-align:center">';
modalRating += '<div class="form-group">';
modalRating += '<span class="text-rating text-danger">Tidak Puas</span>';
modalRating += '<br><br>';
modalRating += '<span class="my-rating-9"></span><br>';
modalRating += '<input type="hidden" name="jumlah_rating" id="jumlah_rating" required>';
modalRating += '</div>';

modalRating += '</div>';
modalRating += '<div class="modal-footer">';
modalRating += '<button type="button" class="btn btn-default btn-hide-satu" data-dismiss="modal">Close</button>';
modalRating += '<button type="button" onclick="submitRating()"  class="btn btn-primary btn-hide-satu" id="simpan_rating_fdsgdh">Submit</button>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '</div>';
modalRating += '<input type="hidden" name="_token" value="'+token+'">';
modalRating += '<input type="hidden" name="id" id="idRating">';
modalRating += '</form>';

$('body').append(modalRating);
$('.btn-rating').each(function(){
	$(this).on('click',function(){
	    $(".btn-hide-satu").show();
		let id = $(this).data('id');

		$('#idRating').val(id);
		
		$.ajax({
		    url:mainpath+"/cari-rating/"+id,
		    method:"GET",
		    data:{
		        "id":id,
		    },
		    success:function(data){
		        if(data.rating!=""){
		            var currentIndex = data.rating;
		            $(".my-rating-9").starRating({
                    	  starShape: 'rounded',
                    	  starSize: 40,
                    	  initialRating: data.rating,
                    	  disableAfterRate: true,
                    	  emptyColor: '#DEDEDE',
                    	  hoverColor: '#EDBD19',
                    	  strokeColor: '#EDBD19',
                    	  activeColor: '#EDBD19',
                    	  disableAfterRate: true,
		            });
		            if (currentIndex < 1) {
            		  $(".text-rating").text("Tidak Puas");
            		}else if(currentIndex >2 &&  currentIndex <=3){
            		  $(".text-rating").text("Kurang Puas");
            		}else if(currentIndex > 3){
            		  $(".text-rating").text("Puas");
            		}
		            $(".btn-hide-satu").hide();
		        }else{
                	$(".my-rating-9").starRating({
                    	  starShape: 'rounded',
                    	  starSize: 40,
                    	  initialRating: 0,
                    	  disableAfterRate: false,
                    	  emptyColor: '#DEDEDE',
                    	  hoverColor: '#EDBD19',
                    	  strokeColor: '#EDBD19',
                    	  activeColor: '#EDBD19',
                    	  disableAfterRate: false,
                    	  onHover: function(currentIndex, currentRating, $el){
                    		// $('.live-rating').text(currentIndex);
                    		if (currentIndex < 1) {
                    		  $(".text-rating").text("Tidak Puas");
                    		}else if(currentIndex >2 &&  currentIndex <=3){
                    		  $(".text-rating").text("Kurang Puas");
                    		}else if(currentIndex > 3){
                    		  $(".text-rating").text("Puas");
                    		}
                    		$("#jumlah_rating").val(currentIndex);
                    	  },
                    	  onLeave: function(currentIndex, currentRating, $el){
                    		// $('.live-rating').text(currentRating);
                    		if (currentRating < 1) {
                    		  $(".text-rating").text("Tidak Puas");
                    		}else if(currentRating >2 &&  currentRating <=3){
                    		  $(".text-rating").text("Kurang Puas");
                    		}else if(currentRating > 3){
                    		  $(".text-rating").text("Puas");
                    		}
                    		$("#jumlah_rating").val(currentRating);
                    
                    	  }
                    	});
		        }
		    },
		    error:function(data){
		        alert(data);
		    }
		})
		$('#modalRating').modal('show');
	})
});

 function submitRating() {
	if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
		swal("Perhatian","Pilih Rating dulu","warning");
			// $(".text-rating").text("Pilih Rating dulu");
	  }else{
		  $("#form-rating").submit();
	  }
  }
  
$(".btn-beri-rating").click(function(){
   alert("asd"); 
});