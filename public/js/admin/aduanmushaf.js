/**
 * ADUAN MODALS ACTION
 */
	let loading = '';
	loading += '<div id="loading" class="sk-fading-circle">';
	loading += '<div class="sk-circle1 sk-circle"></div>';
	loading += '<div class="sk-circle2 sk-circle"></div>';
	loading += '<div class="sk-circle3 sk-circle"></div>';
	loading += '<div class="sk-circle4 sk-circle"></div>';
	loading += '<div class="sk-circle5 sk-circle"></div>';
	loading += '<div class="sk-circle6 sk-circle"></div>';
	loading += '<div class="sk-circle7 sk-circle"></div>';
	loading += '<div class="sk-circle8 sk-circle"></div>';
	loading += '<div class="sk-circle9 sk-circle"></div>';
	loading += '<div class="sk-circle10 sk-circle"></div>';
	loading += '<div class="sk-circle11 sk-circle"></div>';
	loading += '<div class="sk-circle12 sk-circle"></div>';
	loading += '</div>';

	let modalAduan = '';
	modalAduan += '<form method="post" enctype="multipart/form-data" action="'+mainpath+'/tindak-lanjut">';
	modalAduan += '<div id="modalAduan" class="modal fade" role="dialog">';
	modalAduan += '<div class="modal-dialog">';
	modalAduan += '<div class="modal-content">';
	modalAduan += '<div class="modal-header">';
	modalAduan += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
	modalAduan += '<h4 class="modal-title">Tindak Lanjut</h4>';
	modalAduan += '</div>';
	modalAduan += '<div class="modal-body">';
	modalAduan += loading;
	modalAduan += '<div class="form-group">';
	modalAduan += '<label for="fileAduan">File:</label>';
	modalAduan += '<embed width="100%" height="100%" name="plugin" class="embed" src="">';
	modalAduan += '<input type="file" name="file" class="form-control" id="fileAduan" required="">';
	modalAduan += '<p class="help"><i>*Abaikan jika tidak ingin diganti</i></p>';
	modalAduan += '</div>';
	modalAduan += '<div class="row">';
	modalAduan += '<div class="form-group">';
	modalAduan += '<label class="radio-inline">';
	modalAduan += '<input type="checkbox" name="kirim_pelapor" value="Kirim Pelapor" checked="true">Kirim Ke Pelapor';
	modalAduan += '</label>'
	modalAduan += '</div>';
	modalAduan += '</div>';
	modalAduan += '</div>';
	modalAduan += '<div class="modal-footer">';
	modalAduan += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
	modalAduan += '<button id="submitAduan" type="submit" class="btn btn-primary">Submit</button>';
	modalAduan += '</div>';
	modalAduan += '</div>';
	modalAduan += '</div>';
	modalAduan += '</div>';
	modalAduan += '<input type="hidden" name="_token" value="'+token+'">';
	modalAduan += '<input type="text" name="id" id="idAduan">';
	modalAduan += '</form>';

	$('body').append(modalAduan);

	$('.btn-tindak-lanjut').each(function(){
		$(this).on('click',function(){
			let id = $(this).data('id');
			$("#idAduan").val(id);

			$("#modalAduan").find("#loading").show();

			var request = $.ajax({
			  url: mainpath+'/load-tindak-lanjut',
			  type: "GET",
			  data: {
			  	id : id
			  }
			});

			request.done(function(msg) {
				$('#modalAduan').modal('show');

				let file    = msg.file;
				let status  = msg.api_status;
				let message = msg.api_message;

				if (status === 1) {
					let embed = $('#modalAduan').find('.embed');

					if (file == '') {
						embed.hide();
					}else{
						embed.show();
					}
					let newEmbed = '<embed width="100%" height="100%" name="plugin" class="embed" src="'+file+'">';
				  	embed.replaceWith(newEmbed);

					if (file == '') {
						$('#fileAduan').attr('required',true);
						$('embed').hide()
					}else{
						$('#fileAduan').attr('required',false);
						$('embed').show()
					}

					$('#fileAduan').on('change',function(){
						let embed = $('#modalAduan').find('.embed');
						let input = this;
						let url   = $(this).val();
						let ext   = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

						if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg" || ext == "pdf")){
							let reader = new FileReader();

							reader.onload = function (e) {
								let result = e.target.result;
								let newEmbed = '<embed width="100%" height="100%" name="plugin" class="embed" src="'+result+'">';
								embed.replaceWith(newEmbed);
							}
							reader.readAsDataURL(input.files[0]);
						}else{
							if (file == '') {
								embed.hide(500)
							}else{
								embed.show();
								embed.attr('src', file);
							}
						}
					})

				}else{
					$('#modalDisposisi').modal('hide');
					alert('Error : data tidak ditemukan');
				}
			})
		})
	})