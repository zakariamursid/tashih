<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


CRUDBooster::routeController('/', 'FrontBerandaController');

CRUDBooster::routeController('auth', 'FrontAuthController');

// Route::get('cek-tanda-tashih', function () {
//    return view('front/cektandatashih');
// });

Route::get('pengajuan-tanda-tashih', function () {
   return redirect('pengajuan-tashih');
});

Route::get('pelaporan-mushaf-bermasalah', function () {
   return redirect('aduan-mushaf-bermasalah');
});

CRUDBooster::routeController('persyaratan-pengajuan-tashih', 'FrontPersyaratanPengajuanTashihController');
CRUDBooster::routeController('petunjuk-teknis-pencetakan', 'FrontPetunjukTeknisPencetakanController');
CRUDBooster::routeController('pendaftaran-penerbit', 'FrontPendaftaranPenerbitController');
CRUDBooster::routeController('regulasi', 'FrontRegulasiController');
CRUDBooster::routeController('download-materi', 'FrontDownloadMateriController');
Route::get('informasi/{key}', 'FrontInformasiController@getIndex');
CRUDBooster::routeController('info-seputar-lajnah', 'FrontInfoSeputarLajnahController');
// Route::get('info-seputar-lajnah/{key}', 'FrontInfoSeputarLajnahController@getDetail');

CRUDBooster::routeController('info-layanan-pentashihan', 'FrontGalleryMushafAlquranController');
// Route::get('info-layanan-pentashihan/{key}', 'FrontGalleryMushafAlquranController@getDetail');

CRUDBooster::routeController('banner-layanan', 'FrontBannerLayananController');

CRUDBooster::routeController('aduan-mushaf-bermasalah', 'FrontPelaporanMushafBermasalahController');
CRUDBooster::routeController('pengajuan-tashih', 'FrontPengajuanTandaTashihController');
CRUDBooster::routeController('forum', 'FrontForumController');

// CRUDBooster::routeController('faq', 'FrontFaqControl');
CRUDBooster::routeController('faq', 'FrontListFaqControl');

CRUDBooster::routeController('cek-tanda-tashih', 'FrontCekTandaTashihController');
Route::get('info-penerbit', 'FrontCekTandaTashihController@getInfo');

CRUDBooster::routeController('list-siaran-pers', 'FrontListSiaranPersController');
// Route::get('list-siaran-pers/{key}', 'FrontListSiaranPersController@getDetail');

Route::get('revisi','FrontRevisiController@getIndex');
Route::post('postrating','FrontBerandaController@saveRating');
CRUDBooster::routeController('survey','FrontSurveyController');
Route::get('list-file/{id}','FrontDownloadMateriController@getKoreksiTashih');
Route::get('download-list-file/{id}','FrontDownloadMateriController@getDownloadTansih');

CRUDBooster::routeController('search', 'FrontSearchController');

Route::post('api/scan-qrcode', 'FrontCekTandaTashihController@postApiScan');