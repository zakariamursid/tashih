<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('list_city','CustomApiController@getListCity');
Route::get('list_district','CustomApiController@getListDistrict');
Route::get('rekap_pentashihan','CustomApiController@getRekapPentashihan');
Route::get('mushaf_lama','CustomApiController@getMushafLama');