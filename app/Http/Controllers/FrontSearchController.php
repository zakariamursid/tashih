<?php

namespace App\Http\Controllers;

use App\Services\BannerLayananServices;
use App\Services\ListSiaranPersServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Helpers\Kemenag;
use App\Repositories\ListSiaranPersRepositories;

class FrontSearchController extends Controller
{
    function getIndex() {
        $search = g('text_search');
        $type = g('type_search');

        $result['banner_layanan'] = BannerLayananServices::listBannerLayanan();
        $result['info_penerbit'] = ListSiaranPersServices::searchInfoPenerbit($search);
        $result['siaran_pers'] = ListSiaranPersServices::searchListSiaranPers($search);
        $result['info_layanan'] = ListSiaranPersServices::searchListInfoLayananTashih($search);
        $result['info_seputar'] = ListSiaranPersServices::searchListInfoSeputarLajnah($search);

        return view('front.searchcategory', $result);
    }

    function postCategory() {
        $search = g('text_search');
        $type = g('type_search');

        if (in_array($type,  ['Umum'])) {
            $session = 'all';
        }elseif (in_array($type, ['Info Seputar Lajnah'])) {
            $session = 'info_seputar';
        }elseif (in_array($type, ['Info Layanan Pentashihan'])) {
            $session = 'info_layanan';
        }elseif (in_array($type, ['Siaran Pers'])) {
            $session = 'siaran_pers';
        }elseif (in_array($type, ['Informasi Penerbit'])) {
            $session = 'info_penerbit';
        }

        Session::put('search_text', $search);
        Session::put('type_search', $session);

        $get = Kemenag::gSession('type_search');
        $text = Kemenag::gSession('search_text');

        $result['banner_layanan'] = BannerLayananServices::listBannerLayanan();
        if (in_array($get, ['all', 'info_penerbit'])) {
            $result['info_penerbit'] = ListSiaranPersServices::searchInfoPenerbit($text);
        }
        if (in_array($get, ['all', 'siaran_pers'])) {
            $result['siaran_pers'] = ListSiaranPersServices::searchListSiaranPers($text);
        }
        if (in_array($get, ['all', 'info_layanan'])) {
            $result['info_layanan'] = ListSiaranPersServices::searchListInfoLayananTashih($text);
        }
        if (in_array($get, ['all', 'info_seputar'])) {
            $result['info_seputar'] = ListSiaranPersServices::searchListInfoSeputarLajnah($text);
        }
        $result['search_text'] = $text;
        $result['type_search'] = $type;

        return view('front.searchcategory', $result);
    }
}
