<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\BannerLayananServices;

class FrontBannerLayananController extends Controller
{
    public function getRead($slug)
    {
        $result['detail'] = BannerLayananServices::findBannerLayanan($slug);
        $result['random'] = BannerLayananServices::listRandomBannerLayanan($slug);

        return view('front.bannerlayanandetail', $result);
    }
}
