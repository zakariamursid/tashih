<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontAuthController extends Controller
{
	public function postContactUs(){
		if(empty(Request::input('g-recaptcha-response'))){
			return redirect()->back()->with([
				'auth_message'=>'Please check the the captcha form.'
			]);
		}else{
			$email = CRUDBooster::getSetting('email_contact_us');

			$data_email['nama']  = Request::input('nama');
			$data_email['email'] = Request::input('email');
			$data_email['pesan'] = Request::input('pesan');
			CRUDBooster::sendEmail(['to'=>$email,'data'=>$data_email,'template'=>'contact_us']);
			DB::table('contact_us')->insert($data_email);

			return redirect()->back()->with([
				'auth_message'=>'Terima kasih, pesan anda sudah kami terima'
			]);
		}
	}

	public function postLogin(){
		
	}

	public function postForgotPassword(){
		$email = Request::input('email');

		$check = DB::table('cms_users')
			->where('email',$email)
			->first();

		$generatePass = Kemenag::generatePassword(6);

//		$value_pass = ($check?$check->password_value:null);
//		if ($value_pass) {
//			$new_pass = $value_pass;
//		}else{
//			$new_pass = $generatePass;
//		}

		$data_email['id']       = $check->id;
		$data_email['id_user']  = $check->id_penerbit;
		$data_email['name']     = $check->name;
//        $data_email['password'] = $check->password_value;
        $data_email['password'] = $generatePass;
		CRUDBooster::sendEmail(['to'=>$email,'data'=>$data_email,'template'=>'forgot_password_penerbit']);

		if (empty($check)) {
			$message = 'Akun anda belum terdaftar';
		}else{
			if ($check->status != 'Aktif') {
				if ($check->status == 'Belum Aktif') {
					$message = 'Akun anda belum aktif';
				}else{
					$message = 'Akun anda telah dibekukan oleh admin';
				}
			}else{
		
				DB::table('cms_users')
					->where('id', $check->id)
					->update([
						'password_value' => $generatePass,
						'password' => Hash::make($generatePass),
					]);

				$data_email['id']       = $check->id;
				$data_email['id_user']  = $check->id_penerbit;
				$data_email['name']     = $check->name;
//				$data_email['password'] = $check->password_value;
                $data_email['password'] = $generatePass;
                CRUDBooster::sendEmail(['to'=>$email,'data'=>$data_email,'template'=>'forgot_password_penerbit']);

				$message = 'Password telah kami kirimkan ke email anda';
			}
		}

		return redirect()->back()->with([
				'auth_message'=>$message
			]);
	}
}