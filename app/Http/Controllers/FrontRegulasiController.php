<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontRegulasiController extends Controller
{
	public function getIndex(){
		
		Kemenag::menu('pentashihan');
		Kemenag::menuSub('regulasi');

		$regulasi = DB::table('regulasi')
			->whereNull('deleted_at')
			->orderBy('tahun','DESC')
			->orderBy('nomor','DESC')
			->get();

		$data['regulasi'] = $regulasi;

		return view('front/regulasi',$data);
	}

}