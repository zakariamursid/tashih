<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminLaporanController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function getIndex(){
			$total_penerbit = DB::table('cms_users')
				->select(DB::raw('YEAR(created_at) year'))
				->where('id_cms_privileges',2)
				->groupBy('year')
				->orderBy('year','DESC')
				->limit(10)
				->get();
			foreach ($total_penerbit as $row) {
				$jumlah_penerbit_pertahun = DB::table('cms_users')
					->whereYear('created_at',$row->year)
					->where('id_cms_privileges',2)
					->count();

				$row->total = $jumlah_penerbit_pertahun; 
			}

			$total_mushaf = DB::table('proses_pentashihan')
				->select(DB::raw('YEAR(created_at) year'))
				->groupBy('year')
				->orderBy('year','DESC')
				->whereNull('deleted_at')
				->limit(10)
				->get();
			foreach ($total_mushaf as $row) {
				$jumlah_mushaf_pertahun = DB::table('proses_pentashihan')
					->whereYear('created_at',$row->year)
					->whereNull('deleted_at')
					->count();

				$row->total = $jumlah_mushaf_pertahun; 
			}

			$total_rekap = DB::table('proses_pentashihan_ukuran')
				->select(DB::raw('YEAR(created_at) year'))
				->groupBy('year')
				->orderBy('year','DESC')
				->whereNotNull('nomor')
				->limit(10)
				->get();
			foreach ($total_rekap as $row) {
				$jumlah_rekap_pertahun = DB::table('proses_pentashihan_ukuran')
					->whereYear('created_at',$row->year)
					->whereNotNull('nomor')
					->count();

				$row->total = $jumlah_rekap_pertahun; 
			}
			
			$proses_pentashihan_ukuran = DB::table('proses_pentashihan_ukuran')
				->select(DB::raw('YEAR(proses_pentashihan.created_at) year'))
				->join("proses_pentashihan","proses_pentashihan.id","=","proses_pentashihan_ukuran.id_proses_pentashihan")
				->groupBy('year')
				->orderBy('year','DESC')
				->get();
			foreach ($proses_pentashihan_ukuran as $ukur) {
				$proses_pentashihan_ukur = DB::table('proses_pentashihan_ukuran')
				    ->select(DB::raw('SUM(proses_pentashihan_ukuran.oplah) as semua_oplah'))
    				->join("proses_pentashihan","proses_pentashihan.id","=","proses_pentashihan_ukuran.id_proses_pentashihan")
					->whereYear('proses_pentashihan.created_at',$ukur->year)
					->first();

				$ukur->total = $proses_pentashihan_ukur->semua_oplah; 
			}

			$total_aduan = DB::table('laporan_mushaf_bermasalah')
				->select(DB::raw('YEAR(created_at) year'))
				->groupBy('year')
				->orderBy('year','DESC')
				->whereNull('deleted_at')
				->limit(10)
				->get();
			foreach ($total_aduan as $row) {
				$jumlah_aduan_pertahun = DB::table('laporan_mushaf_bermasalah')
					->whereYear('created_at',$row->year)
					->whereNull('deleted_at')
					->count();

				$row->total = $jumlah_aduan_pertahun; 
			}
			
			$total_penerbit_ditolak = DB::table('cms_users')
				->select(DB::raw('YEAR(created_at) year'))
				->where('id_cms_privileges',2)
				->where("status","Dibekukan")
				->groupBy('year')
				->orderBy('year','DESC')
				->get();
			foreach ($total_penerbit_ditolak as $data) {
				$jumlah_penerbit_ditolak = DB::table('cms_users')
					->whereYear('created_at',$data->year)
					->where('id_cms_privileges',2)
					->count();

				$data->total = $jumlah_penerbit_ditolak; 
			}

			$arr                   = [];
			$arr['page_title']     = 'Laporan';
			$arr['total_penerbit'] = $total_penerbit;
			$arr['total_penerbit_ditolak'] = $total_penerbit_ditolak;
			$arr['total_mushaf']   = $total_mushaf;
			$arr['total_aduan']    = $total_aduan;
			$arr['total_rekap']    = $total_rekap;
			$arr['total_oplah']    = $proses_pentashihan_ukuran;

			return view('admin/laporan',$arr);
		}

	    public function cbInit() {
	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "laporan_mushaf_bermasalah";	        
			$this->title_field         = "nama_pelapor";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = TRUE;
			$this->button_delete       = TRUE;
			$this->button_edit         = TRUE;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
			$this->col[] = array("label"=>"Nama Pelapor","name"=>"nama_pelapor" );
			$this->col[] = array("label"=>"Email Pelapor","name"=>"email_pelapor" );
			$this->col[] = array("label"=>"Phone Pelapor","name"=>"phone_pelapor" );
			$this->col[] = array("label"=>"Pekerjaan Pelapor","name"=>"pekerjaan_pelapor" );
			# END COLUMNS DO NOT REMOVE THIS LINE
			
			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ["label"=>"Nama Pelapor","name"=>"nama_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Email Pelapor","name"=>"email_pelapor","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Phone Pelapor","name"=>"phone_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Pekerjaan Pelapor","name"=>"pekerjaan_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Alamat Pelapor","name"=>"alamat_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Nama Penerbit","name"=>"nama_penerbit","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Alamat Penerbit","name"=>"alamat_penerbit","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Judul Mushaf","name"=>"judul_mushaf","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Scan Cover Mushaf","name"=>"scan_cover_mushaf","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Scan Surat Tanda Tashih","name"=>"scan_surat_tanda_tashih","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"No Surat Tanda Tashih","name"=>"no_surat_tanda_tashih","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Tindak Lanjut","name"=>"tindak_lanjut","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}