<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminRekapTandaTashihController extends \crocodicstudio\crudbooster\controllers\CBController {


		function findColor($status) {
			switch ($status) {
				case 'Registrasi Berhasil':
					$color = 'btn-success';
					break;

				case 'Naskah Diterima':
					$color = 'btn-warning';
					break;

				case 'Tidak Lolos Verifikasi':
					$color = 'btn-default';
					break;

				case 'Lolos Verifikasi':
					$color = 'btn-success';
					break;

				case 'Proses Perbaikan Naskah':
					$color = 'btn-warning';
					break;

				case 'Pentashihan Tahap Selanjutnya':
					$color = 'btn-primary';
					break;

				case 'Selesai':
					$color = 'btn-success';
					break;
				
				default:
					$color = 'btn-default';
					break;
			}

			return $color;
		}

		function findProsesPentashihan($id) {
			return DB::table('proses_pentashihan')
				->whereNull('deleted_at')
				->where('id', $id)
				->first();
		}

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = false;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "proses_pentashihan_ukuran";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"No. Registrasi","name"=>"id_proses_pentashihan","join"=>"proses_pentashihan,nomor_registrasi"];
			$this->col[] = ["label"=>"Nama Mushaf","name"=>"id_proses_pentashihan","join"=>"proses_pentashihan,nama_produk"];
			$this->col[] = ["label"=>"Nama penerbit","name"=>"id_proses_pentashihan","callback" => function ($row)
			{
				$id = $row->id_proses_pentashihan;
				$find = $this->findUsers($id);

				return $find->name;
			}];
			$this->col[] = ["label"=>"Nomor Tanda Tashih","name"=>"nomor"];
			$this->col[] = ["label"=>"Kode Tanda Tashih","name"=>"kode"];
            $this->col[] = ["label"=>"Ukuran","name"=>"ukuran"];
            $this->col[] = ["label"=>"Tanggal Penetapan","name"=>"tanggal_penetapan", "callback" => function ($row) {
                $date = $row->tanggal_penetapan;
                return ($date?date('d M Y', strtotime($date)):'');
            }];
            $this->col[] = ['label'=>'Penerbitan Tanda Tashih','name'=>'surat_penerbitan', 'callback' => function ($row) {
                $file = asset($row->surat_penerbitan);
                return ($file?"<a href='".$file."' target='_blank' class='btn btn-xs btn-danger'><i class='fa fa-eye'></i> Lihat File</a>":'-');
            }];
            $this->col[] = ['label'=>'Tanda Tashih ','name'=>'scan_tanda_tashih', 'callback' => function ($row) {
                $file = asset($row->scan_tanda_tashih);
                return ($file?"<a href='".$file."' target='_blank' class='btn btn-xs btn-primary'><i class='fa fa-eye'></i> Lihat File</a>":'-');
            }];
//			$this->col[] = ["label"=>"Rencana Oplah","name"=>"oplah"];
			// $this->col[] = ["label"=>"Status","name"=>"id_proses_pentashihan", "callback" => function ($row)
			// {
			// 	$find = $this->findProsesPentashihan($row->id_proses_pentashihan);
			// 	$status = ($find?$find->status:null);

			// 	$color = $this->findColor($status);
                
            //     if($status=="Lolos Verifikasi"){
            //         $text_status = 'Lolos Verifikasi (Proses Pentashihan)';
            //     }else{
            //         $text_status = $status;   
            //     }

			// 	$html = '
			// 		<div class="btn-group">
			// 			<button type="button" class="btn '.$color.' btn-xs">'.$text_status.'</button>
			// 		</div>';

            //     return $html;
			// }];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Proses Pentashihan','name'=>'id_proses_pentashihan','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'proses_pentashihan,nama_produk'];
			$this->form[] = ['label'=>'Ukuran','name'=>'ukuran','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Oplah','name'=>'oplah','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Nomor','name'=>'nomor','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Kode','name'=>'kode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Tanggal Penetapan','name'=>'tanggal_penetapan','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Surat Penerbitan','name'=>'surat_penerbitan','type'=>'upload','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Scan Tanda Tashih','name'=>'scan_tanda_tashih','type'=>'upload','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Proses Pentashihan","name"=>"id_proses_pentashihan","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"proses_pentashihan,nama_produk"];
			//$this->form[] = ["label"=>"Ukuran","name"=>"ukuran","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Oplah","name"=>"oplah","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Nomor","name"=>"nomor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Kode","name"=>"kode","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Tanggal Penetapan","name"=>"tanggal_penetapan","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
			//$this->form[] = ["label"=>"Surat Penerbitan","name"=>"surat_penerbitan","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Scan Tanda Tashih","name"=>"scan_tanda_tashih","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
			$query->whereNull("proses_pentashihan.deleted_at")
			->where("proses_pentashihan.status", "Selesai");
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

		public function findUsers($id_proses_tashih)
		{
			return DB::table('proses_pentashihan')
				->select('cms_users.name')
				->join('cms_users', 'cms_users.id', '=', 'proses_pentashihan.id_cms_users')
				->whereNull('proses_pentashihan.deleted_at')
				->where('proses_pentashihan.id', $id_proses_tashih)
				->first();
		}
	}