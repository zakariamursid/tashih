<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontPelaporanMushafBermasalahController extends Controller
{
	public function getIndex(){
		Kemenag::menu('pelaporanmushafbermasalah');

		$jenis_kesalahan = DB::table('m_jenis_kesalahan')
			->whereNull('deleted_at')
			->orderBy('name','ASC')
			->get();

		$result['jenis_kesalahan'] = $jenis_kesalahan;

		return view('front/pelaporanmushafbermasalah',$result);
	}

	public function postSave(){
		$valid = Validator::make(Request::all(),[
			'scan_cover_mushaf'=>'required|mimes:jpeg,bmp,png,gif,svg,pdf',
			'scan_surat_tanda_tashih'=>'required|mimes:jpeg,bmp,png,gif,svg,pdf',
		]);
		
		if($valid->fails()){
			return redirect()->back()->with([
				'message' => $valid->errors()->first(),
				'message_type' => 'danger'
			])->withInput();
		} else {
			$now                  = Kemenag::now();
			$request              = Request::all();
			$halaman              = Request::input('halaman');
			$gambar_halaman       = Request::file('gambar_halaman');
			$id_m_jenis_kesalahan = Request::input('jenis_kesalahan');
			$desc_masalah         = Request::input('deskripsi_kesalahan');
			$total_document       = count($halaman);
	
			$save['created_at'] = $now;
			$save['updated_at'] = $now;
			
			foreach ($request as $key => $value) {
				if ($key == 'halaman' || $key == 'gambar_halaman' || $key == 'jenis_kesalahan' || $key == 'deskripsi_kesalahan' || $key == '_token') continue;
	
				if ($key == 'scan_cover_mushaf' || $key == 'scan_surat_tanda_tashih') { //jika ini file
					$filename = Kemenag::uploadFile($key,'laporan_mushaf_bermasalah',true);
	
					$save[$key] = $filename;
				}else{
					$save[$key] = $value;
				}
			}
	
			$id = DB::table('laporan_mushaf_bermasalah')->insertGetId($save);
	
			for ($i=0; $i < $total_document ; $i++) { 
				$save_detail['created_at']                   = $now;
				$save_detail['updated_at']                   = $now;
				$save_detail['id_laporan_mushaf_bermasalah'] = $id;
				$save_detail['halaman']                      = $halaman[$i];
				$save_detail['gambar']                       = Kemenag::uploadFileMultiple($i,'gambar_halaman','laporan_mushaf_bermasalah',true);
				$save_detail['id_m_jenis_kesalahan']         = $id_m_jenis_kesalahan[$i];
				$save_detail['desc_masalah']		         = $desc_masalah[$i];
	
				DB::table('laporan_mushaf_bermasalah_detail')->insert($save_detail);
			}
	
			if ($id) {
				$cms_users = DB::table('cms_users')
					->where('id_cms_privileges','!=',2)
					->pluck('id');
				$config['content']      = "Aduan Mushaf Bermasalah";
				$config['to']           = CRUDBooster::adminPath('laporan-mushaf-bermasalah?id='.$id);
				$config['id_cms_users'] = $cms_users; //This is an array of id users
				CRUDBooster::sendNotification($config);
	
				$data_email['name'] = Request::input('nama_pelapor');
				CRUDBooster::sendEmail(['to'=>Request::input('email_pelapor'),'data'=>$data_email,'template'=>'aduan_tashih_bermasalah']);
	
				return redirect()->back()->with([
					'message'=>'Terimakasih telah mengirimkan laporan anda',
					'message_type'=>'success'
				])->withInput();
			}else{
				return redirect()->back()->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				])->withInput();
			}
		}
	}
}