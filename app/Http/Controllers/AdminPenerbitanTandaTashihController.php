<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;

	use App\Repositories\SurveyRepositories;
	use App\Repositories\SurveyJawabanRepositories;
	use App\Services\RevisiSurvey\SurveyAnswerServices;

	class AdminPenerbitanTandaTashihController extends \crocodicstudio\crudbooster\controllers\CBController {

		// public function postPernyataan(){
		// 	$data = Request::all();

		// 	$save['created_at'] = Kemenag::now();
		// 	foreach ($data as $key => $value) {
		// 		if($key == '_token') continue;
		// 		$save[$key] = $value;
		// 	}

		// 	$check = DB::table('proses_pentashihan_surat_pernyataan')
		// 		->where('id_proses_pentashihan',Request::input('id_proses_pentashihan'))
		// 		->first();

		// 	if (empty($check)) {
		// 		$act = DB::table('proses_pentashihan_surat_pernyataan')->insert($save);

		// 		if ($act) {
		// 			$proses_pentashihan = DB::table('proses_pentashihan')
		// 				->where('id',Request::input('id_proses_pentashihan'))
		// 				->first();
		// 			$message = 'Konfirmasi Pernyataan - '.$proses_pentashihan->nomor_registrasi;
		// 			$cms_users = DB::table('cms_users')
		// 				->where('id_cms_privileges',1)
		// 				->pluck('id');

		// 			$config['content']      = $message;
		// 			$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$proses_pentashihan->id);
		// 			$config['id_cms_users'] = $cms_users; //This is an array of id users
		// 			CRUDBooster::sendNotification($config);

		// 			$config['content']      = $message;
		// 			$config['to']           = CRUDBooster::adminPath('penerbitan-tanda-tashih/detail/'.$proses_pentashihan->id);
		// 			$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
		// 			CRUDBooster::sendNotification($config);

		// 			$save_pernyataan['updated_at']       = Kemenag::now();
		// 			$save_pernyataan['surat_pernyataan'] = 1;
		// 			DB::table('proses_pentashihan')->where('id',$proses_pentashihan->id)->update($save_pernyataan);

		// 			return redirect()->back()->with([
		// 				'message'=>'Surat peryataan berhasil di kirim',
		// 				'message_type'=>'success'
		// 			]);
		// 		}else{
		// 			return redirect()->back()->with([
		// 				'message'=>'Surat peryataan gagal di kirim',
		// 				'message_type'=>'warning'
		// 			]);
		// 		}
		// 	}else{
		// 		return redirect()->back()->with([
		// 			'message'=>'Surat peryataan berhasil di kirim',
		// 			'message_type'=>'success'
		// 		]);
		// 	}
		// }

		public function postPernyataan(){
			try {
				$data = Request::all();

				$save['created_at'] = Kemenag::now();
				foreach ($data as $key => $value) {
					if($key == '_token') continue;
					$save[$key] = $value;
				}
	
				$check = DB::table('proses_pentashihan_surat_pernyataan')
					->where('id_proses_pentashihan',Request::input('id_proses_pentashihan'))
					->first();
	
				if (empty($check)) {
					$act = DB::table('proses_pentashihan_surat_pernyataan')->insert($save);
	
					if ($act) {
						$proses_pentashihan = DB::table('proses_pentashihan')
							->where('id',Request::input('id_proses_pentashihan'))
							->first();
						$message = 'Konfirmasi Pernyataan - '.$proses_pentashihan->nomor_registrasi;
						$cms_users = DB::table('cms_users')
							->where('id_cms_privileges',1)
							->pluck('id');
	
						$config['content']      = $message;
						$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$proses_pentashihan->id);
						$config['id_cms_users'] = $cms_users; //This is an array of id users
						CRUDBooster::sendNotification($config);
	
						$config['content']      = $message;
						$config['to']           = CRUDBooster::adminPath('penerbitan-tanda-tashih/detail/'.$proses_pentashihan->id);
						$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
						CRUDBooster::sendNotification($config);
	
						$save_pernyataan['updated_at']       = Kemenag::now();
						$save_pernyataan['surat_pernyataan'] = 1;
						DB::table('proses_pentashihan')->where('id',$proses_pentashihan->id)->update($save_pernyataan);
	
						$res['status'] = 1;
						$res['message'] = 'Surat peryataan berhasil di kirim';
					}else{
						$res['status'] = 0;
						$res['message'] = 'Surat peryataan gagal di kirim';
					}
				}else{
					$act = DB::table('proses_pentashihan_surat_pernyataan')
						->where('id_proses_pentashihan',Request::input('id_proses_pentashihan'))
						->update($save);

					$res['status'] = 1;
					$res['message'] = 'Surat peryataan berhasil di kirim';
				}
			} catch (\Throwable $th) {
				$res['status'] = 0;
				$res['message'] = $th->getMessage();
			}

			return response()->json($res);
		}

		public function postCheckPernyataan()
		{
			try {
				$id_proses_pentashihan = Request::input('id_proses_pentashihan');

				$check = DB::table('proses_pentashihan_surat_pernyataan')
					->where('id_proses_pentashihan', $id_proses_pentashihan)
					->first();
	
				$res['status'] = 1;
				$res['item'] = [
					"id" => ($check->id?:0),
					"id_proses_pentashihan" => ($check->id_proses_pentashihan?:0),
					"nama" => ($check->nama?:""),
					"jabatan" => ($check->jabatan?:""),
					"alamat" => ($check->alamat?:""),
					"phone" => ($check->phone?:""),
					"email" => ($check->email?:""),
					"nama_percetakan" => ($check->nama_percetakan?:""),
					"alamat_percetakan" => ($check->alamat_percetakan?:""),
					"rencana_pelaksanaan" => ($check->rencana_pelaksanaan?:""),
				];
	
			} catch (\Throwable $th) {
				$res['status'] = 0;
				$res['message'] = $th->getMessage();
			}
			return response()->json($res);
		}
		
		public function postCheckSurvey()
		{
			try {
				$id_proses_pentashihan = Request::input('id_proses_pentashihan');

				$isUserAnswer = SurveyAnswerServices::checkIsAnswer($id_cms_users, $id_kategori, 'Survey Download '.$id_proses_pentashihan);
	
				$res['status'] = 1;
				$res['item'] = [
					"isUserAnswer" => ($isUserAnswer?'Yes':'No'),
				];
			} catch (\Throwable $th) {
				$res['status'] = 0;
				$res['message'] = $th->getMessage();
			}
			return response()->json($res);
		}

		public function postCheckTandaTansih()
		{
			try {
				$id_proses_pentashihan = Request::input('id_proses_pentashihan');
				$item = $this->listCheckTandaTansih($id_proses_pentashihan);

				$res['status'] = 1;
				$res['item'] = $item;
			} catch (\Throwable $th) {
				$res['status'] = 0;
				$res['message'] = $th->getMessage();
			}
			return response()->json($res);
		}

		public function getTandaTashih($id){
			$ukuran = DB::table('proses_pentashihan_ukuran')
				->select('id','ukuran','oplah','surat_penerbitan','scan_tanda_tashih')
				->where('id_proses_pentashihan',$id)
				->get();

			foreach ($ukuran as $row) {
				$row->surat_penerbitan  = ($row->surat_penerbitan == '' ? '' : asset($row->surat_penerbitan));
				$row->scan_tanda_tashih = ($row->scan_tanda_tashih == '' ? '' : asset($row->scan_tanda_tashih));
			}

			$response['api_status']  = 1;
			$response['api_message'] = 'Success';
			$response['item']        = $ukuran;

			return response()->json($response);
		}

		public function postLaporanPencetakan(){
			$ukuran_cetak = Request::input('ukuran_cetak');
			$oplah_cetak = Request::input('oplah_cetak');

			foreach ($ukuran_cetak as $x => $xrow) {
				$find_ukuran = $xrow;
				$find_oplah = $oplah_cetak[$x];
				
				$save['created_at']            = Kemenag::now();
				$save['cetakan_ke']			   = Request::input('pencetakan_ke');
				$save['id_proses_pentashihan'] = Request::input('id_proses_pentashihan_laporan');
				$save['tanggal_cetak']         = Request::input('tanggal_laporan');
				// $save['oplah']                 = Request::input('oplah_laporan');
				$save['ukuran'] 			   = $find_ukuran;
				$save['oplah'] 				   = $find_oplah;
				$save['nama_percetakan']       = Request::input('nama_percetakan_laporan');
				$save['alamat_percetakan']     = Request::input('alamat_percetakan_laporan');

				$act = DB::table('proses_pentashihan_laporan_percetakan')->insert($save);
			}

			if ($act) {
				$proses_pentashihan = DB::table('proses_pentashihan')
					->where('id',$save['id_proses_pentashihan'])
					->first();
				$message = 'Laporan Pencetakan - '.$proses_pentashihan->nomor_registrasi;
				$cms_users = DB::table('cms_users')
						->where('id_cms_privileges',1)
						->pluck('id');

				$config['content']      = $message;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$save['id_proses_pentashihan']);
				$config['id_cms_users'] = $cms_users; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = $message;
				$config['to']           = CRUDBooster::adminPath('penerbitan-tanda-tashih/detail/'.$save['id_proses_pentashihan']);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect(CRUDBooster::mainpath('detail/'.$save['id_proses_pentashihan']))->with([
					'message'=>'Laporan berhasil di kirim',
					'message_type'=>'success'
				]);
			}else{
				return redirect()->back()->with([
					'message'=>'Laporan gagal dikirim',
					'message_type'=>'warning'
				]);
			}
		}
		
		public function postRating()
		{
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',Request::input('id_proses_pentashihan'))
				->first();

			$save['created_at']            = Kemenag::now();
			$save['updated_at']            = Kemenag::now();
			$save['id_cms_users'] = $proses_pentashihan->id_cms_users;
			$save['id_proses_pentashihan'] = Request::input('id_proses_pentashihan');
			$save['jumlah_rating'] = Request::input('jumlah_rating');
			$save['komentar'] = Request::input('komentar');
			$action = DB::table('proses_pentashihan_rating')->insert($save);
			
			$cms_users = DB::table("cms_users")->where("id",CRUDBooster::myId())->first();
			
			if ($action) {

				$config['content']      = "User memberi rating - ".$cms_users->name;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id_proses_pentashihan);
				$config['id_cms_users'] = [$cms_users->id]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Pemberian Rating";
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id_proses_pentashihan);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with(['message'=>'Rating berhasil ditambahkan', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
		}
		
		public function getRatingUs($id)
		{
			$ukuran = DB::table('proses_pentashihan_rating')
	    		->where('id_proses_pentashihan',$id)
	    		->get();

	    	foreach ($ukuran as $row) {
				$row->jumlah_rating             = ($row->jumlah_rating == '' ? '' : $row->jumlah_rating);
				$row->komentar             = ($row->komentar == '' ? '---' : $row->komentar);
	    	}
			
			$response['api_status']  = 1;
			$response['api_message'] = 'Success';
			$response['item']        = $ukuran;
			return response()->json($response);
		}

	    public function cbInit() {
	    	$segment = Request::segment(3);
			$id      = Request::segment(4);

	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "proses_pentashihan";	        
			$this->title_field         = "nama_produk";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = FALSE;
			$this->button_delete       = FALSE;
			$this->button_edit         = FALSE;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = FALSE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = FALSE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
			$this->col[] = array("label"=>"Cover","name"=>"gambar_cover","image"=>true);
			$this->col[] = array("label"=>"Nomor Registrasi","name"=>"nomor_registrasi");
			$this->col[] = array("label"=>"Nama Produk","name"=>"nama_produk");
			$this->col[] = array("label"=>"Tanggal Pengajuan","name"=>"created_at","callback"=>function($row){
				return date('d M Y',strtotime($row->created_at));
			});
			$this->col[] = array("label"=>"Download Tanda Tashih","name"=>"surat_pernyataan","callback"=>function($row){
				$tags = '';

				// if ($row->surat_pernyataan != 1) {
					// $tags .= '<button type="button" class="btn btn-warning btn-xs btn-pernyataan" data-id="'.$row->id.'">Kirim Pernyataan</button>';
				// }else{
					// $tags .= '<button type="button" class="btn btn-success btn-xs btn-tanda-tashih" data-id="'.$row->id.'">Tanda Tashih</button>';
					$tags .= '<button type="button" class="btn btn-success btn-xs btn-combinepentasihan" data-toggle="modal" data-target="#modalCombinePentasihan" data-id="'.$row->id.'">Download tanda tashih</button>';
				// }
				return $tags;
			});
			// $this->col[] = ['label'=>'Beri Rating',"name"=>"id","callback"=>function ($row){
        	// 	$kueri=DB::table("proses_pentashihan_rating")
        	// 		->where('id_cms_users',CRUDBooster::myId())
        	// 		->where("id_proses_pentashihan",$row->id)
        	// 		->first(); 
        			
        	// 	if(empty($kueri->id)){
        	// 	    return "<a href='javascript:void(0)' data-id='".$row->id."' class='btn btn-xs btn-danger btn-rating'><i class='fa fa-star'></i> Beri Rating</a>";
        	// 	}else{
        	// 	    return "<p>Anda sudah memberi rating klik untuk melihat rating .</p><a href='javascript:void(0)' data-id='".$row->id."' class='btn btn-xs btn-primary btn-rating-look'><i class='fa fa-star'></i> Lihat Rating</a>";
        	// 	}
			// }];
			# END COLUMNS DO NOT REMOVE THIS LINE
			
			# START FORM DO NOT REMOVE THIS LINE
			$r_cms_users = ($segment == 'edit' ? true : false);
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();
			if ($proses_pentashihan->status == 'Registrasi Berhasil' || $proses_pentashihan->status == 'Naskah Diterima') {
				$status_verifikasi = 'Sedang Diverifikasi';
			}elseif ($proses_pentashihan->status == 'Tidak Lolos Verifikasi') {
				$status_verifikasi = 'Tidak Lolos Verifikasi';
			}else{
				$status_verifikasi = 'Lolos Verifikasi';
			}
			$this->form = [];
			$this->form[] = ["label"=>"Nomor Registrasi","name"=>"nomor_registrasi","type"=>"text"];
			$this->form[] = ["label"=>"Nama Produk","name"=>"nama_produk","type"=>"text"];
			$this->form[] = ["label"=>"Nama Penerbit","name"=>"id_cms_users","type"=>"select","datatable"=>"cms_users,name", "disabled"=>$r_cms_users];
			$this->form[] = ["label"=>"Penanggung Jawab","name"=>"penanggung_jawab_produk","type"=>"text"];
			$this->form[] = ["label"=>"Nama Percetakan","name"=>"nama_percetakan","type"=>"text"];
			$this->form[] = ["label"=>"Keterangan","name"=>"keterangan","type"=>"textarea"];
			$this->form[] = ["label"=>"Jenis Naskah","name"=>"id_m_jenis_naskah","type"=>"checkbox","datatable"=>"m_jenis_naskah,name"];
			$this->form[] = ["label"=>"Jenis Mushaf","name"=>"id_m_jenis_mushaf","type"=>"select2","datatable"=>"m_jenis_mushaf,name"];
			$this->form[] = ["label"=>"Penanggung Jawab Materi Tambahan","name"=>"penanggung_jawab_materi_tambahan","type"=>"textarea"];
			$this->form[] = ["label"=>"Materi Tambahan","name"=>"id_m_materi_tambahan","type"=>"checkbox","datatable"=>"m_materi_tambahan,name"];
			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Materi Tambahan Lainnya","name"=>"materi_lainnya","type"=>"checkbox","dataenum"=>$proses_pentashihan->materi_lainnya];
			}

			$this->form[] = ["label"=>"Tashih Internal","name"=>"tashih_internal","type"=>"radio","dataenum"=>"Sudah;Belum"];
			$this->form[] = ["label"=>"Surat Permohonan","name"=>"surat_permohonan","type"=>"upload"];
			$this->form[] = ["label"=>"Gambar Cover","name"=>"gambar_cover","type"=>"upload"];
			$this->form[] = ["label"=>"Bukti Tashih Internal","name"=>"bukti_tashih_internal","type"=>"upload"];
			$this->form[] = ["label"=>"Dokumen Naskah","name"=>"dokumen_naskah","type"=>"upload"];
			$this->form[] = ["label"=>"File Naskah","name"=>"file_naskah","type"=>"upload"];
			$this->form[] = ["label"=>"Disposisi Nomor Agenda","name"=>"disposisi_nomor_agenda","type"=>"text"];
			$this->form[] = ["label"=>"Disposisi File","name"=>"disposisi_file","type"=>"upload"];
			$this->form[] = ["label"=>"Bukti Penerimaan","name"=>"bukti_penerimaan","type"=>"upload"];

			$this->form[] = ["label"=>"Nomor Resi","name"=>"no_resi","type"=>"text"];
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"radio","dataenum"=>"Registrasi Berhasil;Naskah Diterima;Lolos Verifikasi;Tidak Lolos Verifikasi;Proses Perbaikan Naskah;Pentashihan Tahap Selanjutnya;Selesai"];
			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Status Verifikasi","name"=>"id","type"=>"radio","dataenum"=>$status_verifikasi];
				$this->form[] = ["label"=>"Tanggal Verifikasi","name"=>"tanggal_verifikasi",'type'=>'date'];
				$this->form[] = ["label"=>"Tanggal Deadline","name"=>"tanggal_deadline",'type'=>'date'];
				$this->form[] = ["label"=>"File Verifikasi","name"=>"bukti_verifikasi",'type'=>'upload'];
				
				$pernyataan[] = ['label'=>'*Nama','name'=>'nama','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Jabatan','name'=>'jabatan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Alamat','name'=>'alamat','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Nomor Telepon','name'=>'phone','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Email','name'=>'email','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Nama Percetakan','name'=>'nama_percetakan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Alamat Percetakan','name'=>'alamat_percetakan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Rencana Pelaksanaan','name'=>'rencana_pelaksanaan','type'=>'text','required'=>true];
				$this->form[] = ['label'=>'Surat Pernyataan','name'=>'proses_pentashihan_surat_pernyataan','type'=>'child','columns'=>$pernyataan,'table'=>'proses_pentashihan_surat_pernyataan','foreign_key'=>'id_proses_pentashihan'];
			}
			
			$ukuran[] = ['label'=>'*Ukuran','name'=>'ukuran','type'=>'text','required'=>true];
			$ukuran[] = ['label'=>'*Oplah','name'=>'oplah','type'=>'text','required'=>true];
			if ($segment == 'detail' && $proses_pentashihan->status == 'Selesai') {
				$ukuran[] = ['label'=>'*Nomor Tanda Tashih','name'=>'nomor','type'=>'text'];
				$ukuran[] = ['label'=>'*Kode Tanda Tashih','name'=>'kode','type'=>'text'];
				$ukuran[] = ['label'=>'*Tanggal Penetapan','name'=>'tanggal_penetapan','type'=>'text'];
				$ukuran[] = ['label'=>'*Surat Penerbitan','name'=>'surat_penerbitan','type'=>'upload'];
				$ukuran[] = ['label'=>'*Scan Tanda Tashih','name'=>'scan_tanda_tashih','type'=>'upload'];
			}
			$this->form[] = ['label'=>'Ukuran & Oplah','name'=>'proses_pentashihan_ukuran','type'=>'child','columns'=>$ukuran,'table'=>'proses_pentashihan_ukuran','foreign_key'=>'id_proses_pentashihan'];

			if ($segment == 'edit') {
				$columns[] = ['label'=>'*Materi Lainnya','name'=>'name','type'=>'text','required'=>true];
				$this->form[] = ['label'=>'Materi Tambahan Lainnya','name'=>'proses_pentashihan_materi_tambahan','type'=>'child','columns'=>$columns,'table'=>'proses_pentashihan_materi_tambahan','foreign_key'=>'id_proses_pentashihan'];
			}

			$tashih[] = ['label'=>'User','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
			$tashih[] = ['label'=>'Juz','name'=>'juz','type'=>'text',"required"=>true];

			$revisi[] = ['label'=>'Admin','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
			$revisi[] = ['label'=>'Tanggal Koreksi','name'=>'created_at','type'=>'hidden'];
			$revisi[] = ['label'=>'Pesan','name'=>'pesan','type'=>'textarea',"required"=>true];
			$revisi[] = ['label'=>'Lampiran','name'=>'Lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Revisi','name'=>'revisi','type'=>'textarea'];
			$revisi[] = ['label'=>'Lampiran Revisi','name'=>'revisi_lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Tanggal Revisi','name'=>'revisi_date','type'=>'hidden'];
			$this->form[] = ['label'=>'Riwayat Koreksi','name'=>'proses_pentashihan_koreksi','type'=>'child','columns'=>$revisi,'table'=>'proses_pentashihan_koreksi','foreign_key'=>'id_proses_pentashihan'];

			if ($segment == 'detail') {
				$bukti_penerimaan[] = ['label'=>'Bukti Penerimaan','name'=>'bukti_penerimaan','type'=>'upload'];
				$bukti_penerimaan[] = ['label'=>'Tanggal Penerimaan','name'=>'tanggal_penerimaan','type'=>'date'];
				$bukti_penerimaan[] = ['label'=>'Tanggal Deadline','name'=>'tanggal_deadline','type'=>'date'];
				$this->form[] = ['label'=>'Riwayat Bukti Penerimaan','name'=>'proses_pentashihan_bukti_penerimaan','type'=>'child','columns'=>$bukti_penerimaan,'table'=>'proses_pentashihan_bukti_penerimaan','foreign_key'=>'id_proses_pentashihan'];
			}

			$laporan_pencetakan[] = ['label'=>'Tanggal Pencetakan','name'=>'tanggal_cetak','type'=>'text'];
			$laporan_pencetakan[] = ['label'=>'Oplah','name'=>'oplah','type'=>'text'];
			$laporan_pencetakan[] = ['label'=>'Nama Percetakan','name'=>'nama_percetakan','type'=>'text'];
			$laporan_pencetakan[] = ['label'=>'Alamat Percetakan','name'=>'alamat_percetakan','type'=>'text'];
			$this->form[] = ['label'=>'Laporan Pencetakan','name'=>'proses_pentashihan_laporan_percetakan','type'=>'child','columns'=>$laporan_pencetakan,'table'=>'proses_pentashihan_laporan_percetakan','foreign_key'=>'id_proses_pentashihan'];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'Laporan Pencetakan','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-edit','color'=>'info btn-laporan-pencetakan','showIf'=>"[surat_pernyataan] == '1'"];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "
				$('#table-detail').find('tr').each(function(){
					let field = $(this).find('td:first-child').text();
					if(field == 'Status Verifikasi'){
						$(this).find('td:last-child').find('.badge').html('".$status_verifikasi."');
					}
				})

				$(document).ready(function () {
					$('.price-format').priceFormat({
						prefix:'', thousandsSeparator:'.', centsLimit: 0, clearOnEmpty:false
					});
				});
	        ";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
			*/
			// $id_kategori = SurveyRepositories::checkSurveyActive();
			$id_cms_users = CRUDBooster::myId();
			// $isUserAnswerSurvey = SurveyJawabanRepositories::isUserAnswer($id_kategori, $id_cms_users);
			$isUserAnswerSurvey = [];

	        $this->pre_index_html = '
	        	<script type="text/javascript">
		        	var token = "'.csrf_token().'";
		        	var asset = "'.asset('/').'";
		        	var mainpath = "'.CRUDBooster::mainpath().'";
		        	var pathSurvey = "'.url('survey').'";
					var name = "'.CRUDBooster::myName().'";
					var isUserAnswerSurvey = "'.(count($isUserAnswerSurvey) > 0 ? 'Yes' : 'No').'";
			    </script>
			';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
			// $this->load_js[] = asset("js/admin/penerbitan.js");
			$this->load_js[] = asset("js/admin/penerbitanRemake.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->where('proses_pentashihan.status','Selesai'); 
	        $query->where('proses_pentashihan.id_cms_users',CRUDBooster::myId()); 
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

		public function listProsesUkuranPetashihan($id_tashih)
		{
			return DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan', $id_tashih)
				->orderby('id', 'asc')
				->get();
		}

		public function countProsesPencetakanPentashihan($id_tashih)
		{
			return DB::table('proses_pentashihan_laporan_percetakan')
				->where('id_proses_pentashihan', $id_tashih)
				->groupby('id_proses_pentashihan')
				->orderby('id', 'asc')
				->count();
		}

		public function postListUkuranOplah()
		{
			$id_tashih = \Request::input('id_proses_pentashihan');
			$list = $this->listProsesUkuranPetashihan($id_tashih);

			$cetak = $this->countProsesPencetakanPentashihan($id_tashih);

			$res['status'] = 1;
			$res['item'] = $list;
			$res['count_cetak_tashih'] = ($cetak == 0 ? 1 : $cetak+1);

			return response()->json($res, 200);
		}

		public function checkTandaTansih($id_proses_pentashihan)
		{
			return DB::table('proses_pentashihan_ukuran')
				->select('id', 'surat_penerbitan', 'scan_tanda_tashih')
				->where('id_proses_pentashihan', $id_proses_pentashihan)
				->get();
		}

		public function listCheckTandaTansih($id_proses_pentashihan)
		{
			$result = $this->checkTandaTansih($id_proses_pentashihan);
			foreach ($result as $x => $row) {
				$row->surat_penerbitan = asset($row->surat_penerbitan);
				$row->scan_tanda_tashih = asset($row->scan_tanda_tashih);
			}

			return $result;
		}
	}