<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Maatwebsite\Excel\Facades\Excel;
	use App\Services\SurveyServices;
	use App\Services\SurveyOpsiJawabanServices;
	use App\Repositories\KategoriSurveyRepositories;
	use App\Repositories\SurveyOpsiJawabanRepositories;

	class AdminJawabanSurveyController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function checkSurvey()
		{
			if (in_array(CRUDBooster::myPrivilegeName(), ['Penerbit'])) {
				$tabel = true;
			}else{
				$tabel = false;
			}
			return $tabel;
		}

	    public function cbInit() {
			$tabel = $this->checkSurvey();
			// dd(CRUDBooster::myPrivilegeName());

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = ($tabel ? false : true);
			$this->button_bulk_action = ($tabel ? false : true);
			$this->button_action_style = "button_icon";
			$this->button_add = ($tabel ? false : true);
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "jawaban_survey";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Responden","name"=>"id_cms_users","join"=>"cms_users,name"];
			$this->col[] = ['label'=>"Unsur","name"=>"id","callback"=>function ($row)
			{
				return $row->nama_unsur;
			}];
			$this->col[] = ["label"=>"Pertanyaan Survey","name"=>"id_survey","join"=>"survey,pertanyaan_survey"];
			$this->col[] = ["label"=>"Jawaban","name"=>"jawaban"];
			$this->col[] = ['label'=>"Pilihan Jawaban","name"=>"id","callback"=>function ($row)
			{
				return $row->option_survey;
			}];
			$this->col[] = ['label'=>"Nilai","name"=>"id","callback"=>function ($row)
			{
				return self::switchOpsi($row->option_survey);
			}];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Responden','name'=>'id_cms_users','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			$this->form[] = ['label'=>'Pertanyaan Survey','name'=>'id_survey','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'survey,pertanyaan_survey'];
			$this->form[] = ['label'=>'Jawaban','name'=>'jawaban','type'=>'select','validation'=>'required','width'=>'col-sm-10','dataenum'=>'Ya;Tidak'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Cms Users','name'=>'id_cms_users','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name'];
			//$this->form[] = ['label'=>'Survey','name'=>'id_survey','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'survey,pertanyaan_survey'];
			//$this->form[] = ['label'=>'Jawaban','name'=>'jawaban','type'=>'select','validation'=>'required','width'=>'col-sm-10','dataenum'=>'Ya;Tidak'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
			*/
	        $this->index_button = [];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
			*/
	        $this->index_statistic = [];



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
			*/
			$js = "
				$(document).find('#export-data select[name=fileformat] option[value=pdf]').remove();
				$(document).find('#export-data .toggle_advanced_report').remove();
			";
	        $this->script_js = $js;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
			*/
			$html = '
				<div class="box">
					<div class="box-body">
						<div class="form-group">
							<label>Nama Survei</label>
							<select name="select_survey" id="select_survey" class="form-control" onchange="location = this.value;">
								<option value="">Pilih Nama Survei**</option>
								'.self::listSurvey().'
							</select>
						</div>
					</div>
				</div>
			';
			// $html = null;
	        $this->pre_index_html = $html;
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
			*/
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
			//Your code here
			$name_survey_id = $_GET['name_survey_id'];
			$query->select(
					'survey_two.id_survey_unsur',
					'jawaban_survey.*',
					'survey_unsur.nama as nama_unsur',
					'survey_opsi_jawaban.type_option as option_survey'
				)
				->join('survey as survey_two', function ($join)
				{
					$join->on('survey_two.id', '=', 'jawaban_survey.id_survey');
				})->leftjoin('survey_unsur', function ($join)
				{
					$join->on('survey_unsur.id', '=', 'survey_two.id_survey_unsur');
				})
				->leftjoin('survey_opsi_jawaban', function ($join)
				{
					$join->on('survey_opsi_jawaban.id', '=', 'jawaban_survey.id_survey_opsi_jawaban');
				})
				->leftjoin('kategori_survey', function ($join) use ($name_survey_id)
				{
					$join->on('kategori_survey.id', '=', 'survey_unsur.id_kategori_survey');
					if ($name_survey_id) {
						$join->where('kategori_survey.id', $name_survey_id);
					}
				})
				->where(function ($q) use ($name_survey_id)
				{
					if ($name_survey_id) {
						$q->WhereNotNull('kategori_survey.id');
						$q->WhereNotNull('survey_unsur.id');
						$q->WhereNotNull('survey_two.id');
						$q->WhereNotNull('survey_opsi_jawaban.id');
					}
				});
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



		//By the way, you can still create your own method in here... :)
		public function listSurvey()
		{
			$data = KategoriSurveyRepositories::listSurvey();

			$list = [];
			foreach ($data as $x => $row) {
				$list[] = '<option '.($_GET['name_survey_id']==$row->id?'selected':'').' value="'.request()->fullUrlWithQuery(['name_survey_id'=>$row->id]).'">'.$row->jenis_kategori.'</option>';
			}

			return implode(" ", $list);
		} 

		public function switchOpsi($value)
		{
			switch ($value) {
				case 'A':
					$ret = 4;
					break;

				case 'B':
					$ret = 3;
					break;

				case 'C':
					$ret = 2;
					break;

				case 'D':
					$ret = 1;
					break;
				
				default:
					$ret = $value;
					break;
			}

			return $ret;
		}

		public function postExportData()
		{
			$limit = request('limit');
			$columns = request('columns');
			$filetype = request('fileformat');
			$filename = request('filename');
			$papersize = request('page_size');
			$papersize = request('page_size');
			$paperorientation = request('page_orientation');
			
			$response['columns'] = $columns;
			$response['result'] = SurveyOpsiJawabanServices::exportOpsiJawaban($limit);

			switch ($filetype) {
				case 'xls':
					Excel::create($filename, function ($excel) use ($response, $paperorientation, $filename) {
						$excel->setTitle($filename)->setCreator("crudbooster.com")->setCompany(CRUDBooster::getSetting('appname'));
						$excel->sheet($filename, function ($sheet) use ($response, $paperorientation) {
							$sheet->setOrientation($paperorientation);
							$sheet->loadview('admin.JawabanSurvey.excel', $response);
						});
					})->export('xls');
					break;
				case 'csv':
					Excel::create($filename, function ($excel) use ($response, $paperorientation, $filename) {
						$excel->setTitle($filename)->setCreator("crudbooster.com")->setCompany(CRUDBooster::getSetting('appname'));
						$excel->sheet($filename, function ($sheet) use ($response, $paperorientation) {
							$sheet->setOrientation($paperorientation);
							$sheet->loadview('admin.JawabanSurvey.excel', $response);
						});
					})->export('csv');
					break;
			}
		}
	}