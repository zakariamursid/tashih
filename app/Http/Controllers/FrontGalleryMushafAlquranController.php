<?php
namespace App\Http\Controllers;

use App\Services\BannerLayananServices;
use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontGalleryMushafAlquranController extends Controller
{
	public function getIndex($key=null){
		Kemenag::menu('info');
		Kemenag::menuSub('info-layanan');

		$gallery_mushaf = DB::table('gallery_mushaf')
			->orderby('id','desc')
			->whereNull('deleted_at')
			->paginate(8);

		$list_id = '';
		$total   = count($gallery_mushaf);
		$max_row = $total-1;
		$num     = 0;
		foreach ($gallery_mushaf as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();
			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 70);
			$row->title   = $title.($length_title > 70 ? '...' : '');

			$list_id .= $row->id;
			$list_id .= ($num < $max_row ? ';' : '');

			$content        = strip_tags($row->content);
			$length_content = strlen($content);
			$content        = substr($content, 0, 130);
			$row->content   = $content.($length_content > 130 ? '...' : '');

			$num++;
		}
		$id = explode(';', $list_id);

		$info_lainnya = DB::table('gallery_mushaf')
// 			->whereNotIn('id',$id)
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
		foreach ($info_lainnya as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();

			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 50);
			$row->title   = $title.($length_title > 50 ? '...' : '');
		}

		$result['gallery_mushaf'] = $gallery_mushaf;
		$result['info_lainnya']        = $info_lainnya;
        $result['banner_layanan'] 	   = BannerLayananServices::listBannerLayanan();

		return view('front/gallerymushafalquran',$result);

	}

	public function getRead($slug){
		Kemenag::menu('info');
		Kemenag::menuSub('info-layanan');

		$gallery_mushaf = DB::table('gallery_mushaf')
			->where('slug',$slug)
			->whereNull('deleted_at')
			->first();

		if (empty($gallery_mushaf)) {
			return redirect('404');
		}

		$cms_users = DB::table('cms_users')
				->where('id',$gallery_mushaf->id_cms_users)
				->first();
		$gallery_mushaf->admin_name = $cms_users->name;
		$gallery_mushaf->image      = ($gallery_mushaf->image == '' ? asset('image/no-image.png') : asset($gallery_mushaf->image));

		$info_lainnya = DB::table('gallery_mushaf')
			->whereNotIn('id',[$gallery_mushaf->id])
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
		foreach ($info_lainnya as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();

			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->tite);
			$row->title   = $row->title.($length_title > 50 ? '...' : '');
		}

		$result['gallery_mushaf'] = $gallery_mushaf;
		$result['info_lainnya']        = $info_lainnya;
        $result['banner_layanan'] 	   = BannerLayananServices::listBannerLayanan();

		return view('front/gallerymushafalqurandetail',$result);
	}
}