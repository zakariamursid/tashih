<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;

	class AdminMonitoringController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {
	    	$segment = Request::segment(3);
	    	$monitoring = DB::table('monitoring')
	       			->where('id',Request::segment(4))
	       			->first();

	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "monitoring";	        
			$this->title_field         = "nama_petugas";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_delete       = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_edit         = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Created At","name"=>"created_at"];
			// $this->col[] = ["label"=>"Temuan","name"=>"temuan"];
			$this->col[] = ["label"=>"Nama Petugas","name"=>"nama_petugas_view"];
			$this->col[] = ["label"=>"Tipe Lokasi","name"=>"id_m_tipe_lokasi","join"=>"m_tipe_lokasi,name"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			if ($segment != 'detail') {
				$this->form[] = ["label"=>"Nama Petugas","name"=>'nama_petugas',"type"=>"select2","datatable"=>"cms_users,name","datatable_where"=>"id_cms_privileges != 2","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			}else{
				$this->form[] = ["label"=>"Nama Petugas","name"=>'nama_petugas',"type"=>"text"];
			}
			$this->form[] = ["label"=>"Waktu Pengawasan","name"=>"waktu_temuan","type"=>"datetime","required"=>TRUE,"validation"=>"required|date_format:Y-m-d H:i:s"];
			$this->form[] = ["label"=>"Tipe Lokasi","name"=>"id_m_tipe_lokasi","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"m_tipe_lokasi,name"];
			$this->form[] = ["label"=>"Alamat Lokasi","name"=>"alamat_lokasi","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			// $this->form[] = ["label"=>"Temuan","name"=>"temuan","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"radio","dataenum"=>"Aktif;Pending;Selesai","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# END FORM DO NOT REMOVE THIS LINE   
			
			if ($segment == 'detail') {
				$columns[] = ['label'=>'Tanggal','name'=>'created_at','type'=>'datetime'];
				$columns[] = ['label'=>'Teruskan Ke','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name'];
				$columns[] = ['label'=>'Aksi','name'=>'id_m_rekomendasi_aksi','type'=>'select','datatable'=>'m_rekomendasi_aksi,name'];
				$columns[] = ['label'=>'Pesan','name'=>'pesan','type'=>'text'];
				$columns[] = ['label'=>'Lampiran','name'=>'id','type'=>'text'];
				$this->form[] = ['label'=>'Laporan Pengawasan Monitoring','name'=>'monitoring_tidak_lanjut','type'=>'child','columns'=>$columns,'table'=>'monitoring_tidak_lanjut','foreign_key'=>'id_monitoring'];
			}  

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, success, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        // if (CRUDBooster::isSuperadmin()) {
	        	$this->addaction[] = ['label'=>'Laporan Pengawasan','url'=>'[id]','icon'=>'fa fa-check','color'=>'info open-modal'];
	        // }
	        


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	       	$select_petugas = '';
	       	if ($segment == 'edit') {
	       		$id = explode(',', $monitoring->nama_petugas);
	       		if (count($id) > 0) {
	       			$select_petugas .= '['.$monitoring->nama_petugas;
	       			$select_petugas .= ']';
	       		}
	       		$value_nama_petugas = '$("#nama_petugas").val('.$select_petugas.').trigger("change");';
	       	}elseif ($segment == '') {
	       		$value_nama_petugas .= '$("#nama_petugas").val("").trigger("change");';
	       	}else{
	       		$value_nama_petugas .= '$("#nama_petugas").val("").trigger("change");';
	       	}
	       	$js = '';
	       	if ($segment != '') {
	       		$js .= '
					$("#nama_petugas").find("option[value=\'\']").remove();
		        	$("#nama_petugas").attr("multiple","multiple");
		        	$("#nama_petugas").attr("name","nama_petugas[]");
					$("#nama_petugas").select2({
						placeholder: "Please select a Pegawai"	
					})
	       		';
	       	}
	       	$detail = '';
	       	if ($segment == 'detail') {
	       		$id = explode(',', $monitoring->nama_petugas);
	       		$cms_users = DB::table('cms_users')
	       			->whereIn('id',$id)
	       			->pluck('name');
	       		$nama = [];
	       		for ($i=0; $i < count($cms_users) ; $i++) { 
	       			array_push($nama,$cms_users[$i]);
	       		}
	       		$nama = implode(', ',$nama);
	       		// $detail = '$("#table-detail").find("tr:first-child").find("td:nth-child(2)").html("'.$nama.'")';
	       		$detail = '
	       		let myloop = 0;
	       		$("#table-detail").find(tr).each(function(){
					if(myloop < 1){
						$(this).find("td:nth-child(2)").html("'.$nama.'");
					}
					myloop++;
	       		})';
	       	}
	        $this->script_js = $js.$value_nama_petugas.$detail;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	       	
	       	$m_rekomendasi_aksi = DB::table('m_rekomendasi_aksi')
	       		->whereNull('deleted_at')
	       		->get();
	       	$rekomendasi_aksi = '';
	       	foreach ($m_rekomendasi_aksi as $row) {
	       		$rekomendasi_aksi .= '<div class="radio-inline">';
				$rekomendasi_aksi .= '<input class="form-check-input" id="ra'.$row->id.'" name="rekomendasi_aksi" type="radio" value="'.$row->id.'" required="">';
				$rekomendasi_aksi .= '<label class="form-check-label" for="ra'.$row->id.'">'.$row->name.'</label>';
				$rekomendasi_aksi .= '</div>';
	       	}


	       	$list_cms_users = DB::table('cms_users')
	       		->where('id_cms_privileges',1)
	       		->orderBy('name','ASC')
	       		->get();
	       	$cms_users = '';
       		foreach ($list_cms_users as $row) {
       			$cms_users .= '<option value="'.$row->id.'">'.$row->name.'</option>';
	       	}

	        $this->pre_index_html = '
	        	<script type="text/javascript">
		        	var token = "'.csrf_token().'";
		        	var asset = "'.asset('/').'";
		        	var mainpath = "'.CRUDBooster::mainpath().'";
			    </script>
				<div id="tindakLanjut" class="modal fade" role="dialog">
				  	<div class="modal-dialog modal-lg">
						<form method="POST" action="'.CRUDBooster::mainpath('tindak-lanjut').'" enctype="multipart/form-data">
						    <div class="modal-content">
							    <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title">Laporan Pengawasan Monitoring</h4>
							    </div>
							    <div class="modal-body">
							    	<div class="row" style="padding: 6px 0;">
							    		<div class="col-sm-3">
							    			<div class="pull-right">
							    				<label>Deskripsi Temuan *</label>
							    			</div>
							    		</div>
							    		<div class="col-sm-9">
							    			<textarea class="form-control" rows="3" name="pesan" required=""></textarea>
							    		</div>
							    	</div>

									<div class="row" style="padding: 6px 0;">
							    		<div class="col-sm-3">
							    			<div class="pull-right">
							    				<label>Rekomendais Aksi *</label>
							    			</div>
							    		</div>
							    		<div class="col-sm-9 form-group">
							    			'.$rekomendasi_aksi.'
							    		</div>
							    	</div>

									<div class="row" style="padding: 6px 0;">
							    		<div class="col-sm-3">
							    			<div class="pull-right">
							    				<label>Diteruskan kepada *</label>
							    			</div>
							    		</div>
							    		<div class="col-sm-9">
							    			<select class="form-control" name="id_cms_users" required="">
							    				'.$cms_users.'
							    			</select>
							    		</div>
							    	</div>

									<div class="row" style="padding: 6px 0;">
							    		<div class="col-sm-3">
							    			<div class="pull-right">
							    				<label>
							    					Lampiran 
							    					<button type="button" class="btn btn-info btn-xs" id="buttonLampiran"><i class="fa fa-plus"></i></button>
							    				</label>
							    			</div>
							    		</div>
							    		<div class="col-sm-9" id="wrapperLampiran">
							    			<div class="row">
							    				<div class="col-sm-6" class="lampiran">
							    					<input type="file" name="lampiran[]" class="form-control">
							    				</div>
							    				<div class="col-sm-6" class="lampiran">
							    					<input type="file" name="lampiran[]" class="form-control">
							    					<button type="button" class="btn btn-warning btn-remove btn-xs"><i class="fa fa-times"></i></button>
							    				</div>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							   	<div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							        <button type="submit" class="btn btn-primary">Submit</button>
							    </div>
					    	</div>
					    	<input type="hidden" id="idMonitoring" name="id_monitoring" value="">
					    	<input type="hidden" name="_token" value="'.csrf_token().'">
						</form>
				  	</div>
				</div>
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset("js/admin/monitoring.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = '
				.lampiran{
					position:relative;
				}
				.btn-remove{
					position:absolute;
					right:20px;
					top:5px;
				}
				#wrapperLampiran .col-sm-6{
					margin-bottom:10px;
				}
				#table-monitoring_tidak_lanjut .id > .td-label{
					color:transparent;
				}
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        if (Request::get('id') != '') {
	        	$query->where('monitoring.id',Request::get('id'));
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 
	    
	    public function postAddSave(){
	    	$request_petugas = Request::input('nama_petugas');

	    	$cms_users = DB::table('cms_users')
	    		->whereIn('id',$request_petugas)
	    		->pluck('name');
	    	$nama_petugas_view = '';
	    	for ($i=0; $i < count($cms_users) ; $i++) { 
				$nama_petugas_view .= $cms_users[$i];
				$nama_petugas_view .= ($i < count($cms_users)-1 ? ',' : '');
			}
			$nama_petugas = '';
	    	for ($i=0; $i < count($request_petugas) ; $i++) { 
				$nama_petugas .= $request_petugas[$i];
				$nama_petugas .= ($i < count($request_petugas)-1 ? ', ' : '');
			}

	    	// $nama_petugas_view = implode(",", $cms_users);
			$save['nama_petugas']      = $nama_petugas;
			$save['nama_petugas_view'] = $nama_petugas_view;
			$save['created_at']        = Kemenag::now();
			$save['waktu_temuan']      = Request::input('waktu_temuan');
			$save['id_m_tipe_lokasi']  = Request::input('id_m_tipe_lokasi');
			$save['alamat_lokasi']     = Request::input('alamat_lokasi');
			$save['temuan']            = Request::input('temuan');
			$save['status']            = Request::input('status');
	    	$action = DB::table('monitoring')->insert($save);

			if ($action) {
				if (Request::input('submit') == 'Save') {
					return redirect(CRUDBooster::mainpath())->with(['message'=>'Data berhasil ditambahkan', 'message_type'=>'success']);
				}else{
					return redirect()->back()->with(['message'=>'Data berhasil ditambahkan', 'message_type'=>'success']);
				}
				
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
	    }

	    public function postTindakLanjut(){
			$now      = Kemenag::now();
			$lampiran = Request::file('lampiran');

			$save['created_at']            = $now;
			$save['updated_at']            = $now;
			$save['id_monitoring']         = Request::input('id_monitoring');
			$save['pesan']                 = Request::input('pesan');
			$save['id_cms_users']          = Request::input('id_cms_users');
			$save['id_m_rekomendasi_aksi'] = Request::input('rekomendasi_aksi');
			$id = DB::table('monitoring_tidak_lanjut')->insertGetId($save);

			if ($id) {
				for ($i=0; $i < count($lampiran) ; $i++) { 
					$image = Kemenag::uploadFileMultiple($i,'lampiran','tidak_lanjut',true);
					if($image == '') continue;

					$save_lampiran['created_at']                 = $now;
					$save_lampiran['id_monitoring_tidak_lanjut'] = $id;
					$save_lampiran['image']                      = Kemenag::uploadFileMultiple($i,'lampiran','tidak_lanjut',true);
					DB::table('monitoring_tidak_lanjut_lampiran')->insert($save_lampiran);
				}

				$config['content']      = "Pengawasan Peredaran";
				$config['to']           = CRUDBooster::adminPath('pengawasan-peredaran?id='.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Pengawasan Peredaran";
				$config['to']           = CRUDBooster::adminPath('pengawasan-peredaran?id='.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with(['message'=>'Data berhasil ditambahkan', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
	    }

	    public function postEditSave($id){
	    	$request_petugas = Request::input('nama_petugas');

	    	$cms_users = DB::table('cms_users')
	    		->whereIn('id',$request_petugas)
	    		->pluck('name');
	    	$nama_petugas_view = '';
	    	for ($i=0; $i < count($cms_users) ; $i++) { 
				$nama_petugas_view .= $cms_users[$i];
				$nama_petugas_view .= ($i < count($cms_users)-1 ? ',' : '');
			}
			$nama_petugas = '';
	    	for ($i=0; $i < count($request_petugas) ; $i++) { 
				$nama_petugas .= $request_petugas[$i];
				$nama_petugas .= ($i < count($request_petugas)-1 ? ', ' : '');
			}

	    	// $nama_petugas_view = implode(",", $cms_users);
			$save['nama_petugas']      = str_replace(' ', '', $nama_petugas);
			$save['nama_petugas_view'] = str_replace(',', ', ', $nama_petugas_view);
			$save['created_at']        = Kemenag::now();
			$save['waktu_temuan']      = Request::input('waktu_temuan');
			$save['id_m_tipe_lokasi']  = Request::input('id_m_tipe_lokasi');
			$save['alamat_lokasi']     = Request::input('alamat_lokasi');
			$save['temuan']            = Request::input('temuan');
			$save['status']            = Request::input('status');
			$action = DB::table('monitoring')->where('id',$id)->update($save);

			if ($action) {
				if (Request::input('submit') == 'Save') {
					return redirect(CRUDBooster::mainpath())->with(['message'=>'Data berhasil di edit', 'message_type'=>'success']);
				}else{
					return redirect()->back()->with(['message'=>'Data berhasil di edit', 'message_type'=>'success']);
				}
				
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
	    }

	    public function getLoadLampiran(){
	    	$id = Request::get('id');

	    	$lampiran = DB::table('monitoring_tidak_lanjut_lampiran')
	    		->where('id_monitoring_tidak_lanjut',$id)
	    		->get();
	    	echo "<div class='row'>";
	    	foreach ($lampiran as $row) {
	    		$extension = strtolower(Kemenag::extension($row->image));
	    		echo '<div class="col-sm-6" style="margin-bottom:10px;">';
	    		if ($extension == ('jpg' || 'png' || 'jpeg' || 'gif')) {
	    			echo '<center><img src="'.asset($row->image).'" class="img-thumbnail" style="height:300px; width:auto;"></center>';
	    		}else{
	    			echo '<center><iframe src="'.asset($row->image).'" style="height: 300px;width: 100%;"></iframe></center>';
	    		}
	    		echo '</div>';
	    	}
    		echo '</div>';
	    }

	    public function Extension($string){

	    }

	}