<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontPersyaratanPengajuanTashihController extends Controller
{
	public function getIndex(){
		$page = DB::table('page')
			->where('id',2)
			->first();

		$cms_users = DB::table('cms_users')
			->where('id',$page->id_cms_users)
			->first();

		$data['page']       = $page;
		$data['title']      = 'Persyaratan Administrasi Penerbitan Al-Qur\'an';
		$data['name']       = $cms_users->name;
		$data['breadcrumb'] = 'PERSYARATAN PENGAJUAN TASHIH';
		return view('front/page',$data);
	}
}