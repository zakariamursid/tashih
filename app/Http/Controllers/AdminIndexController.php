<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Services\RevisiSurvey\SurveyAnswerServices;
	use App\Services\RevisiSurvey\SurveyConnectServices;

	class AdminIndexController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function status($status){
			switch ($status) {
				case 'Registrasi Berhasil':
					$color = 'btn-success';
					break;

				case 'Naskah Diterima':
					$color = 'btn-warning';
					break;

				case 'Tidak Lolos Verifikasi':
					$color = 'btn-default';
					break;

				case 'Lolos Verifikasi':
					$color = 'btn-success';
					break;

				case 'Proses Perbaikan Naskah':
					$color = 'btn-warning';
					break;

				case 'Pentashihan Tahap Selanjutnya':
					$color = 'btn-primary';
					break;

				case 'Selesai':
					$color = 'btn-success';
					break;
				
				default:
					$color = 'btn-default';
					break;
			}

			return $color;
		}

		public function findUkuranTashih($id, $type = 'kode')
		{
			$data = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$id)
				->get();

			$list = [];
			foreach ($data as $x => $row) {
				if ($type == 'kode') {
					if (empty($row->kode)) {
						continue;
					}
					array_push($list, $row->kode);
				}elseif ($type == 'nomor') {
					if (empty($row->nomor)) {
						continue;
					}
					array_push($list, $row->nomor);
				}elseif ($type == 'oplah') {
					if (empty($row->oplah)) {
						continue;
					}
					array_push($list, $row->oplah);
				}elseif ($type == 'ukuran') {
					if (empty($row->ukuran)) {
						continue;
					}
					array_push($list, $row->ukuran);
				}
			}
			$list = '('.implode('), (',$list).')';
			return $list;
		}

		public function getIndex(){
			$total_penerbit = DB::table('cms_users')
				->where('id_cms_privileges',2)
				->count();
			$total_pentashihan = DB::table('proses_pentashihan')
				->whereNull('deleted_at')
				->count();
			$total_info_lajnah = DB::table('info_seputar_lajnah')
				->whereNull('deleted_at');
			if (CRUDBooster::myPrivilegeId() == 3) {
				$total_info_lajnah->where('id_cms_users',CRUDBooster::myId());
			}
			$total_info_lajnah = $total_info_lajnah->count();
			$total_gallery_mushaf = DB::table('gallery_mushaf')
				->whereNull('deleted_at');
			if (CRUDBooster::myPrivilegeId() == 3) {
				$total_gallery_mushaf->where('id_cms_users',CRUDBooster::myId());
			}
			$total_gallery_mushaf = $total_gallery_mushaf->count();

			$proses_pentashihan = DB::table('proses_pentashihan')
				->select(
					'proses_pentashihan.*'
				)
				// ->leftjoin('proses_pentashihan_ukuran', 'proses_pentashihan_ukuran.id_proses_pentashihan', '=', 'proses_pentashihan.id')
				// ->leftjoin('cms_users', 'cms_users.id', '=', 'proses_pentashihan.id_cms_users')
				->orderby('proses_pentashihan.id','DESC')
				->paginate(12);
			
			$nomor = 1;
			foreach ($proses_pentashihan as $row) {
				$color = $this->status($status);
				$row->color  = $color;

				$row->ukuran_nomor = $this->findUkuranTashih($row->id, 'nomor');
				$row->ukuran_kode = $this->findUkuranTashih($row->id, 'kode');
				$row->ukuran_oplah = $this->findUkuranTashih($row->id, 'oplah');
				$row->ukuran_size = $this->findUkuranTashih($row->id, 'ukuran');

				$cms_users = DB::table('cms_users')
					->where('id',$row->id_cms_users)
					->first();

				$row->nama_penerbit = $cms_users->name;
				$row->no = $nomor++;
			}

			$tanggal = [];

			date_default_timezone_set("Asia/Jakarta");
			$from = date("Y-m-d H:i:s");
			$to = date("Y-m-d H:i:s", strtotime('+6 months', strtotime($from)));
			
			$db_status_tansih=DB::table('proses_pentashihan')
				->whereNotnull('tanggal_deadline')
				->whereBetween("tanggal_deadline",[$from,$to])
				->groupby("id")
				->get();
				
			foreach($db_status_tansih as $data_row){
				$cms_users = DB::table('cms_users')
							->where('id',$data_row->id_cms_users)
							->first();
				$name = $cms_users->name;
				
				$tanggaldata['tanggal_deadline'] = ($data_row->tanggal_deadline) ? $data_row->tanggal_deadline : '';
				$tanggaldata['nama_produk'] = ($data_row->nama_produk) ? $data_row->nama_produk : '';
				$tanggaldata['name'] = ($name) ? $name : '';
				$tanggaldata['nomor_registrasi'] = ($data_row->nomor_registrasi) ? $data_row->nomor_registrasi : '';
				$tanggal[] = $tanggaldata;
			}
			
			$time = []; 
			foreach($tanggal as $data_satu){
				$time[] = strtotime($data_satu['tanggal_deadline']);
			}
			
			array_multisort($time, SORT_ASC, $tanggal);


			$arr['page_title']           = 'dashboard';
			$arr['total_penerbit']       = $total_penerbit;
			$arr['total_pentashihan']    = $total_pentashihan;
			$arr['db_status_tansih'] = $tanggal;
			$arr['total_info_lajnah']    = $total_info_lajnah;
			$arr['total_gallery_mushaf'] = $total_gallery_mushaf;
			$arr['proses_pentashihan']   = $proses_pentashihan;
			$arr['history_tashih_penerbit'] = $this->historyPestashihanPenerbit();
			$data['isUserAnswer'] = SurveyAnswerServices::checkIsAnswer(CRUDBooster::myId(), SurveyConnectServices::findSurveyIDActive(), 'Survey Umum');

			return view('admin/index',$arr);
		}

	    public function cbInit() {
	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "cms_users";	        
			$this->title_field         = "name";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = TRUE;
			$this->button_delete       = TRUE;
			$this->button_edit         = TRUE;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
			$this->col[] = array("label"=>"Name","name"=>"name" );
			$this->col[] = array("label"=>"Photo","name"=>"photo" ,"image"=>true);
			$this->col[] = array("label"=>"Email","name"=>"email" );
			$this->col[] = array("label"=>"Privileges","name"=>"id_cms_privileges","join"=>"cms_privileges,name");

			# END COLUMNS DO NOT REMOVE THIS LINE
			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ["label"=>"Name","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			$this->form[] = ["label"=>"Photo","name"=>"photo","type"=>"upload","required"=>TRUE,"validation"=>"required|image|max:3000","help"=>"File types support : JPG, JPEG, PNG, GIF, BMP"];
			$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:cms_users","placeholder"=>"Please enter a valid email address"];
			$this->form[] = ["label"=>"Password","name"=>"password","type"=>"password","required"=>TRUE,"validation"=>"min:3|max:32","help"=>"Minimum 5 characters. Please leave empty if you did not change the password."];
			$this->form[] = ["label"=>"Cms Privileges","name"=>"id_cms_privileges","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_privileges,name"];
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Penerbit","name"=>"id_penerbit","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"penerbit,name"];
			$this->form[] = ["label"=>"Password Value","name"=>"password_value","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Phone","name"=>"phone","type"=>"number","required"=>TRUE,"validation"=>"required|numeric","placeholder"=>"You can only enter the number only"];
			$this->form[] = ["label"=>"Pic","name"=>"pic","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Address","name"=>"address","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"M Jenis Lembaga","name"=>"id_m_jenis_lembaga","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"m_jenis_lembaga,name"];
			$this->form[] = ["label"=>"Surat Permohonan","name"=>"surat_permohonan","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Npwp","name"=>"npwp","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Akte Notaris","name"=>"akte_notaris","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Company Profile","name"=>"company_profile","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Siup","name"=>"siup","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Tdp","name"=>"tdp","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Send Profile","name"=>"send_profile","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

		public function listPestashihanPenerbit()
		{
			return DB::table('proses_pentashihan_histori_status')
				->select(
					'proses_pentashihan_histori_status.*',
					DB::raw('DATE(proses_pentashihan_histori_status.created_at) as tanggal')
				)
				->join('proses_pentashihan','proses_pentashihan.id','=','proses_pentashihan_histori_status.id_proses_pentashihan')
				->join('cms_users','cms_users.id','=','proses_pentashihan.id_cms_users')
				->where('proses_pentashihan.id_cms_users',CRUDBooster::myId())
				->orderby('tanggal','DESC')
				->get();
		}

		public function findProsesPestashihan($id_proses_pentashihan)
		{
			return DB::table('proses_pentashihan')
				->select(
					'proses_pentashihan.*',
					'cms_users.name as cms_users_name'
				)
				->leftjoin('cms_users', 'cms_users.id', '=', 'proses_pentashihan.id_cms_users')
				->where('proses_pentashihan.id', $id_proses_pentashihan)
				->orderby("proses_pentashihan.tanggal_deadline","desc")
				->get();
		}

		public function historyPestashihanPenerbit()
		{
			$data = $this->listPestashihanPenerbit();

			foreach ($data as $x => $xrow) {
				$proses_tashih = $this->findProsesPestashihan($xrow->id_proses_pentashihan);
				$xrow->proses_tashih = $proses_tashih;

				$color = $this->status($xrow->status);
				$xrow->color = $color;
			}

			return $data;
		}
	}