<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;

	class AdminLaporanMushafBermasalahController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function postTindakLanjut() {
			$save['updated_at']    = Kemenag::now();
			$save['tindak_lanjut'] = Kemenag::uploadFile('file','mushaf_bermasalah',true);
			$act = DB::table('laporan_mushaf_bermasalah')->where('id',Request::input('id'))->update($save);

			if ($act) {
				if (Request::input('kirim_pelapor') != '') {
					$laporan_mushaf_bermasalah = DB::table('laporan_mushaf_bermasalah')
						->where('id',Request::input('id'))
						->first();
					$data_email['name'] = $laporan_mushaf_bermasalah->nama_pelapor;
					$attachments        = [storage_path('app/'.$laporan_mushaf_bermasalah->tindak_lanjut)];
					CRUDBooster::sendEmail(['to'=>$laporan_mushaf_bermasalah->email_pelapor,'data'=>$data_email,'template'=>'tindak_lanjut_aduan','attachments'=>$attachments]);
				}
				return redirect()->back()->with([
					'message'=>'Tindak Lanjut berhasil Dikirim.',
					'message_type'=>'success'
				]);
			}else{
				return redirect()->back()->with([
					'message'=>'Tindak Lanjut gagal Dikirim.',
					'message_type'=>'warning'
				]);
			}
		}

		public function getLoadTindakLanjut(){
			$id = Request::input('id');
			$data = DB::table('laporan_mushaf_bermasalah')->where('id',$id)->first();

			if (empty($data)) {
				$response['api_status']  = 0;
				$response['api_message'] = 'failed';
			}else{
				$response['api_status']  = 1;
				$response['api_message'] = 'success';
				$response['file']        = ($data->tindak_lanjut == '' ? '' : asset($data->tindak_lanjut));
			}

			return response()->json($response);
		}

	    public function cbInit() {
	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "laporan_mushaf_bermasalah";	        
			$this->title_field         = "nama_pelapor";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_delete       = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_edit         = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = TRUE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Created At","name"=>"created_at"];
			$this->col[] = ["label"=>"Nama Pelapor","name"=>"nama_pelapor"];
			$this->col[] = ["label"=>"Judul Mushaf","name"=>"judul_mushaf"];
			$this->col[] = ["label"=>"Penerbit","name"=>"nama_penerbit"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ["label"=>"Nama Pelapor","name"=>"nama_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Email Pelapor","name"=>"email_pelapor","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Nomor Telepon Pelapor","name"=>"phone_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Pekerjaan Pelapor","name"=>"pekerjaan_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Alamat Pelapor","name"=>"alamat_pelapor","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Nama Penerbit","name"=>"nama_penerbit","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Alamat Penerbit","name"=>"alamat_penerbit","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Judul Mushaf","name"=>"judul_mushaf","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"No Surat Tanda Tashih","name"=>"no_surat_tanda_tashih","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Scan Cover Mushaf","name"=>"scan_cover_mushaf","type"=>"upload","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Scan Surat Tanda Tashih","name"=>"scan_surat_tanda_tashih","type"=>"upload","required"=>TRUE,"validation"=>"required|min:1|max:255"];

			$columns[] = ['label'=>'Surah dan ayat','name'=>'halaman','type'=>'number','required'=>true];
			$columns[] = ['label'=>'Gambar','name'=>'gambar','type'=>'upload','required'=>true];
			$columns[] = ['label'=>'Jenis Kesalahan','name'=>'id_m_jenis_kesalahan','type'=>'select','datatable'=>'m_jenis_kesalahan,name','required'=>true];
			$columns[] = ['label'=>'Deskripsi Kesalahan','name'=>'desc_masalah','type'=>'textarea','required'=>true];

			$this->form[] = ['label'=>'Scan Dokumen Bermasalah','name'=>'laporan_mushaf_bermasalah_detail','type'=>'child','columns'=>$columns,'table'=>'laporan_mushaf_bermasalah_detail','foreign_key'=>'id_laporan_mushaf_bermasalah'];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	        if (CRUDBooster::isSuperadmin()) {
	        	$this->addaction[] = ['label'=>'Tindak Lanjut','url'=>"javascript:void(0)' data-id='[id]'",'icon'=>'fa fa-check','color'=>'success btn-tindak-lanjut'];
	        }
	       


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = '
				$("#table-laporan_mushaf_bermasalah_detail").find("tbody").find("a").each(function(){
					let url        = $(this).attr("href");
					let parent     = $(this).parent();
					let mainParent = parent.parent();
					let halaman    = mainParent.find(".halaman").find("span").html();
					let image      = "<a data-lightbox=\'roadtrip\' rel=\'group_{image}\' title=\'Halaman: "+halaman+"\' href=\'"+url+"\'><img width=\'40px\' height=\'40px\' src=\'"+url+"\'></a>";

					$(this).remove();
					parent.append(image); 
				})
	        ';


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '
				<script type="text/javascript">
		        	var token = "'.csrf_token().'";
		        	var asset = "'.asset('/').'";
		        	var mainpath = "'.CRUDBooster::mainpath().'";
			    </script>
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset('js/admin/aduanmushaf.js');
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        if (Request::get('id') != '') {
	        	$query->where('laporan_mushaf_bermasalah.id',Request::get('id'));
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}