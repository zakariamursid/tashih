<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;
	use App;
	use Mail;
	use Response;
	use Validator;
	use Illuminate\Support\Facades\Storage;

	class AdminListTashihController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function generateIdPenerbit(){
			$cms_users = DB::table('cms_users')
				->where('id_cms_privileges',2)
				->where(function($q){
					$q->orWhereNull('id_penerbit');
					$q->orWhere('id_penerbit','');
				})
				->get();

			foreach ($cms_users as $row) {
				$save['id_penerbit'] = Kemenag::generateIdPenerbit();
				DB::table('cms_users')->where('id',$row->id)->update($save);
			}
		}

		public function generateNomorRegistrasi(){
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where(function($q){
					$q->orWhere('nomor_registrasi','');
					$q->orWhereNull('nomor_registrasi');

				})
				->get();

			foreach ($proses_pentashihan as $row) {
				$year = date('Y',strtotime($row->created_at));

				$check_proses_pentashihan = DB::table('proses_pentashihan')
					->where(function($q){
						$q->orWhereNotNull('nomor_registrasi');
						$q->orWhere('nomor_registrasi','!=','');
					})
					->whereYear('created_at',$year)
					->orderBy('id','DESC')
					->whereNull('deleted_at')
					->first();

				if (empty($check_proses_pentashihan)) {
					$nomor_registrasi = '1/'.$year;
				}else{
					$slice = explode('/', $check_proses_pentashihan->nomor_registrasi);
					$nomor = $slice[0]+1;
					$nomor_registrasi = $nomor.'/'.$year;
				}

				$save['updated_at']       = Kemenag::now();
				$save['nomor_registrasi'] = $nomor_registrasi;

				DB::table('proses_pentashihan')->where('id',$row->id)->update($save);
			}
		}

		public function getDownloadRevisi($id)
		{
			$tabel=DB::table('proses_pentashihan_koreksi')
			->where("id",$id)
			->first();

			if ($tabel->lampiran==null) {
				return "<script>alert('Tidak ada Lampiran Revisi')</script>";
			}else{
				return response()->download(storage_path('app/'.$tabel->lampiran)); 
			}
		}

		public function getDownloadTansih($id)
		{
			$tabel=DB::table('proses_pentashihan_ukuran')
			->where("id_proses_pentashihan",$id)
			->first();

			if ($tabel->surat_penerbitan==null) {
				return "<script>alert('Tidak ada Surat Penerbitan')</script>";
			}else{
				return response()->download(storage_path('app/'.$tabel->surat_penerbitan)); 
			}
		}
		
		public function getKoreksiTashih($id){
		    $ukuran = DB::table('proses_pentashihan_koreksi')
	    		->where('id_proses_pentashihan',$id)
	    		->get();
			
			$response['api_status']  = 1;
			$response['api_message'] = 'Success';
			$response['item']        = $ukuran;
			return response()->json($response);
		}

		public function postDownloadTansih()
		{
			$simpan=DB::table('rating')
			->insert([
				"created_at"=> date("Y-m-d h:i:s"),
				"updated_at"=> date("Y-m-d h:i:s"),
				"id_cms_users"=> Request::input('id'),
				"rating"=> Request::input('jumlah_rating'),
			]);
			if ($simpan) {
				$tabel=DB::table('proses_pentashihan_ukuran')
				->where("id_proses_pentashihan",Request::input('idtashih'))
				->first();

				if ($tabel->surat_penerbitan==null) {
					return "<script>alert('Tidak ada Surat Penerbitan')</script>";
				}else{
					response()->download(storage_path('app/'.$tabel->surat_penerbitan));
					return redirect(CRUDBooster::mainpath())->with([
						'message'=>'Terima kasih telah memberi rating kami',
						'message_type'=>'success'
					]);
				}
			}else{
				return "<script>alert('Gagal memberi rating, coba beberapa saat lagi?')</script>";
			}
		}
		
		public function postUploadBukti(){
		    $id = Request::input('id_proses_pentashihan');
 			// $nominal = str_replace('.','',Request::input('PNBP'));
 			$valid = Validator::make(Request::all(),[
	           'bukti_pembayaran'=>'mimes:pdf,jpg,jpeg,png,pneg,bmp,gif'
	        ]);		
            // dd(Request::all());
	        if($valid->fails()){
	          	return CRUDBooster::redirectback($valid->errors(),'error');
    		}else{
    		    $save['bukti_PNBP']   = Kemenag::uploadFile('bukti_pembayaran','proses_pentashihan',true);
    		  //  $save['PNBP']               = $nominal;
    		    $action = DB::table('proses_pentashihan')->where('id',$id)->update($save);
    		    
    		    if($action){
    		        return CRUDBooster::redirectback('Upload Bukti pembayaran berhasil','success');
    		    }else{
    		      return CRUDBooster::redirectback('Maaf ada yang salah coba hubungi pihak admin','error');
    		    }
    		}
		}
		
		public function getCariPnbp($id){
		    $check = DB::table("proses_pentashihan")
		    ->whereNull("deleted_at")
		    ->where("id",$id)
		    ->first();
		    
		    $url       = ($check->bukti_pembayaran != '' ? url($check->bukti_pembayaran) : '');
			$extension = Kemenag::extension($url);
			
			$url_cek       = ($check->bukti_PNBP != '' ? url($check->bukti_PNBP) : '');
			$extension_cek = Kemenag::extension($url_cek);
		    
		    $res['message'] = 'success';
		    $res['nominal'] = ($check->PNBP) ? number_format($check->PNBP,0,',','.') : '';
		    $res['eksten'] = ($extension) ? $extension : '';
		    $res['link_file'] = ($url) ? $url : '';
		    $res['eksten_cek'] = ($extension_cek) ? $extension_cek : '';
		    $res['link_file_cek'] = ($url_cek) ? $url_cek : '';
		    
		    return response()->json($res);
		}
		
		public function getDownloadPnbp($id){
		    $check = DB::table("proses_pentashihan")
		    ->whereNull("deleted_at")
		    ->where("id",$id)
		    ->first();
		    
		    if(empty($check->bukti_pembayaran)){
		        return CRUDBooster::redirectback('Maaf file tidak ditemukan','error');
		    }else{
		        return response()->download(storage_path('app/'.$check->bukti_pembayaran));
		    }
		}

		function findColor($status) {
			switch ($status) {
				case 'Registrasi Berhasil':
					$color = 'btn-success';
					break;

				case 'Naskah Diterima':
					$color = 'btn-warning';
					break;

				case 'Tidak Lolos Verifikasi':
					$color = 'btn-default';
					break;

				case 'Lolos Verifikasi':
					$color = 'btn-success';
					break;

				case 'Proses Perbaikan Naskah':
					$color = 'btn-warning';
					break;

				case 'Pentashihan Tahap Selanjutnya':
					$color = 'btn-primary';
					break;

				case 'Selesai':
					$color = 'btn-success';
					break;
				
				default:
					$color = 'btn-default';
					break;
			}
			return $color;
		}

	    public function cbInit() {
	    	self::generateNomorRegistrasi();
			$segment      = Request::segment(3);
			$id_cms_users = CRUDBooster::myId();
	    	if($segment == 'detail'){
	    		$id = Request::segment(4);
	    		$detail = DB::table('proses_pentashihan')
	    			->where('id',$id)
	    			->first();

	    		if ($detail->id_cms_users != $id_cms_users) {
		    		return redirect(CRUDBooster::mainpath())->with([
						'message'=>'Error ID '.$id,
						'message_type'=>'danger'
					]);
	    		}
	    	}

	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "proses_pentashihan";	        
			$this->title_field         = "nama_produk";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = TRUE;
			$this->button_delete       = FALSE;
			$this->button_edit         = ($segment == '' ? FALSE : TRUE);
			$this->button_detail       = ($segment == '' ? FALSE : TRUE);
			$this->button_show         = TRUE;
			$this->button_filter       = FALSE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = FALSE;	
			$this->sidebar_mode		   = 'collapse'; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
	       // $this->col[] = ['label'=>"PNBP",'name'=>'PNBP','callback'=>function($row){
	       //     return 'Rp.'.number_format($row->PNBP,0,',','.');
	       // }];
	       // $this->col[] = ['label'=>"Bukti Pembayaran",'name'=>'bukti_pembayaran','image'=>true];
			$this->col[] = array("label"=>"Nomor Registrasi","name"=>"nomor_registrasi");
			$this->col[] = array("label"=>"Jenis Permohonan","name"=>"jenis_permohonan");
			$this->col[] = array("label"=>"Nama Produk","name"=>"nama_produk");
			$this->col[] = array("label"=>"Status","name"=>"status","callback"=>function($row){
                $list_status = ['Naskah Diterima','Verifikasi Naskah','Proses Tashih','Proses Perbaikan','Terima Perbaikan','Final'];
				$color = $this->findColor($row->status);
				
				if($row->status=='Lolos Verifikasi'){
					$text_status = 'Lolos Verifikasi (Proses Pentashihan)'; 
				}else{
					$text_status = $row->status;
				}

				$html = '
					<div class="btn-group">
						<button type="button" class="btn '.$color.' btn-xs">'.$text_status.'</button>
					</div>
                ';

                return $html;
			});
			$this->col[] = array("label"=>"Tanggal Pengajuan","name"=>"created_at","callback"=>function($row){
				return date('d M Y',strtotime($row->created_at));
			});
			$this->col[] = array("label"=>"Tanggal deadline proses pentashihan","name"=>"tanggal_deadline","callback"=>function($row){
				$status = $row->status;
				if (in_array($status, ['Proses Perbaikan Naskah'])) {
					return '<span class="btn btn-xs btn-warning">Tidak ada deadline , naskah dalam masaperbaikan oleh penerbit</span>';
				} else {
					return ($row->tanggal_deadline == '' ? '' : date('d M Y',strtotime($row->tanggal_deadline)));
				}
			});
			// $this->col[] = array("label"=>"No Resi","name"=>"no_resi");
			$this->col[] = array("label"=>"Download Berkas","name"=>"id","callback"=>function($row){
				$tags = '';

					if ($row->status != 'Tidak Lolos Verifikasi') {
						$tags .= '<a href="'.CRUDBooster::mainpath('bukti-pendaftaran/'.$row->id).'" target="_blank" class="btn btn-primary btn-xs">Bukti Pendaftaran</a>';
					}

				return $tags;
			});

			$tableratings=DB::table("rating")
				->where("id_cms_users",CRUDBooster::myId())
				->count();

			if($tableratings<=0){
				$this->col[]=['label'=>'Beri Rating','name'=>'id','callback'=>function ($row)
				{
					if ($row->status=="Selesai") {
						$tags = '<a href="#" class="btn btn-info btn-xs btn-download-tashih" data-toggle="data" data-id="'.CRUDBooster::myId().'" data-idtashih="'.$row->id.'"  data-toggle="modal" data-target="#form-rating">Beri Rating</a>';					
					}else{
                        $tablerating=DB::table("rating")
                            ->where("id_cms_users",CRUDBooster::myId())
                            ->count();

						if(empty($tablerating->id_cms_users)){
							$tags ='<span class="btn btn-danger btn-xs">Proses Tashih</span>';
						}else{
							$tags ='<span class="btn btn-success btn-xs">Sudah diranting</span>';
						}
					}
					return $tags;
				}];
			}

			$this->col[] = array("label"=>'Download Koreksi',"name"=>"id","callback"=>function ($row)
			{
			    $tags = '';
				if ($row->status=="Proses Perbaikan Naskah") {
					$tags .= '<span class="btn btn-warning btn-xs btn-download-koreksi-satu" data-toggle="modal" data-target="#modalDownloadKoreksi" data-idtashih="'.$row->id.'">Download Koreksi</span>';
				}else if($row->status=="Selesai"){
					$tags ='<span class="btn btn-success btn-xs">Sudah Terbit</span>';
				}else{
					$tags ='<span class="btn btn-success btn-xs">Belum ada koreksi</span>';
				}

				return $tags;
			});

			$this->col[] = array("label"=>'Download Tanda Tashih',"name"=>"id","callback"=>function ($row)
			{
				$tablerating=DB::table("rating")
					->where("id_cms_users",CRUDBooster::myId())
					->count();

				if ($row->status=="Selesai") {
					if($tablerating>0){
						$tags = '<a href="'.CRUDBooster::mainpath('download-tansih/'.$row->id).'" class="btn btn-success btn-xs">Download Tanda Tashih</a>';
					}else{
						$tags = '<a href="#" class="btn btn-success btn-xs btn-pesan-beri-rating">Download Tanda Tashih</a>';						
					}
				}else{
					$tags ='<span class="btn btn-danger btn-xs">Tanda Tashih Belum Terbit</span>';
				}

				return $tags;
			});
			$this->col[] = ['label'=>'Tanda PNBP','name'=>'id','callback'=>function($row){
			    $check = DB::table("proses_pentashihan")
                    ->whereNull("deleted_at")
                    ->where("id",$row->id)
                    ->first();
    		    
    		    if(!empty($check->bukti_pembayaran) && !empty($check->PNBP) && !empty($check->bukti_PNBP)){
    		        $html= '<a href="javascript:void(0)" class="btn btn-xs btn-primary look-pnbp-list-satu" data-id="'.$row->id.'"><span class="fa fa-file-o"></span> Tanda PNBP</a>'; 
    		    }
    		    if(empty($check->bukti_PNBP)){
    		        $html_satu = '<a href="javascript:void(0)" class="btn btn-xs btn-primary upload-bukti" data-id="'.$row->id.'"><span class="fa fa-download"></span> Unggah Bukti Pembayaran</a>';     
    		    }
    		    return $html_satu.'&nbsp;'.$html;
			}];
			
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',Request::segment(4))
				->first();
			if ($proses_pentashihan->status == 'Registrasi Berhasil' || $proses_pentashihan->status == 'Naskah Diterima') {
				$status_verifikasi = 'Sedang Diverifikasi';
			}elseif ($proses_pentashihan->status == 'Tidak Lolos Verifikasi') {
				$status_verifikasi = 'Tidak Lolos Verifikasi';
			}else{
				$status_verifikasi = 'Lolos Verifikasi';
			}
			if ($proses_pentashihan->tanggal_verifikasi == '') {
				$date_type = 'text';
			}else{
				$date_type = 'date';
			}
			$this->form = [];
			$this->form[] = ["label"=>"Nomor Registrasi","name"=>"nomor_registrasi","type"=>"text"];
			$this->form[] = ["label"=>"Nama Produk","name"=>"nama_produk","type"=>"text"];
			$this->form[] = ["label"=>"Nama Penerbit","name"=>"id_cms_users","type"=>"select","datatable"=>"cms_users,name", "disabled"=>$id_cms_users];
			$this->form[] = ["label"=>"Penanggung Jawab","name"=>"penanggung_jawab_produk","type"=>"text"];
			$this->form[] = ["label"=>"Jenis Permohonan","name"=>"jenis_permohonan","type"=>"text"];

			if($proses_pentashihan->jenis_permohonan == 'Pembaruan/perpanjangan Surat Tanda Tashih')
			{
				$this->form[] = ['label'=>'No. Pendaftaran mushaf lama ','name'=>'id_mushaf_lama','type'=>'select','datatable'=>'proses_pentashihan,nomor_registrasi'];
			}
			else if($proses_pentashihan->jenis_permohonan == "Surat Izin Edar")
			{
				$this->form[] = ['label'=>'Negara asal mushaf ','name'=>'id_countries','type'=>'select','datatable'=>'countries,name'];
				$this->form[] = ["label"=>"Penerbit Asal","name"=>"nama_penerbit_asal","type"=>"text"];
				$this->form[] = ["label"=>"Lembaga Pentashih Asal","name"=>"nama_lembaga_pentashih_asal","type"=>"text"];
				$this->form[] = ["label"=>"Bukti Tashih Lembaga Pentashih Asal","name"=>"bukti_tashih_lembaga_pentashih_asal",'type'=>'upload'];
			}

			$this->form[] = ["label"=>"Scan Surat Pernyataan tidak ada perubahan pada master naskah","name"=>"file_surat_pernyataan","type"=>"upload"];

			$this->form[] = ["label"=>"Nama Percetakan","name"=>"nama_percetakan","type"=>"text"];
			$this->form[] = ["label"=>"Keterangan","name"=>"keterangan","type"=>"textarea"];
			$this->form[] = ["label"=>"Jenis Naskah","name"=>"id_m_jenis_naskah","type"=>"checkbox","datatable"=>"m_jenis_naskah,name"];
			$this->form[] = ["label"=>"Jenis Mushaf","name"=>"id_m_jenis_mushaf","type"=>"select2","datatable"=>"m_jenis_mushaf,name"];
			$this->form[] = ["label"=>"Penanggung Jawab Materi Tambahan","name"=>"penanggung_jawab_materi_tambahan","type"=>"textarea"];
			$this->form[] = ["label"=>"Materi Tambahan","name"=>"id_m_materi_tambahan","type"=>"checkbox","datatable"=>"m_materi_tambahan,name"];
			$this->form[] = ["label"=>"Materi Tambahan Lainnya","name"=>"materi_lainnya","type"=>"checkbox","dataenum"=>$proses_pentashihan->materi_lainnya];

			$this->form[] = ["label"=>"Tashih Internal","name"=>"tashih_internal","type"=>"radio","dataenum"=>"Sudah;Belum"];
			$this->form[] = ["label"=>"Surat Permohonan","name"=>"surat_permohonan","type"=>"upload"];
			$this->form[] = ["label"=>"Gambar Cover","name"=>"gambar_cover","type"=>"upload"];
			$this->form[] = ["label"=>"Bukti Tashih Internal","name"=>"bukti_tashih_internal","type"=>"upload"];
			$this->form[] = ["label"=>"Dokumen Naskah","name"=>"dokumen_naskah","type"=>"upload"];
			$this->form[] = ["label"=>"File Naskah","name"=>"file_naskah","type"=>"upload"];
			$this->form[] = ["label"=>"Disposisi Nomor Agenda","name"=>"disposisi_nomor_agenda","type"=>"text"];
			$this->form[] = ["label"=>"Disposisi File","name"=>"disposisi_file","type"=>"upload"];
			$this->form[] = ["label"=>"Bukti Penerimaan","name"=>"bukti_penerimaan","type"=>"upload"];
			// $this->form[] = ["label"=>"Nomor Resi","name"=>"no_resi","type"=>"text"];
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"radio","dataenum"=>"Registrasi Berhasil;Naskah Diterima;Lolos Verifikasi;Tidak Lolos Verifikasi;Proses Perbaikan Naskah;Pentashihan Tahap Selanjutnya;Selesai"];
			$this->form[] = ["label"=>"Status Verifikasi","name"=>"id","type"=>"radio","dataenum"=>$status_verifikasi];
			$this->form[] = ["label"=>"Tanggal Verifikasi","name"=>"tanggal_verifikasi",'type'=>$date_type];
			$this->form[] = ["label"=>"Tanggal Deadline","name"=>"tanggal_deadline",'type'=>$date_type];
			$this->form[] = ["label"=>"File Verifikasi","name"=>"bukti_verifikasi",'type'=>'upload'];



			$ukuran[] = ['label'=>'*Ukuran','name'=>'ukuran','type'=>'text','required'=>true];
			$ukuran[] = ['label'=>'*Oplah','name'=>'oplah','type'=>'text','required'=>true];
			// dd($ukuran);

			$this->form[] = ['label'=>'Ukuran & Oplah','name'=>'proses_pentashihan_ukuran','type'=>'child','columns'=>$ukuran,'table'=>'proses_pentashihan_ukuran','foreign_key'=>'id_proses_pentashihan'];

			$revisi[] = ['label'=>'Admin','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
			$revisi[] = ['label'=>'Tanggal Koreksi','name'=>'created_at','type'=>'hidden'];
			$revisi[] = ['label'=>'Pesan','name'=>'pesan','type'=>'textarea'];
			$revisi[] = ['label'=>'Lampiran','name'=>'Lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Revisi','name'=>'revisi','type'=>'textarea'];
			$revisi[] = ['label'=>'Lampiran Revisi','name'=>'revisi_lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Tanggal Revisi','name'=>'revisi_date','type'=>'hidden'];
			$this->form[] = ['label'=>'Riwayat Koreksi','name'=>'proses_pentashihan_koreksi','type'=>'child','columns'=>$revisi,'table'=>'proses_pentashihan_koreksi','foreign_key'=>'id_proses_pentashihan'];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, success, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
	       // $this->addaction[] = ['label'=>'Unggah Bukti Pembayaran','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-upload','color'=>'primary upload-bukti'];
	        $this->addaction[] = ['label'=>'Kirim Revisi','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-edit','color'=>'danger btn-revisi','showIf'=>"[status] == 'Proses Perbaikan Naskah'"];
			$this->addaction[] = ['label'=>'Kirim No Resi','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-envelope-o','color'=>'warning kirim-resi','showIf'=>"[status] != 'Tidak Lolos Verifikasi' && [no_resi] == ''"];
			$this->addaction[] = ['label'=>'','url'=>CRUDBooster::mainpath('detail').'/[id]','icon'=>'fa fa-eye','color'=>'primary'];
            

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	       	if ($segment == 'add' || $segment == 'edit' || $segment == 'ralat') {
	       		$js = '
	       		$(\'input[name="tashih_internal"]\').change(function(){
					let val = $(this).val();
					if (val == \'Sudah\') {
						$(\'#labelBuktiTashihInternal\').html(\'Scan Bukti Tashih Internal * <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>\');
						$(\'#buktiTashihInternal\').attr(\'required\',true)
					}else{
						$(\'#labelBuktiTashihInternal\').html(\'Scan Bukti Tashih Internal <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>\');
						$(\'#buktiTashihInternal\').attr(\'required\',false)
					}
				})

				$(\'#lainnya\').change(function(){
					if (this.checked) {
						$("#wrapper").show(500);
						$(\'#wrapperAppend\').find(\'.lainnya\').each(function(){
							$(this).attr(\'required\',true);
						})
					}else{
						$("#wrapper").hide(500);
						$(\'#wrapperAppend\').find(\'.lainnya\').each(function(){
							$(this).attr(\'required\',false);
						})
					}
				})

				$(\'input[name="materi_tambahan_lain[]"]\').change(function(){
					let checkedNum = $(\'input[name="materi_tambahan_lain[]"]:checked\').length;

					if (checkedNum === 0) {
						$(\'#penanggungJawabMateriTambahan\').attr(\'required\',false);
						$(\'#labelPenanggungJawabMateriTambahan\').html(\'Penanggung Jawab Materi Tambahan <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Dapat diisi lebih dari 1 orang, dan dipisahkan oleh tanda koma. misal: Andi Fulan, Ilham Fulan, Ridwan Fulan"></i><br><small class="text-success font-weight-bold">Isi penanggung jawab materi tambahan</small>\');
					}else{
						$(\'#labelPenanggungJawabMateriTambahan\').html(\'Penanggung Jawab Materi Tambahan * <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Dapat diisi lebih dari 1 orang, dan dipisahkan oleh tanda koma. misal: Andi Fulan, Ilham Fulan, Ridwan Fulan"></i><br><small class="text-success font-weight-bold">Isi penanggung jawab materi tambahan</small>\');
						$(\'#penanggungJawabMateriTambahan\').attr(\'required\',true);
					}
				})

				$(\'#buttonWrapper\').click(function(){
					let view = \'\';
					view += \'<div class="col-sm-6 form-group relative">\'
					view += \'<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control">\'
					view += \'<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>\'
					view += \'</div>\'

					$(\'#wrapperAppend\').append(view);
				})
				$(\'#wrapperAppend\').on(\'click\',\'.btn-remove\',function(){
					$(this).parent().remove();
				})

				$(\'.select2\').select2({});

				$(function () {
				  $(\'[data-toggle="tooltip"]\').tooltip()
				})
				
				$(\'#id_m_jenis_naskah\').on(\'change\',function(){
					let text = $(this).find(\'option:selected\').text();
					let array = text.split(" ");

					$.each(array,function(index,value){
						console.log(value)

						if (value == \'Digital\' || value == \'digital\' || value == \'DIGITAL\') {
							console.log("show")
							$(\'#fileNaskah\').parent().show(600);
							$(\'#fileNaskah\').attr(\'required\',true);
						}else{
							// $(\'#fileNaskah\').parent().hide(600);
							$(\'#fileNaskah\').attr(\'required\',false);
							$(\'#fileNaskah\').val(\'\');
						}
					})
				})
				';

			}else{
				$js = '';
			}

	        $this->script_js = $js.'
				$("#table-detail").find("tr").each(function(){
					let field = $(this).find("td:first-child").text();
					if(field == "Status Verifikasi"){
						$(this).find("td:last-child").find(".badge").html("'.$status_verifikasi.'");
					}
				})
				
				$(".btn-pesan-beri-rating").click(function(){
				   return swal("Alert","Beri rating kami dahulu","warning"); 
				});
	        ';


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '
				<script type="text/javascript">
		        	var token = "'.csrf_token().'";
		        	var asset = "'.asset('/').'";
		        	var mainpath = "'.CRUDBooster::mainpath().'";
			    </script>
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
			$this->load_js[] = asset("vendor/crudbooster/assets/select2/dist/js/select2.full.min.js");
			$this->load_js[] = asset("js/admin/listtashih.js");
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = '
				.delete-data{
	        		cursor:pointer;
	        	}
				embed{
					width:100%;
					height:350px;
					border:none;
					cursor:pointer;
				}
				.sk-fading-circle {
				  margin: 100px auto;
				  width: 40px;
				  height: 40px;
				  position: relative;
				}

				.sk-fading-circle .sk-circle {
				  width: 100%;
				  height: 100%;
				  position: absolute;
				  left: 0;
				  top: 0;
				}

				.sk-fading-circle .sk-circle:before {
				  content: "";
				  display: block;
				  margin: 0 auto;
				  width: 15%;
				  height: 15%;
				  background-color: #333;
				  border-radius: 100%;
				  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
				          animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
				}
				.sk-fading-circle .sk-circle2 {
				  -webkit-transform: rotate(30deg);
				      -ms-transform: rotate(30deg);
				          transform: rotate(30deg);
				}
				.sk-fading-circle .sk-circle3 {
				  -webkit-transform: rotate(60deg);
				      -ms-transform: rotate(60deg);
				          transform: rotate(60deg);
				}
				.sk-fading-circle .sk-circle4 {
				  -webkit-transform: rotate(90deg);
				      -ms-transform: rotate(90deg);
				          transform: rotate(90deg);
				}
				.sk-fading-circle .sk-circle5 {
				  -webkit-transform: rotate(120deg);
				      -ms-transform: rotate(120deg);
				          transform: rotate(120deg);
				}
				.sk-fading-circle .sk-circle6 {
				  -webkit-transform: rotate(150deg);
				      -ms-transform: rotate(150deg);
				          transform: rotate(150deg);
				}
				.sk-fading-circle .sk-circle7 {
				  -webkit-transform: rotate(180deg);
				      -ms-transform: rotate(180deg);
				          transform: rotate(180deg);
				}
				.sk-fading-circle .sk-circle8 {
				  -webkit-transform: rotate(210deg);
				      -ms-transform: rotate(210deg);
				          transform: rotate(210deg);
				}
				.sk-fading-circle .sk-circle9 {
				  -webkit-transform: rotate(240deg);
				      -ms-transform: rotate(240deg);
				          transform: rotate(240deg);
				}
				.sk-fading-circle .sk-circle10 {
				  -webkit-transform: rotate(270deg);
				      -ms-transform: rotate(270deg);
				          transform: rotate(270deg);
				}
				.sk-fading-circle .sk-circle11 {
				  -webkit-transform: rotate(300deg);
				      -ms-transform: rotate(300deg);
				          transform: rotate(300deg); 
				}
				.sk-fading-circle .sk-circle12 {
				  -webkit-transform: rotate(330deg);
				      -ms-transform: rotate(330deg);
				          transform: rotate(330deg); 
				}
				.sk-fading-circle .sk-circle2:before {
				  -webkit-animation-delay: -1.1s;
				          animation-delay: -1.1s; 
				}
				.sk-fading-circle .sk-circle3:before {
				  -webkit-animation-delay: -1s;
				          animation-delay: -1s; 
				}
				.sk-fading-circle .sk-circle4:before {
				  -webkit-animation-delay: -0.9s;
				          animation-delay: -0.9s; 
				}
				.sk-fading-circle .sk-circle5:before {
				  -webkit-animation-delay: -0.8s;
				          animation-delay: -0.8s; 
				}
				.sk-fading-circle .sk-circle6:before {
				  -webkit-animation-delay: -0.7s;
				          animation-delay: -0.7s; 
				}
				.sk-fading-circle .sk-circle7:before {
				  -webkit-animation-delay: -0.6s;
				          animation-delay: -0.6s; 
				}
				.sk-fading-circle .sk-circle8:before {
				  -webkit-animation-delay: -0.5s;
				          animation-delay: -0.5s; 
				}
				.sk-fading-circle .sk-circle9:before {
				  -webkit-animation-delay: -0.4s;
				          animation-delay: -0.4s;
				}
				.sk-fading-circle .sk-circle10:before {
				  -webkit-animation-delay: -0.3s;
				          animation-delay: -0.3s;
				}
				.sk-fading-circle .sk-circle11:before {
				  -webkit-animation-delay: -0.2s;
				          animation-delay: -0.2s;
				}
				.sk-fading-circle .sk-circle12:before {
				  -webkit-animation-delay: -0.1s;
				          animation-delay: -0.1s;
				}

				@-webkit-keyframes sk-circleFadeDelay {
				  0%, 39%, 100% { opacity: 0; }
				  40% { opacity: 1; }
				}

				@keyframes sk-circleFadeDelay {
				  0%, 39%, 100% { opacity: 0; }
				  40% { opacity: 1; } 
				}
				.pointer > *{
					cursor:pointer;
				}
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        if (Request::segment(3) == 'ralat' || Request::segment(3) == 'add' || Request::segment(3) == 'edit') {
	        	$this->load_css[] = asset("css/admin/custom.css");
	        }
	        $this->load_css[] = asset("vendor/crudbooster/assets/select2/dist/css/select2.min.css");
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->where('proses_pentashihan.status','!=','Selesai'); 
	        $query->where('proses_pentashihan.id_cms_users',CRUDBooster::myId()); 

	        if (Request::get('id') != '') {
	        	$query->where('id',Request::get('id'));
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 
	    
	    public function postResi(){
	    	$id_proses_pentashihan = Request::input('id_proses_pentashihan');

			$save['updated_at']   = Kemenag::now();
			$save['no_resi']      = Request::input('no_resi');
			$act = DB::table('proses_pentashihan')->where('id',$id_proses_pentashihan)->update($save);

			if ($act) {
				$id_cms_users = DB::table('cms_users')
					->where('id_cms_privileges',1)
					->pluck('id');

				$config['content']      = "Update nomor Resi ".Request::input('no_resi');
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id_proses_pentashihan);
				$config['id_cms_users'] = $id_cms_users; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Update nomor Resi ".Request::input('no_resi');
				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id_proses_pentashihan);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with([
					'message'=>'Nomor resi berhasil di update',
					'message_type'=>'success'
				]);
			}else{
				return redirect()->back()->with([
					'message'=>'Nomor resi gagal di update',
					'message_type'=>'warning'
				]);
			}
	    }

	    public function getAdd(){
	    	//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			
			// dd(CRUDBooster::myId());

			$jenis_naskah = DB::table('m_jenis_naskah')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();
			$jenis_mushaf = DB::table('m_jenis_mushaf')
				->whereNull('deleted_at')
				->orderBy('name','DESC')
				->get();
			$materi_tambahan = DB::table('m_materi_tambahan')
				->whereNull('deleted_at')
				->get();

			$countries = DB::table('countries')
				->get();

			$mushaf_lama = DB::table('proses_pentashihan')
			->where('id_cms_users',CRUDBooster::myId())
			->whereNull('deleted_at')
			->orderBy('id','desc')
			->get();

			$data               = [];
			$data['page_title'] = 'Add Tashih';
			$data['action']     = 'insert';
			$data['token']      = csrf_token();

			$data['jenis_naskah']                       = $jenis_naskah;
			$data['jenis_mushaf']                       = $jenis_mushaf;
			$data['countries']                       = $countries;
			$data['materi_tambahan']                    = $materi_tambahan;
			$data['mushaf_lama']            			= $mushaf_lama;
			$data['proses_pentashihan']                 = null;
			$data['proses_pentashihan_materi_tambahan'] = null;
			$data['proses_pentashihan_ukuran']          = null;
			$data['id_cms_users']          = CRUDBooster::myId();
			$data['cms_users']          = DB::table('cms_users')
											->where('id',CRUDBooster::myId())
											->first();
			$data['message'] = DB::table('message_alert')->where('flag','pendaftaran-mushaf')->first();
			$this->cbView('admin/admin_proses_pentashihan',$data);
	    }

	    public function getSetting(){
	    	$proses_pentashihan = DB::table('proses_pentashihan')
				->where(function($q){
					$q->orWhere('nomor_registrasi','');
					$q->orWhereNull('nomor_registrasi');

				})
				->get();

			foreach ($proses_pentashihan as $row) {
				$year = date('Y',strtotime($row->created_at));

				$check_proses_pentashihan = DB::table('proses_pentashihan')
					->where(function($q){
						$q->orWhereNotNull('nomor_registrasi');
						$q->orWhere('nomor_registrasi','!=','');
					})
					->whereYear('created_at',$year)
					->orderBy('id','DESC')
					->whereNull('deleted_at')
					->first();

				if (empty($check_proses_pentashihan)) {
					$nomor_registrasi = '1/'.$year;
				}else{
					$slice = explode('/', $check_proses_pentashihan->nomor_registrasi);
					$nomor = $slice[0]+1;
					$nomor_registrasi = $nomor.'/'.$year;
				}

				$save['updated_at']       = Kemenag::now();
				$save['nomor_registrasi'] = $nomor_registrasi;

				DB::table('proses_pentashihan')->where('id',$row->id)->update($save);
			}
	    }

	    public function postInsert(){
			$request              = Request::all();
			$now                  = Kemenag::now();
			$materi_lainnya       = Request::input('materi_lainnya');
			$materi_tambahan_lain = Request::input('materi_tambahan_lain');
			$jenis_naskah         = Request::input('id_m_jenis_naskah');
			$oplah                = Request::input('oplah');
			$ukuran               = Request::input('ukuran');
			$id_cms_users         = CRUDBooster::myId();
			$year                 = date('Y');
			
			$valid = Validator::make(Request::all(),[
				'id_m_jenis_naskah'=>'required',
				'new_file_surat_pernyataan'=>'nullable|file|max:2000',
				'new_surat_permohonan'=>'required|file|max:2000',
				'new_gambar_cover'=>'required|file|max:2000',
				'new_dokumen_naskah'=>'required|file|max:2000'
			],
			[
			    'id_m_jenis_naskah.required'=>'Jenis naskah field is required.'
			]
			);
			if($valid->fails()){
    			return redirect()->back()->with([
    				'message'=>$valid->errors(),
    				'message_type'=>'error'
    			]);
    		}else{
    		    if ($id_cms_users == '') {
    				return redirect('/')->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}
    
    			$check_proses_pentashihan = DB::table('proses_pentashihan')
    				->where(function($q){
    					$q->orWhereNotNull('nomor_registrasi');
    					$q->orWhere('nomor_registrasi','!=','');
    				})
    				->whereYear('created_at',$year)
    				->orderBy('id','DESC')
    				->whereNull('deleted_at')
    				->first();
    
    			if (empty($check_proses_pentashihan)) {
    				$nomor_registrasi = '1/'.$year;
    			}else{
    				$slice = explode('/', $check_proses_pentashihan->nomor_registrasi);
    				$nomor = $slice[0]+1;
    				$nomor_registrasi = $nomor.'/'.$year;
    			}
	
				$new_surat_pernyataan = Kemenag::uploadFile('new_surat_pernyataan' , 'surat_pernyataan', true);
				
    			$save['created_at']           = $now;
    			$save['updated_at']           = $now;
    			$save['nomor_registrasi']     = $nomor_registrasi;
    			$save['id_cms_users']         = $id_cms_users;
    			$save['status_penerimaan']    = 0;
    			$save['id_m_materi_tambahan'] = ($materi_tambahan_lain != '' ? implode(';', $materi_tambahan_lain) : '');
				$save['id_m_jenis_naskah']    = ($jenis_naskah != '' ? implode(';', $jenis_naskah) : '');

    			foreach ($request as $key => $value) {
    				if ($key == 'materi_lainnya' || $key == 'materi_tambahan_lain' || $key == '_token' || $key == 'lainnya' || $key == 'surat_permohonan' || $key == 'gambar_cover' || $key == 'dokumen_naskah' || $key == 'bukti_tashih_internal' || $key == 'file_naskah' || $key == 'oplah' || $key == 'ukuran' || $key == 'id_m_jenis_naskah') continue;
    
    				if ($key == 'new_surat_permohonan' || $key == 'new_file_surat_pernyataan' || $key == 'new_gambar_cover' || $key == 'new_dokumen_naskah' || $key == 'new_bukti_tashih_internal' || $key == 'new_file_naskah') {
    					if (Request::hasFile($key)) {
    						$save[str_replace('new_', '', $key)] = Kemenag::uploadFile($key,'proses_pentashihan',true);
    					}
    				}else{
    					$save[$key] = $value;
    				}
    			}
				unset($save['mushaf_lama_id']);
    			$id = DB::table('proses_pentashihan')->insertGetId($save);
    			
    			if ($id) {

    				for ($i=0; $i < count($materi_lainnya); $i++) {
    					if($materi_lainnya[$i] == '' || $materi_lainnya[$i] == ' ' || $materi_lainnya[$i] == "\n") continue;
    
    					$save_tambahan['created_at']            = $now;
    					$save_tambahan['updated_at']            = $now;
    					$save_tambahan['name']                  = $materi_lainnya[$i];
    					$save_tambahan['id_proses_pentashihan'] = $id;
    					DB::table('proses_pentashihan_materi_tambahan')->insert($save_tambahan);
    				}
    
    				for ($i=0; $i < count($oplah) ; $i++) { 
    					$save_ukuran['created_at']            = $now;
    					$save_ukuran['updated_at']            = $now;
    					$save_ukuran['id_proses_pentashihan'] = $id;
    					$save_ukuran['ukuran']                = $ukuran[$i];
    					$save_ukuran['oplah']                 = $oplah[$i];
    					DB::table('proses_pentashihan_ukuran')->insert($save_ukuran);
    				}
    
    				$cms_users = DB::table('cms_users')
    					->where('id',$save['id_cms_users'])
    					->first();
    
    				$data_email['name'] = $cms_users->name;
    				CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'info_pendaftaran_mushaf']);
    
    				$id_cms_users = DB::table('cms_users')
    					->where('id_cms_privileges',1)
    					->pluck('id');
    
    				$config['content']      = "Request Proses Pentashihan Baru";
    				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
    				$config['id_cms_users'] = $id_cms_users; //This is an array of id users
    				CRUDBooster::sendNotification($config);
    
    				$config['content']      = "Registrasi Pentashihan Berhasil";
    				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
    				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
    				CRUDBooster::sendNotification($config);
    
    				$materi_tambahan_lain = DB::table('proses_pentashihan_materi_tambahan')
    		    		->where('id_proses_pentashihan',$id)
    		    		->orderBy('id','ASC')
    		    		->pluck('name');
    				$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
    				$materi_lainnya = str_replace(',',';',$format);
    
    				$save_materi_lainnya['updated_at']     = Kemenag::now();
    				$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;
    				DB::table('proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
    
    				$to_sender=DB::table("cms_users")
    					->where('id',$id_cms_users)
    					->first();
    
    				$to_send=$to_sender->email;
    
    				\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
    				\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
    				\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
    				\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
    				\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
    				Mail::send('mail.send-status-regis',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>'Berhasil Registrasi'],function ($pesan) use ($to_send){
    					$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
    					$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
    				});
    				
    				return redirect(CRUDBooster::mainpath())->with([
    					'message'=>'Registrasi anda berhasil, silahkan cetak bukti pendaftaran mushaf <a href="'.CRUDBooster::mainpath('bukti-pendaftaran/'.$id).'" target="_blank">disini</a>',
    					'message_type'=>'success'
    				]);
    			}else{
    
    				return redirect()->back()->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}   
    		}
	    }
	    public function postInsertSave(){
			$now                  = Kemenag::now();
			$id_cms_users         = CRUDBooster::myId();
			
			try{
    		    if ($id_cms_users == '') {
					return response()->json(['status' => 500, 'message' => 'Something went wrong', 'flag' => 'error'], 200); 
    			}

				$temp = DB::table('temp_proses_pentashihan')
					->select('temp_proses_pentashihan.*','cms_users.name as cms_users_name','m_jenis_naskah.name as m_jenis_naskah_name')
					->leftJoin('cms_users','temp_proses_pentashihan.id_cms_users','cms_users.id')
					->leftJoin('m_jenis_naskah','temp_proses_pentashihan.id_m_jenis_naskah','m_jenis_naskah.id')
					->where('temp_proses_pentashihan.id_cms_users',$id_cms_users)
					->orderBy('temp_proses_pentashihan.id','desc')
					->first();

				if ($temp->id != null)
				{

    			$save['created_at']           = $now;
    			$save['updated_at']           = $now;
    			$save['nomor_registrasi']     = $temp->nomor_registrasi;
    			$save['jenis_permohonan']     = $temp->jenis_permohonan;
    			$save['id_m_jenis_naskah']     = $temp->id_m_jenis_naskah;
    			$save['file_aplikasi']     = $temp->file_aplikasi;
    			$save['id_m_jenis_mushaf']     = $temp->id_m_jenis_mushaf;
    			$save['id_m_materi_tambahan']     = $temp->id_m_materi_tambahan;
    			$save['materi_lainnya']     = $temp->materi_lainnya;
    			$save['penanggung_jawab_produk']     = $temp->penanggung_jawab_produk;
    			$save['nama_produk']     = $temp->nama_produk;
    			$save['ukuran']     = $temp->ukuran;
    			$save['oplah']     = $temp->oplah;
    			$save['nama_percetakan']     = $temp->nama_percetakan;
    			$save['keterangan']     = $temp->keterangan;
    			$save['tashih_internal']     = $temp->tashih_internal;
    			$save['penanggung_jawab_materi_tambahan']     = $temp->penanggung_jawab_materi_tambahan;
    			$save['surat_permohonan']     = $temp->surat_permohonan;
    			$save['gambar_cover']     = $temp->gambar_cover;
    			$save['dokumen_naskah']     = $temp->dokumen_naskah;
    			$save['bukti_tashih_internal']     = $temp->bukti_tashih_internal;
    			// $save['status']     = $temp->status;
    			$save['disposisi_nomor_agenda']     = $temp->disposisi_nomor_agenda;
    			$save['disposisi_file']     = $temp->disposisi_file;
    			$save['file_naskah']     = $temp->file_naskah;
    			$save['bukti_penerimaan']     = $temp->bukti_penerimaan;
				$save['nama_penerima']     = $temp->nama_penerima;
				$save['tanggal_diterima']     = $temp->tanggal_diterima;
				$save['status_penerimaan']     = $temp->status_penerimaan;
				$save['tanggal_verifikasi']     = $temp->tanggal_verifikasi;
				$save['tanggal_deadline']     = $temp->tanggal_deadline;
				$save['estimasi']     = $temp->estimasi;
				$save['bukti_verifikasi']     = $temp->bukti_verifikasi;
				$save['nomor_tanda_tashih']     = $temp->nomor_tanda_tashih;
				$save['kode_tanda_tashih']     = $temp->kode_tanda_tashih;
				$save['surat_penerbitan']     = $temp->surat_penerbitan;
				$save['scan_tanda_tashih']     = $temp->scan_tanda_tashih;
				$save['no_resi']     = $temp->no_resi;
				$save['surat_pernyataan']     = $temp->surat_pernyataan;
				$save['submit']     = $temp->submit;
				$save['PNBP']     = $temp->PNBP;
				$save['bukti_pembayaran']     = $temp->bukti_pembayaran;
				$save['nominal_PNBP']     = $temp->nominal_PNBP;
				$save['bukti_PNBP']     = $temp->bukti_PNBP;
				$save['status_PNBP']     = $temp->status_PNBP;
				$save['file_surat_pernyataan']     = $temp->file_surat_pernyataan;
				$save['id_cms_users']     = $temp->id_cms_users;

				$save['id_mushaf_lama']     = $temp->id_mushaf_lama;
				$save['id_proses_pentashihan_ukuran']     = $temp->id_proses_pentashihan_ukuran;
				$save['id_countries']     = $temp->id_countries;
				$save['nama_penerbit_asal']     = $temp->nama_penerbit_asal;
				$save['bukti_tashih_lembaga_pentashih_asal']     = $temp->bukti_tashih_lembaga_pentashih_asal;

    			$id = DB::table('proses_pentashihan')->insertGetId($save);


    			if ($id) {

					$materi_lainnya = DB::table('temp_proses_pentashihan_materi_tambahan')->where('id_proses_pentashihan',$temp->id)->get();

					if(count($materi_lainnya) > 0)
					{
						foreach($materi_lainnya as $val)
						{
							$save_tambahan['created_at']            = $now;
							$save_tambahan['updated_at']            = $now;
							$save_tambahan['name']                  = $val->name;
							$save_tambahan['id_proses_pentashihan'] = $id;
							DB::table('proses_pentashihan_materi_tambahan')->insert($save_tambahan);	
						}
					}

					$oplah = DB::table('temp_proses_pentashihan_ukuran')->where('id_proses_pentashihan',$temp->id)->get();
					
					if(count($materi_lainnya) > 0)
					{
						foreach($materi_lainnya as $val)
						{

							$save_ukuran['created_at']            = $now;
							$save_ukuran['updated_at']            = $now;
							$save_ukuran['id_proses_pentashihan'] = $id;
							$save_ukuran['ukuran']                = $val->ukuran;
							$save_ukuran['oplah']                 = $val->oplah;
							DB::table('proses_pentashihan_ukuran')->insert($save_ukuran);
						}
    				}
    
    				$cms_users = DB::table('cms_users')
    					->where('id',$save['id_cms_users'])
    					->first();
    				$data_email['name'] = $cms_users->name;
    				CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'info_pendaftaran_mushaf']);
    
    				$id_cms_users = DB::table('cms_users')
    					->where('id_cms_privileges',1)
    					->pluck('id');
    
    				$config['content']      = "Request Proses Pentashihan Baru";
    				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
    				$config['id_cms_users'] = $id_cms_users; //This is an array of id users
    				CRUDBooster::sendNotification($config);
    
    				$config['content']      = "Registrasi Pentashihan Berhasil";
    				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
    				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
    				CRUDBooster::sendNotification($config);
    
    				$materi_tambahan_lain = DB::table('proses_pentashihan_materi_tambahan')
    		    		->where('id_proses_pentashihan',$id)
    		    		->orderBy('id','ASC')
    		    		->pluck('name');
    				$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
    				$materi_lainnya = str_replace(',',';',$format);
    
    				$save_materi_lainnya['updated_at']     = Kemenag::now();
    				$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;
    				DB::table('proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
    
    				$to_sender=DB::table("cms_users")
    					->where('id',$id_cms_users)
    					->first();
    
    				$to_send=$to_sender->email;
    
    				\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
    				\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
    				\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
    				\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
    				\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
    				Mail::send('mail.send-status-regis',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>'Berhasil Registrasi'],function ($pesan) use ($to_send){
    					$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
    					$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
    				});

					$temp_proses = DB::table('temp_proses_pentashihan')
					->where('id_cms_users',CRUDBooster::myId())
					->get();
	
					foreach($temp_proses as $val)
					{
						// DB::table('temp_proses_pentashihan_materi_tambahan')->where('id_proses_pentashihan',$val->id)->delete();
						// DB::table('temp_proses_pentashihan_ukuran')->where('id_proses_pentashihan',$val->id)->delete();
						// DB::table('temp_proses_pentashihan_materi_tambahan')->where('id',$val->id)->delete();
					}

					$redirect = CRUDBooster::mainpath('bukti-pendaftaran/'.$id);

					return response()->json(['status' => 200, 'message' => 'Selamat, Anda berhasil mendaftarkan mushaf Anda. Silakan mengunduh bukti pendaftaran mushaf pada link di bawah ini. Lalu cetak serta tandatangani dan stempel bukti pendaftaran tersebut. Kemudian kirim print out mushaf yang telah didaftarakan dan bukti pendaftaran yang telah ditandatangani dan distempel ke kantor LPMQ.'
					,'redirect' => $redirect], 200); 
				}
				else{
    
					return response()->json(['status' => 500, 'message' => 'Something went wrong', 'flag' => 'error'], 200); 
    			}
				}else{
					return response()->json(['status' => 500, 'message' => 'Empty Temp','flag' => 'empty_temp'], 200); 
    			}   
    		}
			catch (\Exception $e)
			{
				return response()->json(['status' => 500, 'message' => $e->getMessage(), 'flag' => 'error'], 200); 
			}
	    }

	    public function postTempInsert(){
			$request              = Request::all();
			$now                  = Kemenag::now();
			$materi_lainnya       = Request::input('materi_lainnya');
			$materi_tambahan_lain = Request::input('materi_tambahan_lain');
			$jenis_naskah         = Request::input('id_m_jenis_naskah');
			$oplah                = Request::input('oplah');
			$ukuran               = Request::input('ukuran');
			$id_cms_users         = CRUDBooster::myId();
			$year                 = date('Y');

			$fields = [
				'id_m_jenis_naskah'=>'required',
				'new_file_surat_pernyataan'=>'nullable|file|max:500',
				'new_surat_permohonan'=>'required|file|max:500',
				'new_gambar_cover'=>'required|file|max:500',
				'new_dokumen_naskah'=>'required|file|max:500'
			];

			if($request['jenis_permohonan'] == "Surat Izin Edar")
			{
				$fields_other = [
					'id_countries' => 'required',
					'nama_penerbit_asal' => 'required',
					'nama_lembaga_pentashih_asal' => 'required'
					,'bukti_tashih_lembaga_pentashih_asal' => 'required|file|max:500'
				];

				$fields = array_merge($fields,$fields_other);
			}
			else if($request['jenis_permohonan'] == "Pembaruan/perpanjangan Surat Tanda Tashih")
			{
				$fields_other = [
					'id_mushaf_lama' => 'required'
				];

				$fields = array_merge($fields,$fields_other);
			}
		
			$custom_message = 			[
			    'id_m_jenis_naskah.required'=>'Jenis naskah field is required.'
			];

			$valid = Validator::make(Request::all(),
				$fields,
				$custom_message
			);

			$message = $valid->messages();
            $message_all = $message->all();

			if($valid->fails()){
    			return redirect()->back()->with([
    				'message'=>implode(', ', $message_all),
    				'message_type'=>'error'
    			]);
    		}else{
    		    if ($id_cms_users == '') {
    				return redirect('/')->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}
    
				$temp_proses = DB::table('temp_proses_pentashihan')
				->where('id_cms_users',$id_cms_users)
				->get();

				foreach($temp_proses as $val)
				{
					// DB::table('temp_proses_pentashihan_materi_tambahan')->where('id_proses_pentashihan',$val->id)->delete();
					// DB::table('temp_proses_pentashihan_ukuran')->where('id_proses_pentashihan',$val->id)->delete();
					// DB::table('temp_proses_pentashihan_materi_tambahan')->where('id',$val->id)->delete();
				}

    			$check_proses_pentashihan = DB::table('proses_pentashihan')
    				->where(function($q){
    					$q->orWhereNotNull('nomor_registrasi');
    					$q->orWhere('nomor_registrasi','!=','');
    				})
    				->whereYear('created_at',$year)
    				->orderBy('id','DESC')
    				->whereNull('deleted_at')
    				->first();
    
    			if (empty($check_proses_pentashihan)) {
    				$nomor_registrasi = '1/'.$year;
    			}else{
    				$slice = explode('/', $check_proses_pentashihan->nomor_registrasi);
    				$nomor = $slice[0]+1;
    				$nomor_registrasi = $nomor.'/'.$year;
    			}
	
				$new_surat_pernyataan = Kemenag::uploadFile('new_surat_pernyataan' , 'surat_pernyataan', true);
				
    			$save['created_at']           = $now;
    			$save['updated_at']           = $now;
    			$save['nomor_registrasi']     = $nomor_registrasi;
    			$save['id_cms_users']         = $id_cms_users;
    			$save['status_penerimaan']    = 0;
    			$save['id_m_materi_tambahan'] = ($materi_tambahan_lain != '' ? implode(';', $materi_tambahan_lain) : '');
				$save['id_m_jenis_naskah']    = ($jenis_naskah != '' ? implode(';', $jenis_naskah) : '');

    			foreach ($request as $key => $value) {
    				if ($key == 'materi_lainnya' || $key == 'materi_tambahan_lain' || $key == '_token' || $key == 'lainnya' || $key == 'surat_permohonan' || $key == 'gambar_cover' || $key == 'dokumen_naskah' || $key == 'bukti_tashih_internal' || $key == 'file_naskah' || $key == 'oplah' || $key == 'ukuran' || $key == 'id_m_jenis_naskah') continue;
    
    				if ($key == 'new_surat_permohonan' || $key == 'bukti_tashih_lembaga_pentashih_asal' || $key == 'new_file_surat_pernyataan' || $key == 'new_gambar_cover' || $key == 'new_dokumen_naskah' || $key == 'new_bukti_tashih_internal' || $key == 'new_file_naskah') {
    					if (Request::hasFile($key)) {
    						$save[str_replace('new_', '', $key)] = Kemenag::uploadFile($key,'proses_pentashihan',true);
    					}
    				}else{
    					$save[$key] = $value;
    				}
    			}
				// unset($save['mushaf_lama_id']);
    			$id = DB::table('temp_proses_pentashihan')->insertGetId($save);
				// dd($save['id_cms_users']);
    			
    			if ($id) {

    				for ($i=0; $i < count($materi_lainnya); $i++) {
    					if($materi_lainnya[$i] == '' || $materi_lainnya[$i] == ' ' || $materi_lainnya[$i] == "\n") continue;
    
    					$save_tambahan['created_at']            = $now;
    					$save_tambahan['updated_at']            = $now;
    					$save_tambahan['name']                  = $materi_lainnya[$i];
    					$save_tambahan['id_proses_pentashihan'] = $id;
    					DB::table('temp_proses_pentashihan_materi_tambahan')->insert($save_tambahan);
    				}
    
    				for ($i=0; $i < count($oplah) ; $i++) { 
    					$save_ukuran['created_at']            = $now;
    					$save_ukuran['updated_at']            = $now;
    					$save_ukuran['id_proses_pentashihan'] = $id;
    					$save_ukuran['ukuran']                = $ukuran[$i];
    					$save_ukuran['oplah']                 = $oplah[$i];
    					DB::table('temp_proses_pentashihan_ukuran')->insert($save_ukuran);
    				}
    
    				$cms_users = DB::table('cms_users')
    					->where('id',$save['id_cms_users'])
    					->first();
    
    				$id_cms_users = DB::table('cms_users')
    					->where('id_cms_privileges',1)
    					->pluck('id');
    
    				$materi_tambahan_lain = DB::table('temp_proses_pentashihan_materi_tambahan')
    		    		->where('id_proses_pentashihan',$id)
    		    		->orderBy('id','ASC')
    		    		->pluck('name');
    				$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
    				$materi_lainnya = str_replace(',',';',$format);
    
    				$save_materi_lainnya['updated_at']     = Kemenag::now();
    				$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;

    				DB::table('temp_proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
    
    				return redirect(CRUDBooster::mainpath('preview-insert'))->with([
    					'message'=>'Registrasi anda berhasil, silahkan cetak bukti pendaftaran mushaf <a href="'.CRUDBooster::mainpath('bukti-pendaftaran/'.$id).'" target="_blank">disini</a>',
    					'message_type'=>'success'
    
    				// return redirect(CRUDBooster::mainpath('preview-insert'))->with([
    				// 	'message'=>'Registrasi anda berhasil, silahkan cetak bukti pendaftaran mushaf <a href="'.CRUDBooster::mainpath('bukti-pendaftaran/'.$id).'" target="_blank">disini</a>',
    				// 	'message_type'=>'success'
    				]);
    			}else{
    
    				return redirect()->back()->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}   
    		}
	    }
	    public function postTempEdit(){
			$request              = Request::all();
			$now                  = Kemenag::now();
			$materi_lainnya       = Request::input('materi_lainnya');
			$materi_tambahan_lain = Request::input('materi_tambahan_lain');
			$jenis_naskah         = Request::input('id_m_jenis_naskah');
			$oplah                = Request::input('oplah');
			$ukuran               = Request::input('ukuran');
			$id_cms_users         = CRUDBooster::myId();
			$year                 = date('Y');
			
			$fields = [
				'id_m_jenis_naskah'=>'required',
				'jenis_permohonan'=>'required',
				// 'new_file_surat_pernyataan'=>'nullable|file|max:2000',
				// 'new_surat_permohonan'=>'required|file|max:2000',
				// 'new_gambar_cover'=>'required|file|max:2000',
				// 'new_dokumen_naskah'=>'required|file|max:2000'
				'temp_id'=>'required'
			];

			if($request['jenis_permohonan'] == "Surat Izin Edar")
			{
				$fields_other = [
					'id_countries' => 'required',
					'nama_penerbit_asal' => 'required',
					'nama_lembaga_pentashih_asal' => 'required'
					// ,'bukti_tashih_lembaga_pentashih_asal' => 'required|file|max:500'
				];

				$fields = array_merge($fields,$fields_other);
			}
		
			$custom_message = 			[
			    'id_m_jenis_naskah.required'=>'Jenis naskah field is required.'
			];

			$valid = Validator::make(Request::all(),
				$fields,
				$custom_message
			);

			$message = $valid->messages();
            $message_all = $message->all();

			if($valid->fails()){
    			return redirect()->back()->with([
    				'message'=>implode(', ', $message_all),
    				'message_type'=>'error'
    			]);
    		}else{
    		    if ($id_cms_users == '') {
    				return redirect('/')->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}
    
    			$save['created_at']           = $now;
    			$save['updated_at']           = $now;
    			$save['nomor_registrasi']     = $nomor_registrasi;
    			$save['id_cms_users']         = $id_cms_users;
    			$save['status_penerimaan']    = 0;
    			$save['id_m_materi_tambahan'] = ($materi_tambahan_lain != '' ? implode(';', $materi_tambahan_lain) : '');
				$save['id_m_jenis_naskah']    = ($jenis_naskah != '' ? implode(';', $jenis_naskah) : '');

    			foreach ($request as $key => $value) {

    				if ($key == 'materi_lainnya' || $key == 'materi_tambahan_lain' || $key == '_token' || $key == 'lainnya' || $key == 'surat_permohonan' || $key == 'gambar_cover' || $key == 'dokumen_naskah' || $key == 'bukti_tashih_internal' || $key == 'file_naskah' || $key == 'oplah' || $key == 'ukuran' || $key == 'id_m_jenis_naskah') continue;
    
    				if ($key == 'new_surat_permohonan' || $key == 'bukti_tashih_lembaga_pentashih_asal' || $key == 'new_file_surat_pernyataan' || $key == 'new_gambar_cover' || $key == 'new_dokumen_naskah' || $key == 'new_bukti_tashih_internal' || $key == 'new_file_naskah') {
    					if (Request::hasFile($key)) {
    						$save[str_replace('new_', '', $key)] = Kemenag::uploadFile($key,'proses_pentashihan',true);
    					}
    				}else{
    					$save[$key] = $value;
    				}
    			}

				// unset($save['mushaf_lama_id']);
				unset($save['temp_id']);
    			// $id = DB::table('temp_proses_pentashihan')->insertGetId($save);
				$id = Request::input('temp_id');
				DB::table('temp_proses_pentashihan')->where('id',$id)->update($save);

				// var_dump($save);
				// die();


    			
    			if ($id) {

					DB::table('temp_proses_pentashihan_materi_tambahan')->where('id_proses_pentashihan',$id)->delete();
					DB::table('temp_proses_pentashihan_ukuran')->where('id_proses_pentashihan',$id)->delete();
					DB::table('temp_proses_pentashihan_materi_tambahan')->where('id',$id)->delete();

    				for ($i=0; $i < count($materi_lainnya); $i++) {
    					if($materi_lainnya[$i] == '' || $materi_lainnya[$i] == ' ' || $materi_lainnya[$i] == "\n") continue;
    
    					$save_tambahan['created_at']            = $now;
    					$save_tambahan['updated_at']            = $now;
    					$save_tambahan['name']                  = $materi_lainnya[$i];
    					$save_tambahan['id_proses_pentashihan'] = $id;
    					DB::table('temp_proses_pentashihan_materi_tambahan')->insert($save_tambahan);
    				}
    
    				for ($i=0; $i < count($oplah) ; $i++) { 
    					$save_ukuran['created_at']            = $now;
    					$save_ukuran['updated_at']            = $now;
    					$save_ukuran['id_proses_pentashihan'] = $id;
    					$save_ukuran['ukuran']                = $ukuran[$i];
    					$save_ukuran['oplah']                 = $oplah[$i];
    					DB::table('temp_proses_pentashihan_ukuran')->insert($save_ukuran);
    				}
    
    				$cms_users = DB::table('cms_users')
    					->where('id',$save['id_cms_users'])
    					->first();
    
    				$id_cms_users = DB::table('cms_users')
    					->where('id_cms_privileges',1)
    					->pluck('id');
    
    				$materi_tambahan_lain = DB::table('temp_proses_pentashihan_materi_tambahan')
    		    		->where('id_proses_pentashihan',$id)
    		    		->orderBy('id','ASC')
    		    		->pluck('name');
    				$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
    				$materi_lainnya = str_replace(',',';',$format);
    
    				$save_materi_lainnya['updated_at']     = Kemenag::now();
    				$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;

    				DB::table('temp_proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
    
    				return redirect(CRUDBooster::mainpath('preview-insert'));
    				// return redirect(CRUDBooster::mainpath('preview-insert'))->with([
    				// 	'message'=>'Registrasi anda berhasil, silahkan cetak bukti pendaftaran mushaf <a href="'.CRUDBooster::mainpath('bukti-pendaftaran/'.$id).'" target="_blank">disini</a>',
    				// 	'message_type'=>'success'
    				// ]);
    			}else{
    
    				return redirect()->back()->with([
    					'message'=>'Something went wrong',
    					'message_type'=>'danger'
    				]);
    			}   
    		}
	    }

		public function getPreviewInsert()
		{
	    	//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$id_cms_users = CRUDBooster::myId();

			$check = DB::table('temp_proses_pentashihan')
				->select('temp_proses_pentashihan.*','cms_users.name as cms_users_name','m_jenis_naskah.name as m_jenis_naskah_name','proses_pentashihan.nomor_registrasi as mushaf_lama_nomor_registrasi','countries.name as countries_name')
				->leftJoin('proses_pentashihan','proses_pentashihan.id','temp_proses_pentashihan.id_mushaf_lama')
				->leftJoin('cms_users','temp_proses_pentashihan.id_cms_users','cms_users.id')
				->leftJoin('m_jenis_naskah','temp_proses_pentashihan.id_m_jenis_naskah','m_jenis_naskah.id')
				->leftJoin('countries','temp_proses_pentashihan.id_countries','countries.id')
				->where('temp_proses_pentashihan.id_cms_users',$id_cms_users)
				->orderBy('temp_proses_pentashihan.id','desc')
				->first();


			if($check->id != null)
			{
				$check->proses_pentashihan_ukuran = DB::table('temp_proses_pentashihan_ukuran')->where("id_proses_pentashihan",$check->id)->get();
				$check->materi_tambahan = DB::table('temp_proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$check->id)
				->get();
			}

			if ($id_cms_users == '') {
				return redirect('/admin')->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}elseif ($check->id_cms_users != $id_cms_users) {
				return redirect()->back()->with([
					'message'=>'Error ID '.$id,
					'message_type'=>'danger'
				]);
			}

			$data               = [];
			$data['page_title'] = 'Preview Tashih';
			$data['token']      = csrf_token();

			$data['row']          = $check;

			$this->cbView('admin/admin_preview_proses_pentashihan',$data);
		}
		public function getRalat($id)
		{
			$jenis_naskah = DB::table('m_jenis_naskah')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();
			$jenis_mushaf = DB::table('m_jenis_mushaf')
				->whereNull('deleted_at')
				->orderBy('name','DESC')
				->get();
			$materi_tambahan = DB::table('m_materi_tambahan')
				->whereNull('deleted_at')
				->get();
			$countries = DB::table('countries')
				->get();

			$mushaf_lama = DB::table('proses_pentashihan')
			->where('id_cms_users',CRUDBooster::myId())
			->whereNull('deleted_at')
			->orderBy('id','desc')
			->get();

			$proses_pentashihan = DB::table('temp_proses_pentashihan')
				->where('id',$id)
				->first();
			$proses_pentashihan_materi_tambahan = DB::table('temp_proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$id)
				->get();
			$proses_pentashihan_ukuran = DB::table('temp_proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$id)
				->get();


			$data               = [];
			$data['page_title'] = 'Edit Tashih';
			$data['token']      = csrf_token();

			$data['jenis_naskah']                       = $jenis_naskah;
			$data['jenis_mushaf']                       = $jenis_mushaf;
			$data['countries']                       = $countries;
			$data['materi_tambahan']                    = $materi_tambahan;
			$data['mushaf_lama']            			= $mushaf_lama;
			$data['proses_pentashihan']                 = $proses_pentashihan;
			$data['proses_pentashihan_materi_tambahan'] = $proses_pentashihan_materi_tambahan;
			$data['proses_pentashihan_ukuran']          = $proses_pentashihan_ukuran;
			$data['id_cms_users']          = CRUDBooster::myId();

			$data['message'] = DB::table('message_alert')->where('flag','pendaftaran-mushaf')->first();
			$this->cbView('admin/admin_ralat_proses_pentashihan',$data);

		}

	    public function getEdit($id){
	    	//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$id_cms_users = CRUDBooster::myId();

			$check = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();

			if ($id_cms_users == '') {
				return redirect('/admin')->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}elseif ($check->id_cms_users != $id_cms_users) {
				return redirect()->back()->with([
					'message'=>'Error ID '.$id,
					'message_type'=>'danger'
				]);
			}elseif ($check->status != 'Registrasi Berhasil') {
				return redirect()->back()->with([
					'message'=>'Tashih sudah diterima, silahkan hubungi admin untuk melakukan perubahan data',
					'message_type'=>'danger'
				]);
			}

			$jenis_naskah = DB::table('m_jenis_naskah')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();
			$jenis_mushaf = DB::table('m_jenis_mushaf')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();
			$materi_tambahan = DB::table('m_materi_tambahan')
				->whereNull('deleted_at')
				->get();
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();
			$proses_pentashihan_materi_tambahan = DB::table('proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$id)
				->whereNull('deleted_at')
				->get();
			$proses_pentashihan_ukuran = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$id)
				->get();

			$data               = [];
			$data['page_title'] = 'Edit Tashih';
			$data['action']     = 'update/'.$id;
			$data['token']      = csrf_token();

			$data['jenis_naskah']                       = $jenis_naskah;
			$data['jenis_mushaf']                       = $jenis_mushaf;
			$data['materi_tambahan']                    = $materi_tambahan;
			$data['proses_pentashihan']                 = $proses_pentashihan;
			$data['proses_pentashihan_materi_tambahan'] = $proses_pentashihan_materi_tambahan;
			$data['proses_pentashihan_ukuran']          = $proses_pentashihan_ukuran;

			$this->cbView('admin/admin_proses_pentashihan',$data);
	    }

	    public function postEdit($id){
			$request              = Request::all();
			$now                  = Kemenag::now();
			$materi_lainnya       = Request::input('materi_lainnya');
			$materi_tambahan_lain = Request::input('materi_tambahan_lain');
			$oplah                = Request::input('oplah');
			$ukuran               = Request::input('ukuran');
			$id_cms_users         = CRUDBooster::myId();

			$check = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();

			if ($id_cms_users == '') {
				return redirect('/')->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}elseif ($check->id_cms_users != $id_cms_users) {
				return redirect()->back()->with([
					'message'=>'Error ID '.$id,
					'message_type'=>'danger'
				]);
			}elseif ($check->status != 'Registrasi Berhasil') {
				return redirect()->back()->with([
					'message'=>'Tashih sudah diterima, silahkan hubungi admin untuk melakukan perubahan data',
					'message_type'=>'danger'
				]);
			}

			$save['created_at']           = $now;
			$save['updated_at']           = $now;
			$save['id_cms_users']         = $id_cms_users;
			$save['id_m_materi_tambahan'] = ($materi_tambahan_lain != '' ? implode(';', $materi_tambahan_lain) : '');

			foreach ($request as $key => $value) {
				if ($key == 'materi_lainnya' || $key == 'materi_tambahan_lain' || $key == '_token' || $key == 'lainnya' || $key == 'surat_permohonan' || $key == 'gambar_cover' || $key == 'dokumen_naskah' || $key == 'bukti_tashih_internal' || $key == 'file_naskah' || $key == 'oplah' || $key == 'ukuran') continue;

				if ($key == 'new_surat_permohonan' || $key == 'new_gambar_cover' || $key == 'new_dokumen_naskah' || $key == 'new_bukti_tashih_internal' || $key == 'new_file_naskah') {
					if (Request::hasFile($key)) {
						$save[str_replace('new_', '', $key)] = Kemenag::uploadFile($key,'proses_pentashihan',true);
					}
				}else{
					$save[$key] = $value;
				}
			}

			$act = DB::table('proses_pentashihan')->where('id',$id)->update($save);

			if ($act) {
				for ($i=0; $i < count($materi_lainnya); $i++) {
					if($materi_lainnya[$i] == '' || $materi_lainnya[$i] == ' ' || $materi_lainnya[$i] == "\n") continue;

					$check = DB::table('proses_pentashihan_materi_tambahan')
						->where('id_proses_pentashihan',$id)
						->where('name',$materi_lainnya[$i])
						->first();

					if (empty($check)) {
						$save_tambahan['created_at']            = $now;
						$save_tambahan['updated_at']            = $now;
						$save_tambahan['name']                  = $materi_lainnya[$i];
						$save_tambahan['id_proses_pentashihan'] = $id;

						DB::table('proses_pentashihan_materi_tambahan')->insert($save_tambahan);
					}
				}

				DB::table('proses_pentashihan_ukuran')->where('id_proses_pentashihan',$id)->delete();
				for ($i=0; $i < count($oplah) ; $i++) { 
					$save_ukuran['created_at']            = $now;
					$save_ukuran['updated_at']            = $now;
					$save_ukuran['id_proses_pentashihan'] = $id;
					$save_ukuran['ukuran']                = $ukuran[$i];
					$save_ukuran['oplah']                 = $oplah[$i];
					DB::table('proses_pentashihan_ukuran')->insert($save_ukuran);
				}

				if ($materi_lainnya) {
					DB::table('proses_pentashihan_materi_tambahan')
						->where('id_proses_pentashihan',$id)
						->whereNotIn('name',$materi_lainnya)
						->delete();
				}

				$cms_users = DB::table('cms_users')
					->where('id',$save['id_cms_users'])
					->first();

				return redirect(CRUDBooster::mainpath())->with([
					'message'=>'Tashih anda sudah berhasil diubah',
					'message_type'=>'success'
				]);
			}else{

				return redirect()->back()->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}
	    }

	    public function getLoadKoreksi(){
			$id = Request::get('id');

			$koreksi = DB::table('proses_pentashihan_koreksi')
				->where('id_proses_pentashihan',$id)
				->whereNull('deleted_at')
				->orderBy('id','DESC')
				->get();

			foreach ($koreksi as $row) {
				$cms_users = DB::table('cms_users')
					->where('id',$row->id_cms_users)
					->first();

				$row->admin_name = $cms_users->name;

				$row->created_at  = ($row->created_at == '' ? '' : date('d M Y H:i',strtotime($row->created_at)));
				$row->revisi_date = ($row->revisi_date == '' ? '' : date('d M Y H:i',strtotime($row->revisi_date)));

				$row->lampiran        = ($row->lampiran == '' ? '' : asset($row->lampiran));
				$row->revisi_lampiran = ($row->revisi_lampiran == '' ? '' : asset($row->revisi_lampiran));

				$row->updated_at = ($row->updated_at == '' ? '' : $row->updated_at);
				$row->deleted_at = ($row->deleted_at == '' ? '' : $row->deleted_at);
				$row->pesan      = ($row->pesan == '' ? '' : $row->pesan);
				$row->revisi     = ($row->revisi == '' ? '' : $row->revisi);
			}

			if (empty($koreksi)) {
				$response['api_status']  = 0;
				$response['api_message'] = 'Data tidak ditemukan'; 
			}else{
				$response['api_status']  = 1;
				$response['api_message'] = 'success'; 
				$response['data']        = $koreksi;
			}

			return response()->json($response);
		}

		public function postKoreksi(){
			
			$id_proses_pentashihan = Request::input('id_proses_pentashihan');
			$revisi_terakhir = DB::table('proses_pentashihan_koreksi')
				->where('id_proses_pentashihan',$id_proses_pentashihan)
				->orderBy('id','DESC')
				->first();
            $valid = Validator::make(Request::all(),[
				'lampiran'=>'file|max:2000',
			]);
			
			if($valid->fails()){
			    return CRUDBooster::redirectback($valid->errors(),"error");
			}else{
			    if (!empty($revisi_terakhir)) {
    			
    				$save['updated_at']      = date('Y-m-d H:i:s');
    				$save['revisi_date']     = date('Y-m-d H:i:s');
    				$save['revisi']          = Request::input('pesan');
    				$save['revisi_lampiran'] = Kemenag::uploadFile('lampiran','koreksi/'.$id_proses_pentashihan,true);
    				$action = DB::table('proses_pentashihan_koreksi')->where('id',$revisi_terakhir->id)->update($save);
    
    				if ($action) {
    					$proses_pentashihan = DB::table('proses_pentashihan')
    						->where('id',$id_proses_pentashihan)
    						->first();
    
    					$id_cms_users = DB::table('cms_users')
    						->where('id_cms_privileges',1)
    						->pluck('id');
    
    					$config['content']      = "Revisi Naskah - ".$proses_pentashihan->nomor_registrasi;
    					$config['to']           = CRUDBooster::adminPath('proses-pentashihan?id='.$id_proses_pentashihan);
    					$config['id_cms_users'] = $id_cms_users; //This is an array of id users
    					CRUDBooster::sendNotification($config);
    
    					$config['content']      = "Revisi Naskah - ".$proses_pentashihan->nomor_registrasi;
    					$config['to']           = CRUDBooster::adminPath('list-tashih?id='.$id_proses_pentashihan);
    					$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
    					CRUDBooster::sendNotification($config);
    
    					$save_status['updated_at']        = $save['created_at'];
    					$save_status['status']            = 'Pentashihan Tahap Selanjutnya';
    					$save_status['status_penerimaan'] = 1;
    					DB::table('proses_pentashihan')->where('id',Request::input('id_proses_pentashihan'))->update($save_status);
    					
    					$to_sender=DB::table("proses_pentashihan")
    					->select("cms_users.email","proses_pentashihan.*")
    					->join("cms_users","cms_users.id","=","proses_pentashihan.id_cms_users")
    					->where('proses_pentashihan.id',$id_proses_pentashihan)
    					->first();
    
    					$to_send=$to_sender->email;
    
    					\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
    					\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
    					\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
    					\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
    					\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
    					Mail::send('mail.send-status',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>'Pentashihan Tahap Selanjutnya'],function ($pesan) use ($to_send){
    						// dd(CRUDBooster::getsetting("appname"));
    						$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
    						$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
    					});
    
    					return redirect()->back()->with(['message'=>'Revisi berhasil dikirim', 'message_type'=>'success']);
    				}else{
    					return redirect()->back()->with(['message'=>'Revisi gagal dikirim', 'message_type'=>'warning']);
    				}
    			}else{
    				return redirect()->back()->with(['message'=>'Data masih dalah proses tashih', 'message_type'=>'warning']);
    			}   
			}
		}

		public function getBuktiPendaftaran($id){
			ini_set('max_execution_time', 900);
			ini_set("memory_limit","8048M");
			ini_set('display_errors', 1);

			//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}
			$id_cms_users    = CRUDBooster::myId();
			$ukuran          = [];
			$oplah           = [];
			$materi_tambahan = [];

			$cms_users = DB::table('cms_users')
				->where('id',$id_cms_users)
				->first();
			$jenis_lembaga = DB::table('m_jenis_lembaga')
				->where('id',$cms_users->id_m_jenis_lembaga)
				->first();
			// $proses_pentashihan = DB::table('proses_pentashihan')
			// 	->where('id',$id)
			// 	->first();


			$proses_pentashihan = DB::table('proses_pentashihan')
				->select('proses_pentashihan.*','cms_users.name as cms_users_name','m_jenis_naskah.name as m_jenis_naskah_name','proses_pentashihan.nomor_registrasi as mushaf_lama_nomor_registrasi','countries.name as countries_name')
				->leftJoin('cms_users','proses_pentashihan.id_cms_users','cms_users.id')
				->leftJoin('m_jenis_naskah','proses_pentashihan.id_m_jenis_naskah','m_jenis_naskah.id')
				->leftJoin('countries','proses_pentashihan.id_countries','countries.id')
				->where('proses_pentashihan.id',$id)
				->first();

			$jenis_naskah = DB::table('m_jenis_naskah')
				->where('id',$proses_pentashihan->id_m_jenis_naskah)
				->first();
			$jenis_mushaf = DB::table('m_jenis_mushaf')
				->where('id',$proses_pentashihan->id_m_jenis_mushaf)
				->first();
			$proses_pentashihan_ukuran = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$id)
				->orderBy('id','ASC')
				->get();
			foreach ($proses_pentashihan_ukuran as $row) {
				array_push($ukuran, $row->ukuran);
				array_push($oplah, $row->oplah);
			}
			$proses_pentashihan_materi_tambahan = DB::table('proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$id)
				->orderBy('id','ASC')
				->get();
			foreach ($proses_pentashihan_materi_tambahan as $row) {
				array_push($materi_tambahan, $row->name);
			}
			$proses_pentashihan->ukuran          = (!empty($ukuran) ? implode(', ', $ukuran) : '-');
			$proses_pentashihan->oplah           = (!empty($oplah) ? implode(', ', $oplah) : '-');
			$proses_pentashihan->materi_tambahan = (!empty($materi_tambahan) ? implode(', ', $materi_tambahan) : '-');
			$proses_pentashihan->jenis_naskah    = $jenis_naskah->name;
			$proses_pentashihan->jenis_mushaf    = $jenis_mushaf->name;

			if ($id_cms_users == '') {
				return redirect('/admin')->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}
			// elseif ($proses_pentashihan->id_cms_users != $id_cms_users) {
			// 	return redirect(CRUDBooster::mainpath())->with([
			// 		'message'=>'Error ID '.$id,
			// 		'message_type'=>'danger'
			// 	]);
			// }

			$arr                       = [];
			$arr['cms_users']          = $cms_users;
			$arr['jenis_lembaga']      = $jenis_lembaga;
			$arr['proses_pentashihan'] = $proses_pentashihan;
			// dd($arr);

			$view     = view('report/buktipendaftaran',$arr)->render();
			$filename = "bukti-pendaftaran_".str_replace(' ', '_', $proses_pentashihan->nama_produk);
			$pdf      = App::make('dompdf.wrapper');
			$pdf->loadHTML($view);
			$pdf->setPaper('A4','portrait');
			return $pdf->stream($filename.'.pdf');
			
			return $view;

		}

		public function getDeleteAttachment()
		{
			$id = Request::get('id');
			$column = Request::get('column');
		
			$row = DB::table('temp_proses_pentashihan')->where('id', $id)->first();

			$file = str_replace('uploads/', '', $row->{$column});
			if (Storage::exists($file)) {
				Storage::delete($file);
			}
	
			DB::table('temp_proses_pentashihan')->where('id', $id)->update([$column => null]);
	
			CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans('crudbooster.alert_delete_data_success'), 'success');
		}

		// public function getDetail($id)
		// {
		//     	//Create an Auth
		// 		if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
		// 			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		// 		}
		// 		$check = DB::table('proses_pentashihan')
		// 		->select('proses_pentashihan.*','cms_users.name as cms_users_name','m_jenis_naskah.name as m_jenis_naskah_name','countries.name as countries_name')
		// 		->leftJoin('cms_users','proses_pentashihan.id_cms_users','cms_users.id')
		// 		->leftJoin('m_jenis_naskah','proses_pentashihan.id_m_jenis_naskah','m_jenis_naskah.id')
		// 		->leftJoin('countries','proses_pentashihan.id_countries','countries.id')
		// 		->where('proses_pentashihan.id',$id)
		// 		->first();


		// 		if($check->id != null)
		// 		{
		// 			$check->proses_pentashihan_ukuran = DB::table('proses_pentashihan_ukuran')->where("id_proses_pentashihan",$check->id)->get();
		// 			$check->materi_tambahan = DB::table('proses_pentashihan_materi_tambahan')
		// 			->where('id_proses_pentashihan',$check->id)
		// 			->get();
		// 		}


		// 		$data               = [];
		// 		$data['page_title'] = 'Detail Tashih';
		// 		$data['token']      = csrf_token();
	
		// 		$data['row']                 = $check;
		// 		// dd($data);
		// 		$this->cbView('admin/admin_detail_pentashihan',$data);		
		// }

	}