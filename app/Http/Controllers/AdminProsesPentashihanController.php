<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;
	use Mail;
	use Validator;

	class AdminProsesPentashihanController extends \crocodicstudio\crudbooster\controllers\CBController {

		public function postRating()
		{
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',Request::input('id'))
				->first();

			$save['created_at']            = Kemenag::now();
			$save['updated_at']            = Kemenag::now();
			$save['id_cms_users'] = $proses_pentashihan->id_cms_users;
			$save['id_proses_pentashihan'] = Request::input('id');
			$save['jumlah_rating'] = Request::input('jumlah_rating');
			$save['komentar'] = Request::input('komentar');
			$action = DB::table('proses_pentashihan_rating')->insert($save);
			if ($action) {
			    $cms_users = DB::table('cms_users')->where('id',$proses_pentashihan->id_cms_users)->first();

				$config['content']      = "Admin memberi rating - ".$cms_users->name;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.Request::input('id'));
				$config['id_cms_users'] = [$cms_users->id]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Pemberian Rating";
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.Request::input('id'));
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with(['message'=>'Rating berhasil ditambahkan', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
		}

		public function getCalendarRange()
		{
			$array=[];
			$db=DB::table('master_calendar')
			->get();

			foreach ($db as $key => $value) {
				$start=strtotime($value->tanggal_awal);
				$end=strtotime($value->tanggal_akhir);
				$jmldetik=24*3600;
				for ($i= $start; $i <= $end ; $i+=$jmldetik) { 
					$array[]=date('Y-m-d',$i);
				}
			}
			
			return json_encode($array);
		}

		public function getHari($tglawal,$tglakhir)
		{
			// dd($tglawal,$tglakhir);
			// cuti.php
			// penanggalan Indonesia
			setlocale(LC_TIME, 'id_ID.UTF8');

			// tanggalnya diubah formatnya ke Y-m-d
			// $awal_cuti = date_create_from_format('Y-m-d', strtotime($tglawal));

			// $awal_cuti = date_format($tglawal, 'd-m-Y');
			$awal_cuti = strtotime($tglawal);
			// dd($awal_cuti);

			// $akhir_cuti = date_create_from_format('Y-m-d', $tglakhir);
			// $akhir_cuti = date_format($tglakhir, 'd-m-Y');
			$akhir_cuti = strtotime($tglakhir);

			$haricuti = array();
			$sabtuminggu = array();

			$kantor=array();

			$kueri=DB::table('master_calendar')
			->get();
			foreach ($kueri as $data) {
				for ($l=strtotime($data->tanggal_awal); $l <=strtotime($data->tanggal_akhir) ; $l+=(60*60*24)) {
					for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
						if (date("d-m-Y",$i)==date("d-m-Y",$l)) {
							// MINGGU
							if(date('w', $i) == '0'){
								continue;
							}else{
								$kantor[]=$i;
							}
							// SABTU MINGGU
							// if(date('w', $i) == '0' || date('w', $i) == '6'){
							// 	continue;
							// }else{
							// 	$kantor[]=$i;
							// }
						}
					}
				}
			}

			// MINGGU
			for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
					if (date('w', $i) !== '0') {
							$haricuti[] = $i;
				    }else {
				      $sabtuminggu[] = $i;
				    }
			}
			// SABTU MINGGU
			// for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
			// 	if (date('w', $i) !== '0' || date('w', $i) !== '6') {
			// 			$haricuti[] = $i;
			// 	}else {
			// 	  $sabtuminggu[] = $i;
			// 	}
			// }

			$liburkantor=count($kantor);
			$jumlah_cuti = count($haricuti);

			$jumlah_sabtuminggu = count($sabtuminggu);
			$abtotal = $jumlah_cuti + $jumlah_sabtuminggu;
			$tidak_libur=$abtotal-($liburkantor+$jumlah_sabtuminggu);
			// echo "<pre>";
			// echo "<h1>Sistem Cuti Online</h1>";
			// echo "Mulai Cuti : " . date('d-m-Y', $awal_cuti) . "<br>";
			// echo "Terakhir Cuti : " . date('d-m-Y', $akhir_cuti) . "<br>";
			// echo "Jumlah Hari Masuk : " . $tidak_libur ."<br>";
			// echo "Jumlah Sabtu/Minggu : " . $jumlah_sabtuminggu ."<br>";
			// echo "Libur Kantor: " . $liburkantor ."<br>";
			// echo "Total Hari : " . $abtotal ."<br>";

			return $tidak_libur;
		}
		
		public function getPembayaran($id){
		    $check = DB::table('proses_pentashihan')
		      //  ->select("id",'PNBP','bukti_pembayaran')
		        ->whereNull("deleted_at")
		        ->where("id",$id)
		        ->first();
		    
		    
		    $url       = ($check->bukti_pembayaran != '' ? url($check->bukti_pembayaran) : '');
			$extension = Kemenag::extension($url);
			
			$url_cek       = ($check->bukti_PNBP != '' ? url($check->bukti_PNBP) : '');
			$extension_cek = Kemenag::extension($url_cek);
				
		    $res['api_status'] = 1;
		    $res['api_message'] = 'success';
		    $res['pnbp'] = ($check->PNBP) ? number_format($check->PNBP,0,',','.') : '';
		    $res['bukti_pembayaran'] = ($url) ? $url : '';
		    $res['extension'] =$extension;
		    $res['extension_bukti_PNBP'] =$extension_cek;
	  	    $res['bukti_pembayaran_bukti_PNBP'] = ($url_cek) ? $url_cek : '';
	  	    
	  	    return response()->json($res);
		}
    
        public function postSetPnbp(){
            $id = Request::input('id');
            $nominal = str_replace('.','',Request::input('PNBP'));
            $valid = Validator::make(Request::all(),[
                'bukti_pembayaran'=>'mimes:pdf,jpg,jpeg,png,pneg,bmp,gif'
            ]);		
	            
	        if($valid->fails()){
    			return CRUDBooster::redirectback($valid->errors(),'error');
    		}else{
    		    $save['bukti_pembayaran']   = Kemenag::uploadFile('bukti_pembayaran','proses_pentashihan',true);
    		    $save['PNBP']               = $nominal;
                $update = DB::table("proses_pentashihan")
                    ->where("id",$id)
                    ->update($save);
                    
                if($update){
                    return CRUDbooster::redirectback("Berhasil update PNBP","success");
                }else{
                    return CRUDbooster::redirectback("gagal update PNBP","error");                
                }
    		}
		}
		
		function findColor($status) {
			switch ($status) {
				case 'Registrasi Berhasil':
					$color = 'btn-success';
					break;

				case 'Naskah Diterima':
					$color = 'btn-warning';
					break;

				case 'Tidak Lolos Verifikasi':
					$color = 'btn-default';
					break;

				case 'Lolos Verifikasi':
					$color = 'btn-success';
					break;

				case 'Proses Perbaikan Naskah':
					$color = 'btn-warning';
					break;

				case 'Pentashihan Tahap Selanjutnya':
					$color = 'btn-primary';
					break;

				case 'Selesai':
					$color = 'btn-success';
					break;
				
				default:
					$color = 'btn-default';
					break;
			}

			return $color;
		}

		public function listPentashih()
		{
			return DB::table('cms_users')
				->select(
					'id',
					'name',
					'email',
					'phone'
				)
				->where('id_cms_privileges', 3)
				->orderby('id', 'asc')
				->get();
		}
        
	    public function cbInit() {
			// dd(CRUDBooster::getsetting("email_sender"));
			$segment = Request::segment(3);
			$id      = Request::segment(4);

	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "proses_pentashihan";	        
			$this->title_field         = "nama_produk";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = FALSE;
			$this->button_delete       = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);
			$this->button_edit         = (CRUDBooster::isSuperadmin() ? TRUE : FALSE);
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = TRUE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "collapse"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
			$this->col[] = array("label"=>"Nomor Registrasi","name"=>"nomor_registrasi");
			$this->col[] = array("label"=>"Nama Mushaf","name"=>"nama_produk");
			$this->col[] = array("label"=>"Nama Penerbit","name"=>"id_cms_users","join"=>"cms_users,name");
			$this->col[] = array("label"=>"Penanggung Jawab","name"=>"penanggung_jawab_produk");
			$this->col[] = array("label"=>"Tanggal Pengajuan","name"=>"created_at","callback"=>function($row){
				return date('d M Y',strtotime($row->created_at));
			});
			$this->col[] = array("label"=>"Tanggal deadline pentashihan","name"=>"tanggal_deadline","callback"=>function($row){
				if(empty($row->tanggal_deadline)){
					return "<span class='btn btn-xs btn-info'>Belum ada deadline</span>";   
				}elseif($row->status == 'Proses Perbaikan Naskah'){
					return "<span class='btn btn-xs btn-info'>Tidak ada deadline pentashihan</span>";
				}else{
					return date('d M Y',strtotime($row->tanggal_deadline));
				}
			});
			$this->col[] = array("label"=>"Status","name"=>"status","callback"=>function($row){
				$list_status = ['Naskah Diterima','Verifikasi Naskah','Proses Tashih','Proses Perbaikan','Terima Perbaikan','Final'];
				$li = '';
				$color = $this->findColor($row->status);

				$list = '
                  <button type="button" class="btn '.$color.' btn-xs dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                  	'.$li.'
                  </ul>
                ';
                
                if($row->status=="Lolos Verifikasi"){
                    $text_status = 'Lolos Verifikasi (Proses Pentashihan)';
                }else{
                    $text_status = $row->status;   
                }

				$html = '
				<div class="btn-group">
                  <button type="button" class="btn '.$color.' btn-xs">'.$text_status.'</button>
                  ';
                $html .= '</div>
                ';

               	$proses_pentashihan = DB::table('proses_pentashihan')
               		->where('id',$row->id)
               		->first();
               	$row->status_penerimaan = $proses_pentashihan->status_penerimaan;

                return $html;
			});
			$this->col[] = ['label'=>"PNBP","name"=>"id","callback"=>function ($row){
			    $proses_pentashihan = DB::table('proses_pentashihan')
               		->where('id',$row->id)
               		->first();
               	
               	if(empty($proses_pentashihan->PNBP) && empty($proses_pentashihan->bukti_pembayaran)){
               	    $html_satu = '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-upload-pnbp" data-id="'.$row->id.'"><span class="fa fa-file"></span> PNBP</a>';
               	}else{
               	    $html = '<a href="javascript:void(0)" class="btn btn-xs btn-primary btn-lihat-bukti" data-id="'.$row->id.'"><span class="fa fa-aye"></span> Lihat Bukti Pembayaran</a>';
               	}
               	
               	return $html_satu.' '.$html;
			}];
			// $this->col[] = array("label"=>"Status Penerimaan","name"=>"status_penerimaan");
			# END COLUMNS DO NOT REMOVE THIS LINE
			
			# START FORM DO NOT REMOVE THIS LINE
			$r_cms_users = ($segment == 'edit' ? true : false);
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();
			if ($proses_pentashihan->status == 'Registrasi Berhasil' || $proses_pentashihan->status == 'Naskah Diterima') {
				$status_verifikasi = 'Sedang Diverifikasi';
			}elseif ($proses_pentashihan->status == 'Tidak Lolos Verifikasi') {
				$status_verifikasi = 'Tidak Lolos Verifikasi';
			}else{
				$status_verifikasi = 'Lolos Verifikasi';
			}
			$this->form = [];
            $this->form[] = ["label"=>"Jenis Permohonan","name"=>"jenis_permohonan",'type'=>'select','dataenum'=>'Surat Tanda tashih Baru;Pembaruan/perpanjangan Surat Tanda Tashih;Surat Izin Edar'];
			$this->form[] = ["label"=>"Nomor Registrasi","name"=>"nomor_registrasi","type"=>"text"];

			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Tanggal pendaftaran mushaf","name"=>"created_at","type"=>"text"];
			}
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"radio","dataenum"=>"Registrasi Berhasil;Naskah Diterima;Lolos Verifikasi;Tidak Lolos Verifikasi;Proses Perbaikan Naskah;Pentashihan Tahap Selanjutnya;Selesai"];


			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Nama Penerbit","name"=>"id_cms_users","type"=>"select","datatable"=>"cms_users,name", "disabled"=>$r_cms_users];
				$this->form[] = ["label"=>"ID Penerbit","name"=>"id_cms_users","type"=>"select","datatable"=>"cms_users,id_penerbit", "disabled"=>$r_cms_users];
			}
			$this->form[] = ["label"=>"Nama Produk","name"=>"nama_produk","type"=>"text"];
			$this->form[] = ["label"=>"Penanggung Jawab","name"=>"penanggung_jawab_produk","type"=>"text"];
			$this->form[] = ["label"=>"Jenis Naskah","name"=>"id_m_jenis_naskah","type"=>"checkbox","datatable"=>"m_jenis_naskah,name"];
			$this->form[] = ["label"=>"Jenis Mushaf","name"=>"id_m_jenis_mushaf","type"=>"select2","datatable"=>"m_jenis_mushaf,name"];
			$this->form[] = ["label"=>"Nama Percetakan","name"=>"nama_percetakan","type"=>"text"];
			$this->form[] = ["label"=>"Keterangan","name"=>"keterangan","type"=>"textarea"];

			if($proses_pentashihan->jenis_permohonan == 'Pembaruan/perpanjangan Surat Tanda Tashih')
			{
				$this->form[] = ['label'=>'No. Pendaftaran mushaf lama ','name'=>'id_mushaf_lama','type'=>'select','datatable'=>'proses_pentashihan,nomor_registrasi'];
			}
			else if($proses_pentashihan->jenis_permohonan == "Surat Izin Edar")
			{
				$this->form[] = ['label'=>'Negara asal mushaf ','name'=>'id_countries','type'=>'select','datatable'=>'countries,name'];
				$this->form[] = ["label"=>"Penerbit Asal","name"=>"nama_penerbit_asal","type"=>"text"];
				$this->form[] = ["label"=>"Lembaga Pentashih Asal","name"=>"nama_lembaga_pentashih_asal","type"=>"text"];
				$this->form[] = ["label"=>"Bukti Tashih Lembaga Pentashih Asal","name"=>"bukti_tashih_lembaga_pentashih_asal",'type'=>'upload'];
			}


			$ukuran[] = ['label'=>'*Ukuran','name'=>'ukuran','type'=>'text','required'=>true];
			$ukuran[] = ['label'=>'*Oplah','name'=>'oplah','type'=>'text','required'=>true];
			if ($segment == 'detail' && $proses_pentashihan->status == 'Selesai') {
				$ukuran[] = ['label'=>'*Nomor Tanda Tashih','name'=>'nomor','type'=>'text'];
				$ukuran[] = ['label'=>'*Kode Tanda Tashih','name'=>'kode','type'=>'text'];
				$ukuran[] = ['label'=>'*Tanggal Penetapan','name'=>'tanggal_penetapan','type'=>'text'];
				// $ukuran[] = ['label'=>'*Surat Penerbitan','name'=>'surat_penerbitan','type'=>'upload'];
				// $ukuran[] = ['label'=>'*Scan Tanda Tashih','name'=>'scan_tanda_tashih','type'=>'upload'];
			}

			$this->form[] = ["label"=>"Surat Pernyataan tidak ada perubahan pada master naskah","name"=>"file_surat_pernyataan","type"=>"upload"];


			$this->form[] = ['label'=>'Ukuran & Oplah','name'=>'proses_pentashihan_ukuran','type'=>'child','columns'=>$ukuran,'table'=>'proses_pentashihan_ukuran','foreign_key'=>'id_proses_pentashihan'];


			$this->form[] = ["label"=>"File Naskah","name"=>"file_naskah","type"=>"upload"];

			$this->form[] = ["label"=>"Materi Tambahan","name"=>"id_m_materi_tambahan","type"=>"checkbox","datatable"=>"m_materi_tambahan,name"];
			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Materi Tambahan Lainnya","name"=>"materi_lainnya","type"=>"checkbox","dataenum"=>$proses_pentashihan->materi_lainnya];
			}
			$this->form[] = ["label"=>"Penanggung Jawab Materi Tambahan","name"=>"penanggung_jawab_materi_tambahan","type"=>"textarea"];

			$this->form[] = ["label"=>"Surat permohonan tanda tashih mushaf baru","name"=>"surat_permohonan","type"=>"upload"];
			$this->form[] = ["label"=>"Gambar Cover","name"=>"gambar_cover","type"=>"upload"];
			$this->form[] = ["label"=>"Dokumen Naskah","name"=>"dokumen_naskah","type"=>"upload"];
			$this->form[] = ["label"=>"Tashih Internal","name"=>"tashih_internal","type"=>"radio","dataenum"=>"Sudah;Belum"];
			$this->form[] = ["label"=>"Bukti Tashih Internal","name"=>"bukti_tashih_internal","type"=>"upload"];
			$this->form[] = ["label"=>"Disposisi Nomor Agenda","name"=>"disposisi_nomor_agenda","type"=>"text"];
			// $this->form[] = ["label"=>"Disposisi File","name"=>"disposisi_file","type"=>"upload"];
			$this->form[] = ["label"=>"PNBP","name"=>"PNBP","type"=>"number"];
			$this->form[] = ["label"=>"Bukti Penerimaan","name"=>"bukti_penerimaan","type"=>"upload"];
			$this->form[] = ["label"=>"Nomor Resi","name"=>"no_resi","type"=>"text"];


			// $this->form[] = ["label"=>"Bukti Pembayaran","name"=>"bukti_pembayaran","type"=>"upload"];


			if ($segment == 'detail') {
				$this->form[] = ["label"=>"Status Verifikasi","name"=>"id","type"=>"radio","dataenum"=>$status_verifikasi];
				$this->form[] = ["label"=>"Tanggal Verifikasi","name"=>"tanggal_verifikasi",'type'=>'date'];
				$this->form[] = ["label"=>"Tanggal Deadline","name"=>"tanggal_deadline",'type'=>'date'];
				$this->form[] = ["label"=>"File Verifikasi","name"=>"bukti_verifikasi",'type'=>'upload'];
				
				$pernyataan[] = ['label'=>'*Nama','name'=>'nama','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Jabatan','name'=>'jabatan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Alamat','name'=>'alamat','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Nomor Telepon','name'=>'phone','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Email','name'=>'email','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Nama Percetakan','name'=>'nama_percetakan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Alamat Percetakan','name'=>'alamat_percetakan','type'=>'text','required'=>true];
				$pernyataan[] = ['label'=>'*Rencana Pelaksanaan','name'=>'rencana_pelaksanaan','type'=>'text','required'=>true];
				$this->form[] = ['label'=>'Surat Pernyataan','name'=>'proses_pentashihan_surat_pernyataan','type'=>'child','columns'=>$pernyataan,'table'=>'proses_pentashihan_surat_pernyataan','foreign_key'=>'id_proses_pentashihan'];
			}
			

			if ($segment == 'edit') {
				$columns[] = ['label'=>'*Materi Lainnya','name'=>'name','type'=>'text','required'=>true];
				$this->form[] = ['label'=>'Materi Tambahan Lainnya','name'=>'proses_pentashihan_materi_tambahan','type'=>'child','columns'=>$columns,'table'=>'proses_pentashihan_materi_tambahan','foreign_key'=>'id_proses_pentashihan'];
			}

			$tashih[] = ['label'=>'User','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
			$tashih[] = ['label'=>'Juz','name'=>'juz','type'=>'text',"required"=>true];
			$this->form[] = ['label'=>'List Tashih (Admin)','name'=>'proses_pentashihan_user','type'=>'child','columns'=>$tashih,'table'=>'proses_pentashihan_user','foreign_key'=>'id_proses_pentashihan'];

			$revisi[] = ['label'=>'Admin','name'=>'id_cms_users','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
			$revisi[] = ['label'=>'Tanggal Koreksi','name'=>'created_at','type'=>'hidden'];
			$revisi[] = ['label'=>'Pesan','name'=>'pesan','type'=>'textarea',"required"=>true];
			$revisi[] = ['label'=>'Lampiran','name'=>'Lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Revisi','name'=>'revisi','type'=>'textarea'];
			$revisi[] = ['label'=>'Lampiran Revisi','name'=>'revisi_lampiran','type'=>'upload'];
			$revisi[] = ['label'=>'Tanggal Revisi','name'=>'revisi_date','type'=>'hidden'];
			$this->form[] = ['label'=>'Riwayat Koreksi','name'=>'proses_pentashihan_koreksi','type'=>'child','columns'=>$revisi,'table'=>'proses_pentashihan_koreksi','foreign_key'=>'id_proses_pentashihan'];

			if ($segment == 'detail') {
				// $bukti_penerimaan[] = ['label'=>'Bukti Penerimaan','name'=>'bukti_penerimaan','type'=>'upload'];
				$bukti_penerimaan[] = ['label'=>'Nama Penerima','name'=>'nama_penerima','type'=>'text'];
				$bukti_penerimaan[] = ['label'=>'Tanggal Penerimaan','name'=>'tanggal_penerimaan','type'=>'date'];
				$bukti_penerimaan[] = ['label'=>'Tanggal Deadline','name'=>'tanggal_deadline','type'=>'date'];
				$this->form[] = ['label'=>'Riwayat Bukti Penerimaan','name'=>'proses_pentashihan_bukti_penerimaan','type'=>'child','columns'=>$bukti_penerimaan,'table'=>'proses_pentashihan_bukti_penerimaan','foreign_key'=>'id_proses_pentashihan'];
			}
			// $this->form[] = ["label"=>"Nama Penerima","name"=>"nama_penerima","type"=>"text"];
			// $this->form[] = ["label"=>"Tanggal Penerimaan","name"=>"tanggal_diterima","type"=>"date"];
			// // $this->form[] = ["label"=>"Tanggal Deadline","name"=>"tanggal_deadline","type"=>"date"];

			if ($segment == 'detail') {
				$laporan_pencetakan[] = ['label'=>'Pencetakan ke','name'=>'cetakan_ke','type'=>'text'];
				$laporan_pencetakan[] = ['label'=>'Tanggal Pencetakan','name'=>'tanggal_cetak','type'=>'text'];
				$laporan_pencetakan[] = ['label'=>'Ukuran','name'=>'ukuran','type'=>'text'];
				$laporan_pencetakan[] = ['label'=>'Oplah','name'=>'oplah','type'=>'text'];
				$laporan_pencetakan[] = ['label'=>'Nama Percetakan','name'=>'nama_percetakan','type'=>'text'];
				$laporan_pencetakan[] = ['label'=>'Alamat Percetakan','name'=>'alamat_percetakan','type'=>'text'];
				$this->form[] = ['label'=>'Laporan Pencetakan','name'=>'proses_pentashihan_laporan_percetakan','type'=>'child','columns'=>$laporan_pencetakan,'table'=>'proses_pentashihan_laporan_percetakan','foreign_key'=>'id_proses_pentashihan'];
			}
			# END FORM DO NOT REMOVE THIS LINE
			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, success, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
			*/
			// $kueri=DB::table("proses_pentashihan_rating")
			// ->where('id_cms_users',CRUDBooster::myId())
			// ->first();
	        $this->addaction = array();
	        if (CRUDBooster::isSuperadmin()) {
				$this->addaction[] = ['label'=>'Penerimaan Naskah','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-book','color'=>'primary btn-penerimaan','showIf'=>"[status] == 'Registrasi Berhasil'"];
				// if(empty($kueri->id)){
				// $this->addaction[] = ['label'=>'Beri Rating','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-star','color'=>'primary btn-rating','showIf'=>"[status] == 'Selesai'"];
				// }else{
				$this->addaction[] = ['label'=>'Lihat Rating','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-star','color'=>'primary btn-rating-look','showIf'=>"[status] == 'Selesai'"];
				// }
				$this->addaction[] = ['label'=>'Bukti Penerimaan','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-book','color'=>'primary btn-penerimaan-estimasi','showIf'=>"[status] == 'Pentashihan Tahap Selanjutnya' AND [status_penerimaan] == 1"];
				$this->addaction[] = ['label'=>'Koreksi','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-edit','color'=>'warning btn-koreksi','showIf'=>"[status] == 'Lolos Verifikasi' or [status] == 'Pentashihan Tahap Selanjutnya' "];
				$this->addaction[] = ['label'=>'Verifikasi','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-edit','color'=>'danger btn-verifikasi','showIf'=>"[status] == 'Naskah Diterima'"];
				$this->addaction[] = ['label'=>'Tanda Tashih','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-star','color'=>'success btn-finalisasi','showIf'=>"([status] == 'Lolos Verifikasi' or [status] == 'Pentashihan Tahap Selanjutnya' or [status] == 'Selesai') AND [status_penerimaan] != 1"];
				$this->addaction[] = ['label'=>'Disposisi','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-check','color'=>'info btn-disposisi'];
				$this->addaction[] = ['label'=>'','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-clone','color'=>'info btn-pentashihan'];
				
				// $this->addaction[] = ['label'=>'Lihat Bukti Pembayaran','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-eye','color'=>'primary btn-lihat-bukti','showIf'=>"[PNBP] !='' && [bukti_pembayaran] !=''"];
				
				// $this->addaction[] = ['label'=>'PNBP','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-eye','color'=>'primary btn-upload-pnbp','showIf'=>"[PNBP] =='' && [bukti_pembayaran] ==''"];
	        }


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
			// $this->alert[] = ['message'=>'Tidak ada deadline pentashihan, naskah dalam masa perbaikan penerbit','type'=>'info'];

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
			   
			$this->script_js = "	
				$(document).ready(function() {
					// $('#namaPenerimaan').val('".CRUDBooster::myName()."');
				});

				$('a').each(function(){
					let text = $(this).html();

					if(text == 'Download File' || text == 'Download file'){
						$(this).attr('target','_blank');
					}
				})

				$('.panel-body').find('.row').children('.col-sm-10').addClass('col-sm-offset-1');
				$('#riwayatkoreksiid_cms_users').parent().parent().hide();
				$('#riwayatkoreksiid_cms_users').val('".CRUDBooster::myId()."').trigger('change');;
				$('#riwayatkoreksicreated_at').val(formatDate())

				$('#btn-add-table-riwayatkoreksi').on('click',function(){
					let now = formatDate();

					$('#riwayatkoreksiid_cms_users').val('".CRUDBooster::myId()."').trigger('change');
					$('#riwayatkoreksicreated_at').val(now);
				})

				function formatDate() {
					now    = new Date();
					year   = '' + now.getFullYear();
					month  = '' + (now.getMonth() + 1); if (month.length == 1) { month = '0' + month; }
					day    = '' + now.getDate(); if (day.length == 1) { day = '0' + day; }
					hour   = '' + now.getHours(); if (hour.length == 1) { hour = '0' + hour; }
					minute = '' + now.getMinutes(); if (minute.length == 1) { minute = '0' + minute; }
					second = '' + now.getSeconds(); if (second.length == 1) { second = '0' + second; }
					return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
				}
				
				$('#table-detail').find('tr').each(function(){
					let field = $(this).find('td:first-child').text();
					if(field == 'Status Verifikasi'){
						$(this).find('td:last-child').find('.badge').html('".$status_verifikasi."');
					}
				})
				
				$('#datepicker').flatpickr({
					disable:".$this->getCalendarRange().",
					dateFormat: 'Y-m-d',
					locale: {
						'firstDayOfWeek': 1
					},
					onChange: [function(){
						if($('#datepicker').val()==''){

						}else if($('#datepicker_akhir').val()==''){

						}else{
							$.ajax({
								url:'".CRUDBooster::adminpath('proses-pentashihan/hari')."/'+$('#datepicker').val()+'/'+$('#datepicker_akhir').val(),
								method:'GET',
								success:function(paramas){
									// alert(paramas);
									$('#estimasi').val(paramas);
								},
								error:function(paramas){
									alert(paramas);
								}
							});
						}
					}]
				});
				$('#datepicker_akhir').flatpickr({
					disable: ".$this->getCalendarRange().",
					dateFormat: 'Y-m-d',
					locale: {
						'firstDayOfWeek': 1
					},
					onChange: [function(){
						if($('#datepicker').val()==''){

						}else if($('#datepicker_akhir').val()==''){

						}else{
							$.ajax({
								url:'".CRUDBooster::adminpath('proses-pentashihan/hari')."/'+$('#datepicker').val()+'/'+$('#datepicker_akhir').val(),
								method:'GET',
								success:function(paramas){
									// alert(paramas);
									$('#estimasi').val(paramas);
								},
								error:function(paramas){
									alert(paramas);
								}
							});
						}
					}]
				});
				
				$('#datepicker_awal').flatpickr({
					disable:".$this->getCalendarRange().",
					dateFormat: 'Y-m-d',
					locale: {
						'firstDayOfWeek': 1
					},
					onChange: [function(){
						if($('#datepicker_awal').val()==''){

						}else if($('#datepicker_aks').val()==''){

						}else{
							$.ajax({
								url:'".CRUDBooster::adminpath('proses-pentashihan/hari')."/'+$('#datepicker_awal').val()+'/'+$('#datepicker_aks').val(),
								method:'GET',
								success:function(paramas){
									// alert(paramas);
									$('#estimasi_satu').val(paramas);
								},
								error:function(paramas){
									alert(paramas);
								}
							});
						}
					}]
				});
				
				$('#datepicker_aks').flatpickr({
					disable:".$this->getCalendarRange().",
					dateFormat: 'Y-m-d',
					locale: {
						'firstDayOfWeek': 1
					},
					onChange: [function(){
						if($('#datepicker_awal').val()==''){

						}else if($('#datepicker_aks').val()==''){

						}else{
							$.ajax({
								url:'".CRUDBooster::adminpath('proses-pentashihan/hari')."/'+$('#datepicker_awal').val()+'/'+$('#datepicker_aks').val(),
								method:'GET',
								success:function(paramas){
									// alert(paramas);
									$('#estimasi_satu').val(paramas);
								},
								error:function(paramas){
									alert(paramas);
								}
							});
						}
					}]
				});
				
				$('#tanggalDiterima').flatpickr({
					dateFormat: 'Y-m-d',
					locale: {
						'firstDayOfWeek': 1
					},
				});
	        ";


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	       	$admin = DB::table('cms_users')
	       		->where('id_cms_privileges',3)
	       		->orderBy('name','ASC')
	       		->get();
			$total_admin = count($admin);
			$no_admin    = 1;
			$list_admin  = '';
	       	foreach ($admin as $row) {
	       		$list_admin .= '{"id": '.$row->id.',"nama": "'.$row->name.'"}';
	       		$list_admin .= ($total_admin == $no_admin++ ? '' : ',');
	       	}
	        $this->pre_index_html = '
				<script type="text/javascript">
		        	var token = "'.csrf_token().'";
		        	var asset = "'.asset('/').'";
		        	var mainpath = "'.CRUDBooster::mainpath().'";
		        	var admin = ['.$list_admin.'];
			    </script>
			    
			    <div id="modalIframe" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Modal Header</h4>
				      </div>
				      <div class="modal-body">
				        <iframe src="'.asset('uploads/1/2018-05/akte_notaris_mutu.pdf').'"></iframe>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <a href="#" target="_blank" class="btn btn-success">Open File</a>
				      </div>
				    </div>

				  </div>
				</div>
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        if ($segment == '') {
	        	$this->load_js[] = asset("vendor/crudbooster/assets/select2/dist/js/select2.full.min.js");
	        	$this->load_js[] = asset("js/admin/pentashihan.js");
	        }
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
			$this->style_css = '
				.flatpickr-day.flatpickr-disabled, .flatpickr-day.flatpickr-disabled:hover{
					background:#e74c3c;
					color:#FFF;
				}
	        	.delete-data{
	        		cursor:pointer;
	        	}
				embed{
					width:100%;
					height:350px;
					border:none;
					cursor:pointer;
				}
				.sk-fading-circle {
				  margin: 100px auto;
				  width: 40px;
				  height: 40px;
				  position: relative;
				}

				.sk-fading-circle .sk-circle {
				  width: 100%;
				  height: 100%;
				  position: absolute;
				  left: 0;
				  top: 0;
				}

				.sk-fading-circle .sk-circle:before {
				  content: "";
				  display: block;
				  margin: 0 auto;
				  width: 15%;
				  height: 15%;
				  background-color: #333;
				  border-radius: 100%;
				  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
				          animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
				}
				.sk-fading-circle .sk-circle2 {
				  -webkit-transform: rotate(30deg);
				      -ms-transform: rotate(30deg);
				          transform: rotate(30deg);
				}
				.sk-fading-circle .sk-circle3 {
				  -webkit-transform: rotate(60deg);
				      -ms-transform: rotate(60deg);
				          transform: rotate(60deg);
				}
				.sk-fading-circle .sk-circle4 {
				  -webkit-transform: rotate(90deg);
				      -ms-transform: rotate(90deg);
				          transform: rotate(90deg);
				}
				.sk-fading-circle .sk-circle5 {
				  -webkit-transform: rotate(120deg);
				      -ms-transform: rotate(120deg);
				          transform: rotate(120deg);
				}
				.sk-fading-circle .sk-circle6 {
				  -webkit-transform: rotate(150deg);
				      -ms-transform: rotate(150deg);
				          transform: rotate(150deg);
				}
				.sk-fading-circle .sk-circle7 {
				  -webkit-transform: rotate(180deg);
				      -ms-transform: rotate(180deg);
				          transform: rotate(180deg);
				}
				.sk-fading-circle .sk-circle8 {
				  -webkit-transform: rotate(210deg);
				      -ms-transform: rotate(210deg);
				          transform: rotate(210deg);
				}
				.sk-fading-circle .sk-circle9 {
				  -webkit-transform: rotate(240deg);
				      -ms-transform: rotate(240deg);
				          transform: rotate(240deg);
				}
				.sk-fading-circle .sk-circle10 {
				  -webkit-transform: rotate(270deg);
				      -ms-transform: rotate(270deg);
				          transform: rotate(270deg);
				}
				.sk-fading-circle .sk-circle11 {
				  -webkit-transform: rotate(300deg);
				      -ms-transform: rotate(300deg);
				          transform: rotate(300deg); 
				}
				.sk-fading-circle .sk-circle12 {
				  -webkit-transform: rotate(330deg);
				      -ms-transform: rotate(330deg);
				          transform: rotate(330deg); 
				}
				.sk-fading-circle .sk-circle2:before {
				  -webkit-animation-delay: -1.1s;
				          animation-delay: -1.1s; 
				}
				.sk-fading-circle .sk-circle3:before {
				  -webkit-animation-delay: -1s;
				          animation-delay: -1s; 
				}
				.sk-fading-circle .sk-circle4:before {
				  -webkit-animation-delay: -0.9s;
				          animation-delay: -0.9s; 
				}
				.sk-fading-circle .sk-circle5:before {
				  -webkit-animation-delay: -0.8s;
				          animation-delay: -0.8s; 
				}
				.sk-fading-circle .sk-circle6:before {
				  -webkit-animation-delay: -0.7s;
				          animation-delay: -0.7s; 
				}
				.sk-fading-circle .sk-circle7:before {
				  -webkit-animation-delay: -0.6s;
				          animation-delay: -0.6s; 
				}
				.sk-fading-circle .sk-circle8:before {
				  -webkit-animation-delay: -0.5s;
				          animation-delay: -0.5s; 
				}
				.sk-fading-circle .sk-circle9:before {
				  -webkit-animation-delay: -0.4s;
				          animation-delay: -0.4s;
				}
				.sk-fading-circle .sk-circle10:before {
				  -webkit-animation-delay: -0.3s;
				          animation-delay: -0.3s;
				}
				.sk-fading-circle .sk-circle11:before {
				  -webkit-animation-delay: -0.2s;
				          animation-delay: -0.2s;
				}
				.sk-fading-circle .sk-circle12:before {
				  -webkit-animation-delay: -0.1s;
				          animation-delay: -0.1s;
				}

				@-webkit-keyframes sk-circleFadeDelay {
				  0%, 39%, 100% { opacity: 0; }
				  40% { opacity: 1; }
				}

				@keyframes sk-circleFadeDelay {
				  0%, 39%, 100% { opacity: 0; }
				  40% { opacity: 1; } 
				}
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        if ($_GET['id'] != '') {
				$query->where(function ($query)
				{
					return $query
					->where('proses_pentashihan.id',Request::get('id'));
				});
	        }  
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	    	$materi_tambahan_lain = DB::table('proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$id)
				->orderBy('id','ASC')
				->pluck('name');
			$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
			$materi_lainnya = str_replace(',',';',$format);

			$save_materi_lainnya['updated_at']     = Kemenag::now();
			$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;
			DB::table('proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	    	$materi_tambahan_lain = DB::table('proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$id)
				->orderBy('id','ASC')
				->pluck('name');
			$format         = str_replace(['[',']','"'],'',$materi_tambahan_lain);
			$materi_lainnya = str_replace(',',';',$format);

			$save_materi_lainnya['updated_at']     = Kemenag::now();
			$save_materi_lainnya['materi_lainnya'] = $materi_lainnya;
			DB::table('proses_pentashihan')->where('id',$id)->update($save_materi_lainnya);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

	    public function getUkuran($id){
	    	$ukuran = DB::table('proses_pentashihan_ukuran')
	    		->where('id_proses_pentashihan',$id)
	    		->get();

	    	foreach ($ukuran as $row) {
				$row->nomor             = ($row->nomor == '' ? '' : $row->nomor);
				$row->kode              = ($row->kode == '' ? '' : $row->nomor);
				$row->tanggal_penetapan = ($row->tanggal_penetapan == '' ? '' : $row->tanggal_penetapan);

	    		$url = asset('cek-tanda-tashih/scan/'.$id.'?key='.$row->id);
	    		$row->qrcode = 'https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl='.$url.'&choe=UTF-8';

	    		$row->surat_penerbitan = ($row->surat_penerbitan == '' ? '' : asset($row->surat_penerbitan));
	    		$row->scan_tanda_tashih = ($row->scan_tanda_tashih == '' ? '' : asset($row->scan_tanda_tashih));
	    	}
			
			$response['api_status']  = 1;
			$response['api_message'] = 'Success';
			$response['item']        = $ukuran;
			return response()->json($response);
		}
		
		public function getRatingUs($id)
		{
			$ukuran = DB::table('proses_pentashihan_rating')
	    		->where('id_proses_pentashihan',$id)
	    		->get();

	    	foreach ($ukuran as $row) {
				$row->jumlah_rating             = ($row->jumlah_rating == '' ? '' : $row->jumlah_rating);
				$row->komentar             = ($row->komentar == '' ? '---' : $row->komentar);
	    	}
			
			$response['api_status']  = 1;
			$response['api_message'] = 'Success';
			$response['item']        = $ukuran;
			return response()->json($response);
		}

	    public function getChangeStatus($status){
	    	$id = base64_decode(Request::get('key'));

			$save['updated_at'] = Kemenag::now();
			$save['status']     = $status;
			$action = DB::table('proses_pentashihan')
				->where('id',$id)
				->update($save);

			if ($action) {
				return redirect()->back()->with([
					'message'=>'Status berhasil diubah',
					'message_type'=>'success'
				]);
			}else{
				return redirect()->back()->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}
	    }

		public function getLoadDisposisi(){
			$id = Request::get('id');

			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();

			if (empty($proses_pentashihan)) {
				$response['api_status']  = 0;
				$response['api_message'] = 'Data tidak ditemukan'; 
			}else{
				$proses_pentashihan->extension      = self::extension($proses_pentashihan->disposisi_file);
				$proses_pentashihan->disposisi_file = ($proses_pentashihan->disposisi_file == '' ? '' : asset($proses_pentashihan->disposisi_file));

				$response['api_status']  = 1;
				$response['api_message'] = 'success'; 
				$response['data']        = $proses_pentashihan;
			}

			return response()->json($response);
		}

		public function postUpdateDisposisi(){
			$id = Request::get('id');

			$save['updated_at']             = Kemenag::now();
			$save['disposisi_nomor_agenda'] = Request::input('disposisi_nomor_agenda');
			if (Request::hasFile('disposisi_file')) {
				$save['disposisi_file'] = Kemenag::uploadFile('disposisi_file','disposisi',true);
			}
			$action = DB::table('proses_pentashihan')->where('id',$id)->update($save);

			if ($action) {
                $proses_pentashihan = DB::table('proses_pentashihan')->where('id',$id)->first();

				$config['content']      = "Disposisi Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Disposisi Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with([
					'message'=>'Update disposisi berhasil',
					'message_type'=>'success'
				]);
			}else{
				return redirect()->back()->with([
					'message'=>'Something went wrong',
					'message_type'=>'danger'
				]);
			}
		}

		public function getLoadKoreksi(){
			$id = Request::get('id');

			$koreksi = DB::table('proses_pentashihan_koreksi')
				->where('id_proses_pentashihan',$id)
				->whereNull('deleted_at')
				->orderBy('id','DESC')
				->get();

			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->first();

			foreach ($koreksi as $row) {
				$cms_users = DB::table('cms_users')
					->where('id',$row->id_cms_users)
					->first();

				$row->admin_name = $cms_users->name;

				$row->created_at  = ($row->created_at == '' ? '' : date('d M Y H:i',strtotime($row->created_at)));
				$row->revisi_date = ($row->revisi_date == '' ? '' : date('d M Y H:i',strtotime($row->revisi_date)));

				$row->lampiran        = ($row->lampiran == '' ? '' : asset($row->lampiran));
				$row->revisi_lampiran = ($row->revisi_lampiran == '' ? '' : asset($row->revisi_lampiran));

				$row->updated_at = ($row->updated_at == '' ? '' : $row->updated_at);
				$row->deleted_at = ($row->deleted_at == '' ? '' : $row->deleted_at);
				$row->pesan      = ($row->pesan == '' ? '' : $row->pesan);
				$row->revisi     = ($row->revisi == '' ? '' : $row->revisi);
			}

			if (empty($koreksi)) {
				$response['api_status']  = 0;
				$response['api_message'] = 'Data tidak ditemukan'; 
			}else{
				$response['api_status']  = 1;
				$response['api_message'] = 'success'; 
				$response['submit']      = ($proses_pentashihan->status_penerimaan == 1 ? 0 : 1);
				$response['data']        = $koreksi;
			}

			return response()->json($response);
		}

		public function postKoreksi(){
			if (!CRUDBooster::isSuperadmin()) {
				return redirect('admin')->with(['message'=>'Sorry, you don\'t have previlage here ', 'message_type'=>'danger']);
			}else{
				$id_proses_pentashihan = Request::input('id_proses_pentashihan');
				
				$save['created_at']            = date('Y-m-d H:i:s');
				$save['id_cms_users']          = CRUDBooster::myId();
				$save['pesan']                 = Request::input('pesan');
				$save['lampiran']              = Kemenag::uploadFile('lampiran','koreksi/'.$id_proses_pentashihan,true);
				$save['id_proses_pentashihan'] = $id_proses_pentashihan;
				$action = DB::table('proses_pentashihan_koreksi')->insert($save);

				if ($action) {
					$proses_pentashihan = DB::table('proses_pentashihan')
						->where('id',$id_proses_pentashihan)
						->first();

					$config['content']      = "Koreksi Naskah - ".$proses_pentashihan->nomor_registrasi;
					$config['to']           = CRUDBooster::adminPath('proses-pentashihan?id='.$id_proses_pentashihan);
					$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
					CRUDBooster::sendNotification($config);

					$config['content']      = "Koreksi Naskah - ".$proses_pentashihan->nomor_registrasi;
					$config['to']           = CRUDBooster::adminPath('list-tashih?id='.$id_proses_pentashihan);
					$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
					CRUDBooster::sendNotification($config);

					$save_status['updated_at'] = $save['created_at'];
					$save_status['status']     = 'Proses Perbaikan Naskah';
					DB::table('proses_pentashihan')->where('id',$save['id_proses_pentashihan'])->update($save_status);

					$to_sender=DB::table("proses_pentashihan")
					->select("cms_users.email","proses_pentashihan.*")
					->join("cms_users","cms_users.id","=","proses_pentashihan.id_cms_users")
					->where('proses_pentashihan.id',$id_proses_pentashihan)
					->first();

					$to_send=$to_sender->email;

					\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
					\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
					\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
					\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
					\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
					Mail::send('mail.send-status',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>'Proses Perbaikan Naskah'],function ($pesan) use ($to_send){

						$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
						$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
					});

					return redirect()->back()->with(['message'=>'Koreksi berhasil ditambahkan', 'message_type'=>'success']);
				}else{
					return redirect()->back()->with(['message'=>'Koreksi gagal ditambahkan', 'message_type'=>'warning']);
				}
			}
		}

		public function getHapusRevisi($id){
			if (!CRUDBooster::isSuperadmin()) {
				return redirect('admin')->with(['message'=>'Sorry, you don\'t have previlage here ', 'message_type'=>'danger']);
			}else{
				$action = DB::table('proses_pentashihan_koreksi')
					->where('id',$id)
					->delete();

				if ($action) {
					return redirect()->back()->with(['message'=>'Data berhasil dihapus', 'message_type'=>'success']);
				}else{
					return redirect()->back()->with(['message'=>'Data gagal dihapus', 'message_type'=>'warning']);
				}
			}
		}

		public function postPenerimaanNaskah(){
			$id = Request::input('id');

			$save['updated_at']        = Kemenag::now();
			// $save['bukti_penerimaan']  = Kemenag::uploadFile('bukti_penerimaan','proses_pentashihan',true);
			$save['nama_penerima']     = Request::input('nama_penerima');
			$save['tanggal_diterima']            = Request::input('tanggal_diterima');
			$save['status']            = 'Naskah Diterima';
			$save['status_penerimaan'] = 0;
			$action = DB::table('proses_pentashihan')->where('id',$id)->update($save);

			DB::table('proses_pentashihan_histori_status')
			->insert([
				'created_at'=>Kemenag::now(),
				'updated_at'=>Kemenag::now(),
				'id_proses_pentashihan'=>$id,
				'status'=>'Naskah Diterima',
			]);

			if ($action) {
				$proses_pentashihan = DB::table('proses_pentashihan')
					->where('id',$id)
					->first();

				$config['content']      = "Bukti Penerimaan Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Naskah Diterima - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$save_penerimaan['created_at']            = Kemenag::now();
				$save_penerimaan['id_proses_pentashihan'] = $id;
				$save_penerimaan['nama_penerima']     = Request::input('nama_penerima');
				// $save_penerimaan['bukti_penerimaan']      = $save['bukti_penerimaan'];
				$save_penerimaan['tanggal_penerimaan']    = date('Y-m-d');
				DB::table('proses_pentashihan_bukti_penerimaan')->insert($save_penerimaan);
				
				$to_sender=DB::table("proses_pentashihan")
				->select("cms_users.email","proses_pentashihan.*")
				->join("cms_users","cms_users.id","=","proses_pentashihan.id_cms_users")
				->where('proses_pentashihan.id',$id)
				->first();

				$to_send=$to_sender->email;

				\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
				\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
				\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
				\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
				\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
				Mail::send('mail.send-status',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>'Naskah Diterima'],function ($pesan) use ($to_send){

					$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
					$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
				});

				return redirect()->back()->with(['message'=>'Naskah sudah diterima', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
		}

		public function postVerifikasiNaskah(){
			$id       = Request::input('id');
			$estimasi = Request::input('estimasi');
// 			$nominal = str_replace('.','',Request::input('PNBP'));
			
			$now      = Kemenag::now();
// 			$deadline = date('Y-m-d H:i:s',strtotime($now.' +'.$estimasi.' day'));
            $deadline = Request::input('datepicker_akhir');
	        
	   //     $valid = Validator::make(Request::all(),[
	   //             'bukti_pembayaran'=>'mimes:pdf,jpg,jpeg,png,pneg,bmp,gif'
	   //         ]);		
	            
	   //    if($valid->fails()){
	   //        //dd($valid->errors());
    // 			return CRUDBooster::redirectback($valid->errors(),'error');
    // 		}else{
			$save['updated_at']         = $now;
			$save['tanggal_verifikasi'] = $now;
			$save['tanggal_deadline']   = $deadline;
			$save['bukti_verifikasi']   = Kemenag::uploadFile('bukti_verifikasi','proses_pentashihan',true);
// 			$save['bukti_pembayaran']   = Kemenag::uploadFile('bukti_pembayaran','proses_pentashihan',true);
			$save['status']             = Request::input('status');
// 			$save['PNBP']               = $nominal;
			$save['estimasi']           = Request::input('estimasi');
			$action = DB::table('proses_pentashihan')->where('id',$id)->update($save);

			DB::table('proses_pentashihan_histori_status')
			->insert([
				'created_at'=>Kemenag::now(),
				'updated_at'=>Kemenag::now(),
				'id_proses_pentashihan'=>$id,
				'status'=>Request::input('status'),
			]);

			if ($action) {
				$proses_pentashihan = DB::table('proses_pentashihan')
					->wherE('id',$id)
					->first();

				if ($save['status'] == 'Lolos Verifikasi') {
					$message = 'Naskah Lolos Verifikasi - '.$proses_pentashihan->nomor_registrasi;
					
				}else{
					$message = 'Naskah Tidak Lolos Verifikasi - '.$proses_pentashihan->nomor_registrasi;;
				}

				$config['content']      = $message;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = $message;
				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$last_penerimaan = DB::table('proses_pentashihan_bukti_penerimaan')
					->where('id_proses_pentashihan',$id)
					->orderBy('id','ASC')
					->first();
				if (!empty($last_penerimaan) && ($last_penerimaan->estimasi == '' || $last_penerimaan->tanggal_deadline == '')) {
					$save_penerimaan['updated_at']       = Kemenag::now();
					$save_penerimaan['tanggal_estimasi'] = date('Y-m-d'); 
					$save_penerimaan['estimasi']         = $save['estimasi'];
					$save_penerimaan['tanggal_deadline'] = $deadline;
					DB::table('proses_pentashihan_bukti_penerimaan')->where('id',$last_penerimaan->id)->update($save_penerimaan);
				}

				$to_sender=DB::table("proses_pentashihan")
				->select("cms_users.email","proses_pentashihan.*")
				->join("cms_users","cms_users.id","=","proses_pentashihan.id_cms_users")
				->where('proses_pentashihan.id',$id)
				->first();

				$to_send=$to_sender->email;

				\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
				\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
				\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
				\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
				\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
				Mail::send('mail.send-status',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>Request::input('status')],function ($pesan) use ($to_send){
					$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
					$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
				});

				return redirect()->back()->with(['message'=>$message, 'message_type'=>'success']);
				
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}   
	       //}
		}

		public function postFinalisasiNaskah(){
			$id                = Request::input('id');
			$id_ukuran         = Request::input('id_ukuran');
			$nomor             = Request::input('nomor_tanda_tashih');
			$kode              = Request::input('kode_tanda_tashih');
			$tanggal_penetapan = Request::input('tanggal_penetapan');
			$surat_penerbitan  = 'surat_penerbitan';
			$scan_tanda_tashih = 'scan_tanda_tashih';

			$save['status']             = 'Selesai';
			$save['updated_at']         = Kemenag::now();
			$action = DB::table('proses_pentashihan')->where('id',$id)->update($save);
			if ($action) {

				for ($i=0; $i < count($id_ukuran) ; $i++) { 
					$surat_penerbitan = Kemenag::uploadFileMultiple($i,'surat_penerbitan','proses_pentashihan',true);
					$scan_tanda_tashih = Kemenag::uploadFileMultiple($i,'scan_tanda_tashih','proses_pentashihan',true);

					$save_ukuran[$i]['updated_at']        = Kemenag::now();
					$save_ukuran[$i]['kode']              = $kode[$i];
					$save_ukuran[$i]['nomor']             = $nomor[$i];
					$save_ukuran[$i]['tanggal_penetapan'] = $tanggal_penetapan[$i];
					if ($surat_penerbitan != '') {
						$save_ukuran[$i]['surat_penerbitan']  = $surat_penerbitan;
					}
					if($scan_tanda_tashih != ''){
						$save_ukuran[$i]['scan_tanda_tashih']  = $scan_tanda_tashih;
					}
					
					DB::table('proses_pentashihan_ukuran')->where('id',$id_ukuran[$i])->update($save_ukuran[$i]);
				}

				$proses_pentashihan = DB::table('proses_pentashihan')
					->where('id',$id)
					->first();

				$config['content']      = "Finalisasi Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Finalisasi Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('penerbitan-tanda-tashih/detail/'.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$to_sender=DB::table("proses_pentashihan")
				->select("cms_users.email","proses_pentashihan.*")
				->join("cms_users","cms_users.id","=","proses_pentashihan.id_cms_users")
				->where('proses_pentashihan.id',$id)
				->first();

				$to_send=$to_sender->email;

				\Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
				\Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
				\Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
				\Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
				\Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));
				Mail::send('mail.send-status',["tgl"=>date('d M Y H:i:s'),"id"=>$to_sender->id,"status"=>"Selesai"],function ($pesan) use ($to_send){
					
					$pesan->to($to_send,CRUDBooster::getsetting("appname"))->subject("Pentashihan Mushaf Al-Quran");
					$pesan->from(CRUDBooster::getsetting("email_sender"),'Pentashihan Mushaf Al-Quran');
				});

				return redirect()->back()->with(['message'=>'Proses pentashihan telah selesai', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}
		}

		public function postTashihUser(){
			$save['created_at']            = Kemenag::now();
			$save['id_proses_pentashihan'] = Request::input('id');
			$save['id_cms_users']          = Request::input('id_cms_users');
			$save['juz']                   = Request::input('juz');
			$action = DB::table('proses_pentashihan_user')->insert($save);
			if ($action) {
				$proses_pentashihan = DB::table('proses_pentashihan')
					->where('id',Request::input('id'))
					->first();
				$cms_users = DB::table('cms_users')
					->where('id',Request::input('id_cms_users'))
					->first();

				$config['content']      = "Pembagian Pentashihan - ".$cms_users->name;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.Request::input('id'));
				$config['id_cms_users'] = [$cms_users->id]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Penugasan Pentashihan";
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.Request::input('id'));
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with(['message'=>'Tashih berhasil ditambahkan', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
			}

			return Request::all();
		}

		public function postBuktiPenerimaan(){
			$id                 = Request::input('id_proses_pentashihan');
			$estimasi           = Request::input('estimasi');
			$bukti_penerimaan   = Request::input('bukti_penerimaan');
			$tanggal_penerimaan = Request::input('tanggal_penerimaan');
			$tanggal_dead = Request::input('tanggal_deadline_saka');
			$deadline = date('Y-m-d H:i:s',strtotime(Kemenag::now().' +'.$estimasi.' day'));
			$nama_penerima = Request::input('nama_penerima');

			$save['id_proses_pentashihan'] = $id;
			$save['estimasi']              = $estimasi;
			$save['bukti_penerimaan']      = Kemenag::uploadFile('bukti_penerimaan','proses_pentashihan',true);
			$save['tanggal_penerimaan']    = $tanggal_penerimaan;
			$save['tanggal_estimasi']      = $tanggal_dead;
			$save['tanggal_deadline']      = $tanggal_dead;
			$save['nama_penerima']      = $nama_penerima;
			$act = DB::table('proses_pentashihan_bukti_penerimaan')->insert($save);

			if ($act) {
				$save_tashih['updated_at']        = Kemenag::now();
				$save_tashih['status_penerimaan'] = 0;
				$save_tashih['bukti_penerimaan']  = $save['bukti_penerimaan'];
				$save_tashih['tanggal_deadline']  = $tanggal_dead;
				$save_tashih['estimasi']          = $estimasi;
				DB::table('proses_pentashihan')->where('id',$id)->update($save_tashih);

				$proses_pentashihan = DB::table('proses_pentashihan')
					->where('id',$id)
					->first();

				$config['content']      = "Penerimaan Naskah - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('proses-pentashihan/detail/'.$id);
				$config['id_cms_users'] = [CRUDBooster::myId()]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				$config['content']      = "Naskah Diterima - ".$proses_pentashihan->nomor_registrasi;
				$config['to']           = CRUDBooster::adminPath('list-tashih/detail/'.$id);
				$config['id_cms_users'] = [$proses_pentashihan->id_cms_users]; //This is an array of id users
				CRUDBooster::sendNotification($config);

				return redirect()->back()->with(['message'=>'Data berhasil ditambah', 'message_type'=>'success']);
			}else{
				return redirect()->back()->with(['message'=>'Data gagal ditambah', 'message_type'=>'warning']);
			}
		}

		public function extension($str){
			if ($str == '') {
				$result = null;
			}else{
				$data   = explode('.', $str);
				$length = count($data)-1;
				$result = $data[$length];
			}

			return $result;
		}

		public function getListUsersTashih()
		{
			$find = $this->listPentashih();

			$res['message'] = 'success';
			$res['item'] = $find;

			return response()->json($res, 200);
		}

		public function getDetail($id)
		{
	    	//Create an Auth
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {    
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$check = DB::table('proses_pentashihan')
				->select('proses_pentashihan.*','cms_users.name as cms_users_name','m_jenis_naskah.name as m_jenis_naskah_name','proses_pentashihan.nomor_registrasi as mushaf_lama_nomor_registrasi','countries.name as countries_name')
				->leftJoin('cms_users','proses_pentashihan.id_cms_users','cms_users.id')
				->leftJoin('m_jenis_naskah','proses_pentashihan.id_m_jenis_naskah','m_jenis_naskah.id')
				->leftJoin('countries','proses_pentashihan.id_countries','countries.id')
				->where('proses_pentashihan.id',$id)
				->first();

			if($check->id != null)
			{
				$check->proses_pentashihan_user = DB::table('proses_pentashihan_user')
												->select('proses_pentashihan_user.*','cms_users.name as cms_users_name')
												->leftJoin('cms_users','cms_users.id','proses_pentashihan_user.id_cms_users')
												->where("id_proses_pentashihan",$check->id)
												->get();

				$check->proses_pentashihan_koreksi = DB::table('proses_pentashihan_koreksi')
												->select('proses_pentashihan_koreksi.*','cms_users.name as cms_users_name')
												->leftJoin('cms_users','cms_users.id','proses_pentashihan_koreksi.id_cms_users')
												->where("proses_pentashihan_koreksi.id_proses_pentashihan",$check->id)
												->get();

				$check->proses_pentashihan_laporan_percetakan = DB::table('proses_pentashihan_laporan_percetakan')->where("id_proses_pentashihan",$check->id)->get();
				$check->proses_pentashihan_surat_pernyataan = DB::table('proses_pentashihan_surat_pernyataan')->where("id_proses_pentashihan",$check->id)->get();
				$check->proses_pentashihan_bukti_penerimaan = DB::table('proses_pentashihan_bukti_penerimaan')->where("id_proses_pentashihan",$check->id)->get();
				$check->proses_pentashihan_ukuran = DB::table('proses_pentashihan_ukuran')->where("id_proses_pentashihan",$check->id)->get();
				$check->materi_tambahan = DB::table('proses_pentashihan_materi_tambahan')
				->where('id_proses_pentashihan',$check->id)
				->get();
			}

			$data               = [];
			$data['page_title'] = 'Preview Tashih';
			$data['token']      = csrf_token();

			$data['row']          = $check;
			$this->cbView('admin/admin_detail_proses_pentashihan',$data);
		}

	}