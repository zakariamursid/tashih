<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontCekTandaTashihController extends Controller
{
	public function getIndex(){
		Kemenag::menu('cektandatashih');
		$proses_pentashihan = DB::table('proses_pentashihan')
		    ->whereNull('deleted_at')
			->where('status','Selesai');
			
		$berdasarkan        = Request::get('berdasarkan');
		$kata_kunci         = Request::get('kata_kunci');

		if ($berdasarkan != '') {
			switch ($berdasarkan) {
				case 'Nama Penerbit':
					$penerbit = DB::table('cms_users')
						->where('name','LIKE','%'.$kata_kunci.'%')
						->where('id_cms_privileges',2)
						->pluck('id');

					$proses_pentashihan = $proses_pentashihan->whereIn('id_cms_users',$penerbit);
					break;

				case 'Nama Mushaf':
					$proses_pentashihan = $proses_pentashihan->where('nama_produk','LIKE','%'.$kata_kunci.'%');
					break;

				case 'Nomor Tanda Tashih':
					$ukuran = DB::table('proses_pentashihan_ukuran')
						->where('nomor','LIKE','%'.$kata_kunci.'%')
						->pluck('id_proses_pentashihan');
					// $proses_pentashihan = $proses_pentashihan->where('nomor_tanda_tashih','LIKE','%'.$kata_kunci.'%');
					$proses_pentashihan = $proses_pentashihan->whereIn('id',$ukuran);
					break;
			}
		}

		$proses_pentashihan = $proses_pentashihan->paginate(10);

		foreach ($proses_pentashihan as $x => $xrow) {
			$cms_users = DB::table('cms_users')
				->where('id',$xrow->id_cms_users)
				->first();

			$xrow->users_name = ($cms_users?$cms_users->name:'');

			$nomor_tanda_tashih = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$xrow->id)
				->orderBy('id','DESC')
				->get();

			$nomor_tashih = [];
			foreach ($nomor_tanda_tashih as $yrow) {
				array_push($nomor_tashih, $yrow->nomor);
			}
			$nomor_tashih = '('.implode('), (',$nomor_tashih).')';
			$xrow->nomor_tashih = $nomor_tashih;

			$xrow->ukurantashih = $nomor_tanda_tashih;

			$cover = asset($xrow->dokumen_naskah);
// 			if (file_exists($cover)) {
// 				$cover = $cover;
// 			}else{
// 				$cover = 'https://via.placeholder.com/430x300?text=Image+Not+Found';
// 			}

			$xrow->cover = $cover;
		}
		
		$data = [];
		$data['proses_pentashihan'] = $proses_pentashihan;
		
		if(Request::ajax()){
			$data['kunci_satu']=Session::get('berdasarkan');
			$data['kunci_dua']=Session::get('kata_kunci');
			return view('front.tabelcektandatansih', $data);
		}

		$data['kunci_satu']=Request::get('berdasarkan');
		$data['kunci_dua']=Request::get('kata_kunci');
		return view('front/cektandatashih', $data);
	}

	public function getInfo(){
		Kemenag::menu('info');
		Kemenag::menuSub('info-penerbit');
		
		$proses_pentashihan = DB::table('cms_users')
			->where("status","Aktif")
            ->where('id_cms_privileges',2)
			->paginate(10);

		foreach ($proses_pentashihan as $x => $xrow) {
			$datamushaf = DB::table('proses_pentashihan')
				->where("id_cms_users", $xrow->id)
				->whereNull("deleted_at")
				->groupby("nama_produk")
				->get();

			foreach ($datamushaf as $y => $yrow) {
				$ukurantashih = DB::table('proses_pentashihan_ukuran')
					->where('id_proses_pentashihan', $yrow->id)
					->first();

				$yrow->ukuran = ($ukurantashih->ukuran?:'---'); 
				$yrow->kode = ($ukurantashih->kode?:'---'); 
				$yrow->id_ukuran = ($ukurantashih->id?:0); 

				$cover = asset($yrow->dokumen_naskah);
				// if (file_exists($cover)) {
				// 	$cover = $cover;
				// }else{
				// 	$cover = 'https://via.placeholder.com/430x300?text=Image+Not+Found';
				// }
	
				$yrow->cover = $cover;
			}

			$xrow->datamushaf = $datamushaf;
		}

		$data = [];
		$data['proses_pentashihan'] = $proses_pentashihan;
		return view('front/infopenerbit', $data);
	}

	public function getScan($id){
	    Kemenag::menu('cektandatashih');
		if (CRUDBooster::myId() != '') {
			$proses_pentashihan = DB::table('proses_pentashihan')
				->where('id',$id)
				->whereNull('deleted_at')
				->first();
			if (CRUDBooster::myPrivilegeId() == 2 && $proses_pentashihan->id_cms_users == CRUDBooster::myId()) {
				return redirect(CRUDBooster::adminPath('list-tashih/detail/'.$id));
			}else{
				return redirect(CRUDBooster::adminPath('proses-pentashihan/detail/'.$id));
			}
		}

		$proses_pentashihan = DB::table('proses_pentashihan')
			->where('status','Selesai')
			->whereNull("deleted_at")
			->where('id',$id)
			->limit(1)
			->paginate(100);
			
		foreach ($proses_pentashihan as $x => $xrow) {
			$cms_users = DB::table('cms_users')
				->where('id',$xrow->id_cms_users)
				->first();

			$xrow->users_name = ($cms_users?$cms_users->name:'');

			$nomor_tanda_tashih = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$xrow->id)
				->orderBy('id','DESC')
				->get();

			$nomor_tashih = [];
			foreach ($nomor_tanda_tashih as $yrow) {
				array_push($nomor_tashih, $yrow->nomor);
			}
			$nomor_tashih = '('.implode('), (',$nomor_tashih).')';
			$xrow->nomor_tashih = $nomor_tashih;

			$xrow->ukurantashih = $nomor_tanda_tashih;

			$cover = asset($xrow->dokumen_naskah);
// 			if (file_exists($cover)) {
// 				$cover = $cover;
// 			}else{
// 				$cover = 'https://via.placeholder.com/430x300?text=Image+Not+Found';
// 			}

			$xrow->cover = $cover;
		}

		$data = [];
		$data['proses_pentashihan'] = $proses_pentashihan;
		$data['scan']=$id;
		return view('front/cektandatashih', $data);
	}
	
    public function postApiScan(){
        $valid['code'] = "required";
        Kemenag::Validator($valid);
            
        // get params
        $url = Request::input("code");
        // removew params from url
        $url = preg_replace('/'. (false ? '(\&|)'.false.'(\=(.*?)((?=&(?!amp\;))|$)|(.*?)\b)' : '(\?.*)').'/i' , '', $url);
        // get code from link
        $get =  explode('/', $url);
        $code = $get[5];
        // check is numeric or not
        if(is_numeric($code)){
            $proses_pentashihan_count = DB::table('proses_pentashihan')
    			->where('status','Selesai')
    			->whereNull("deleted_at")
    			->where('id',$code)
    			->count();
    			
            // check is data exists or not in database
            if($proses_pentashihan_count <= 0){
                $res['api_status'] = 0;
                $res['api_title'] = "Opps, something went wrong";
                $res['api_message'] = "Proses Pentashihan is not found.";
                $res['code'] = $code;
            }else{
                 $proses_pentashihan = DB::table('proses_pentashihan')
        			->where('status','Selesai')
        			->whereNull("deleted_at")
        			->where('id',$code)
        			->get();
        		
                // make new aray
                $list = [];
    			foreach($proses_pentashihan as $x => $xrow){
    			    $xrow->id  = ($xrow->id?:0);
    			    $xrow->created_at  = ($xrow->created_at?:"");
    			    $xrow->updated_at  = ($xrow->updated_at?:"");
    			    $xrow->deleted_at  = ($xrow->deleted_at?:"");
    			    $xrow->nomor_registrasi  = ($xrow->nomor_registrasi?:"");
    			    $xrow->id_cms_users  = ($xrow->id_cms_users?:0);
    			    $xrow->id_m_jenis_naskah  = ($xrow->id_m_jenis_naskah?:"");
    			    $xrow->file_aplikasi  = ($xrow->file_aplikasi?:"");
    			    $xrow->id_m_jenis_mushaf  = ($xrow->id_m_jenis_mushaf?:0);
    			    $xrow->id_m_materi_tambahan  = ($xrow->id_m_materi_tambahan?:"");
    			    $xrow->materi_lainnya  = ($xrow->materi_lainnya?:"");
    			    $xrow->penanggung_jawab_produk  = ($xrow->penanggung_jawab_produk?:"");
    			    $xrow->nama_produk  = ($xrow->nama_produk?:"");
    			    $xrow->ukuran  = ($xrow->ukuran?:"");
    			    $xrow->oplah  = ($xrow->oplah?:"");
    			    $xrow->nama_percetakan  = ($xrow->nama_percetakan?:"");
    			    $xrow->keterangan  = ($xrow->keterangan?:"");
    			    $xrow->tashih_internal  = ($xrow->tashih_internal?:"");
    			    $xrow->penanggung_jawab_materi_tambahan  = ($xrow->penanggung_jawab_materi_tambahan?:"");
    			    $xrow->surat_permohonan  = (Kemenag::file($xrow->surat_permohonan)?:"");
    			    $xrow->gambar_cover  = (Kemenag::file($xrow->gambar_cover)?:"");
    			    $xrow->dokumen_naskah  = (Kemenag::file($xrow->dokumen_naskah)?:"");
    			    $xrow->bukti_tashih_internal  = ($xrow->bukti_tashih_internal?:"");
    			    $xrow->status  = ($xrow->status?:"");
    			    $xrow->disposisi_nomor_agenda  = ($xrow->disposisi_nomor_agenda?:"");
    			    $xrow->disposisi_file  = ($xrow->disposisi_file?:"");
    			    $xrow->file_naskah  = ($xrow->file_naskah?:"");
    			    $xrow->bukti_penerimaan  = (Kemenag::file($xrow->bukti_penerimaan)?:"");
    			    $xrow->status_penerimaan  = ($xrow->status_penerimaan?:0);
    			    $xrow->tanggal_verifikasi  = ($xrow->tanggal_verifikasi?:"");
    			    $xrow->tanggal_deadline  = ($xrow->tanggal_deadline?:"");
    			    $xrow->estimasi  = ($xrow->estimasi?:0);
    			    $xrow->bukti_verifikasi  = (Kemenag::file($xrow->bukti_verifikasi)?:"");
    			    $xrow->nomor_tanda_tashih  = ($xrow->nomor_tanda_tashih?:"");
    			    $xrow->kode_tanda_tashih  = ($xrow->kode_tanda_tashih?:"");
    			    $xrow->surat_penerbitan  = ($xrow->surat_penerbitan?:"");
    			    $xrow->scan_tanda_tashih  = ($xrow->scan_tanda_tashih?:"");
    			    $xrow->no_resi  = ($xrow->no_resi?:"");
    			    $xrow->surat_pernyataan  = ($xrow->surat_pernyataan?:0);
    			    $xrow->submit  = ($xrow->submit?:"");
    			    $xrow->PNBP  = ($xrow->PNBP?:"");
    			    $xrow->bukti_pembayaran  = ($xrow->bukti_pembayaran?:"");
    			    $xrow->nominal_PNBP  = ($xrow->nominal_PNBP?:0);
    			    $xrow->bukti_PNBP  = ($xrow->bukti_PNBP?:"");
    			    $xrow->status_PNBP  = ($xrow->status_PNBP?:"");
    			    
    		        $list[] = $xrow;
    			}
                
                $res['api_status'] = 1;
                $res['api_title'] = "success";
                $res['api_message'] = "success";
                $res['code'] = $code;
                $res['item'] = $list;    
            }
        }else{
            $res['api_status'] = 0;
            $res['api_title'] = "Opps, something went wrong";
            $res['api_message'] = "Can't get code params";
            $res['code'] = $code;
        }
        return response()->json($res);
    }
}