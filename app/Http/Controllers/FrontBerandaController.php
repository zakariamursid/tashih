<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use App\Services\ListSiaranPersServices;
use App\Services\BannerLayananServices;

class FrontBerandaController extends Controller
{
	public function getIndex(){
		Kemenag::menu('beranda');

		$banner = DB::table('banner')
			->whereNull('deleted_at')
			->where('status','Aktif')
			->orderBy('updated_at','DESC')
			->get();
		foreach ($banner as $databanner) {
			$databanner->admin_name = Kemenag::adminName($databanner->id_cms_users);
			$databanner->image      = ($databanner->image == '' ? Kemenag::emptyImage() : asset($databanner->image));
			
			$databanner->link = url('info-seputar-lajnah/read/'.$databanner->slug);
		}

		$new_info = [];
		$mushaf = DB::table('gallery_mushaf')
			->orderBy('id','DESC')
			->whereNull('deleted_at')
			->limit(3)
			->get();
		$list_id_mushaf = '';
		$total   = count($mushaf);
		$max_row = $total-1;
		$num     = 0;
		foreach ($mushaf as $row_mushaf) {
			if(empty($row_mushaf->title)){
			    continue;
			}else{
			    $cms_users = DB::table('cms_users')
				->where('id',$row_mushaf->id_cms_users)
				->first();
			$row_mushaf->admin_name = $cms_users->name;
			$row_mushaf->image      = ($row_mushaf->image == '' ? asset('image/no-image.png') : asset($row_mushaf->image));
			$row_mushaf->href       = asset('info-layanan-pentashihan/read/'.$row_mushaf->slug);

			$length_title                   = strlen($row_mushaf->title);
			$title                          = substr($row_mushaf->title, 0, 55);
			$row_mushaf->title = $title.($length_title > 55 ? '...' : '');

			$list_id_mushaf .= $row_mushaf->id;
			$list_id_mushaf .= ($num < $max_row ? ';' : '');

			$num++;
			array_push($new_info, $row_mushaf);
			}
		}
		$id_mushaf = explode(';', $list_id_mushaf);

		$info_seputar_lajnah = DB::table('info_seputar_lajnah')
			->orderby('id','desc')
			->whereNull('deleted_at')
			->limit(3)
			->get();

		$list_id = '';
		$total   = count($info_seputar_lajnah);
		$max_row = $total-1;
		$num     = 0;
		foreach ($info_seputar_lajnah as $row_info_seputar_lajnah) {
			$cms_users = DB::table('cms_users')
				->where('id',$row_info_seputar_lajnah->id_cms_users)
				->first();
			$row_info_seputar_lajnah->admin_name = $cms_users->name;
			$row_info_seputar_lajnah->image      = ($row_info_seputar_lajnah->image == '' ? asset('image/no-image.png') : asset($row_info_seputar_lajnah->image));
			$row_info_seputar_lajnah->href       = asset('info-seputar-lajnah/read/'.$row_info_seputar_lajnah->slug);

			$length_title                   = strlen($row_info_seputar_lajnah->title);
			$title                          = substr($row_info_seputar_lajnah->title, 0, 55);
			$row_info_seputar_lajnah->title = $title.($length_title > 55 ? '...' : '');

			$list_id .= $row_info_seputar_lajnah->id;
			$list_id .= ($num < $max_row ? ';' : '');

			$num++;

			array_push($new_info, $row_info_seputar_lajnah);
		}
		$id       = explode(';', $list_id);
		$new_info = collect($new_info)->sortBy('created_at')->reverse()->toArray();

		$info_lainnya = DB::table('info_seputar_lajnah')
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			// ->whereNotIn('id',$id)
			->limit(6)
			->get();

		foreach ($info_lainnya as $row_info_lainnya) {
			$row_info_lainnya->href  = asset('info-seputar-lajnah/read/'.$row_info_lainnya->slug);
			$row_info_lainnya->image = ($row_info_lainnya->image == '' ? asset('image/no-image.png') : asset($row_info_lainnya->image));
		}

//		$list_gallery_mushaf = DB::table('gallery_mushaf')
//			->whereNull('deleted_at')
//			->limit(6)
//			// ->whereNotIn('id',$id_mushaf)
//			->orderBy('id','DESC')
//			->get();

//		$total          = count($list_gallery_mushaf);
//		$akar           = $total/4;
//		$jml_master     = ceil($akar);
//		$num            = 0;
//		$second_loop    = 0;
//		$gallery_mushaf = [];
		
//		foreach ($list_gallery_mushaf as $row_list_gallery_mushaf) {
//			if($num == $jml_master) break;
//			if ($num < $jml_master) {
//				$cms_users = DB::table('cms_users')
//					->where('id',$row_list_gallery_mushaf->id_cms_users)
//					->first();
//				$admin_name = $cms_users->name;
//
//				$strip      = strip_tags($row_list_gallery_mushaf->content);
//				$lenght     = strlen($content);
//				$content    = substr($strip, 0, 370).($lenght > 370 ? '...' : '');
//				$image      = ($row_list_gallery_mushaf->image == '' ? Kemenag::emptyImage() : asset($row_list_gallery_mushaf->image));
//				$created_at = Kemenag::dateIndonesia($row_list_gallery_mushaf->created_at);
//
//				$row_list_gallery_mushaf->image      = $image;
//				$row_list_gallery_mushaf->content    = $content;
//				$row_list_gallery_mushaf->created_at = $created_at;
//				$row_list_gallery_mushaf->admin_name = $admin_name;
//
//				array_push($gallery_mushaf, $row_list_gallery_mushaf);
//			}
//			$num++;
//		}
//
//		foreach ($gallery_mushaf as $row_gallery_mushaf) {
//			$skip  = $jml_master+($second_loop*3);
//			$limit = 3;
//			$mini_gallery_mushaf = DB::table('gallery_mushaf')
//				->whereNull('deleted_at')
//				->limit($limit)
//				->skip($skip)
//				->orderBy('id','DESC')
//				->get();
//
//			foreach ($mini_gallery_mushaf as $row_mini_gallery_mushaf) {
//				$cms_users = DB::table('cms_users')
//					->where('id',$row_mini_gallery_mushaf->id_cms_users)
//					->first();
//				$admin_name = $cms_users->name;
//
//				$strip      = strip_tags($row_mini_gallery_mushaf->content);
//				$lenght     = strlen($content);
//				$content    = substr($strip, 0, 370).($lenght > 370 ? '...' : '');
//				$image      = ($row_mini_gallery_mushaf->image == '' ? Kemenag::emptyImage() : asset($row_mini_gallery_mushaf->image));
//				$created_at = Kemenag::dateIndonesia($row_mini_gallery_mushaf->created_at);
//
//				$row_mini_gallery_mushaf->image      = $image;
//				$row_mini_gallery_mushaf->content    = $content;
//				$row_mini_gallery_mushaf->created_at = $created_at;
//				$row_mini_gallery_mushaf->admin_name = $admin_name;
//				$row_mini_gallery_mushaf->href       = asset('info-layanan-pentashihan/read/'.$row_mini_gallery_mushaf->slug);
//			}
//
//			$row_gallery_mushaf->mini = $mini_gallery_mushaf;
//			$row_gallery_mushaf->href = asset('info-layanan-pentashihan/read/'.$row_gallery_mushaf->slug);
//			$second_loop++;
//		}
        
		$result['banner']      = $banner;
		$result['info_seputar_lajnah'] = $info_seputar_lajnah;
		$result['info_lainnya']        = $info_lainnya;
		$result['gallery_mushaf']      = $this->listGallery();
		$result['new_info']            = ListSiaranPersServices::listBannerHeader();
		$result['banner_layanan'] 	   = BannerLayananServices::listBannerLayanan();

		return view('front/beranda',$result);
	}

	function listGallery() {
	    return DB::table('gallery_mushaf')
			->whereNull('deleted_at')
			->limit(6)
			->orderBy('id','DESC')
			->get();
    }

	public function getRating()
	{
		$simpan=DB::table('rating')
		->insert([
			"created_at"=> date("Y-m-d h:i:s"),
			"updated_at"=> date("Y-m-d h:i:s"),
			"id_cms_users"=> Request::get('id_cms_users'),
			"rating"=> Request::get('jumlah_rating'),
		]);
		if ($simpan) {
			return response()->json(['status'=>'ok']);
		}else{
			return response()->json(['status'=>'ggl']);
		}
	}
	
	public function getLocation()
	{
		$db=DB::table('master_pointing_user')
			->select('master_pointing_user.lat','master_pointing_user.lng','cms_users.*')
			->join('cms_users','master_pointing_user.id_cms_users','=','cms_users.id')
			->where('cms_users.status','Aktif')
			->get();
		return response()->json($db);
	}
}