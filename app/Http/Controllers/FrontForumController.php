<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontForumController extends Controller
{

	public function getIndex(){
		return redirect('forum/pertanyaan');
	}

	public function getPertanyaanGenerate(){
		$forum = DB::table('forum')
			->where('type','Pertanyaan')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->whereNotNull('id_cms_user')
			->whereNull('nama_penanya')
			->get();

		foreach ($forum as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_user)
				->first();

			$save['updated_at']   = Kemenag::now();
			$save['nama_penanya'] = $cms_users->name;
			DB::table('forum')->where('id',$row->id)->update($save);
		}
	}

	public function getPertanyaan(){
		Kemenag::menu('forum');
		Kemenag::menuSub('forum-pertanyaan');

		$forum = DB::table('forum')
			->where('type','Pertanyaan')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->paginate(5);
		foreach ($forum as $row_forum) {
			$cms_users = DB::table('cms_users')
				->where('id',$row_forum->id_cms_user)
				->first();
			$jawaban = DB::table('forum_jawaban')
				->where('id_forum',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();
			foreach ($jawaban as $row_jawaban) {
				$cms_users_jawab = DB::table('cms_users')
					->where('id',$row_jawaban->id_cms_user)
					->first();
				$row_jawaban->name = $cms_users_jawab->name;
			}

			$row_forum->name          = $row_forum->nama_penanya;
			$row_forum->total_jawaban = count($jawaban);
			$row_forum->jawaban       = $jawaban;
		}

		$result['forum'] = $forum;

		return view('front/forumpertanyaan',$result);
	}

	public function getPertanyaanJson(){
		$forum = DB::table('forum')
			->where('type','Pertanyaan')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->paginate(5);
		$response = [];
		foreach ($forum as $row_forum) {
			$jawaban = DB::table('forum_jawaban')
				->where('id_forum',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();

			$list_jawaban = [];
			foreach ($jawaban as $row_jawaban) {
				$cms_users_jawab = DB::table('cms_users')
					->where('id',$row_jawaban->id_cms_user)
					->first();
				$row_jawaban->name = $cms_users_jawab->name;

				$rest_jawaban['name']    = $cms_users_jawab->name;
				$rest_jawaban['tanggal'] = Kemenag::dateIndonesia($row_jawaban->created_at);
				$rest_jawaban['content'] = nl2br($row_jawaban->isi);
				$rest_jawaban['lampiran'] = ($row_jawaban->lampiran == '' ? '-' : asset($row_jawaban->lampiran));
				array_push($list_jawaban, $rest_jawaban);
			}

			$rest['id']            = $row_forum->id;
			$rest['nama']          = $row_forum->nama_penanya;
			$rest['tanggal']       = Kemenag::dateIndonesia($row_forum->created_at);
			$rest['judul']         = $row_forum->judul;
			$rest['content']       = nl2br($row_forum->isi);
			$rest['lampiran']      = ($row_forum->lampiran == '' ? '-' : asset($row_forum->lampiran));
			$rest['total_jawaban'] = count($jawaban);
			$rest['jawaban']       = $list_jawaban;

			array_push($response, $rest);
		}

		return response()->json($response);
	}

	public function postPertanyaan(){
		$valid = Validator::make(Request::all(),[
			'lampiran'=>'required|mimes:jpeg,bmp,png,gif,svg,pdf',
		]);

		if($valid->fails()){
			return redirect('forum/pertanyaan')->with([
				'auth_message'=> $valid->errors()->first(),
			]);
		}else{
			if(empty(Request::input('g-recaptcha-response'))){
				return redirect('forum/pertanyaan')->with([
					'auth_message'=>'Please check the the captcha form.'
				]);
			}else {
				$now  = Kemenag::now();
	
				$save['created_at']   = $now;
				$save['updated_at']   = $now;
				$save['type']         = 'Pertanyaan';
				$save['judul']        = Request::input('judul');
				$save['isi']          = Request::input('isi');
				$save['lampiran']     = Kemenag::uploadFile('lampiran','forum-pertanyaan',true);
				$save['nama_penanya'] = Request::input('nama_penanya');
				$id = DB::table('forum')->insertGetId($save);
				
				if ($id) {
					$cms_users = DB::table('cms_users')
						->where('id_cms_privileges','!=',2)
						->pluck('id');
					$config['content']      = "Pertanyaan Baru";
					$config['to']           = CRUDBooster::adminPath('tanya-jawab?id='.$id);
					$config['id_cms_users'] = $cms_users; //This is an array of id users
					CRUDBooster::sendNotification($config);
	
					return redirect('forum/pertanyaan')->with([
						'auth_message'=>'Pertanyaan berhasil di ajukan.'
					]);
				}else{
					return redirect('forum/pertanyaan')->with([
						'auth_message'=>'something went wrong'
					]);
				}
			}
		}
	}

	public function postPertanyaanJawaban(){
		$now  = Kemenag::now();
		$page = Request::input('page');
		$id   = Request::input('id_forum');

		$save['created_at']  = $now;
		$save['updated_at']  = $now;
		$save['isi']         = Request::input('isi');
		$save['lampiran']    = Kemenag::uploadFile('lampiran','forum-pertanyaan',true);
		$save['id_forum']    = Request::input('id_forum');
		$save['id_cms_user'] = CRUDBooster::myId();
		DB::table('forum_jawaban')->insert($save);

		return redirect('forum/pertanyaan?page='.$page.'&id='.$id);
	}

	public function getKonsultasi(){
		if (CRUDBooster::myId() == '') {
			return redirect('forum/pertanyaan')->with([
				'auth_message'=>'Undifined Page'
			]);
		}
		Kemenag::menu('forum');
		Kemenag::menuSub('forum-konsultasi');

		$forum = DB::table('forum')
			->where('type','Konsultasi')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->paginate(5);
		foreach ($forum as $row_forum) {
			$cms_users = DB::table('cms_users')
				->where('id',$row_forum->id_cms_user)
				->first();
			$jawaban = DB::table('forum_jawaban')
				->where('id_forum',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();
			foreach ($jawaban as $row_jawaban) {
				$cms_users_jawab = DB::table('cms_users')
					->where('id',$row_jawaban->id_cms_user)
					->first();
				$row_jawaban->name = $cms_users_jawab->name;
			}

			$row_forum->name          = $cms_users->name;
			$row_forum->total_jawaban = count($jawaban);
			$row_forum->jawaban       = $jawaban;
		}

		$result['forum'] = $forum;

		return view('front/forumkonsultasi',$result);
	}

	public function getKonsultasiJson(){
		$forum = DB::table('forum')
			->where('type','Konsultasi')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->paginate(5);
		$response = [];
		foreach ($forum as $row_forum) {
			$cms_users = DB::table('cms_users')
				->where('id',$row_forum->id_cms_user)
				->first();
			$jawaban = DB::table('forum_jawaban')
				->where('id_forum',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();

			$list_jawaban = [];
			foreach ($jawaban as $row_jawaban) {
				$cms_users_jawab = DB::table('cms_users')
					->where('id',$row_jawaban->id_cms_user)
					->first();
				$row_jawaban->name = $cms_users_jawab->name;

				$rest_jawaban['name']    = $cms_users_jawab->name;
				$rest_jawaban['tanggal'] = Kemenag::dateIndonesia($row_jawaban->created_at);
				$rest_jawaban['content'] = nl2br($row_jawaban->isi);
				$rest_jawaban['lampiran'] = ($row_jawaban->lampiran == '' ? '-' : asset($row_jawaban->lampiran));
				array_push($list_jawaban, $rest_jawaban);
			}

			$rest['id']            = $row_forum->id;
			$rest['nama']          = $cms_users->name;
			$rest['tanggal']       = Kemenag::dateIndonesia($row_forum->created_at);
			$rest['judul']         = $row_forum->judul;
			$rest['content']       = nl2br($row_forum->isi);
			$rest['lampiran']      = ($row_forum->lampiran == '' ? '-' : asset($row_forum->lampiran));
			$rest['total_jawaban'] = count($jawaban);
			$rest['jawaban']       = $list_jawaban;

			array_push($response, $rest);
		}

		return response()->json($response);
	}

	public function postKonsultasi(){
		if (CRUDBooster::myId() == '') {
			return redirect('forum/pertanyaan')->with([
				'auth_message'=>'Undifined Page'
			]);
		}
		$now  = Kemenag::now();

		$save['created_at']  = $now;
		$save['updated_at']  = $now;
		$save['type']        = 'Konsultasi';
		$save['judul']       = Request::input('judul');
		$save['isi']         = Request::input('isi');
		$save['id_cms_user'] = CRUDBooster::myId();
		$save['lampiran']    = Kemenag::uploadFile('lampiran','forum-konsultasi',true);
		$id = DB::table('forum')->insertGetId($save);
		
		if ($id) {
			$cms_users = DB::table('cms_users')
				->where('id_cms_privileges','!=',2)
				->pluck('id');
			$config['content']      = "Konsultasi Baru";
			$config['to']           = CRUDBooster::adminPath('konsultasi?id='.$id);
			$config['id_cms_users'] = $cms_users; //This is an array of id users
			CRUDBooster::sendNotification($config);

			return redirect('forum/konsultasi')->with([
				'auth_message'=>'Konsultasi berhasil di ajukan.'
			]);
		}else{

			return redirect('forum/konsultasi')->with([
				'auth_message'=>'something went wrong'
			]);
		}
	}

	public function postKonsultasiJawaban(){
		if (CRUDBooster::myId() == '') {
			return redirect('forum/pertanyaan')->with([
				'auth_message'=>'Undifined Page'
			]);
		}
		$now  = Kemenag::now();
		$page = Request::input('page');
		$id   = Request::input('id_forum');

		$save['created_at']  = $now;
		$save['updated_at']  = $now;
		$save['isi']         = Request::input('isi');
		$save['lampiran']    = Kemenag::uploadFile('lampiran','forum-konsultasi',true);
		$save['id_forum']    = Request::input('id_forum');
		$save['id_cms_user'] = CRUDBooster::myId();
		DB::table('forum_jawaban')->insert($save);

		$forum = DB::table('forum')
			->where('id',$save['id_forum'])
			->first();
		if ($forum->type == 'Konsultasi' && $forum->id_cms_user != $save['id_cms_user']) {
			$config['content']      = "Jawaban Konsultasi";
			$config['to']           = CRUDBooster::adminPath('konsultasi?id='.$id);
			$config['id_cms_users'] = [$forum->id_cms_user]; //This is an array of id users
			CRUDBooster::sendNotification($config);
		}

		return redirect('forum/konsultasi?page='.$page.'&id='.$id);
	}
}