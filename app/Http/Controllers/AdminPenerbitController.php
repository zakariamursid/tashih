<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;
	use Hash;

	class AdminPenerbitController extends \crocodicstudio\crudbooster\controllers\CBController {
		public function findUsers($id){
			$users = "kurnia@pertama.com,X7cJYR
				yuni@dwisejahtera.co.id,BcXQc4
				irwan@testing.co.id,g77Umv
				pipin.indrawan@thewalistudio.com,xKOpYv
				almahira.publishing@yahoo.co.id,t3ac3A
				admin@iqroglobal.com,16jAUy
				iqroindonesiaglobal@gmail.com,P5z9gY
				upqkemenag@gmail.com,qIGZDa
				usman.elqurtuby@gmail.com,TH5HAm
				penerbit_sahabat@yahoo.co.id,Gra3Lu
				alfatihberkah@gmail.com,s8yohc
				admuawwan@tigaserangkai.co.id,Ounqbb
				athapublishing@gmail.com,UlF6NQ
				mzamroniahbab@yahoo.com,nzhIQv
				nurilmu@gmail.com,QTu9R4
				sw_dianrakyat@yahoo.com,OC4Bpj
				biahthoyyibah@gmail.com,OqCzUq
				rozimq@ymail.com,fATKIw
				masmahfan@gmail.com,y0VfnR
				m_zamroni_ahbab@yahoo.co.id,o6Jk7G
				alquranpenerbit@gmail.com,qyVIJn
				k_abdullah@ssd.co.id,hOXxBc
				redaksi.aqwam@gmail.com,b3FqSE
				azizabunajma@gmail.com,wzZH5L
				mahfan_77@ymail.com,H35OvO
				babacahyono@gmail.com,4yTATM
				j.art.garut@gmail.com,bas5mY
				mohammadazhari2205@gmail.com,C6JNwn
				cahayaquran212@gmail.com,HP6zIq
				hensoe.hs@gmail.com,b6pFU1
				abd.raup@gmail.com,d03F4X
				pes_nf@yahoo.co.id,AQXJqL
				fpq.mulia@gmail.com,fA63pr
				abdulraup.syaamil@gmail.com,k9tvY0
				dudungjamalaja@gmail.com,X19hHI
				paijo80@gmail.com,XuKjNR
				meccapublishing@gmail.com,a0m74J
				tetst@paijo.com,k8B6EC
				qurankemenag@gmail.com,a6hEUC
				iqro_asad@yahoo.co.id,PhA1bh
				abdulhussen168@gmail.com,UhKP1o
				karyatohaputrasemarang@gmail.com,nF0Uxs
				abdullah_bfd37@yahoo.com,6kbZbu
				ichinta.andri@gmail.com,3h3MSc
				ekaprima100@gmail.com,qqXdGe
				dokumen.smi@gmail.com,raGab6
				usman.abdullah120533@gmail.com,fcyZtO
				insankamil.solo@gmail.com,1cbbFh
				info@dar-almawahib.com,Oxy9mF
				sbabandung@gmail.com,zNvgSC
				neneng@advandigital.com,kRgMQk
				fatwaquran@gmail.com,qPAC0G
				iqbal.rasyid.r@gmail.com,Bx1vhQ
				mufticorner@gmail.com,n62PZO
				almahira_online@yahoo.com,ASQH8J
				zaelani.anton@gmail.com,Aep91D
				alfian@crocodic.com,ysQyDF
				eko.widoyatmo@yahoo.co.id,lzoqs2
				halimquran@gmail.com,dk1GQ3
				bpbiabiyoso@gmail.com,24Pp59
				quran.ziyad@gmail.com,QYNA70
				menarakudus@gmail.com,KrYs6h
				info.yayasanalhikmah@gmail.com,1XBXRV
				masjidagungalazhar@gmail.com,oKebFi
				dave3adi@gmail.com,H6aQxz
				salamah@learn-quran.co,0I5MgM
				abu_afnani@yahoo.co.id,gfGUQf
				pujibayan@gmail.com,7pRlpq
				muslimpanduan@gmail.com,qSfWR8
				sekretarispelangimizan@gmail.com,qMHRlK
				niwang@crocodic.com,9RWqgh";
			$users = explode(PHP_EOL, $users);
			// $d_usr = [];
			for ($i=0; $i <count($users) ; $i++) { 
				$text = str_replace("\n", "", str_replace("\t","",$users[$i]));
				$data = explode(',',$text);

				if ($data[1] == $id) {
					return $data[0];
				}

				$usr['email'] = $data[0];
				$usr['id']    = $data[1];

				// array_push($d_usr, $usr);
			}
		}

		public function getDumpPenerbit(){
			$list           = "3h3MSc,PT. Mentari Utama Unggul,Ruko Puri Niaga No. A6, Jl. Inspeksi Kalimalang Bekasi,Gegen Satrio Berbowo Hermanto
			BcXQc4,CV Dwi Sejahtera,Jl. Kebayoran Lama no. 33 jakarta Selatan,Hj. Wahyuni SH
			6kbZbu,CV CAHAYA AGENCY,Jl. Pahlawan No. 120 Surabaya,Abdullah Husen
			xKOpYv,the WALi studio,Jalan Purbakencana 1 No. 1 RT 5 RW 5 Kelurahan Cipageran  Kecamatan Cimahi Utara, Cimahi 40511,Pipin Indrawan
			t3ac3A,almahira,Komplek KODAM (Jatiwaringin) Jl. Manunggal 2 No.8C Rt.011 Rw.006, Cipinang Melayu, Makasar, Jakarta Timur,M. Abdul Ghoffar
			nF0Uxs,PT. Karya Toha Putra,Jalan Raya Manngkang KM. 16, Semarang, Jawa Tengah,H. Umar Toha Putra, MBA, MSc
			P5z9gY,IQRO INDONESIA GLOBAL,JALAN JERUK LEGI 496 RT.12 RW.35 BANGUNTAPAN, BANTUL, D.I. YOGYAKARTA 
			55198,ANDHI RAHARJO EKO PRASETYO
			qIGZDa,Unit Percetakan Al Quran Kementerian Agama RI,Jl. Puncak Raya No. 65 Ciawi Bogor,Drs. H.M Fakhruddin D, Lc
			TH5HAm,PT CORDOBA INTERNASIONAL INDONESIA (CII),Jalan Setrasari Indah no.33 Bandung,Usman el-Qurthuby
			Gra3Lu,CV Sahabat Klaten,Jl Dr. wahidin Sudirohusodo  47 Klaten,H. Suranto
			s8yohc,Alfatih Berkah Cipta,Pondok Jati Utara RT/RW. 006/003 No. E23 Kel. Jurang Mangu Barat Kec. Pondok Aren Kota Tangerang Selatan Banten 15223 Jln. RC Veteran Ruko No. 17F (seberang lap. Hankam) Kel. Bintaro Kec. Pesanggrahan Jakarta Selatan 12330,Bambang Supriyanto
			Ounqbb,PT TIGA SERANGKAI PUSTAKA MANDIRI,Jln. Dr Supomo 23 Solo 57141 Jawa Tengah,MAS ADMUAWAN
			UlF6NQ,CV ATHA INTERNATIONAL PUBLISHING,Kupang Krajan VII/41-A Kel. Kupang Krajan, Kec. Sawahan Surabaya,Badai Wirya
			QTu9R4,UD. Nur Ilmu,Kapas Madya 4-F/23 Kel. Kapas Madya Baru Kec. Tambak Sari, Surabaya,Hari Purwanto
			OC4Bpj,PT. Dian Rakyat,Jl. Rawagirang No. 8 Kawasan Industri Pulo Gadung Jakarta,Mario Alisjahbana
			PhA1bh,CV. DEPOT IQRO',Selokraman RT. 46 RW.11 Purbayan, Kotagede, Yogyakarta 55173,Hj. SRI REPSA KHANIFATI
			fATKIw,PPQM Fami Bisyauqin,Komplek Perum Griya Sasmita Blok D2 No 9 Serua Bojongsari Depok,Fahrur Rozi
			UhKP1o,Abdullah Husen / M.A. Jaya,Jl. Jamblang Rt. 03 Rw.02 Duri Selatan Kec. Tambora Jakarta Barat,Abdullah Husen
			a6hEUC,mzamroniahbab,jakarta timur,m zamroni ahbab
			a0m74J,UD Mecca Quran,Banjarsari RT.03/RW.01 Kemiri Mojosongo Boyolali Jawa Tengah57321,Agus Muttaqin
			hOXxBc,Qalam (PT. Serambi Semesta Distribusi),Jl. Jeruk Purut No. 51 RT.005/03 Jakarta Selatan,Kuriniawan Abdullah
			b3FqSE,Aqwam Media Profetika,Jl. Menco Raya No. 112 Gonila, Kartasura, Sukoharjo, Solo 57162,Bambang Sukirno
			wzZH5L,UD. Fatwa,Jl. Pakis Gg III A RT.02 RW.14 Cemani Baru, Desa Cemani, Kec. Grogol, Sukoharjo, Solo,Fatwanudin Aziz
			H35OvO,Pustaka Jaya Ilmu,Kp. Raden RT. 003/001 Kel. Jatiranggon Kec. Jatisampurna Kota Bekasi,Kamaluddin Siregar / Mahfan
			4yTATM,Sinergi Prima Magna,Jl. Burahol No.3 Karangasem Laweyan Surakarta Jawa Tengah,Salim Cahyono
			bas5mY,CV Penerbit Jum?natul ‘Al?-ART,Jalan Raya Warung Peuteuy No. 207, Kampung Cileungsing, RT 006, RW 007, Desa Pasawahan, Kecamatan Tarogong Kaler, Garut 44151.,Iwa Gunawan
			C6JNwn,CV. KARYA AGUNG SURABAYA,JALAN TEMPEL SUKOREJO V / 14 SURABAYA,MOHAMAD AZHARI
			HP6zIq,CV.CAHAYA PUTRI,cluster Garden Grove Blok J 30/09 Citra Raya Cikupa Tangerang Banten,Rudi
			b6pFU1,Beras Alfath,Kampung setu Rt. 07 Rw. 01 no. 15 kel. Bintara jaya, Bekasi Barat,Hendra Suherman
			d03F4X,SYGMA EXAMEDIA ARKANLEEMA,Jl. Babakansari I No 71 Kiaracondong Bandung,ABDUL RAUP
			AQXJqL,Pesantren Al-Qur'an Nurul Falah Surabaya,Ketintang Timur PTT 5-B Surabaya,Drs. H. Ali Muaffa
			fA63pr,Yayasan Pelayan Al-Qur'an Mulia,Vila Inti Persada Blok A7/19 Pamulang Timur Tangerang Selatan Banten,Zarkasi Afif, MA
			k9tvY0,PT Sygma Examedia Arkanleema,Jl. Babakansari I No 71 Kiaracondong Bandung,ABDUL RAUP
			X19hHI,Laboratorium Pendidikan Tarbiyatul 'Alamin,Kedungmaling Sooko Mojokerto,H. Dudung Jamal AJA
			qqXdGe,Eka Prima Mandiri,Jl. Rimba Baru 2 RT 04/11 Pasir Kuda, Bogor Barat, Kota Bogor,Moch. Badarno
			raGab6,Sygma Examedia Arkanleema,Jl Babakansari I No 71 Kiaracondong Bandung,Abdul Raup
			fcyZtO,cv ibnu usman,Jl. Mangga No.100 Rt.002/003 Batu Ampar Keramat Jati Condet Jakarta Timur,usman
			1cbbFh,Insan Kamil (Madina Quran),Jl. Rajawali Raya No. 9, Geduren RT.02/RW.03, Gonilan, Kartasura, Kabupaten Sukoharjo, Jawa Tengah 57169,Riyanto
			Oxy9mF,CV.Dar al-mawahib al-islamiyah (DMI),jalan raya condet no.22 Rt. 005 / 003 Kel.Balekambang Kec.Kramat jati Jakarta Timur 13530,husin idrus
			zNvgSC,Sinar Baru Algensindo,Jl. Maskumambang No. 11 A Kota Bandung,Ir. H. Herry  Sefriyadi
			kRgMQk,PT Mediatech Mandiri Indonesia,Jln Sunter Agung Timur IX O 1 No 19 RW 11, Sunter Jaya, Tanjung Priok, Jakarta Utara,Neneng Sutarsih
			qPAC0G,UD. Fatwa,Jl. Pakis Gg. III A. Cemani Baru RT.02/14 Cemani Solo,Fatwanudin Aziz
			n62PZO,PT Kelindo Wiro Sableng,Vivo Sentul, Jl. Raya Bogor KM 50. Blok C1-C2 Cimandala, Sukaraja, Bogor, Jawa Barat.,DESRI MULYAWATI
			ASQH8J,Almahira,Komp. KODAM (Jatiwaringin) Jl. Manunggal 2 No.8C Rt.011 Rw. 006, Kel. Cipinang Melayu, Kec. Makasar, Jakarta Timur,M. Abdul Ghoffar
			Aep91D,a,a,a
			ysQyDF,a,a,a
			lzoqs2,CV.SARANA PENGHAFAL AL-QURAN INDONESIA,JL.Tugu Karya 1 RT.002 RW.012 No.73 Kec.Cipondoh Kel.Cipondoh Kota Tangerang,Eko Widoyatmo
			dk1GQ3,Halim,Perumahan Buah Batu Square Jalan Lemon 2 No. 11 Bojongsoang, Bandung,Muhammad Fauzan Albugis
			24Pp59,Balai Penerbitan Braille Indonesia (BPBI) Abiyoso,Jalan Kerkof No.21 Leuwigajah - Kota Cimahi 40532,Kepala BPBI Abiyoso
			y0VfnR,Kamila Jaya Ilmu,Jl. Kramat Pulo Raya No. 12 B, Kel. Kramat, Kec. Senin, Jakarta Pusat, 10450,Sandro Wijaya
			i76KgL,Fa Menara Kudus,jl menara no.4 kudus 59316 jawa tengah,H A Fathoni
			1XBXRV,Yayasan Tunanetra Al-Hikmah,Jl. Batikan 57 RT 46 RW 12 Pandeyan, Umbul Harjo, Yogyakarta,Ahmad Nasikhin
			oKebFi,Yayasan Pesantren Islam Al Azhar,Kompleks Masjid Agung Al Azhar Jalan Sisingamangaraja, Kebayoran Baru Jakarta Selatan,Dr. H. Shobahussurur Syamsi, M.A
			Bx1vhQ,Khazanah Intelektual,Jl.Srimahi 1 No.1 Bandung,Iqbal Rasyid
			H6aQxz,Khazanah Intelektual,Jl.Srimahi 1 No.1,Iqbal Rasyid
			0I5MgM,Learn Quran Tajwid,Jl. Margonda Raya No. 1, Code Margonda, Depok Town Square, Jawa Barat, 16424,Ummu Salamah
			QYNA70,Ziyad Visi Media,Jl. Banyuanyar Selatan no. 4 Banyuanyar Surakarta,Helmi Rian F.
			gfGUQf,Prima krista sejahtera,Tamansari no.25 kel. Tamansari, kec. Bandung wetan kota bandung,Khoer khoerudin
			7pRlpq,CV.BAYAN QUR'AN,Jl.Tole Iskandar, Perum.Taman Manggis Permai Blok D 17  Depok - Jawa Barat,Muhamad asrori
			qSfWR8,PT. Mediatech Mandiri Indonesia,Jl. Sunter Agung Timur Blok O1 No. 19 kel. Sunter Jaya kec. Tj. Priok Jakarta Utara,Heru Rachman
			qMHRlK,Pelangi Mizan,Jl. Cinambo No. 137 Ujung Berung Bandung - 40294,Sari Meutia
			9RWqgh,Crocodic Test,Banyumanik Semarang,Rino
			KrYs6h,Fa Menara Kudus,jl menara 4 kudus,H A Fathoni";
			$list           = explode(PHP_EOL, $list);
			$password_value = '123456';
			$password       = Hash::make($password_value);
			$result         = [];

			for ($i=0; $i <count($list) ; $i++) { 
				$text  = str_replace("\n", "", str_replace("\t","",$list[$i]));
				$data  = explode(',',$text);
				$email = self::findUsers($data[0]);
				if($email == '') continue;

				$resp['id_penerbit'] = $data[0];
				$resp['name']        = $data[1];
				$resp['address']     = $data[2];
				$resp['pic']         = (isset($data[3]) ? $data[3] : '');
				$resp['email']       = $email;

				$check = DB::table('cms_users')
					->where('id_penerbit',$data[0])
					->first();
				if (empty($check)) {
					$save['created_at']        = date('Y-m-d H:i:s');
					$save['id_penerbit']       = $resp['id_penerbit'];
					$save['name']              = $resp['name'];
					$save['address']           = $resp['address'];
					$save['pic']               = $resp['pic'];
					$save['email']             = $resp['email'];
					$save['id_cms_privileges'] = 2;
					$save['password_value']    = $password_value;
					$save['password']          = $password;
					DB::table('cms_users')->insert($save);
				}else{
					$change['updated_at']        = date('Y-m-d H:i:s');
					$change['id_penerbit']       = $resp['id_penerbit'];
					$change['name']              = $resp['name'];
					$change['address']           = $resp['address'];
					$change['pic']               = $resp['pic'];
					$change['email']             = $resp['email'];
					$change['id_cms_privileges'] = 2;
					$change['password_value']    = $password_value;
					$change['password']          = $password;
					DB::table('cms_users')->where('id',$check->id)->update($change);
				}

				array_push($result, $resp);
			}

			$response['total'] = count($result);
			$response['data']  = $result;

			return response()->json($response);
		}
        
        public function getCoba(){
            return DB::table("asd")->get();
        }
        
        public function getLihatDetail(){
            set_time_limit(0);
            ini_set('memory_limit', '256M');
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "http://neptune.crocodic.net/tashihmailsender/",
              CURLOPT_TIMEOUT => 500,
              CURLOPT_FOLLOWLOCATION => false,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_SSL_VERIFYHOST => false,
              CURLOPT_SSL_VERIFYPEER => false,
              CURLOPT_SSLVERSION => 3,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email_sender\"\r\n\r\ntashih@kemenag.go.id\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email_to\"\r\n\r\naditama@crocodic.com\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"cc\"\r\n\r\ntashihlpmq@gmail.com\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"subject\"\r\n\r\nAkun Penerbit Mushaf Al-Qur'an Indonesia\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"body\"\r\n\r\nYth crocodic test,\n\nAssalamu'alaikum Warahmatullah Wabarakatuh.\n\n\n\nLajnah Pentashihan Mushaf Al-Qur'an telah menyetujui pendaftaran Akun Penerbit Mushaf Al-Qur'an Anda. Dengan Akun Penerbit Muhaf Al-Qur'an ini, Anda dapat mengajukan layanan pentashihan mushaf Al-Qur'an (permohonan Surat Tanda Tashih dan/atau Surat Izin Edar) dengan login ke dalam sistem Layanan Tashih Online. Berikut ini adalah akses masuk Anda ke dalam Website Layanan Tashih Online:\n\n\nKode ID : 2019109\npassword : 123456\n\n\nUntuk login ke dalam sistem Layanan Tashih Online, kunjungi http://tashih.kemenag.go.id/ klik login, lalu isikan kode ID dan password di atas, kemudian klik login.\n\n\nTerima Kasih,\n\n\n\nWassalamu'alaikum Warahmatullah Wabarakatuh.\n\n\n\nLajnah Pentashihan Mushaf Al-Qur'an\n\nBadan Litbang dan Diklat Kementerian Agama RI\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"app\"\r\n\r\nPenerbit Mushaf Al-Qur'an Indonesia\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
              CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Length: 1629",
                "Content-Type: multipart/form-data; boundary=--------------------------871775776355338761456361",
                "Host: neptune.crocodic.net",
                "Postman-Token: f664b78b-e594-42e0-ad1d-16a8c1b8799e,ca608be0-b919-42dc-9563-39d24f21c8ba",
                "User-Agent: PostmanRuntime/7.18.0",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }
        }
            
		public function getUpdateStatus($id){
			if (CRUDBooster::isSuperadmin()) {
				$save['status'] = Request::get('status');
				$action = DB::table('cms_users')->where('id',$id)->update($save);
				if ($action) {
					$cms_users = DB::table('cms_users')
							->where('id',$id)
							->first();

					$data_email['name']     = $cms_users->name;
					$data_email['username'] = $cms_users->id_penerbit;
					$data_email['password'] = $cms_users->password_value;

					if ($save['status'] == 'Aktif') {
						$email = CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'akun_penerbit_anda_disetujui']);
					}
					elseif($save['status'] == 'Dibekukan')
					{
						$email = CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'akun_penerbit_anda_dibekukan']);
					}
					elseif($save['status'] == 'Ditolak')
					{
						$email = CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'akun_penerbit_anda_ditolak']);
					}

					return redirect()->back()->with(['message'=>'Status telah diubah', 'message_type'=>'success']);
				}else{
					return redirect()->back()->with(['message'=>'Something went wrong', 'message_type'=>'danger']);
				}
			}else{
				return redirect()->back()->with(['message'=>'You dont\' have privilege here', 'message_type'=>'danger']);
			}
		}
        
        public function postBeriRating(){
            $id = Request::input("id");
            $rating = Request::input("jumlah_rating");
            
            $check = DB::table("cms_users")
            // ->whereNull('deleted_at')
            ->where("id",$id)
            ->first();
            
            if($check->status=="Dibekukan"){
                return CRUDBooster::redirectback("Maaf akun ini dibekukan","error");
            }elseif($check->status=="Belum ditindaklanjuti"){
                return CRUDBooster::redirectback("Maaf akun ini belum diaktifkan","error");
            }else{
                $update = DB::table("cms_users")
                ->where("id",$id)
                ->update([
                    'rating'=>$rating
                ]);
                
                if($update){
                    return CRUDBooster::redirectback("Berhasil update","success");
                }
            }
        }
        
        public function getCariRating($id){
            $check= DB::table("cms_users")
            // ->whereNull("deleted_at")
            ->where("id",$id)
            ->first();
            
            $res['message'] = 'Success';
            $res['id'] = ($check->id) ? $check->id : '';
            $res['rating'] = ($check->rating) ? (float)$check->rating : '';
            
            return response()->json($res);
        }
        
	    public function cbInit() {
	    	if (Request::segment(3) == '') {
	    		$sidebar_mode = 'collapse';
	    	}else{
	    		$sidebar_mode = 'normal';
	    	}

	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "cms_users";	        
			$this->title_field         = "name";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = TRUE;
			$this->button_delete       = TRUE;
			$this->button_edit         = TRUE;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = TRUE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = $sidebar_mode; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE						      

			# START COLUMNS DO NOT REMOVE THIS LINE
	        $this->col = [];
			$this->col[] = array("label"=>"Tanggal pendaftaran","name"=>"created_at" );
			$this->col[] = array("label"=>"Nama Penerbit","name"=>"name" );
			$this->col[] = array("label"=>"ID Penerbit","name"=>"id_penerbit" );
			$this->col[] = array("label"=>"Provinsi","name"=>"id_province","join"=>"province,name");
			$this->col[] = array("label"=>"Kabupaten / Kota","name"=>"id_city","join"=>"city,name");
			$this->col[] = array("label"=>"No. Telp Penerbit","name"=>"phone" );
			$this->col[] = array("label"=>"Email","name"=>"email" );
			$this->col[] = array("label"=>"Penanggung Jawab","name"=>"pic" );
			$this->col[] = array("label"=>"Status","name"=>"status" ,"callback"=>function($row){
				$btn             = Kemenag::penerbitButton($row->status);
				$aktif           = ($row->status == 'Aktif' ? 'class="disabled"' : '');
				$belum_aktif     = ($row->status == 'Belum ditindaklanjuti' ? 'class="disabled"' : '');
				$ditolak       = ($row->status == 'Ditolak' ? 'class="disabled"' : '');
				$dibekukan       = ($row->status == 'Dibekukan' ? 'class="disabled"' : '');
				$url_aktif       = ($row->status == 'Aktif' ? 'javascript:void(0)' : CRUDBooster::mainpath('update-status/'.$row->id.'?status=Aktif'));
				$url_ditolak       = ($row->status == 'Ditolak' ? 'javascript:void(0)' : CRUDBooster::mainpath('update-status/'.$row->id.'?status=Ditolak'));
				$url_belum_aktif = ($row->status == 'Belum ditindaklanjuti' ? 'javascript:void(0)' : CRUDBooster::mainpath('update-status/'.$row->id.'?status=Belum ditindaklanjuti'));
				$url_dibekukan   = ($row->status == 'Dibekukan' ? 'javascript:void(0)' : CRUDBooster::mainpath('update-status/'.$row->id.'?status=Dibekukan'));

				$status = '
					<div class="dropdown">
						<button type="button" class="btn '.$btn.' btn-xs btn-document dropdown-toggle" data-toggle="dropdown">
							'.$row->status.' <span class="fa fa-caret-down"></span>
						</button>
						<ul class="dropdown-menu">
							<li '.$aktif.'><a href="javascript:void(0)" onclick="'.Kemenag::dialogConfirm($url_aktif).'">Aktif</a></li>
							<li '.$ditolak.'><a href="javascript:void(0)" onclick="'.Kemenag::dialogConfirm($url_ditolak).'">Ditolak</a></li>
							<li '.$dibekukan.'><a href="javascript:void(0)" onclick="'.Kemenag::dialogConfirm($url_dibekukan).'">Dibekukan</a></li>
							<li '.$belum_aktif.'><a href="javascript:void(0)" onclick="'.Kemenag::dialogConfirm($url_belum_aktif).'">Belum ditindaklanjuti</a></li>
						</ul>
					</div>';

				return $status;
			});
			$this->col[] = array("label"=>"Dokumen","name"=>"name","callback"=>function($row){
				$penerbit = DB::table('cms_users')
					->where('id',$row->id)
					->first();
				$result = '';

				$btn       = ($penerbit->surat_permohonan == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->surat_permohonan != '' ? asset($penerbit->surat_permohonan) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						Surat Pendaftaran
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: Surat Pendaftaran"
							href="'.$url.'"
						>
						Surat Pendaftaran
						</a>
					';
				}

				$btn       = ($penerbit->siup == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->siup != '' ? asset($penerbit->siup) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						SIUP dan TDP
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: SIUP dan TDP"
							href="'.$url.'"
						>
						SIUP dan TDP
						</a>
					';
				}

				$btn       = ($penerbit->company_profile == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->company_profile != '' ? asset($penerbit->company_profile) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						Company Profile
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: Company Profile"
							href="'.$url.'"
						>
						Company Profile
						</a>
					';
				}

				$btn       = ($penerbit->akte_notaris == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->akte_notaris != '' ? asset($penerbit->akte_notaris) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						Akte Notaris
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: Akte Notaris"
							href="'.$url.'"
						>
						Akte Notaris
						</a>
					';
				}

				$btn       = ($penerbit->npwp == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->npwp != '' ? asset($penerbit->npwp) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						NPWP
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: NPWP"
							href="'.$url.'"
						>
						NPWP
						</a>
					';
				}

				$btn       = ($penerbit->nib == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->nib != '' ? asset($penerbit->nib) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						NIB
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: NIB"
							href="'.$url.'"
						>
						NIB
						</a>
					';
				}

				$btn       = ($penerbit->scan_ktp == '' ? 'btn-danger' : 'btn-success');
				$url       = ($penerbit->scan_ktp != '' ? asset($penerbit->scan_ktp) : Kemenag::emptyValue('image'));
				$extension = Kemenag::extension($url);
				if ($extension == 'pdf' || $extension == 'PDF') {
					$result .= '
						<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
						KTP Penanggung Jawab
						</a>
					';
				}else{
					$result .= '
						<a class="btn '.$btn.' btn-xs btn-document"
							data-lightbox="roadtrip" 
							rel="group_{penerbit}"
							title="Photo: KTP Penanggung Jawab"
							href="'.$url.'"
						>
						KTP Penanggung Jawab
						</a>
					';
				}

				// $btn       = ($penerbit->tdp == '' ? 'btn-danger' : 'btn-success');
				// $url       = ($penerbit->tdp != '' ? asset($penerbit->tdp) : Kemenag::emptyValue('image'));
				// $extension = Kemenag::extension($url);
				// if ($extension == 'pdf' || $extension == 'PDF') {
				// 	$result .= '
				// 		<a data-toggle="modal" class="btn '.$btn.' btn-xs btn-document" href="#modalIframe" data-url="'.$url.'">
				// 		TDP
				// 		</a>
				// 	';
				// }else{
				// 	$result .= '
				// 		<a class="btn '.$btn.' btn-xs btn-document"
				// 			data-lightbox="roadtrip" 
				// 			rel="group_{penerbit}"
				// 			title="Photo: TDP"
				// 			href="'.$url.'"
				// 		>
				// 		TDP
				// 		</a>
				// 	';
				// }

				return $result;
			});
			# END COLUMNS DO NOT REMOVE THIS LINE
			
			# START FORM DO NOT REMOVE THIS LINE

			$this->form = [];
			$this->form[] = ["label"=>"ID User","name"=>"id_penerbit","type"=>"text","required"=>TRUE,"disabled"=>true,"help"=>"ID User automated generate by system"];
			$this->form[] = ["label"=>"Password","name"=>"password","type"=>"password"];
			$this->form[] = ["label"=>"Password Value","name"=>"password_value","type"=>"hidden"];
			$this->form[] = ["label"=>"Nama Penerbit","name"=>"name","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			$this->form[] = ['label'=>'Provinsi','name'=>'id_province','type'=>'select2','datatable'=>'province,name'];
			$this->form[] = ['label'=>'Kabupaten / Kota','name'=>'id_city','type'=>'select2','datatable'=>'city,name'];
			$this->form[] = ['label'=>'Kecamatan','name'=>'id_district','type'=>'select2','datatable'=>'district,name'];
			
			
			$this->form[] = ["label"=>"Penanggung Jawab","name"=>"pic","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			$this->form[] = ["label"=>"Address","name"=>"address","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			$this->form[] = ["label"=>"Telepon","name"=>"phone","type"=>"number","required"=>TRUE,"validation"=>"required|numeric","placeholder"=>"You can only enter the number only"];
			$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:penerbit","placeholder"=>"Please enter a valid email address"];
			$this->form[] = ["label"=>"Jenis Lembaga","name"=>"id_m_jenis_lembaga","type"=>"select","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"m_jenis_lembaga,name"];
			
			$this->form[] = ["label"=>"Surat Pendaftaran","name"=>"surat_permohonan","type"=>"upload","required"=>TRUE,"validation"=>"image|max:1500"];
			$this->form[] = ["label"=>"SIUP dan TDP","name"=>"siup","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"Company Profile","name"=>"company_profile","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"Akte Notaris","name"=>"akte_notaris","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"NPWP","name"=>"npwp","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"NIB","name"=>"nib","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"KTP Penanggung Jawab","name"=>"scan_ktp","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];

			// $this->form[] = ["label"=>"TDP","name"=>"tdp","type"=>"upload","required"=>TRUE,"validation"=>"max:1500"];
			$this->form[] = ["label"=>"Status","name"=>"status","type"=>"radio","dataenum"=>"Aktif;Belum ditindaklanjuti;Dibekukan","required"=>TRUE,"validation"=>"max:1500"];
			# END FORM DO NOT REMOVE THIS LINE     

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
            $this->addaction[] = ['label'=>'','url'=>'javascript:void(0)\' data-id=\'[id]\'','icon'=>'fa fa-star','color'=>'primary btn-rating','showIf'=>"[status] == 'Aktif'"];
    
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = '
				$("a[href=\'#modalIframe\']").on("click",function(){
					let text = $(this).text();
					let path = $(this).data("url");
					let box  = $("#modalIframe");

					box.find(".modal-title").html(text);
					box.find("iframe").attr("src",path);
					box.find(".modal-footer").find("a").attr("href",path);
				})
				$("#password").on("keyup",function(){
					let value = $(this).val();
					
					$("input[name=\'password_value\']").val(value);
				})
	        ';


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = '
	            <script>
	            var mainpath = "'.CRUDBooster::mainpath().'";
	            var token = "'.csrf_token().'";
	            </script>
				<!-- Modal -->
				<div id="modalIframe" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				        <h4 class="modal-title">Modal Header</h4>
				      </div>
				      <div class="modal-body">
				        <iframe src="'.asset('uploads/1/2018-05/akte_notaris_mutu.pdf').'"></iframe>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <a href="#" target="_blank" class="btn btn-success">Open File</a>
				      </div>
				    </div>

				  </div>
				</div>
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        $this->load_js[] = asset("js/admin/penerbit.js");
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = '
				.dropdown-menu{
					padding:0;
				}
				.dropdown-menu > .disabled{
					background-color:#f4f4f4;
				}
				iframe{
					width:100%;
					height:450px;
					border:none;
				}
				.btn-document{
					margin-bottom:5px;
				}
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->where('id_cms_privileges',2);
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	        $now = Kemenag::now();

	        $penerbit = DB::table('cms_users')
	        	->where('id',$id)
	        	->first();

			$save_cms['updated_at']        = $now;
			$save_cms['id_penerbit']       = Kemenag::generateIdPenerbit();
			$save_cms['id_cms_privileges'] = 2;
			$save_cms['password_value']    = Request::input('password');
			DB::table('cms_users')->where('id',$id)->update($save_cms);

			$data_email['name'] = $penerbit->name;
			CRUDBooster::sendEmail(['to'=>$penerbit->email,'data'=>$data_email,'template'=>'infromasi_register']);

			if ($penerbit->status == 'Aktif') {
				$email_aktif['name']     = $penerbit->name;
				$email_aktif['username'] = $penerbit->id_penerbit;
				$email_aktif['password'] = $penerbit->password_value;
				CRUDBooster::sendEmail(['to'=>$penerbit->email,'data'=>$email_aktif,'template'=>'informasi_akun_dan_password']);
			}
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here
	        $check =DB::table("cms_users")
	        ->where('id',$id)
	        ->first();
	        
	        if(!empty($check->address)){
	            $address = strip_tags($check->address);
	           $address = urlencode( $check->address );
        		$url     = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyDKuY76FBWSQpCoO7a8GJ_h9NpdyLIEID0";
        		$resp    = json_decode( file_get_contents( $url ), true );
        		if ( $resp['status'] === 'OK' ) {
        			$formatted_address = $resp['results'][0]['formatted_address'];
        			$lat               = $resp['results'][0]['geometry']['location']['lat'];
        			$long              = $resp['results'][0]['geometry']['location']['lng'];
        		}
        		
                $db = DB::table("master_pointing_user")
                ->where("id_cms_users",$id)
                ->update([
                    'lat'=>$lat,
                    'lng'=>$long,
                ]);
	        }
	        
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}