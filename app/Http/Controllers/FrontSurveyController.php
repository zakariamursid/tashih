<?php

namespace App\Http\Controllers;

use Request;
use CRUDBooster;
use DB;
use Session;
use Kemenag;

use App\Repositories\SurveyRepositories;
use App\Repositories\SurveyOpsiJawabanRepositories;
use App\Repositories\SurveyJawabanRepositories;

use App\Services\RevisiSurvey\SurveyConnectServices;
use App\Services\RevisiSurvey\SurveyAnswerServices;
use App\Repositories\RevisiSurvey\SurveyOpsiPertanyaanRepositories;
use App\Repositories\RevisiSurvey\SurveyAnswerRepositories;

class FrontSurveyController extends Controller
{
    public function getIndex() {
		$id_cms_users = CRUDBooster::myId();

        if ($id_cms_users == '') {
			return redirect('/')->with([
				'message'=>'Something went wrong',
				'message_type'=>'danger'
			]);
        }

        if(empty(g('type'))) {
            $type = 'Survey Umum';
        } else {
            $type = g('type');
        }

        $id_kategori = SurveyConnectServices::findSurveyIDActive();
        //Create your own query 
        $data = [];
        $data['sidebar_mode']='sidebar-collapse';
        $data['page_title'] = 'Survey';
        $data['result'] = SurveyConnectServices::getSurveyActive();
        $data['isUserAnswer'] = SurveyAnswerServices::checkIsAnswer($id_cms_users, $id_kategori, $type);
        // $data['result'] = SurveyRepositories::getListSurvey($id_cms_users);
        // $data['isUserAnswer'] = SurveyJawabanRepositories::isUserAnswer($id_kategori, $id_cms_users);
        
        return view('admin/admin_survey',$data);
    }

    public function getIframe()
    {
        return view('admin/admin_survey_iframe');
    }

    public function postSave()
    {
        $request              = Request::all();
        $now                  = Kemenag::now();
        $id_cms_users         = CRUDBooster::myId();
        $redirect_link        = Request::input('redirect_link');
        $type_survey        = Request::input('type_survey');

        if (empty($id_cms_users)) {
            return redirect()->back()->with([
                'message'=>'Something went wrong',
                'message_type'=>'danger'
            ]);
        }

        if(empty($type_survey)) {
            $type = 'Survey Umum';
        } else {
            $type = $type_survey;
        }
        
        $id_kategori = SurveyConnectServices::findSurveyIDActive();
        $isUserAnswerSurvey = SurveyAnswerServices::checkIsAnswer($id_cms_users, $id_kategori, $type);

        if($isUserAnswerSurvey){
            return redirect()->back()->with([
                'message'=>'sudah pernah mengisi',
                'message_type'=>'danger'
            ]);
        }else{
            $simpan = false;
            $break = false;
            $listSurvey = SurveyConnectServices::getSurveyActive();

            foreach ($listSurvey as $x => $xrow) {
                $id_pertanyaan = Request::get('id'.$xrow->survey_pertanyaan_id);
                $jawaban_pertanyaan = Request::get('pertanyaan'.$xrow->survey_pertanyaan_id);

                if (empty($id_pertanyaan) || empty($jawaban_pertanyaan)) {
                    $break = true;
                    break;
                }
                
                if ($xrow->type_pertanyaan == 'Pilihan') {
                    $findOpsi = SurveyOpsiPertanyaanRepositories::find($jawaban_pertanyaan);
                    
                    $opsi = $findOpsi->type;
                    $text = $findOpsi->nama;
                    $point = SurveyOpsiPertanyaanRepositories::typePoint($findOpsi->type);
                } else {
                    $point = 0;
                    $text = $jawaban_pertanyaan;
                    $opsi = null;
                }

                $checkUserAnswer = SurveyAnswerRepositories::checkUserAnswerByType($id_cms_users, $xrow->survey_pertanyaan_id, $type);
                if (!$checkUserAnswer) {
                    $save = new SurveyAnswerRepositories;
                    $save->setType_answer($type);
                    $save->setUsers_id($id_cms_users);
                    $save->setSurvey_nama_id($xrow->survey_nama_id);
                    $save->setSurvey_unsur_id($xrow->survey_unsur_id);
                    $save->setSurvey_pertanyaan_id($xrow->survey_pertanyaan_id);
                    $save->setAnswer($text);
                    $save->setPoint($point);
                    $save->setOption($opsi);
                    $save->add();

                    $simpan = true;
                }else{
                    $id_check = $checkUserAnswer->id;

                    $update = new SurveyAnswerRepositories;
                    $update->setId($id_check);
                    $update->setType_answer($type);
                    $update->setUsers_id($id_cms_users);
                    $update->setSurvey_nama_id($xrow->survey_nama_id);
                    $update->setSurvey_unsur_id($xrow->survey_unsur_id);
                    $update->setSurvey_pertanyaan_id($xrow->survey_pertanyaan_id);
                    $update->setAnswer($text);
                    $update->setPoint($point);
                    $update->setOption($opsi);
                    $update->edit();

                    $simpan = true;
                }
            }
        }

        if (!empty($redirect_link)) {

            if($redirect_link == CRUDBooster::adminPath('penerbitan-tanda-tashih'))
            {
                if ($simpan) {
                    $message = 'Survey anda sudah berhasil didaftarkan';
                }elseif($break){
                    $message = 'sudah pernah mengisi';
                }else{
                    $message = 'Something went wrong';
                }
                return redirect(CRUDBooster::mainPath('iframe').'?message='.$message);
            }
            

            $toLink = redirect($redirect_link);
        }else{
            $toLink = redirect()->back();
        }

        if ($simpan) {
            return $toLink->with([
                'message'=>'Survey anda sudah berhasil didaftarkan',
                'message_type'=>'success'
            ]);
        }elseif($break){
                return $toLink->with([
                'message'=>'sudah pernah mengisi',
                'message_type'=>'danger'
            ]);
        }else{
            return $toLink->with([
                'message'=>'Something went wrong',
                'message_type'=>'danger'
            ]);
        }
    }
}
