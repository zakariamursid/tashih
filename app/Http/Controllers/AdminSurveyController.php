<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use App\Repositories\SurveyOpsiJawabanRepositories;

	class AdminSurveyController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "survey";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			if (!empty(g('parent_id'))) {
				$this->col[] = ["label"=>"Nama Survey","name"=>"id_survey_unsur","callback"=>function ($row)
				{
					$id = $row->id_survey_unsur;
					$find = $this->findKategoriSurvey($id);
	
					return $find->name;
				}];
			}
			$this->col[] = ["label"=>"Unsur Survey","name"=>"id_survey_unsur","join"=>"survey_unsur,nama"];
			$this->col[] = ["label"=>"Type","name"=>"type","callback"=>function ($row) {
				$type = $row->type;

				switch ($type) {
					case 'Pilihan Ganda':
						$color = 'btn-primary';
						$text = $type;
						break;
				
					case 'Essay':
						$color = 'btn-warning';
						$text = $type;
						break;
					
					default:
						$color = 'btn-default';
						$text = 'Undefined';
						break;
				}

				$html = '
					<span class="btn btn-xs '.$color.'">'.$text.'</span>
				';

				return $html;
			}];
			$this->col[] = ["label"=>"Pertanyaan Survey","name"=>"pertanyaan_survey"];
			$this->col[] = ["label"=>"Pilihan Jawaban","name"=>"id","callback"=>function ($row)
			{
				$id = $row->id;
				return self::listOpsiColumn($id, 'type_option');
			}];
			$this->col[] = ["label"=>"Redaksi Jawaban","name"=>"id","callback"=>function ($row)
			{
				$id = $row->id;
				return self::listOpsiColumn($id, 'nama');
			}];
			$this->col[] = ["label"=>"Nilai","name"=>"id","callback"=>function ($row)
			{
				$id = $row->id;
				return self::listOpsiColumn($id, 'type_option', true);
			}];
			$this->col[] = ["label"=>"Pilihan Ganda","name"=>"id","callback"=>function ($row)
			{
				$url = 'survey-opsi?parent_table=survey&parent_columns=pertanyaan_survey&parent_columns_alias=&parent_id='.$row->id.'&return_url='.CRUDBooster::adminpath('survey&foreign_key=id_survey&label=Pilihan+Survey');
				$link = CRUDBooster::adminpath($url);
				$html = '';
				if ($row->type == 'Pilihan Ganda') {
					$html = '
						<a href="'.$link.'" class="btn btn-primary btn-xs"><span class="fa fa-list"></span> Pilihan Ganda Survey</a>
					';
				}

				return $html;
			}];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Unsur Survey','name'=>'id_survey_unsur','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'survey_unsur,nama','help'=>'Pilih untuk memasukkan unsur survey'];
			$this->form[] = ['label'=>'Type','name'=>'type','type'=>'radio','width'=>'col-sm-10','dataenum'=>'Pilihan Ganda;Essay'];
			$this->form[] = ['label'=>'Pertanyaan Survey','name'=>'pertanyaan_survey','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Kategori Survey','name'=>'id_kategori_survey','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'kategori_survey,jenis_kategori','help'=>'Pilih  untuk memasukkan jenis kategori'];
			//$this->form[] = ['label'=>'Type','name'=>'type','type'=>'radio','dataenum'=>'Pilihan Ganda;Essay','value'=>'Essay'];
			//$this->form[] = ['label'=>'Pertanyaan Survey','name'=>'pertanyaan_survey','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();
			// $this->sub_module[] = ['label'=>'Pilihan Survey','path'=>'survey-opsi','parent_columns'=>'pertanyaan_survey','foreign_key'=>'id_survey','button_color'=>'success','button_icon'=>'fa fa-bars'];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
// 			$this->addaction[] = ['label'=>'Diactivated','url'=>CRUDBooster::mainpath('set-status/pending/[id]'),'icon'=>'fa fa-edit','color'=>'warning','showIf'=>"[status] == 'active'", 'confirmation' => true];
// 			$this->addaction[] = ['label'=>'Activated','url'=>CRUDBooster::mainpath('set-status/active/[id]'),'icon'=>'fa fa-edit','color'=>'danger','showIf'=>"[status] == 'pending'",'confirmation'=>true];

	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          
			$this->table_row_color[] = ['condition'=>"[status] == 'pending'","color"=>"danger"];
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


		public function getSetStatus($status,$id) {
			DB::table('survey')->where('id',$id)->update(['status'=>$status]);
			
			//This will redirect back and gives a message
			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Good Job Status survey has been updated !","info");
		}
	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

		public function findKategoriSurvey($id_unsur)
		{
			return DB::table('kategori_survey')
				->select('kategori_survey.id', 'kategori_survey.jenis_kategori as name')
				->join('survey_unsur', 'survey_unsur.id_kategori_survey', '=', 'kategori_survey.id')
				->where('survey_unsur.id', $id_unsur)
				->whereNull('kategori_survey.deleted_at')
				->first();
		}

		public function listOpsiColumn($id_survey, $column, $getVal = false)
		{
			$data = SurveyOpsiJawabanRepositories::getListOpsiBySurveyId($id_survey);
			$list = [];
			foreach ($data as $x => $row) {
				$list[] = (!$getVal?$row->$column:self::switchOpsi($row->$column));
			}

			return implode('<br>', $list);
		}

		public function switchOpsi($value)
		{
			switch ($value) {
				case 'A':
					$ret = 4;
					break;

				case 'B':
					$ret = 3;
					break;

				case 'C':
					$ret = 2;
					break;

				case 'D':
					$ret = 1;
					break;
				
				default:
					$ret = $value;
					break;
			}

			return $ret;
		}

	}