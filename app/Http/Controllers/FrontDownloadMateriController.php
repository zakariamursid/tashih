<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontDownloadMateriController extends Controller
{
	public function getIndex(){
		
		Kemenag::menu('downloadmateri');
		
		if(!empty($_GET['category'])){
		    $regulasi = DB::table('download_materi')
		    ->select('download_materi.*','kategori_download.nama_kategori')
		    ->join('kategori_download','kategori_download.id','=','download_materi.id_kategori_download')
		    ->where('download_materi.id_kategori_download','=',$_GET['category'])
			->whereNull('download_materi.deleted_at')
			->paginate(7);
		}else{
		    $regulasi = DB::table('download_materi')
		    ->select('download_materi.*','kategori_download.nama_kategori')
		    ->join('kategori_download','kategori_download.id','=','download_materi.id_kategori_download')
			->whereNull('download_materi.deleted_at')
			->paginate(7); 
		}

		$data['regulasi'] = $regulasi;
		$data['kategori_download'] = DB::table('kategori_download')
					->whereNull('deleted_at')
					->get();

		return view('front/download-materi',$data);
	}
	
	public function getKoreksiTashih($id){
	    $ukuran = DB::table('materi_upload')
    		->where('id_download_materi',$id)
    		->get();
		
		$response['api_status']  = 1;
		$response['api_message'] = 'Success';
		$response['item']        = $ukuran;
		return response()->json($response);
	}
	
	public function getDownloadTansih($id)
	{
		
		$tabel=DB::table('materi_upload')
		->where("id",$id)
		->first();
		if(empty($tabel->id)){
		    $download=DB::table('download_materi')->where('id',$id)->first();
		    if (empty($download->file)) {
    			return "<script>alert('Tidak ada file ditemukan')</script>";
    		}else{
    			return response()->download(storage_path('app/'.$download->file)); 
    		} 
		}else{
		    $tabell=DB::table('materi_upload')
    		->where("id",$id)
    		->first();
    		if (empty($tabell->file)) {
    			return "<script>alert('Tidak ada file ditemukan')</script>";
    		}else{
    		    $a=parse_url($tabell->file);
    		    $ext = pathinfo($a['path'], PATHINFO_EXTENSION);
    		    $name = str_replace(' ','-',$tabell->name_file).'.'.$ext;
    			return response()->download(storage_path('app/'.$a['path']),$name); 
    		} 
		}
		
	}
	

}