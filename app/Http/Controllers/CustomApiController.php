<?php
namespace App\Http\Controllers;
use DB;
use CRUDBooster;

class CustomApiController extends Controller
{
	public function getListCity()
	{
		$code = g('code');
		$name = g('name');

		if($code) {
			$data = DB::table('city')
				->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}$'")
				->where('code', 'like', "$code%")
				->get();
		}
		else if($name) {
			$data = DB::table('city')
				->where('name', 'like', "%$name%")
				->get();
		}
		else {
			$data = DB::table('city')->get();
		}

		return response()->json(['status' => 200,'message' => 'OK','data' => $data], 200);  
	}

	public function getRekapPentashihan()
	{
		$id_proses_pentashihan = g('id_proses_pentashihan');

		if($id_proses_pentashihan) {
			$data = DB::table('proses_pentashihan_ukuran')
				->where('id_proses_pentashihan',$id_proses_pentashihan)
				->whereNotNull('nomor')
				->get();
			
			return response()->json(['status' => 200,'message' => 'OK','data' => $data], 200);  

		}
	}

	public function getMushafLama()
	{
		$id_m_jenis_mushaf = g('id_m_jenis_mushaf');
		$id_cms_users = g('id_cms_users');

		if($id_m_jenis_mushaf) {
			$data = DB::table('proses_pentashihan')
				->where('id_m_jenis_mushaf',$id_m_jenis_mushaf)
				->where('id_cms_users',$id_cms_users)
				->get();
			
			return response()->json(['status' => 200,'message' => 'OK','data' => $data,'test' => CRUDBooster::myId()], 200);  

		}
	}

	public function getListDistrict()
	{
		$code = g('code');
		$name = g('name');

        if (!empty($code)) {
            $data = DB::table('district')
                ->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}[,.][0-9]{2}$'")
                ->where('code', 'like', "$code%")
                ->select('code', 'name')
                ->get();
        } else if(!empty($name)){
            $data = DB::table('district')
                ->where('name', 'like', "%$name%")
                ->select('code', 'name')
                ->get();
        }
        else {
            $data = DB::table('district')
                ->select('code', 'name')
                ->get();
        }		

		return response()->json(['status' => 200,'message' => 'OK','data' => $data], 200);  
	}


}