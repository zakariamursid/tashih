<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use CRUDBooster;

class FrontRevisiController extends Controller
{
    public function getIndex()
    {
        // dd(CRUDBooster::myId());
        $data=array(
            'database'=>DB::table('proses_pentashihan_koreksi')
            ->select("proses_pentashihan_koreksi.*")
            ->join("proses_pentashihan","proses_pentashihan.id","=","proses_pentashihan_koreksi.id_proses_pentashihan")
            ->join("cms_users","cms_users.id","=","proses_pentashihan_koreksi.id_cms_users")
            ->where("proses_pentashihan_koreksi.id_cms_users",CRUDBooster::myId())
            ->get(),
        );
        return view('front.revisi',$data);
    }
}
