<?php

namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontListFaqControl extends Controller
{
    public function getIndex(){
		return redirect('faq/list');
	}
	
	public function getJsonFaq(){
	    $forum = DB::table('faq_kategori')
			->whereNull('deleted_at')
			->orderBy('id','DESC')
			->paginate(10);
		$response = [];
		foreach ($forum as $row_forum) {
			$jawaban = DB::table('faq_isi')
				->where('faq_kategori_id',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();

			$list_jawaban = [];
			foreach ($jawaban as $row_jawaban) {
			    $rest_jawaban['id'] = $row_jawaban->id;
				$rest_jawaban['name']    = $row_jawaban->judul;
				$rest_jawaban['tanggal'] = Kemenag::dateIndonesia($row_jawaban->created_at);
				$rest_jawaban['content'] = nl2br($row_jawaban->isi);
				$rest_jawaban['lampiran'] = ($row_jawaban->lampiran == '' ? '-' : asset($row_jawaban->lampiran));
				array_push($list_jawaban, $rest_jawaban);
			}

			$rest['id']            = $row_forum->id;
			$rest['total_jawaban'] = count($jawaban);
			$rest['jawaban']       = $list_jawaban;

			array_push($response, $rest);
		}

		return response()->json($response);
	}

	public function getList(){
		Kemenag::menu('faq');

		$forum = DB::table('faq_kategori')
	    ->whereNull('deleted_at')
	    ->orderBy('id','DESC')
		->paginate(10);
		foreach ($forum as $row_forum) {
			$jawaban = DB::table('faq_isi')
				->where('faq_kategori_id',$row_forum->id)
				->orderBy('id','DESC')
				->whereNull('deleted_at')
				->get();
			$row_forum->jawaban = $jawaban;
		}
		
		$result['forum'] = $forum;

		return view('front/faq',$result);
	}
	
    public function getDownload($id){
        $jawaban = DB::table('faq_isi')
			->where('id',$id)
			->orderBy('id','DESC')
			->whereNull('deleted_at')
			->first();
			
		if(empty($jawaban->lampiran)){
		    return '<script>alert("file tidak ditemukan")</script>';
		}else{
		    return response()->download(storage_path('app/'.$jawaban->lampiran));
		}
    }

}
