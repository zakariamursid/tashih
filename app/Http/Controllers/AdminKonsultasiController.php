<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Kemenag;

	class AdminKonsultasiController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {
	    	$segment = Request::segment(3);
	    	if ($segment == '') {
	    		$path = 'index';
	    	}else{
	    		$path = $segment;
	    	}
	    	
	    	# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->table 			   = "forum";	        
			$this->title_field         = "judul";
			$this->limit               = 20;
			$this->orderby             = "id,desc";
			$this->show_numbering      = FALSE;
			$this->global_privilege    = FALSE;	        
			$this->button_table_action = TRUE;   
			$this->button_action_style = "button_icon";     
			$this->button_add          = TRUE;
			$this->button_delete       = TRUE;
			$this->button_edit         = TRUE;
			$this->button_detail       = TRUE;
			$this->button_show         = TRUE;
			$this->button_filter       = TRUE;        
			$this->button_export       = FALSE;	        
			$this->button_import       = FALSE;
			$this->button_bulk_action  = TRUE;	
			$this->sidebar_mode		   = "normal"; //normal,mini,collapse,collapse-mini
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Judul","name"=>"judul"];
			$this->col[] = ["label"=>"Lampiran","name"=>"lampiran","download"=>true];
			if (CRUDBooster::isSuperadmin()) {
				$this->col[] = ["label"=>"Nama","name"=>"id_cms_user","join"=>"cms_users,name"];
			}
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Judul','name'=>'judul','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Isi','name'=>'isi','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Lampiran','name'=>'lampiran','type'=>'upload','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Nama','name'=>'id_cms_user','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'cms_users,name','datatable_where'=>'id = '.CRUDBooster::myId(),"disabled"=>true,"value"=>CRUDBooster::myId()];

			if ($path != 'add') {
				$columns[] = ['label'=>'Jawaban','name'=>'isi','type'=>'textarea','required'=>true];
				$columns[] = ['label'=>'Lampiran','name'=>'lampiran','type'=>'upload'];
				$columns[] = ['label'=>'Dibuat','name'=>'created_at','type'=>'hidden','readonly'=>true];
				$columns[] = ['label'=>'Nama','name'=>'id_cms_user','type'=>'select','datatable'=>'cms_users,name',"disabled"=>true];
				$this->form[] = ['label'=>'Jawaban','name'=>'forum_jawaban','type'=>'child','columns'=>$columns,'table'=>'forum_jawaban','foreign_key'=>'id_forum'];
			}
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Judul","name"=>"judul","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			//$this->form[] = ["label"=>"Isi","name"=>"isi","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Lampiran","name"=>"lampiran","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Cms User","name"=>"id_cms_user","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"cms_user,id"];
			//$this->form[] = ["label"=>"Type","name"=>"type","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        if ($path == 'edit') {
	       		$remove_edit_delete = '
					$("#table-jawaban").find(".btn-warning").remove();
					$("#table-jawaban").find(".btn-danger").remove();
	       		';
	       	}else{
	       		$remove_edit_delete = '//lorem ipsum';
	       	}

	        $this->script_js = $remove_edit_delete.'
				// $(".btn.btn-danger.btn-delete.btn-sm").remove();
				// $(".text-muted").remove();
				$("#panel-form-jawaban").find(".panel.panel-default").parent(".col-sm-10").addClass("col-sm-offset-1");
				$("#jawabanid_cms_user").parent().parent().hide();
				$("#jawabanid_cms_user").val("'.CRUDBooster::myId().'").trigger("change");
				$("#jawabancreated_at").val(formatDate())

				$("#btn-add-table-jawaban").click(function(){
					let now = formatDate();

					$("#jawabanid_cms_user").val("'.CRUDBooster::myId().'").trigger("change");
					$("#jawabancreated_at").val(now)
				})

				function formatDate(date) {
					now    = new Date();
					year   = "" + now.getFullYear();
					month  = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
					day    = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
					hour   = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
					minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
					second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
					return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
				}
	        ';


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = '
				.btn.btn-danger.btn-delete.btn-sm{
					// display:none;
				}
	        ';
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->where('type','Konsultasi');
	        if (CRUDBooster::myPrivilegeId() == 2) {
	        	$query->where('forum.id_cms_user',CRUDBooster::myId());
	        }

	        if (Request::get('id') != '') {
	        	$query->where('forum.id',Request::get('id'));
	        }
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here
	        
			$save['updated_at'] = Kemenag::now();
			$save['type']       = 'Pertanyaan';
			DB::table('forum')->where('id',$id)->update($save);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 
	        
			// $save['created_at'] = date('Y-m-d H:i:s');
			// $save['updated_at'] = date('Y-m-d H:i:s');
			// $save['id_cms_user'] = CRUDBooster::myId();

			// $action = DB::table('forum_jawaban')
			// 	->where('id_forum',$id)
			// 	->whereNull('id_cms_user')
			// 	->whereNull('deleted_at')
			// 	->update($save);
	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}