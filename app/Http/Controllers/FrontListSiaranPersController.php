<?php

namespace App\Http\Controllers;

use App\Services\BannerLayananServices;
use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontListSiaranPersController extends Controller
{
    public function getIndex($key=null){
		Kemenag::menu('listsiaranpers');

		$info_seputar_lajnah = DB::table('list_siaran_pers')
			->orderby('id','desc')
			->whereNull('deleted_at')
			->paginate(8);

		$list_id = '';
		$total   = count($info_seputar_lajnah);
		$max_row = $total-1;
		$num     = 0;
		foreach ($info_seputar_lajnah as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();
			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 70);
			$row->title   = $title.($length_title > 70 ? '...' : '');

			$list_id .= $row->id;
			$list_id .= ($num < $max_row ? ';' : '');

			$content        = strip_tags($row->content);
			$length_content = strlen($content);
			$content        = substr($content, 0, 130);
			$row->content   = $content.($length_content > 130 ? '...' : '');

			$num++;
		}
		$id = explode(';', $list_id);

		$info_lainnya = DB::table('list_siaran_pers')
			->whereNotIn('id',$id)
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
		foreach ($info_lainnya as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();

			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 50);
			$row->title   = $title.($length_title > 50 ? '...' : '');
		}

		$result['info_seputar_lajnah'] = $info_seputar_lajnah;
		$result['info_lainnya']        = $info_lainnya;
        $result['banner_layanan'] 	   = BannerLayananServices::listBannerLayanan();

		return view('front/listsiaranpers',$result);

	}

	public function getRead($slug){
		Kemenag::menu('listsiaranpers');


		$info_seputar_lajnah = DB::table('list_siaran_pers')
			->where('slug',$slug)
			->whereNull('deleted_at')
			->first();

		if (empty($info_seputar_lajnah)) {
			return redirect('404');
		}

		$cms_users = DB::table('cms_users')
				->where('id',$info_seputar_lajnah->id_cms_users)
				->first();
		$info_seputar_lajnah->admin_name = $cms_users->name;
		$info_seputar_lajnah->image      = ($info_seputar_lajnah->image == '' ? asset('image/no-image.png') : asset($info_seputar_lajnah->image));

		$info_lainnya = DB::table('list_siaran_pers')
			->whereNotIn('id',[$info_seputar_lajnah->id])
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
		foreach ($info_lainnya as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();

			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->tite);
			$row->title   = $row->title.($length_title > 50 ? '...' : '');
		}

		$result['info_seputar_lajnah'] = $info_seputar_lajnah;
		$result['info_lainnya']        = $info_lainnya;
        $result['banner_layanan'] 	   = BannerLayananServices::listBannerLayanan();

		return view('front/listsiaranpersdetail',$result);
	}
}
