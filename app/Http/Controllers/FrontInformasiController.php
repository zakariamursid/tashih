<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontInformasiController extends Controller
{
	public function getIndex($slug){
		Kemenag::menu('beranda');

		$banner = DB::table('banner')
			->where('slug',$slug)
			->whereNull('deleted_at')
			->first();

		if (empty($banner)) {
			return redirect('404');
		}

		$cms_users = DB::table('cms_users')
				->where('id',$banner->id_cms_users)
				->first();
		$banner->admin_name = $cms_users->name;
		$banner->image      = ($banner->image == '' ? asset('image/no-image.png') : asset($banner->image));

		$info_lainnya = DB::table('banner')
			->whereNotIn('id',[$banner->id])
			->whereNull('deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
		foreach ($info_lainnya as $row) {
			$cms_users = DB::table('cms_users')
				->where('id',$row->id_cms_users)
				->first();

			$row->admin_name = $cms_users->name;
			$row->image      = ($row->image == '' ? asset('image/no-image.png') : asset($row->image));

			$length_title = strlen($row->tite);
			$row->title   = $row->title.($length_title > 50 ? '...' : '');
		}

		$result['banner']       = $banner;
		$result['info_lainnya'] = $info_lainnya;

		return view('front/informasi',$result);
	}
}