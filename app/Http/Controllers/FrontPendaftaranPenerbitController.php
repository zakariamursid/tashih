<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;
use Illuminate\Support\Facades\Storage;

class FrontPendaftaranPenerbitController extends Controller
{
	public function getIndex(){
		if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin()){
			$old = session()->getOldInput();

			if($old)
			{
				$data['code_city'] = DB::table('city')->where('code',$old['id_city'])->first();
				$data['code_district'] = DB::table('district')->where('code',$old['id_district'])->first();
				// dd($data);
				// echo "Not";
			}	
			else
			{
				// echo "Empty";
			}

			// die();
			Kemenag::menu('pentashihan');
			Kemenag::menuSub('pendaftaranPenerbit');

			$jenis_lembaga = DB::table('m_jenis_lembaga')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();

			$data['jenis_lembaga'] = $jenis_lembaga;
			$data['province'] = DB::table('province')->get();
			$data['message'] = DB::table('message_alert')->where('flag','pendaftaran-penerbit')->first();
			return view('front/pendaftaranpenerbit',$data);
		}else{

			return abort(404);
		}
	}

	public function getPreviewInsert()
	{
		$unique_hash = $_GET['uid'];

		$data               = [];
		$data['page_title'] = 'Preview Pendaftaran Penerbit';
		$data['row'] = DB::table('temp_cms_users')
					->select('temp_cms_users.*','province.name as province_name','city.name as city_name','district.name as district_name','m_jenis_lembaga.name as jenis_lembaga_name')
					->leftJoin('province','province.code','temp_cms_users.id_province')
					->leftJoin('city','city.code','temp_cms_users.id_city')
					->leftJoin('district','district.code','temp_cms_users.id_district')
					->leftJoin('m_jenis_lembaga','m_jenis_lembaga.id','temp_cms_users.id_m_jenis_lembaga')
					->where('temp_cms_users.unique_hash',$unique_hash)
					->orderBy('temp_cms_users.id','desc')
					->first();

		if($data['row'] == NULL)
		{
			return abort(404);
		}

		// dd($data);
		return view('front/previewpendaftaranpenerbit',$data);
	}

	public function postTempInsert()
	{
		$now         = Kemenag::now();
		$password    = Kemenag::generatePassword();
		$id_penerbit = Kemenag::generateIdPenerbit();
		
		$lembaga = Request::input('id_m_jenis_lembaga');
		$email =Request::input('email');

		// if ($lembaga == 1) {
		// 	$array = [
		// 		'surat_permohonan'=>'required|mimes:pdf|max:500',
		// 		'npwp'=>'required|mimes:pdf|max:500',
		// 		'company_profile'=>'required|mimes:pdf|max:500',
		// 		'nib'=>'required|mimes:pdf|max:500',
		// 		'scan_ktp'=>'required|mimes:pdf|max:500',
		// 	];
		// }elseif ($lembaga == 2) {
		// 	$array = [
		// 		'surat_permohonan'=>'required|mimes:pdf|max:500',
		// 		'npwp'=>'required|mimes:pdf|max:500',
		// 		'company_profile'=>'required|mimes:pdf|max:500',
		// 		'nib'=>'required|mimes:pdf|max:500',
		// 		'scan_ktp'=>'required|mimes:pdf|max:500',
		// 		'siup'=>'required|mimes:pdf|max:500',
		// 		'akte_notaris'=>'required|mimes:pdf|max:2000',
		// 		// 'akte_notaris'=>'required|mimes:pdf|min:2000|max:31000',
		// 		// 'tdp'=>'required|mimes:pdf|max:2000',
		// 	];
		// }elseif ($lembaga == 3) {
		// 	$array = [
		// 		'surat_permohonan'=>'required|mimes:pdf|max:500',
		// 		'npwp'=>'required|mimes:pdf|max:500',
		// 		'company_profile'=>'required|mimes:pdf|max:500',
		// 		'nib'=>'required|mimes:pdf|max:500',
		// 		'scan_ktp'=>'required|mimes:pdf|max:500',
		// 	];
		// }

		$array2 = [
			'id_province' => 'required',
			'id_city' => 'required',
			'id_district' => 'required',
			'kode_pos' => 'required',
			// 'nib_value' => 'required',
			// 'npwp_value' => 'required',
			'pic_address' => 'required',
			'pic_phone' => 'required',
			'pic_phone' => 'required',
			'pic_email' => 'required',
			'password'	=> 'min:6|required_with:password_confirmation|same:password_confirmation',
			'password_confirmation'	=> 'min:6|required',
		];

		// $array = array_merge($array,$array2);

		$valid = Validator::make(Request::all(),$array2);

		if($valid->fails()){
			return CRUDBooster::redirectback($valid->errors()->first(),'error');
		}else{
		    $save['created_at']         = $now;
    		$save['updated_at']         = $now;
    		$save['name']               = Request::input('name');
    		$save['email']              = Request::input('email');
			$password 					= Request::input('password');
    		$save['password']           = Hash::make($password);
    		$save['id_cms_privileges']  = 2;
    		$save['id_penerbit']        = $id_penerbit;
    		$save['password_value']     = $password;
    		$save['status']             = 'Belum ditindaklanjuti';
    		$save['phone']              = Request::input('phone');
    		$save['pic']                = Request::input('pic');
    		$save['address']            = Request::input('address');
    		$save['id_m_jenis_lembaga'] = Request::input('id_m_jenis_lembaga');
    		$save['send_profile']       = 0;
    

    		$save['id_province'] = Request::input('id_province');
    		$save['id_city'] = Request::input('id_city');
    		$save['id_district'] = Request::input('id_district');
    		$save['kode_pos'] = Request::input('kode_pos');
			
    		$save['nib_value'] = Request::input('nib_value');
    		$save['npwp_value'] = Request::input('npwp_value');

    		$save['pic_address'] = Request::input('pic_address');
    		$save['pic_jabatan'] = Request::input('pic_jabatan');
    		$save['pic_phone'] = Request::input('pic_phone');
    		$save['pic_email'] = Request::input('pic_email');

			$save['unique_hash'] = md5(request()->ip() . request()->userAgent());

    		 // url encode the address
			$address = Request::input('address');
    		
    		$address = urlencode( $address );
    		$url     = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyDKuY76FBWSQpCoO7a8GJ_h9NpdyLIEID0";
    		$resp    = json_decode( file_get_contents( $url ), true );
    		if ( $resp['status'] === 'OK' ) {
    			$formatted_address = $resp['results'][0]['formatted_address'];
    			$lat               = $resp['results'][0]['geometry']['location']['lat'];
    			$long              = $resp['results'][0]['geometry']['location']['lng'];
    		}
    
    		$check = DB::table('cms_users')
    			->where('email',$save['email'])
    			->first();
    
    		if (empty($check)) {
    			$id = DB::table('temp_cms_users')->insertGetId($save);
    			$save_location_map=DB::table('temp_master_pointing_user')
    				->insert([
    					"created_at" => date('Y-m-d h:i:s'),
    					"updated_at" => date('Y-m-d h:i:s'),
    					"id_cms_users" => $id,
    					"lat" => $lat,
    					"lng" => $long,
    				]);
    
    			if ($id) {
    
					$save_file['surat_permohonan']   = Kemenag::uploadFile('surat_permohonan','penerbit/'.$id);
					$save_file['siup']               = Kemenag::uploadFile('siup','penerbit/'.$id);
					$save_file['company_profile']    = Kemenag::uploadFile('company_profile','penerbit/'.$id);
					$save_file['nib']    = Kemenag::uploadFile('nib','penerbit/'.$id);
					$save_file['scan_ktp']    = Kemenag::uploadFile('scan_ktp','penerbit/'.$id);
					
					if(Kemenag::uploadFile('npwp','penerbit/'.$id) !== 'false') {
						$save_file['npwp']               = Kemenag::uploadFile('npwp','penerbit/'.$id);
					}
					if(Kemenag::uploadFile('akte_notaris','penerbit/'.$id) !== 'false') {
						$save_file['akte_notaris']       = Kemenag::uploadFile('akte_notaris','penerbit/'.$id);
					}
					if(Kemenag::uploadFile('tdp','penerbit/'.$id) !== 'false') {
						$save_file['tdp']                = Kemenag::uploadFile('tdp','penerbit/'.$id);
					}

					DB::table('temp_cms_users')->where('id',$id)->update($save_file);

    				return redirect(CRUDBooster::mainpath('preview-insert?uid='.$save['unique_hash']))->with([
    					'message'=>'Lihat dan dan tinjau ulang data yang Anda isikan.<br>
						Jika seluruh data sudah benar, klik Registrasi Penerbit. Jika ada yang salah, klik Edit Data.',
    					'message_type'=>'success'
    				]);
    			}else{
    				return redirect()->back()->with([
    					'message'=>'oops something went wrong, please try again',
    					'message_type'=>'warning'
    				])->withInput();
    			}
    		}else{
    			return redirect()->back()->with([
    				'message'=>'Email yang anda gunakan sudah terdaftar',
    				'message_type'=>'danger'
    			])->withInput();
    		}   
		}		
	}

	public function postSave(){

		$now         = Kemenag::now();
		$password    = Kemenag::generatePassword();
		$id_penerbit = Kemenag::generateIdPenerbit();
		
		$lembaga = Request::input('id_m_jenis_lembaga');
		$email =Request::input('email');

		if ($lembaga == 1) {
			$valid = Validator::make(Request::all(),[
				'surat_permohonan'=>'required|mimes:pdf|max:2000',
				'npwp'=>'required|mimes:pdf|max:2000',
				'company_profile'=>'required|mimes:pdf|max:2000',
				'nib'=>'required|mimes:pdf|max:2000',
				'scan_ktp'=>'required|mimes:pdf|max:2000',
			]);
		}elseif ($lembaga == 2) {
			$valid = Validator::make(Request::all(),[
				'surat_permohonan'=>'required|mimes:pdf|max:2000',
				'npwp'=>'required|mimes:pdf|max:2000',
				'company_profile'=>'required|mimes:pdf|max:2000',
				'nib'=>'required|mimes:pdf|max:2000',
				'scan_ktp'=>'required|mimes:pdf|max:2000',
				'siup'=>'required|mimes:pdf|max:2000',
				'akte_notaris'=>'required|mimes:pdf|max:2000',
				// 'akte_notaris'=>'required|mimes:pdf|min:2000|max:31000',
				// 'tdp'=>'required|mimes:pdf|max:2000',
			]);
		}elseif ($lembaga == 3) {
			$valid = Validator::make(Request::all(),[
				'surat_permohonan'=>'required|mimes:pdf|max:2000',
				'npwp'=>'required|mimes:pdf|max:2000',
				'company_profile'=>'required|mimes:pdf|max:2000',
				'nib'=>'required|mimes:pdf|max:2000',
				'scan_ktp'=>'required|mimes:pdf|max:2000',
			]);
		}

		$valid2 = [
			'id_province' => 'required',
			'id_city' => 'required',
			'id_district' => 'required',
			'kode_pos' => 'required',
			'password'	=> 'min:6|required_with:password_confirmation|same:password_confirmation',
			'password_confirmation'	=> 'min:6|required',
		];

		$valid = array_merge($valid,$valid2);

		if($valid->fails()){
			return CRUDBooster::redirectback($valid->errors()->first(),'error');
		}else{
		    $save['created_at']         = $now;
    		$save['updated_at']         = $now;
    		$save['name']               = Request::input('name');
    		$save['email']              = Request::input('email');
			$password 					= Request::input('password');
    		$save['password']           = Hash::make($password);
    		$save['id_cms_privileges']  = 2;
    		$save['id_penerbit']        = $id_penerbit;
    		$save['password_value']     = $password;
    		$save['status']             = 'Belum ditindaklanjuti';
    		$save['phone']              = Request::input('phone');
    		$save['pic']                = Request::input('pic');
    		$save['address']            = Request::input('address');
    		$save['id_m_jenis_lembaga'] = Request::input('id_m_jenis_lembaga');
    		$save['send_profile']       = 0;
    
			$province = DB::table('province')->where('code',Request::input('id_province'))->first();

    		$save['id_province'] = $province->id;
    		$save['id_city'] = Request::input('id_city');
    		$save['id_district'] = Request::input('id_district');
    		$save['kode_pos'] = Request::input('kode_pos');

    		 // url encode the address
			$address = Request::input('address');
    		
    		$address = urlencode( $address );
    		$url     = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyDKuY76FBWSQpCoO7a8GJ_h9NpdyLIEID0";
    		$resp    = json_decode( file_get_contents( $url ), true );
    		if ( $resp['status'] === 'OK' ) {
    			$formatted_address = $resp['results'][0]['formatted_address'];
    			$lat               = $resp['results'][0]['geometry']['location']['lat'];
    			$long              = $resp['results'][0]['geometry']['location']['lng'];
    		}
    
    		$check = DB::table('cms_users')
    			->where('email',$save['email'])
    			->first();
    
    		if (empty($check)) {
    			$id = DB::table('cms_users')->insertGetId($save);
    			$save_location_map=DB::table('master_pointing_user')
    				->insert([
    					"created_at" => date('Y-m-d h:i:s'),
    					"updated_at" => date('Y-m-d h:i:s'),
    					"id_cms_users" => $id,
    					"lat" => $lat,
    					"lng" => $long,
    				]);
    
    			if ($id) {
    
					$save_file['surat_permohonan']   = Kemenag::uploadFile('surat_permohonan','penerbit/'.$id);
					$save_file['siup']               = Kemenag::uploadFile('siup','penerbit/'.$id);
					$save_file['company_profile']    = Kemenag::uploadFile('company_profile','penerbit/'.$id);
					$save_file['nib']    = Kemenag::uploadFile('nib','penerbit/'.$id);
					$save_file['scan_ktp']    = Kemenag::uploadFile('scan_ktp','penerbit/'.$id);
					
					if(Kemenag::uploadFile('npwp','penerbit/'.$id) !== 'false') {
						$save_file['npwp']               = Kemenag::uploadFile('npwp','penerbit/'.$id);
					}
					if(Kemenag::uploadFile('akte_notaris','penerbit/'.$id) !== 'false') {
						$save_file['akte_notaris']       = Kemenag::uploadFile('akte_notaris','penerbit/'.$id);
					}
					if(Kemenag::uploadFile('tdp','penerbit/'.$id) !== 'false') {
						$save_file['tdp']                = Kemenag::uploadFile('tdp','penerbit/'.$id);
					}

					DB::table('cms_users')->where('id',$id)->update($save_file);

					// send email
    				$data_email['name'] = $email;
    				$data_email['password'] = $check->password_value;
    				CRUDBooster::sendEmail(['to'=>$email,'data'=>$data_email,'template'=>'infromasi_register']);
    
    				return redirect()->back()->with([
    					'message'=>'informasi registrasi anda sudah kami terima, akun anda akan dikirimkan melalui email.',
    					'message_type'=>'success'
    				]);
    			}else{
    				return redirect()->back()->with([
    					'message'=>'oops something went wrong, please try again',
    					'message_type'=>'warning'
    				])->withInput();
    			}
    		}else{
    			return redirect()->back()->with([
    				'message'=>'Email yang anda gunakan sudah terdaftar',
    				'message_type'=>'danger'
    			])->withInput();
    		}   
		}
	}
	public function getLocation()
	{
		
	}

	public function getRalat()
	{
		// dd(Request::old('name'));
		
		if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin()){

			Kemenag::menu('pentashihan');
			Kemenag::menuSub('pendaftaranPenerbit');

			$jenis_lembaga = DB::table('m_jenis_lembaga')
				->whereNull('deleted_at')
				->orderBy('name','ASC')
				->get();

			$unique_hash = $_GET['uid'];

			$data['jenis_lembaga'] = $jenis_lembaga;
			$data['province'] = DB::table('province')->get();
			$data['message'] = DB::table('message_alert')->where('flag','pendaftaran-penerbit')->first();
			
			$data['row'] = DB::table('temp_cms_users')
			->select('temp_cms_users.*','province.name as province_name','city.name as city_name','district.name as district_name','m_jenis_lembaga.name as jenis_lembaga_name','province.code as code_province','city.code as code_city','district.code as code_district')
			->leftJoin('province','province.id','temp_cms_users.id_province')
			->leftJoin('city','city.id','temp_cms_users.id_city')
			->leftJoin('district','district.id','temp_cms_users.id_district')
			->leftJoin('m_jenis_lembaga','m_jenis_lembaga.id','temp_cms_users.id_m_jenis_lembaga')
			->where('temp_cms_users.unique_hash',$unique_hash)
			->orderBy('temp_cms_users.id','desc')
			->first();

			if($data['row'] == NULL)
			{
				return abort(404);
			}

			return view('front/ralatpendaftaranpenerbit',$data);
		}else{

			return abort(404);
		}		
	}

	public function getDeleteAttachment()
	{
		$id = Request::get('id');
		$column = Request::get('column');
	
		$row = DB::table('temp_cms_users')->where('id', $id)->first();

		$file = str_replace('uploads/', '', $row->{$column});

		if (Storage::exists($file)) {
			Storage::delete($file);
		}

		DB::table('temp_cms_users')->where('id', $id)->update([$column => null]);

		CRUDBooster::redirect(Request::server('HTTP_REFERER'), trans('crudbooster.alert_delete_data_success'), 'success');
	}

	public static function postTempEdit()
	{
		$id =  Request::input('id');
		$uid =  Request::input('uid');
		$now         = Kemenag::now();
		// $password    = Kemenag::generatePassword();
		$id_penerbit = Kemenag::generateIdPenerbit();
		
		$lembaga = Request::input('id_m_jenis_lembaga');
		$email = Request::input('email');		

		$array = [
			'id' => 'required',
			'uid' => 'required',
			'id_province' => 'required',
			'id_city' => 'required',
			'id_district' => 'required',
			'kode_pos' => 'required',
			// 'nib_value' => 'required',
			// 'npwp_value' => 'required',
			'pic_address' => 'required',
			'pic_phone' => 'required',
			'pic_phone' => 'required',
			'pic_email' => 'required',
			'password'	=> 'min:6|required_with:password_confirmation|same:password_confirmation',
			'password_confirmation'	=> 'min:6|required',
		];

		$valid = Validator::make(Request::all(),$array);

		if($valid->fails()){

			return redirect()->back()->with([
				'message'=>$valid->errors()->first(),
				'message_type'=>'danger'
			])->withInput();

			// return CRUDBooster::redirectback($valid->errors()->first(),'error');
		}else{
		    $save['created_at']         = $now;
    		$save['updated_at']         = $now;
    		$save['name']               = Request::input('name');
    		$save['email']              = Request::input('email');
			$password 					= Request::input('password');
    		$save['password']           = Hash::make($password);
    		$save['id_cms_privileges']  = 2;
    		$save['id_penerbit']        = $id_penerbit;
    		$save['password_value']     = $password;
    		$save['status']             = 'Belum ditindaklanjuti';
    		$save['phone']              = Request::input('phone');
    		$save['pic']                = Request::input('pic');
    		$save['address']            = Request::input('address');
    		$save['id_m_jenis_lembaga'] = Request::input('id_m_jenis_lembaga');
    		$save['send_profile']       = 0;
    
			$save['id_province'] = Request::input('id_province');
    		$save['id_city'] = Request::input('id_city');
    		$save['id_district'] = Request::input('id_district');
    		$save['kode_pos'] = Request::input('kode_pos');
			
    		$save['nib_value'] = Request::input('nib_value');
    		$save['npwp_value'] = Request::input('npwp_value');

    		$save['pic_address'] = Request::input('pic_address');
    		$save['pic_jabatan'] = Request::input('pic_jabatan');
    		$save['pic_phone'] = Request::input('pic_phone');
    		$save['pic_email'] = Request::input('pic_email');
			// dd($save);

    		 // url encode the address
			$address = Request::input('address');
    		
    		$address = urlencode( $address );
    		$url     = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyDKuY76FBWSQpCoO7a8GJ_h9NpdyLIEID0";
    		$resp    = json_decode( file_get_contents( $url ), true );
    		if ( $resp['status'] === 'OK' ) {
    			$formatted_address = $resp['results'][0]['formatted_address'];
    			$lat               = $resp['results'][0]['geometry']['location']['lat'];
    			$long              = $resp['results'][0]['geometry']['location']['lng'];
    		}
    
    		$check = DB::table('cms_users')
    			->where('email',$save['email'])
    			->first();
    
    		if (empty($check)) {
				// echo $id;
				DB::table('temp_cms_users')->where('id',$id)->update($save);
				// dd($save);
				// die();
				$save_location_map=DB::table('temp_master_pointing_user')
    				->insert([
    					"created_at" => date('Y-m-d h:i:s'),
    					"updated_at" => date('Y-m-d h:i:s'),
    					"id_cms_users" => $id,
    					"lat" => $lat,
    					"lng" => $long,
    				]);
    
    			if ($id) {
					$save_file = [];

					if(Request::hasFile('surat_permohonan'))
					{
						$save_file['surat_permohonan']   = Kemenag::uploadFile('surat_permohonan','penerbit/'.$id);
					}
					if(Request::hasFile('siup'))
					{
						$save_file['siup']               = Kemenag::uploadFile('siup','penerbit/'.$id);
					}
					if(Request::hasFile('company_profile'))
					{
						$save_file['company_profile']    = Kemenag::uploadFile('company_profile','penerbit/'.$id);
					}
					if(Request::hasFile('nib'))
					{
						$save_file['nib']    = Kemenag::uploadFile('nib','penerbit/'.$id);
					}

					if(Request::hasFile('scan_ktp'))
					{
						$save_file['scan_ktp']  = Kemenag::uploadFile('scan_ktp','penerbit/'.$id);
					}
					
					if(Request::hasFile('npwp'))
					{
						if(Kemenag::uploadFile('npwp','penerbit/'.$id) !== 'false') {
							$save_file['npwp']               = Kemenag::uploadFile('npwp','penerbit/'.$id);
						}
					}
					
					if(Request::hasFile('akte_notaris'))
					{
						if(Kemenag::uploadFile('akte_notaris','penerbit/'.$id) !== 'false') {
							$save_file['akte_notaris']       = Kemenag::uploadFile('akte_notaris','penerbit/'.$id);
						}
					}
					
					if(Request::hasFile('tdp'))
					{
						if(Kemenag::uploadFile('tdp','penerbit/'.$id) !== 'false') {
							$save_file['tdp']                = Kemenag::uploadFile('tdp','penerbit/'.$id);
						}
					}

					if(count($save_file) > 0)
					{
						DB::table('temp_cms_users')->where('id',$id)->update($save_file);
					}


    				return redirect(CRUDBooster::mainpath('preview-insert?uid='.$uid))->with([
    					'message'=>'Lihat dan dan tinjau ulang data yang Anda isikan.<br>
						Jika seluruh data sudah benar, klik Registrasi Penerbit. Jika ada yang salah, klik Edit Data.',
    					'message_type'=>'success',
    				])->withInput();;
    			}else{
    				return redirect()->back()->with([
    					'message'=>'oops something went wrong, please try again',
    					'message_type'=>'warning'
    				])->withInput();
    			}
    		}else{
    			return redirect()->back()->with([
    				'message'=>'Email yang anda gunakan sudah terdaftar',
    				'message_type'=>'danger'
    			])->withInput();
    		}   
		}
	}

	public function postInsertSave()
	{
		try
		{
			$array = [
				'id' => 'required',
			];

			$valid = Validator::make(Request::all(),$array);

			if($valid->fails()){
				return response()->json(['status' => 500, 'message' => $valid->errors()->first()
				,'redirect' => $redirect], 200); 
			}

			$temp = DB::table('temp_cms_users')->where('id',g('id'))->first();

			if($temp == NULL)
			{
				return response()->json(['status' => 500, 'message' => 'Empty Temp','flag' => 'empty_temp'], 200); 
			}

			$now         = Kemenag::now();

		    $save['created_at']         = $now;
    		$save['updated_at']         = $now;
    		$save['name']               = $temp->name;
    		$save['email']              = $temp->email;
    		$save['password']           = $temp->password;
    		$save['id_cms_privileges']  = 2;
    		$save['id_penerbit']        = $temp->id_penerbit;
    		$save['password_value']     = $temp->password_value;
    		$save['status']             = 'Belum ditindaklanjuti';
    		$save['phone']              = $temp->phone;
    		$save['pic']             	= $temp->pic;
    		$save['address']            = $temp->address;
    		$save['id_m_jenis_lembaga'] = $temp->id_m_jenis_lembaga;
    		$save['send_profile']       = 0;

			$province = DB::table('province')->where('code',$temp->id_province)->first();
			$city = DB::table('city')->where('code',$temp->id_city)->first();
			$district = DB::table('district')->where('code',$temp->id_district)->first();

    		$save['id_province'] = $province->id;
    		$save['id_city'] = $city->id;
    		$save['id_district'] = $district->id;
    		$save['kode_pos'] = $temp->kode_pos;

			$save['nib_value'] = $temp->nib_value;
    		$save['npwp_value'] = $temp->npwp_value;

    		$save['pic_address'] = $temp->pic_address;
    		$save['pic_jabatan'] = $temp->pic_jabatan;
    		$save['pic_phone'] = $temp->pic_phone;
    		$save['pic_email'] = $temp->pic_email;
		
    		$check = DB::table('cms_users')
    			->where('email',$save['email'])
    			->first();
    
    		if ($check->id == NULL)
			{
				DB::beginTransaction();
				
    			$id = DB::table('cms_users')->insertGetId($save);

				$temp_master_pointing_user = DB::table('temp_master_pointing_user')->where('id_cms_users',$temp->id)->first();

    			$save_location_map= DB::table('master_pointing_user')
    				->insert([
    					"created_at" => date('Y-m-d h:i:s'),
    					"updated_at" => date('Y-m-d h:i:s'),
    					"id_cms_users" => $temp_master_pointing_user->id,
    					"lat" => $temp_master_pointing_user->lat,
    					"lng" => $temp_master_pointing_user->long,
    				]);
    
					$save_file['surat_permohonan']   = $temp->surat_permohonan;
					$save_file['siup']               = $temp->siup;
					$save_file['company_profile']    = $temp->company_profile;
					$save_file['nib']    			 = $temp->nib;
					$save_file['scan_ktp']  		 = $temp->scan_ktp;
					$save_file['npwp']               = $temp->npwp;
					$save_file['akte_notaris']       = $temp->akte_notaris;
					$save_file['tdp']                = $temp->tdp;

					DB::table('cms_users')->where('id',$id)->update($save_file);

					$email = $save['email'];

					if(filter_var($email, FILTER_VALIDATE_EMAIL))
					{
						// send email
						$data_email['name'] = $email;
						$data_email['password'] = $save['password_value'];
						CRUDBooster::sendEmail(['to'=>$email,'data'=>$data_email,'template'=>'infromasi_register']);
					}
					else {
						return response()->json(['status' => 500, 'message' => 'Email ' .$email .' tidak valid', 'flag' => 'error'], 200); 
					}

					DB::table('temp_cms_users')->where('id',$temp->id)->delete();
					DB::table('temp_master_pointing_user')->where('id',$temp_master_pointing_user->id)->delete();
					
					DB::commit();

					return response()->json(['status' => 200, 
					'message' => "Registrasi penerbit Anda telah berhasil. LPMQ akan memverifikasi dan menyetujui atau menolak pendaftaran akun penerbit Al-Qur'an Anda maksimal dua hari kerja. Silakan cek email masuk di kotak masuk dan/atau folder spam pada email yang diisikan saat mengisi Informasi Data Penerbit untuk mengetahui hasil verifikasi pendaftaran penerbit Anda. Jika pendaftaran Anda ditolak, Anda dapat mendaftar akun penerbit Al-Qur’an lagi."
					,'redirect' => $redirect], 200); 
			}
			else
			{
				return response()->json(['status' => 500, 'message' => 'Email yang anda gunakan sudah terdaftar', 'flag' => 'error'], 200); 
			}

		}
		catch (\Exception $e)
		{
			DB::rollback();

			return response()->json(['status' => 500, 'message' => $e->getMessage(), 'flag' => 'error'], 200); 
		}
	}


}