<?php
namespace App\Http\Controllers;

use Session;
use Request;
use Kemenag;
use DB;
use Hash;
use Redirect;
use Validator;
use CRUDBooster;

class FrontPengajuanTandaTashihController extends Controller
{
	public function getIndex(){
		return redirect('admin/list-tashih/add');

		$id_cms_users = CRUDBooster::myId();

		if ($id_cms_users == '') {
			return redirect('/')->with([
				'message'=>'Something went wrong',
				'message_type'=>'danger'
			]);
		}
		
		Kemenag::menu('pentashihan');
		Kemenag::menuSub('pengajuanTashih');

		$jenis_naskah = DB::table('m_jenis_naskah')
			->whereNull('deleted_at')
			->orderBy('name','ASC')
			->get();
		$jenis_mushaf = DB::table('m_jenis_mushaf')
			->whereNull('deleted_at')
			->orderBy('name','ASC')
			->get();
		$materi_tambahan = DB::table('m_materi_tambahan')
			->whereNull('deleted_at')
			->get();

		$result['jenis_naskah']    = $jenis_naskah;
		$result['jenis_mushaf']    = $jenis_mushaf;
		$result['materi_tambahan'] = $materi_tambahan;

		return view('front/pengajuantandatashih',$result);
	}

	public function postSave(){
		return redirect('admin/list-tashih/add');
		
		$request              = Request::all();
		$now                  = Kemenag::now();
		$materi_lainnya       = Request::input('materi_lainnya');
		$materi_tambahan_lain = Request::input('materi_tambahan_lain');
		$id_cms_users         = CRUDBooster::myId();

		if ($id_cms_users == '') {
			return redirect('/')->with([
				'message'=>'Something went wrong',
				'message_type'=>'danger'
			]);
		}

		$save['created_at']           = $now;
		$save['updated_at']           = $now;
		$save['id_cms_users']         = $id_cms_users;
		$save['id_m_materi_tambahan'] = implode(';', $materi_tambahan_lain);

		foreach ($request as $key => $value) {
			if ($key == 'materi_lainnya' || $key == 'materi_tambahan_lain' || $key == '_token' || $key == 'lainnya') continue;

			if ($key == 'surat_permohonan' || $key == 'gambar_cover' || $key == 'dokumen_naskah' || $key == 'bukti_tashih_internal') {
				$save[$key] = Kemenag::uploadFile($key,'proses_pentashihan',true);
			}else{
				$save[$key] = $value;
			}
		}
		$id = DB::table('proses_pentashihan')->insertGetId($save);

		if ($id) {
			
			for ($i=0; $i < count($materi_lainnya); $i++) {
				$save_tambahan['created_at']            = $now;
				$save_tambahan['updated_at']            = $now;
				$save_tambahan['name']                  = $materi_lainnya[$i];
				$save_tambahan['id_proses_pentashihan'] = $id;

				DB::table('proses_pentashihan_materi_tambahan')->insert($save_tambahan);
			}

			$cms_users = DB::table('cms_users')
				->where('id',$save['id_cms_users'])
				->first();

			$data_email['name'] = $cms_users->name;
			CRUDBooster::sendEmail(['to'=>$cms_users->email,'data'=>$data_email,'template'=>'info_pendaftaran_mushaf']);

			return redirect()->back()->with([
				'message'=>'Tashih anda sudah berhasil didaftarkan',
				'message_type'=>'success'
			]);
		}else{

			return redirect()->back()->with([
				'message'=>'Something went wrong',
				'message_type'=>'danger'
			]);
		}
	}
}