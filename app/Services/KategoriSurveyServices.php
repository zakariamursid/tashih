<?php

namespace App\Services;

use App\Repositories\KategoriSurveyRepositories;
use Illuminate\Support\Facades\DB;
use App\Models\KategoriSurvey;

class KategoriSurveyServices
{
    public static function findById($id)
    {
        $data = KategoriSurvey::find($id);
        if ($data) {
            $data->from = ($data->from?date('d M Y, H:i', strtotime($data->from)):'');
            $data->to = ($data->to?date('d M Y, H:i', strtotime($data->to)):'');
        }

        return $data;
    }

    public static function joinUnsur($id_category)
    {
        $data = KategoriSurveyRepositories::joinUnsur($id_category);

        foreach ($data as $x => $row) {
            $row->opsi = KategoriSurveyRepositories::surveyOpsi($row->id_survey_unsur);
        }

        return $data;
    }
}
