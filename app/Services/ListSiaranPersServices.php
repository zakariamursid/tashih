<?php

namespace App\Services;

use App\Repositories\ListSiaranPersRepositories;
use Illuminate\Support\Facades\DB;

class ListSiaranPersServices
{
    public static function getListSiaranPers()
    {
        $search = request('search');
        $total_record = (request('totalRecord') == '' ? 6 : request('totalRecord'));

        $data = ListSiaranPersRepositories::getListSiaranPers($search, $total_record);
        
		$total   = count($data);
		$max_row = $total - 1;
        foreach ($data as $x => $row) {
			$row->users_image      = ($row->users_image == '' ? asset('image/no-image.png') : asset($row->users_image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 70);
			$row->title   = $title.($length_title > 70 ? '...' : '');

			$content        = strip_tags($row->content);
			$length_content = strlen($content);
			$content        = substr($content, 0, 130);
			$row->content   = $content.($length_content > 130 ? '...' : '');

			$num++;
        }
        
        return $data;
    }

    public static function arrayId()
    {
        $list = static::getListSiaranPers();

        $list_id = null;
        $num     = 0;
        foreach ($list as $x => $row) {
            $list_id .= $row->id;
            $list_id .= ($num < $max_row ? ';' : '');
            
			$num++;
        }
        
		$id = explode(';', $list_id);
        return $id;
    }

    public static function getListSiaranPersOthers()
    {
        $arrayId = static::arrayId();

        $data = ListSiaranPersRepositories::getListSiaranPersOthers();

        $list = [];
        foreach ($data as $row) {
			$row->users_image      = ($row->users_image == '' ? asset('image/no-image.png') : asset($row->users_image));

			$length_title = strlen($row->title);
			$title        = substr($row->title, 0, 50);
            $row->title   = $title.($length_title > 50 ? '...' : '');
            
            if (in_array($row->id, $arrayId)) {
                continue;
            }else{
                $list[] = $row;
            }
        }
        
        return $list;
    }

    public static function findListSiaranPers($slug)
    {
        $data = ListSiaranPersRepositories::findListSiaranPers($slug);

        if ($data) {
            $data->users_image      = ($data->users_image == '' ? asset('image/no-image.png') : asset($data->users_image));
        }

        return $data;
    }

    public static function findListSiaranPersOthers($slug)
    {
        $find = ListSiaranPersRepositories::findListSiaranPers($slug);
        $data = ListSiaranPersRepositories::getListSiaranPersOthers();

        $arrayId = [($find?$find->id:null)];
        $list = [];
        foreach ($data as $x => $row) {
			$row->users_image      = ($row->users_image == '' ? asset('image/no-image.png') : asset($row->users_image));

			$length_title = strlen($row->tite);
            $row->title   = $row->title.($length_title > 50 ? '...' : '');
            
            if (in_array($row->id, $arrayId)) {
                continue;
            }else{
                $list[] = $row;
            }
        }
        
        return $list;
    }

    public static function listBannerHeader()
    {
        $data_infolayanan = ListSiaranPersRepositories::listBannerInfoLayanan();

		$data_seputarlajnah = ListSiaranPersRepositories::listBannerSeputarLajnah();

		$data_siaranpers = ListSiaranPersRepositories::listBannerSiaranPers();
            
        $results = $data_infolayanan->union($data_seputarlajnah)->union($data_siaranpers)->get();
        // dd($results);
		foreach ($results as $x => $xrow) {
			$type = $xrow->type;
			if ($type == 'infolayanan') {
				$xrow->link = url('info-layanan-pentashihan/read/'.$xrow->slug);
			}elseif ($type == 'seputarlajnah') {
				$xrow->link = url('info-seputar-lajnah/read/'.$xrow->slug);
			}elseif ($type == 'siaranpers') {
				$xrow->link = url('list-siaran-pers/read/'.$xrow->slug);
			}
        }
        
        return $results;
    }

    public static function searchInfoPenerbit($search) {
        $data = ListSiaranPersRepositories::searchInfoPenerbit($search);

        foreach ($data as $key => $xrow) {
            $mushaf = ListSiaranPersRepositories::listMushafPenerbit($xrow->id);
            $nama_mushaf = [];
            foreach ($mushaf as $yrow) {
                if (!empty($yrow->nama_produk)) {
                    array_push($nama_mushaf, $yrow->nama_produk);
                }
            }
            $nama_mushaf = '- '.implode('<br>- ',$nama_mushaf).'<br>';

            $xrow->mushaf = $nama_mushaf;
        }

        return $data;
    }

    public static function searchListSiaranPers($search) {
        $data = ListSiaranPersRepositories::searchListSiaranPers($search);
        foreach ($data as $x => $xrow) {
            $xrow->content = str_limit(strip_tags($xrow->content), 170, '...');
        }
        return $data;
    }

    public static function searchListInfoLayananTashih($search) {
        $data = ListSiaranPersRepositories::searchListInfoLayananTashih($search);
        foreach ($data as $x => $xrow) {
            $xrow->content = str_limit(strip_tags($xrow->content), 170, '...');
        }
        return $data;
    }

    public static function searchListInfoSeputarLajnah($search) {
        $data = ListSiaranPersRepositories::searchListInfoSeputarLajnah($search);
        foreach ($data as $x => $xrow) {
            $xrow->content = str_limit(strip_tags($xrow->content), 170, '...');
        }
        return $data;
    }

}
