<?php

namespace App\Services;

use App\Repositories\BannerLayananRepositories;
use Illuminate\Support\Facades\DB;

class BannerLayananServices extends BannerLayananRepositories
{
    public static function listBannerLayanan()
    {
        $data = BannerLayananRepositories::listBannerLayanan();

        foreach ($data as $x => $xrow) {
            if (empty($xrow->url)) {
                $link = url('banner-layanan/read/'.$xrow->slug);
            }else{
                $link = $xrow->url;
            }

            $xrow->link = $link;
        }

        return $data;
    }

    public static function findBannerLayanan($slug)
    {
        return BannerLayananRepositories::findBannerLayanan($slug);
    }

    public static function listRandomBannerLayanan($slug)
    {
        $data = BannerLayananRepositories::findBannerLayanan($slug);
        $id = ($data?$data->id:null);
        
        $list = BannerLayananRepositories::listRandomBannerLayanan($id);

        foreach ($list as $x => $xrow) {
            if (empty($xrow->url)) {
                $link = url('banner-layanan/read/'.$xrow->slug);
            }else{
                $link = $xrow->url;
            }

            $xrow->link = $link;
        }

        return $list;
    }
    
}
