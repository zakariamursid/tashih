<?php

namespace App\Services\RevisiSurvey;

use App\Repositories\RevisiSurvey\SurveyAnswerRepositories;
use App\Repositories\RevisiSurvey\SurveyUnsurRepositories;
use App\Repositories\RevisiSurvey\SurveyConnectRepositories;
use Illuminate\Support\Facades\DB;

class SurveyAnswerServices extends SurveyAnswerRepositories
{
    public static function checkIsAnswer($users_id, $nama_survey_id, $type)
    {
        return SurveyAnswerRepositories::checkIsAnswer($users_id, $nama_survey_id, $type);
    }

    public static function getListLaporanSurvey($nama_survey_id = null)
    {
        $result = SurveyAnswerRepositories::getListLaporanSurvey($nama_survey_id);
        $list = self::getListUnsurLaporanSurvey();

        foreach($result as $z => $zrow) {
            if(!isset($list[$zrow->id_unsur]['answer'][$zrow->id_user])){
                foreach($list[$zrow->id_unsur]['survey'] as $id_survey => $row){
                    $push_answer_temp[$id_survey] = null;
                }

                $list[$zrow->id_unsur]['answer'][$zrow->id_user]['name'] = $zrow->user_name;
                $list[$zrow->id_unsur]['answer'][$zrow->id_user]['list'] = $push_answer_temp;
            }

            if (isset($list[$zrow->id_unsur]['answer'][$zrow->id_user])) {
                $push_answer['id_user'] = $zrow->id_user;
                $push_answer['question'] = $zrow->question;
                $push_answer['opsi_value'] = $zrow->opsi_value;
                $list[$zrow->id_unsur]['answer'][$zrow->id_user][$zrow->id_question] = $push_answer;
            }
        }

        return $list;
    }

    public static function getListUnsurLaporanSurvey()
    {
        $unsur = SurveyUnsurRepositories::getAll();

        $list = [];
        foreach ($unsur as $x => $xrow) {
            $push_unsur['id'] = $xrow->id;
            $push_unsur['name'] = $xrow->nama;
            $push_unsur['survey'] = [];
            $push_unsur['answer'] = [];
            $list[$xrow->id] = $push_unsur;

            $survey = SurveyConnectRepositories::getPertanyaanByUnsur($xrow->id);

            foreach ($survey as $y => $yrow) {
                $push_survey['id'] = $yrow->id;
                $push_survey['type'] = $yrow->type;
                $push_survey['pertanyaan_survey'] = $yrow->pertanyaan_survey;

                $yrow->answer = [];
                $list[$xrow->id]['survey'][$yrow->id] = $push_survey;
            }
        }

        return $list;
    }

    public static function countUsersSurvey($nama_survey_id = null)
    {
        $list = SurveyAnswerRepositories::countUsersSurvey($nama_survey_id);
        return ($list ? count($list) : 0);
    }
}
