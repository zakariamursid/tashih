<?php

namespace App\Services\RevisiSurvey;

use App\Repositories\RevisiSurvey\SurveyUnsurRepositories;
use Illuminate\Support\Facades\DB;

class SurveyUnsurServices extends SurveyUnsurRepositories
{
    public static function getListLaporan($nama_survey_id = null)
    {
        $data = SurveyUnsurRepositories::getListLaporan($nama_survey_id);
        foreach ($data as $x => $row) {
            $sum_nilai = SurveyUnsurRepositories::sumNilai($nama_survey_id, $row->id);
            $row->sum_nilai = $sum_nilai;

            $count_nilai = SurveyUnsurRepositories::countNilai($nama_survey_id, $row->id);
            $row->count_nilai = $count_nilai;

            $count_users = SurveyUnsurRepositories::countUsers($nama_survey_id, $row->id);
            $row->count_users = $count_users;

//            $row->average = static::averageSurvey($row->sum_nilai, $row->count_nilai);
//            $average_nilai = SurveyUnsurRepositories::averageNilai($nama_survey_id, $row->id);
            $average_nilai = ($sum_nilai > 0 ? $sum_nilai/$count_nilai :0);
            $row->average_nilai = $average_nilai;

            $row->index = static::indexReportSurvey($average_nilai);
            $mutu = static::mutuPelayananReportSurvey($average_nilai);
            $row->mutu = $mutu;
            $row->kinerja = static::kinerjaPelayananReportSurvey($mutu);
        }
        return $data;
    }

    public static function averageSurvey($avg, $count)
    {
        if ((float) $avg > 0) {
            return $avg/$count;
        } else {
            return 0;
        }
    }

    public static function indexReportSurvey($avg) {
        if ($avg) {
            $get = ($avg/4)*100;
            return $get;
        }else{
            return 0;
        }
    }

    public static function mutuPelayananReportSurvey($index) {
        if ((float) $index >= (float) 3.5323) {
            $mutu = "A";
        }elseif ((float) $index >= (float) 3.0643 && (float) $index <= (float) 3.5323) {
            $mutu = "B";
        }elseif ((float) $index >= (float) 2.59 && (float) $index <= (float) 3.0643) {
            $mutu = "C";
        }elseif ((float) $index >= (float) 1 && (float) $index <= (float) 2.59) {
            $mutu = "D";
        }elseif((float) $index < (float) 1){
            $mutu = "Belum disurvei";
        }
        return $mutu;
    }

    public static function kinerjaPelayananReportSurvey($mutu) {
        if ($mutu == 'A') {
            $kinerja = "Sangat baik";
        }elseif ($mutu == 'B') {
            $kinerja = "Baik";
        }elseif ($mutu == 'C') {
            $kinerja = "Kurang baik";
        }elseif ($mutu == 'D') {
            $kinerja = "Tidak baik";
        }else{
            $kinerja = "Belum disurvei";
        }
        return $kinerja;
    }
}
