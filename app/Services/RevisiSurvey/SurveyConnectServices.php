<?php

namespace App\Services\RevisiSurvey;

use App\Repositories\RevisiSurvey\SurveyConnectRepositories;
use App\Repositories\RevisiSurvey\SurveyOpsiPertanyaanRepositories;
use Illuminate\Support\Facades\DB;

class SurveyConnectServices extends SurveyConnectRepositories
{
    /**
     * list survey
     */
    public static function getSurveyActive()
    {
        $survey = SurveyConnectRepositories::getSurveyActive();
        foreach ($survey as $x => $row) {
            $option = SurveyOpsiPertanyaanRepositories::getByPertanyan($row->survey_pertanyaan_id);
            $row->option = $option;
        }
        return $survey;
    }

    /**
     * survey id active
     */
    public static function findSurveyIDActive()
    {
        $find = SurveyConnectRepositories::findSurveyIDActive();
        return ( $find ? $find->survey_nama_id : null );
    }
}
