<?php

namespace App\Services\RevisiSurvey;

use App\Repositories\RevisiSurvey\SurveyNamaRepositories;
use Illuminate\Support\Facades\DB;

class SurveyNamaServices extends SurveyNamaRepositories
{
    public static function typeStatus($status)
    {
        switch ($status) {
            case 'active':
                $btn = 'btn-success';
                break;

            case 'expired':
                $btn = 'btn-danger';
                break;
            
            default:
                $btn = 'btn-default';
                break;
        }

        return $btn;
    }

    public static function listSurveyOption()
    {
        $data = SurveyNamaRepositories::getAll();
        $list = [];
        foreach ($data as $x => $row) {
            $list[] = '<option '.($_GET['name_survey_id']==$row->id?'selected':'').' value="'.request()->fullUrlWithQuery(['name_survey_id'=>$row->id]).'">'.$row->nama.'</option>';
        }

        return implode(" ", $list);
    }
}
