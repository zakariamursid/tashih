<?php

namespace App\Services;

use App\Repositories\DownloadMateriRepositories;
use Illuminate\Support\Facades\DB;
use Kemenag;

class DownloadMateriServices
{
    public static function getListDownloadMateri()
    {
        $search = request('search');
        $total_record = (request('totalRecord') == '' ? 6 : request('totalRecord'));

        $data = DownloadMateriRepositories::getListDownloadMateri($search, $total_record);
        
        return $data;
    }

    public static function getListMateriUpload($id_materi_upload)
    {
        $data = DownloadMateriRepositories::getListMateriUpload($id_materi_upload);

        foreach ($data as $x => $xrow) {
            $xrow->name_file = ($xrow->name_file?'Materi '.$xrow->name_file:null);

            $takepathfile = parse_url($xrow->file);
            $takeExt = pathinfo($takepathfile['path'], PATHINFO_EXTENSION);
            $name = str_replace(' ','-',$xrow->name_file).'.'.$takeExt;
            $xrow->file = $name;

            // $file = str_replace(url('/'),'', $xrow->file);
            // if (!Kemenag::fileExists($file)) {
            //     continue;
            // }
        }
        return $data;
    }
    
}
