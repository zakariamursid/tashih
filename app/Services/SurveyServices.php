<?php

namespace App\Services;

use App\Repositories\SurveyRepositories;
use Illuminate\Support\Facades\DB;

class SurveyServices extends SurveyRepositories
{
    public static function exportSurvey()
    {
        $result = static::listAnswerSurvey();

        $list = self::listUnsurSurvey();

        foreach($result as $z => $zrow) {
            if(!isset($list[$zrow->id_unsur]['answer'][$zrow->id_user])){
                foreach($list[$zrow->id_unsur]['survey'] as $id_survey => $row){
                    $push_answer_temp[$id_survey] = null;
                }

                $list[$zrow->id_unsur]['answer'][$zrow->id_user]['name'] = $zrow->user_name;
                $list[$zrow->id_unsur]['answer'][$zrow->id_user]['list'] = $push_answer_temp;
            }

            if (isset($list[$zrow->id_unsur]['answer'][$zrow->id_user])) {
                $push_answer['id_user'] = $zrow->id_user;
                $push_answer['question'] = $zrow->question;
                $push_answer['opsi_value'] = $zrow->opsi_value;
                $list[$zrow->id_unsur]['answer'][$zrow->id_user][$zrow->id_question] = $push_answer;
            }
        }

        return $list;
    }

    public static function listAnswerSurvey()
    {
        $data = SurveyRepositories::listAnswerPenerbit();

        foreach ($data as $x => $xrow) {
            $xrow->opsi_value = static::switchValueAnswer($xrow->opsi_text);
        }

        return $data;
    }

    public static function switchValueAnswer($text_answer)
    {
        switch ($text_answer) {
            case 'A':
                $value = 4;
                break;

            case 'B':
                $value = 3;
                break;

            case 'C':
                $value = 2;
                break;

            case 'D':
                $value = 1;
                break;
            
            default:
                $value = 0;
                break;
        }

        return $value;
    }

    public static function listUnsurSurvey() {
        $unsur = SurveyRepositories::listUnsurSurvey();

        $list = [];
        foreach ($unsur as $x => $xrow) {
            $push_unsur['id'] = $xrow->id;
            $push_unsur['name'] = $xrow->nama;
            $push_unsur['survey'] = [];
            $push_unsur['answer'] = [];
            $list[$xrow->id] = $push_unsur;

            $survey = SurveyRepositories::listSurveyPenerbit($xrow->id);

            foreach ($survey as $y => $yrow) {
                $push_survey['id'] = $yrow->id;
                $push_survey['type'] = $yrow->type;
                $push_survey['pertanyaan_survey'] = $yrow->pertanyaan_survey;

                $yrow->answer = [];
                $list[$xrow->id]['survey'][$yrow->id] = $push_survey;
            }
        }

        return $list;
    }


    public static function reportSurvey() {
        $data = SurveyRepositories::listUnsurReport();

        foreach ($data  as $x => $xrow) {
            $take = SurveyRepositories::listAsnwerReport($xrow->id);
            $sum = ($take?$take->sumnilai:0);
            $count = ($take?$take->countnilai:0);
            $avg = ($take?$take->avgnilai:0);

            $xrow->sum = $sum;
            $xrow->count = $count;
            $xrow->avg = $avg;

            $index = static::indexReportSurvey($avg);
            $xrow->index = $index;

            $mutu = static::mutuPelayananReportSurvey($avg);
            $xrow->mutu = $mutu;

            $kinerja = static::kinerjaPelayananReportSurvey($mutu);
            $xrow->kinerja = $kinerja;

            $penerbit = SurveyRepositories::countPenerbitReport($xrow->id);
            $xrow->penerbit = $penerbit;
        }

        return $data;
    }

    public static function indexReportSurvey($avg) {
        if ($avg) {
            $get = ($avg/4)*100;
            return $get;
        }else{
            return 0;
        }
    }

    public static function mutuPelayananReportSurvey($index) {
        if ((float) $index >= (float) 3.5323) {
            $mutu = "A";
        }elseif ((float) $index >= (float) 3.0643 && (float) $index <= (float) 3.5323) {
            $mutu = "B";
        }elseif ((float) $index >= (float) 2.59 && (float) $index <= (float) 3.0643) {
            $mutu = "C";
        }elseif ((float) $index >= (float) 1 && (float) $index <= (float) 2.59) {
            $mutu = "D";
        }elseif((float) $index < (float) 1){
            $mutu = "Belum disurvei";
        }
        return $mutu;
    }

    public static function kinerjaPelayananReportSurvey($mutu) {
        if ($mutu == 'A') {
            $kinerja = "Sangat baik";
        }elseif ($mutu == 'B') {
            $kinerja = "Baik";
        }elseif ($mutu == 'C') {
            $kinerja = "Kurang baik";
        }elseif ($mutu == 'D') {
            $kinerja = "Tidak baik";
        }else{
            $kinerja = "Belum disurvei";
        }
        return $kinerja;
    }
}
