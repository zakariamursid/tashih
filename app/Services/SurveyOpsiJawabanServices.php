<?php

namespace App\Services;

use App\Repositories\SurveyOpsiJawabanRepositories;
use Illuminate\Support\Facades\DB;

class SurveyOpsiJawabanServices extends SurveyOpsiJawabanRepositories
{
    public static function exportOpsiJawaban($limit = 100, $name_survey_id = null)
    {
        $data = SurveyOpsiJawabanRepositories::exportOpsiJawaban($limit, $name_survey_id);
        foreach ($data as $x => $row) {
            $row->nilai = self::switchOpsi($row->option_survey);
        }
        return $data;
    }

    public static function switchOpsi($value)
    {
        switch ($value) {
            case 'A':
                $ret = 4;
                break;

            case 'B':
                $ret = 3;
                break;

            case 'C':
                $ret = 2;
                break;

            case 'D':
                $ret = 1;
                break;
            
            default:
                $ret = $value;
                break;
        }

        return $ret;
    }
}
