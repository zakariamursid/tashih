<?php

if(!function_exists('crocodic_log')) {
    /**
     * @param $activityName
     * @param $activityContent
     * @param int $statusCode
     * Don't forget change $code
     */
    function crocodic_log($activityName, $activityContent, $statusCode = 200)
    {
        $code = env("CROCODIC_LOG_CODE");
        if($code) {
            try {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'http://rnd.crocodic.net/project-log/public/api/logging/log',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 3,
                    CURLOPT_NOSIGNAL=> 1,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('code' => $code, 'name' => $activityName, 'content' => $activityContent, 'status_code' => $statusCode),
                ));
                curl_exec($curl);
                curl_close($curl);
                Log::debug("Crocodic Log: ".$activityName);
            } catch (\Exception $e) {
                Log::error("Crocodic Log: ".$activityName." - ".$e->getMessage());
            }
        }
    }
}
