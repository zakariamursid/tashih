<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Helpers\Kemenag;

class Postmark 
{
    private $template;
    private $from;
    private $to;
    private $subject;
    private $cc;
    private $sender;
    private $content;

    function setTemplate($template) { 
        $this->template = $template; 
    }
    function getTemplate() { 
        return $this->template; 
    }
    function setFrom($from) { 
        $this->from = $from; 
    }
    function getFrom() { 
        return $this->from; 
    }
    function setTo($to) { 
        $this->to = $to; 
    }
    function getTo() { 
        return $this->to; 
    }
    function setSubject($subject) { 
        $this->subject = $subject; 
    }
    function getSubject() { 
        return $this->subject; 
    }
    function setCC($cc) { 
        $this->cc = $cc; 
    }
    function getCC() { 
        return $this->cc; 
    }
    function setSender($email = []) { 
        $this->sender = ['email'=>$email['email'],'name'=>$email['name']]; 
    }
    function getSender() { 
        return $this->sender; 
    }
    function setContent($content = []) { 
        $this->content = $content; 
    }
    function getContent() { 
        return $this->content; 
    }


    function SendTemplate()
    {
        $to = $this->to;
        // $data = ($this->content?:[]);
        $template = $this->template;
        $sender = $this->sender;

        $template = self::first('email_notif_template', ['slug' => $template]);
        $html = $template->content;
        
        $subject = ($this->subject ? $this->subject : $template->subject);
        $attachments = ($template->file?storage_path('app/' . $template->file):null);

        try {
            $log = Mail::send("mail.template.blank", ['content' => $html], function ($message) use ($to, $subject, $sender, $attachments) {
                $message->priority(1);
                $message->to($to);
    
                if ($sender['email']) {
                    $from_name = $sender['name'];
                    $message->from($sender['email'], $from_name);
                }
    
                $message->subject($subject);

                if (!empty($attachments)) {
                    $message->attach($attachments);
                }
            });
            return true;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    public static function parseSqlTable($table)
    {

        $f = explode('.', $table);

        if (count($f) == 1) {
            return ["table" => $f[0], "database" => env('DB_DATABASE')];
        } elseif (count($f) == 2) {
            return ["database" => $f[0], "table" => $f[1]];
        } elseif (count($f) == 3) {
            return ["table" => $f[0], "schema" => $f[1], "table" => $f[2]];
        }

        return false;
    }
    public static function pk($table)
    {
        return self::findPrimaryKey($table);
    }
    public static function findPrimaryKey($table)
	{
		if(!$table)
		{
			return 'id';
		}
		
		$pk = DB::getDoctrineSchemaManager()->listTableDetails($table)->getPrimaryKey();
		if(!$pk) {
            return null;
		}
		return $pk->getColumns()[0];	
	}
    public static function first($table, $id)
    {
        $table = self::parseSqlTable($table)['table'];
        if (is_array($id)) {
            $first = DB::table($table);
            foreach ($id as $k => $v) {
                $first->where($k, $v);
            }

            return $first->first();
        } else {
            $pk = self::pk($table);

            return DB::table($table)->where($pk, $id)->first();
        }
    }
}

