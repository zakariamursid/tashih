<?php 
	namespace App\Helpers;
	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Cache;
	use Image;
	use Route;
	use Schema;
	use Storage;
	use Validator;

	/**
	* ALL FUNCTIONS FOR HELPERS
	*/
	class Kemenag{

		public static function adminName($id){
			$user = DB::table('cms_users')
				->where('id',$id)
				->first();

			return $user->name;
		}

		public static function emptyImage(){
			return asset('image/no-image.png');
		}

		public static function slug($table,$str,$find=0){
			$slug = ($find === 0 ? $str : $str.' '.$find);
			$slug = preg_replace('/\s+/', '-', $slug);
			$slug = strtolower($slug);

			$check = DB::table($table)
				->where('slug',$slug)
				->first();

			if (!empty($check)) {
				$find = $find+1;

				$slug = self::slug($table,$str,$find);
			}

			return $slug;
		}

		public static function dateIndonesia($date){
			$list_bulan = ['','Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

			$tgl   = date('d',strtotime($date));
			$bulan = $list_bulan[number_format(date('m',strtotime($date)))];
			$tahun = date('Y',strtotime($date));
			
			$time  = date('H:i',strtotime($date));

			return $tgl.' '.$bulan.' '.$tahun.', '.$time;
		}

		public static function checkPhone($phone){
			$length = strlen($phone);
			$first  = substr($phone,0,1);

			if ($first == 0) {
				$seconds = substr($phone,1,$length);

				$result = '62'.$seconds;
			}else{
				$result = $phone;
			}

			return $result;
		}

		public static function message(){
			return Session::get('message');
		}

		public static function messageType(){
			return Session::get('message_type');
		}

		public static function getMenu(){
			return Session::get('navigation');
		}

		public static function menu($value=null){
			Session::put('navigation',$value);
		}

		public static function getMenuSub(){
			return Session::get('navigationSub');
		}

		public static function menuSub($value=null){
			Session::put('navigationSub',$value);
		}

		public static function generatePassword($length = 8) {
			$result     = "";
			$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
			$max        = count($characters) - 1;

			for ($i = 0; $i < $length; $i++) {
				$rand   = mt_rand(0, $max);
				$result .= $characters[$rand];
			}

			$result = strtolower($result);

			return $result;
		}

		public static function now(){
			return date('Y-m-d H:i:s');
		}

		public static function uploadFileMultiple($index, $name, $path = null, $encrypt = false, $resize_width = null, $resize_height = null, $id = null)
    {
        if (Request::file($name)[$index]) {
            $path = ($path == null ? '0' : $path);

						$file      = Request::file($name)[$index];
						$ext       = $file->getClientOriginalExtension();
						$filename  = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
						$filesize  = $file->getClientSize() / 1024;
						$file_path = 'uploads/'.$path.'/'.date('Y-m');

            //Create Directory Monthly
            Storage::makeDirectory($file_path);

            if ($encrypt == true) {
                $filename = md5(str_random(5)).'.'.$ext;
            } else {
                $filename = str_slug($filename, '_').'.'.$ext;
            }

            if (Storage::putFileAs($file_path, $file, $filename)) {
                return $file_path.'/'.$filename;
            } else {
                return null;
            }
        } else {
            return 'false';
        }
    }

		public static function uploadFile($name, $path = null, $encrypt = false, $resize_width = null, $resize_height = null, $id = null)
    {
        if (Request::hasFile($name)) {
            $path = ($path == null ? '0' : $path);

						$file      = Request::file($name);
						$ext       = $file->getClientOriginalExtension();
						$filename  = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
						$filesize  = $file->getClientSize() / 1024;
						$file_path = 'uploads/'.$path.'/'.date('Y-m');

            //Create Directory Monthly
            Storage::makeDirectory($file_path);

            if ($encrypt == true) {
                $filename = md5(str_random(5)).'.'.$ext;
            } else {
                $filename = str_slug($filename, '_').'.'.$ext;
            }

            if (Storage::putFileAs($file_path, $file, $filename)) {
                return $file_path.'/'.$filename;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

		public static function generateIdPenerbit(){
			$result = '';
			$result .= date('Y');

			$data = DB::table('cms_users')
				->where('id_penerbit','LIKE','%'.$result.'%')
				->orderby('id_penerbit','DESC')
				->first();
			if (empty($data)) {
				$code = '001';
			}else{
				$code = (int)substr($data->id_penerbit, 4);
				$code ++;
				$code = sprintf("%03s",$code);
			}

			$result .= $code;

			return $result;
		}

		public static function penerbitButton($status){
			switch ($status) {
				case 'Belum ditindaklanjuti':
					$btn = 'btn-info';
					break;

				case 'Dibekukan':
					$btn = 'btn-danger';
					break;

				case 'Aktif':
					$btn = 'btn-success';
					break;

				case 'Dibekukan':
					$btn = 'btn-warning';
					break;
				
				default:
					$btn = '';
					break;
			}

			return $btn;
		}

		public static function extension($url){
			$data   = explode('.', $url);
			$count  = count($data);
			$result = $data[$count-1];

			return $result;
		}

		public static function emptyValue($type = null){
			switch ($type) {
				case 'Image':
					$result = 'http://via.placeholder.com/200x200?text=No+Data';
					break;
				
				default:
					$result = '';
					break;
			}

			return $result;
		}

		public static function dialogConfirm($url = null){
			if ($url == null || $url == '' || $url == 'javascript:void(0)') {
				$result = '';
			}else{
				$result = "swal({   
					title: 'Are you sure ?',   
					text: 'Click Yes to run action!',   
					type: 'warning',   
					showCancelButton: true,   
					confirmButtonColor: '#ff0000',   
					confirmButtonText: 'Yes!',  
					cancelButtonText: 'No',  
					closeOnConfirm: false }, 
					function(){  location.href='$url' })";
			}

			return $result;
		}

		public static function test(){
			return 'wkwkwkw';
		}
		
		public static function file($path = null)
        {
            $check_public = base_path('public/' . $path);
            $check_storage = storage_path('app/' . $path);
    
            if ($path == null) {
                return 'pearl-ui/images/favicon.png';
            } elseif (file_exists($check_public)) {
                return url($path);
            } elseif (file_exists($check_storage)) {
                return url($path);
            } else {
                return 'pearl-ui/images/favicon.png';
            }
		}
		
		public static function fileExists($path = null)
        {
            $check_public = base_path('public/' . $path);
            $check_storage = storage_path('app/' . $path);
    
            if ($path == null) {
                return null;
            } elseif (file_exists($check_public)) {
                return url($path);
            } elseif (file_exists($check_storage)) {
                return url($path);
            } else {
                return null;
            }
        }
        
        public static function Validator($data = [])
        {
            $validator = Validator::make(Request::all(), $data);
    
            if ($validator->fails()) {
                $result = array();
                $message = $validator->errors();
                $result['api_status'] = 0;
                $result['api_title'] = "Opps, something went wrong";
                $result['api_message'] = $message->all(':message')[0];
                $res = response()->json($result, 400);
                $res->send();
                exit;
            }
		}
		
		public static function dateTime()
		{
			return date('Y-m-d H:i:s');
		}

		public static function gSession($name) {
		    return Session::get($name);
        }

        public static function indexReportSurvey($avg) {
            if ($avg) {
                $get = ($avg/4)*100;
                return $get;
            }else{
                return 0;
            }
        }

        public static function mutuPelayananReportSurvey($index) {
            if ($index > 3.5323) {
                $mutu = "A";
            }elseif ($index > 3.0643) {
                $mutu = "B";
            }elseif ($index > 2.59) {
                $mutu = "C";
            }elseif ($index > 1) {
                $mutu = "D";
            }else{
                $mutu = "Belum disurvei";
            }
            return $mutu;
        }

        public static function kinerjaPelayananReportSurvey($mutu) {
            if ($mutu == 'A') {
                $kinerja = "Sangat baik";
            }elseif ($mutu == 'B') {
                $kinerja = "Baik";
            }elseif ($mutu == 'C') {
                $kinerja = "Kurang baik";
            }elseif ($mutu == 'D') {
                $kinerja = "Tidak baik";
            }else{
                $kinerja = "Belum disurvei";
            }
            return $kinerja;
		}
		
		public static function countEmailInbox()
		{
			$level = CRUDBooster::myPrivilegeName();
			$users_id = CRUDBooster::myId();

			if (in_array($level, ['Penerbit'])) {
				return DB::table('email_notif')
					->where('id_cms_users',$users_id)
					->where('is_read', '<>', 1)
					->count();
			}else {
				return 0;
			}
		}
	}
