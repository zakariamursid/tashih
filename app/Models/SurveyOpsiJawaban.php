<?php

namespace App\Models;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyOpsiJawaban extends Model
{
    protected $table = 'survey_opsi_jawaban';

    public static $tableName = 'survey_opsi_jawaban';

    private $id;
    private $id_survey;
    private $nama;
    private $nilai;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setId_survey($id_survey) { 
        $this->id_survey = $id_survey; 
    }
    function getId_survey() { 
        return $this->id_survey; 
    }
    function setNama($nama) { 
        $this->nama = $nama; 
    }
    function getNama() { 
        return $this->nama; 
    }
    function setNilai($nilai) { 
        $this->nilai = $nilai; 
    }
    function getNilai() { 
        return $this->nilai; 
    }

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getId_survey()) {
            $data['id_survey'] = $this->getId_survey();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getNilai()) {
            $data['nilai'] = $this->getNilai();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getId_survey()) {
            $data['id_survey'] = $this->getId_survey();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getNilai()) {
            $data['nilai'] = $this->getNilai();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
