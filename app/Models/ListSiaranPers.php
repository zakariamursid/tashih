<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kemenag;

class ListSiaranPers extends Model
{
    protected $table = 'list_siaran_pers';

    public static $tableName = 'list_siaran_pers';

    private $id;
    private $title;
    private $slug;
    private $image;
    private $url_videos;
    private $content;
    private $id_cms_user;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setTitle($title) { 
        $this->title = $title; 
    }
    function getTitle() { 
        return $this->title; 
    }
    function setSlug($slug) { 
        $this->slug = $slug; 
    }
    function getSlug() { 
        return $this->slug; 
    }
    function setImage($image) { 
        $this->image = $image; 
    }
    function getImage() { 
        return $this->image; 
    }
    function setUrl_videos($url_videos) { 
        $this->url_videos = $url_videos; 
    }
    function getUrl_videos() { 
        return $this->url_videos; 
    }
    function setContent($content) { 
        $this->content = $content; 
    }
    function getContent() { 
        return $this->content; 
    }
    function setId_cms_user($id_cms_user) { 
        $this->id_cms_user = $id_cms_user; 
    }
    function getId_cms_user() { 
        return $this->id_cms_user; 
    }

    
    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getImage()) {
            $data['date_send'] = $this->getImage();
        }
        if ($this->getUrl_videos()) {
            $data['url_videos'] = $this->getUrl_videos();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getId_cms_user()) {
            $data['id_cms_user'] = $this->getId_cms_user();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getImage()) {
            $data['date_send'] = $this->getImage();
        }
        if ($this->getUrl_videos()) {
            $data['url_videos'] = $this->getUrl_videos();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getId_cms_user()) {
            $data['id_cms_user'] = $this->getId_cms_user();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
