<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kemenag;

class DownloadMateri extends Model
{
    protected $table = 'download_materi';

    public static $tableName = 'download_materi';

    private $id;
    private $judul;
    private $id_kategori_download;
    private $deskripsi;
    private $file;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setJudul($judul) { 
        $this->judul = $judul; 
    }
    function getJudul() { 
        return $this->judul; 
    }
    function setId_kategori_download($id_kategori_download) { 
        $this->id_kategori_download = $id_kategori_download; 
    }
    function getId_kategori_download() { 
        return $this->id_kategori_download; 
    }
    function setDeskripsi($deskripsi) { 
        $this->deskripsi = $deskripsi; 
    }
    function getDeskripsi() { 
        return $this->deskripsi; 
    }
    function setFile($file) { 
        $this->file = $file; 
    }
    function getFile() { 
        return $this->file; 
    }
    
    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getJudul()) {
            $data['title'] = $this->getJudul();
        }
        if ($this->getId_kategori_download()) {
            $data['slug'] = $this->getId_kategori_download();
        }
        if ($this->getDeskripsi()) {
            $data['date_send'] = $this->getDeskripsi();
        }
        if ($this->getFile()) {
            $data['url_videos'] = $this->getFile();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getJudul()) {
            $data['title'] = $this->getJudul();
        }
        if ($this->getId_kategori_download()) {
            $data['slug'] = $this->getId_kategori_download();
        }
        if ($this->getDeskripsi()) {
            $data['date_send'] = $this->getDeskripsi();
        }
        if ($this->getFile()) {
            $data['url_videos'] = $this->getFile();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
