<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyPertanyaan extends Model
{
    protected $table = 'survey_pertanyaan';

    public static $tableName = 'survey_pertanyaan';

    private $id;
    private $type;
    private $nama;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setType($type) { $this->type = $type; }
    function getType() { return $this->type; }
    function setNama($nama) { $this->nama = $nama; }
    function getNama() { return $this->nama; }        
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
