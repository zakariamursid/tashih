<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyUnsur extends Model
{
    protected $table = 'survey_unsur';
    protected $appends = ['sum_nilai','average_nilai','count_nilai','count_users'];

    public static $tableName = 'survey_unsur';

    private $id;
    private $nama;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setNama($nama) { $this->nama = $nama; }
    function getNama() { return $this->nama; }    
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }


    /**
     * make eager load table
     */
    public function detailSurveyAnswer()
    {
        return $this->hasMany(SurveyAnswer::class);
    }

    public function getSumNilaiAttribute()
    {
        return (float) $this->detailSurveyAnswer()
                ->whereNull('survey_answer.deleted_at')
                ->sum('survey_answer.point');
    }

    public function getAverageNilaiAttribute()
    {
        return (float) $this->detailSurveyAnswer()
                ->whereNull('survey_answer.deleted_at')
                ->avg('survey_answer.point');
    }

    public function getCountNilaiAttribute()
    {
        return (float) $this->detailSurveyAnswer()
                ->whereNull('survey_answer.deleted_at')
                ->count('survey_answer.point');
    }

    public function getCountUsersAttribute()
    {
        return (float) $this->detailSurveyAnswer()
                ->whereNull('survey_answer.deleted_at')
                ->groupBy('survey_answer.users_id')
                ->count('survey_answer.users_id');
    }
}
