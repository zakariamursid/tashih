<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyConnect extends Model
{
    protected $table = 'survey_connect';

    public static $tableName = 'survey_connect';

    private $id;
    private $survey_nama_id;
    private $survey_unsur_id;
    private $survey_pertanyaan_id;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setSurvey_nama_id($survey_nama_id) { $this->survey_nama_id = $survey_nama_id; }
    function getSurvey_nama_id() { return $this->survey_nama_id; }
    function setSurvey_unsur_id($survey_unsur_id) { $this->survey_unsur_id = $survey_unsur_id; }
    function getSurvey_unsur_id() { return $this->survey_unsur_id; }
    function setSurvey_pertanyaan_id($survey_pertanyaan_id) { $this->survey_pertanyaan_id = $survey_pertanyaan_id; }
    function getSurvey_pertanyaan_id() { return $this->survey_pertanyaan_id; }    
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getSurvey_nama_id()) {
            $data['survey_nama_id'] = $this->getSurvey_nama_id();
        }
        if ($this->getSurvey_unsur_id()) {
            $data['survey_unsur_id'] = $this->getSurvey_unsur_id();
        }
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getSurvey_nama_id()) {
            $data['survey_nama_id'] = $this->getSurvey_nama_id();
        }
        if ($this->getSurvey_unsur_id()) {
            $data['survey_unsur_id'] = $this->getSurvey_unsur_id();
        }
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
