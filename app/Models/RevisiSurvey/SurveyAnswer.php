<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyAnswer extends Model
{
    protected $table = 'survey_answer';

    public static $tableName = 'survey_answer';

    private $id;
    private $type_answer;
    private $users_id;
    private $survey_nama_id;
    private $survey_unsur_id;
    private $survey_pertanyaan_id;
    private $answer;
    private $point;
    private $option;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setType_answer($type_answer) { $this->type_answer = $type_answer; }
    function getType_answer() { return $this->type_answer; }
    function setUsers_id($users_id) { $this->users_id = $users_id; }
    function getUsers_id() { return $this->users_id; }
    function setSurvey_nama_id($survey_nama_id) { $this->survey_nama_id = $survey_nama_id; }
    function getSurvey_nama_id() { return $this->survey_nama_id; }
    function setSurvey_unsur_id($survey_unsur_id) { $this->survey_unsur_id = $survey_unsur_id; }
    function getSurvey_unsur_id() { return $this->survey_unsur_id; }
    function setSurvey_pertanyaan_id($survey_pertanyaan_id) { $this->survey_pertanyaan_id = $survey_pertanyaan_id; }
    function getSurvey_pertanyaan_id() { return $this->survey_pertanyaan_id; }
    function setAnswer($answer) { $this->answer = $answer; }
    function getAnswer() { return $this->answer; }
    function setPoint($point) { $this->point = $point; }
    function getPoint() { return $this->point; } 
    function setOption($option) { $this->option = $option; }
    function getOption() { return $this->option; }      
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getType_answer()) {
            $data['type_answer'] = $this->getType_answer();
        }
        if ($this->getUsers_id()) {
            $data['users_id'] = $this->getUsers_id();
        }
        if ($this->getSurvey_nama_id()) {
            $data['survey_nama_id'] = $this->getSurvey_nama_id();
        }
        if ($this->getSurvey_unsur_id()) {
            $data['survey_unsur_id'] = $this->getSurvey_unsur_id();
        }
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        if ($this->getAnswer()) {
            $data['answer'] = $this->getAnswer();
        }
        if ($this->getPoint()) {
            $data['point'] = $this->getPoint();
        }
        if ($this->getOption()) {
            $data['option'] = $this->getOption();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getType_answer()) {
            $data['type_answer'] = $this->getType_answer();
        }
        if ($this->getUsers_id()) {
            $data['users_id'] = $this->getUsers_id();
        }
        if ($this->getSurvey_nama_id()) {
            $data['survey_nama_id'] = $this->getSurvey_nama_id();
        }
        if ($this->getSurvey_unsur_id()) {
            $data['survey_unsur_id'] = $this->getSurvey_unsur_id();
        }
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        if ($this->getAnswer()) {
            $data['answer'] = $this->getAnswer();
        }
        if ($this->getPoint()) {
            $data['point'] = $this->getPoint();
        }
        if ($this->getOption()) {
            $data['option'] = $this->getOption();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
