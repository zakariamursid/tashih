<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyOpsiPertanyaan extends Model
{
    protected $table = 'survey_opsi_pertanyaan';

    public static $tableName = 'survey_opsi_pertanyaan';

    private $id;
    private $survey_pertanyaan_id;
    private $type;
    private $nama;
    private $point;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setSurvey_pertanyaan_id($survey_pertanyaan_id) { $this->survey_pertanyaan_id = $survey_pertanyaan_id; }
    function getSurvey_pertanyaan_id() { return $this->survey_pertanyaan_id; }
    function setType($type) { $this->type = $type; }
    function getType() { return $this->type; }
    function setNama($nama) { $this->nama = $nama; }
    function getNama() { return $this->nama; }
    function setPoint($point) { $this->point = $point; }
    function getPoint() { return $this->point; }
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getPoint()) {
            $data['point'] = $this->getPoint();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getSurvey_pertanyaan_id()) {
            $data['survey_pertanyaan_id'] = $this->getSurvey_pertanyaan_id();
        }
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getPoint()) {
            $data['point'] = $this->getPoint();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
