<?php

namespace App\Models\RevisiSurvey;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyNama extends Model
{
    protected $table = 'survey_nama';

    public static $tableName = 'survey_nama';

    private $id;
    private $nama;
    private $date_from;
    private $date_to;
    private $status;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setNama($nama) { $this->nama = $nama; }
    function getNama() { return $this->nama; }
    function setDate_from($date_from) { $this->date_from = $date_from; }
    function getDate_from() { return $this->date_from; }
    function setDate_to($date_to) { $this->date_to = $date_to; }
    function getDate_to() { return $this->date_to; }
    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getDate_from()) {
            $data['date_from'] = $this->getDate_from();
        }
        if ($this->getDate_to()) {
            $data['date_to'] = $this->getDate_to();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getNama()) {
            $data['nama'] = $this->getNama();
        }
        if ($this->getDate_from()) {
            $data['date_from'] = $this->getDate_from();
        }
        if ($this->getDate_to()) {
            $data['date_to'] = $this->getDate_to();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
