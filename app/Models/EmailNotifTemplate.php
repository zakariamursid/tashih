<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Kemenag;
use Illuminate\Support\Facades\DB;

class EmailNotifTemplate extends Model
{
    protected $table = 'email_notif_template';

    public static $tableName = 'email_notif_template';

    private $id;
    private $name;
    private $slug;
    private $subject;
    private $content;
    private $file;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setName($name) { 
        $this->name = $name; 
    }
    function getName() { 
        return $this->name; 
    }
    function setSlug($slug) { 
        $this->slug = $slug; 
    }
    function getSlug() { 
        return $this->slug; 
    }
    function setSubject($subject) { 
        $this->subject = $subject; 
    }
    function getSubject() { 
        return $this->subject; 
    }
    function setContent($content) { 
        $this->content = $content; 
    }
    function getContent() { 
        return $this->content; 
    }
    function setFile($file) { 
        $this->file = $file; 
    }
    function getFile() { 
        return $this->file; 
    }

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getName()) {
            $data['name'] = $this->getName();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getSubject()) {
            $data['subject'] = $this->getSubject();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getFile()) {
            $data['file'] = $this->getFile();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getName()) {
            $data['name'] = $this->getName();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getSubject()) {
            $data['subject'] = $this->getSubject();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getFile()) {
            $data['file'] = $this->getFile();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
