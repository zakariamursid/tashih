<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriDownload extends Model
{
    protected $table = 'kategori_download';

    public static $tableName = 'kategori_download';

    private $id;
    private $nama_kategori;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setNama_kategori($nama_kategori) { 
        $this->nama_kategori = $nama_kategori; 
    }
    function getNama_kategori() { 
        return $this->nama_kategori; 
    }

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getNama_kategori()) {
            $data['nama_kategori'] = $this->getNama_kategori();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getNama_kategori()) {
            $data['nama_kategori'] = $this->getNama_kategori();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
