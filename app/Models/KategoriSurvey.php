<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Kemenag;
use Illuminate\Support\Facades\DB;

class KategoriSurvey extends Model
{
    protected $table = 'kategori_survey';

    public static $tableName = 'kategori_survey';

    private $id;
    private $jenis_kategori;
    private $from;
    private $to;
    private $status;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setJenis_kategori($jenis_kategori) { $this->jenis_kategori = $jenis_kategori; }
    function getJenis_kategori() { return $this->jenis_kategori; }
    function setFrom($from) { $this->from = $from; }
    function getFrom() { return $this->from; }
    function setTo($to) { $this->to = $to; }
    function getTo() { return $this->to; }
    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }    
    

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getJenis_kategori()) {
            $data['jenis_kategori'] = $this->getJenis_kategori();
        }
        if ($this->getFrom()) {
            $data['from'] = $this->getFrom();
        }
        if ($this->getTo()) {
            $data['to'] = $this->getTo();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getJenis_kategori()) {
            $data['jenis_kategori'] = $this->getJenis_kategori();
        }
        if ($this->getFrom()) {
            $data['from'] = $this->getFrom();
        }
        if ($this->getTo()) {
            $data['to'] = $this->getTo();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
