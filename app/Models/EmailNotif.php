<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Kemenag;
use Illuminate\Support\Facades\DB;

class EmailNotif extends Model
{
    protected $table = 'email_notif';

    public static $tableName = 'email_notif';

    private $id;
    private $id_email_notif_template;
    private $id_cms_users;
    private $date_send;
    private $is_send;
    private $is_read;
    
    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setId_email_notif_template($id_email_notif_template) { 
        $this->id_email_notif_template = $id_email_notif_template; 
    }
    function getId_email_notif_template() { 
        return $this->id_email_notif_template; 
    }
    function setId_cms_users($id_cms_users) { 
        $this->id_cms_users = $id_cms_users; 
    }
    function getId_cms_users() { 
        return $this->id_cms_users; 
    }
    function setDate_send($date_send) { 
        $this->date_send = $date_send; 
    }
    function getDate_send() { 
        return $this->date_send; 
    }
    function setIs_send($is_send) { 
        $this->is_send = $is_send; 
    }
    function getIs_send() { 
        return $this->is_send; 
    }
    function setIs_read($is_read) { 
        $this->is_read = $is_read; 
    }
    function getIs_read() { 
        return $this->is_read; 
    }
    
    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getId_email_notif_template()) {
            $data['id_email_notif_template'] = $this->getId_email_notif_template();
        }
        if ($this->getId_cms_users()) {
            $data['id_cms_users'] = $this->getId_cms_users();
        }
        if ($this->getDate_send()) {
            $data['date_send'] = $this->getDate_send();
        }
        if ($this->getIs_send()) {
            $data['is_send'] = $this->getIs_send();
        }
        if ($this->getIs_read()) {
            $data['is_read'] = $this->getIs_read();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getId_email_notif_template()) {
            $data['id_email_notif_template'] = $this->getId_email_notif_template();
        }
        if ($this->getId_cms_users()) {
            $data['id_cms_users'] = $this->getId_cms_users();
        }
        if ($this->getDate_send()) {
            $data['date_send'] = $this->getDate_send();
        }
        if ($this->getIs_send()) {
            $data['is_send'] = $this->getIs_send();
        }
        if ($this->getIs_read()) {
            $data['is_read'] = $this->getIs_read();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
