<?php

namespace App\Models;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Survey extends Model
{
    protected $table = 'survey';

    public static $tableName = 'survey';

    private $id;
    private $id_kategori_survey;
    private $type;
    private $pertanyaan_survey;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setId_kategori_survey($id_kategori_survey) { 
        $this->id_kategori_survey = $id_kategori_survey; 
    }
    function getId_kategori_survey() { 
        return $this->id_kategori_survey; 
    }
    function setType($type) { 
        $this->type = $type; 
    }
    function getType() { 
        return $this->type; 
    }
    function setPertanyaan_survey($pertanyaan_survey) { 
        $this->pertanyaan_survey = $pertanyaan_survey; 
    }
    function getPertanyaan_survey() { 
        return $this->pertanyaan_survey; 
    }

    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getId_kategori_survey()) {
            $data['id_kategori_survey'] = $this->getId_kategori_survey();
        }
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getPertanyaan_survey()) {
            $data['pertanyaan_survey'] = $this->getPertanyaan_survey();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getId_kategori_survey()) {
            $data['id_kategori_survey'] = $this->getId_kategori_survey();
        }
        if ($this->getType()) {
            $data['type'] = $this->getType();
        }
        if ($this->getPertanyaan_survey()) {
            $data['pertanyaan_survey'] = $this->getPertanyaan_survey();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
