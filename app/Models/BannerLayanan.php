<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Kemenag;
use Illuminate\Support\Facades\DB;

class BannerLayanan extends Model
{
    protected $table = 'banner_layanan';

    public static $tableName = 'banner_layanan';

    private $id;
    private $title;
    private $slug;
    private $url;
    private $image;
    private $content;
    private $status;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }
    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }
    function setSlug($slug) { $this->slug = $slug; }
    function getSlug() { return $this->slug; }
    function setUrl($url) { $this->url = $url; }
    function getUrl() { return $this->url; }
    function setImage($image) { $this->image = $image; }
    function getImage() { return $this->image; }
    function setContent($content) { $this->content = $content; }
    function getContent() { return $this->content; }
    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }
    

        /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getUrl()) {
            $data['url'] = $this->getUrl();
        }
        if ($this->getImage()) {
            $data['image'] = $this->getImage();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
        if ($this->getSlug()) {
            $data['slug'] = $this->getSlug();
        }
        if ($this->getUrl()) {
            $data['url'] = $this->getUrl();
        }
        if ($this->getImage()) {
            $data['image'] = $this->getImage();
        }
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
