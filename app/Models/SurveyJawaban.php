<?php

namespace App\Models;
use Kemenag;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SurveyJawaban extends Model
{
    protected $table = 'jawaban_survey';

    public static $tableName = 'jawaban_survey';

    private $id;
    private $id_cms_users;
    private $id_survey;
    private $id_survey_opsi_jawaban;
    private $jawaban;

    function setId($id) { 
        $this->id = $id; 
    }
    function getId() { 
        return $this->id; 
    }
    function setId_cms_users($id_cms_users) { 
        $this->id_cms_users = $id_cms_users; 
    }
    function getId_cms_users() { 
        return $this->id_cms_users; 
    }
    function setId_survey($id_survey) { 
        $this->id_survey = $id_survey; 
    }
    function getId_survey() { 
        return $this->id_survey; 
    }
    function setId_survey_opsi_jawaban($id_survey_opsi_jawaban) { 
        $this->id_survey_opsi_jawaban = $id_survey_opsi_jawaban; 
    }
    function getId_survey_opsi_jawaban() { 
        return $this->id_survey_opsi_jawaban; 
    }
    function setJawaban($jawaban) { 
        $this->jawaban = $jawaban; 
    }
    function getJawaban() { 
        return $this->jawaban; 
    }

    
    /**
     * return save to database
     * 
     * @return void
     */ 
    public function add()
    {
        $data['created_at'] = Kemenag::dateTime();
        if ($this->getId_cms_users()) {
            $data['id_cms_users'] = $this->getId_cms_users();
        }
        if ($this->getId_survey()) {
            $data['id_survey'] = $this->getId_survey();
        }
        if ($this->getId_survey_opsi_jawaban()) {
            $data['id_survey_opsi_jawaban'] = $this->getId_survey_opsi_jawaban();
        }
        if ($this->getJawaban()) {
            $data['jawaban'] = $this->getJawaban();
        }
        
        $insert = DB::table(static::$tableName)
            ->insertGetId($data);

        return $insert;
    }

    /**
     * return update to database
     * 
     * @return void
     */ 
    public function edit()
    {
        $id = $this->getId();

        $data['updated_at'] = Kemenag::dateTime();
        if ($this->getId_cms_users()) {
            $data['id_cms_users'] = $this->getId_cms_users();
        }
        if ($this->getId_survey()) {
            $data['id_survey'] = $this->getId_survey();
        }
        if ($this->getId_survey_opsi_jawaban()) {
            $data['id_survey_opsi_jawaban'] = $this->getId_survey_opsi_jawaban();
        }
        if ($this->getJawaban()) {
            $data['jawaban'] = $this->getJawaban();
        }
        
        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->update($data);

        return $update;
    }

    /**
     * return remove from database
     * 
     * @return void
     */ 
    public function remove()
    {
        $id = $this->getId();

        $update = DB::table(static::$tableName)
            ->where(function ($q) use ($id)
            {
                if ($id) {
                    $q->where('id', $id);
                }
            })
            ->delete();

        return $update;
    }
}
