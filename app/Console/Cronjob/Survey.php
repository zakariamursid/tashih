<?php

namespace App\Console\Cronjob;

use Illuminate\Support\Facades\DB;

class Survey
{
    public static function checkWaitingSurvey()
    {
        $up = false;

        $data = DB::table("kategori_survey")
            ->where('status','pending')
            ->whereNull('deleted_at')
            ->orderby('id','asc')
            ->get();

        foreach ($data as $x => $xrow) {
            $from = date('Y-m-d H:i:s', strtotime($xrow->from));
            $to = date('Y-m-d H:i:s', strtotime($xrow->to));

            if (new \Datetime($to) < new \Datetime(date('Y-m-d H:i:s'))) {
                $update = DB::table('kategori_survey')
                    ->where('id', $xrow->id)
                    ->update([
                        'status' => 'expired'
                    ]);

                $up = true;
            }else{
                if (new \Datetime($from) < new \Datetime(date('Y-m-d H:i:s'))) {
                    $update = DB::table('kategori_survey')
                        ->where('id', $xrow->id)
                        ->update([
                            'status' => 'active'
                        ]);
    
                    $up = true;
                }
            }
        }
        
        return $up;
    }

    public static function checkWaitingSurveyRevisi()
    {
        $up = false;

        $data = DB::table("survey_nama")
            ->where('status','pending')
            ->whereNull('deleted_at')
            ->orderby('id','asc')
            ->get();

        foreach ($data as $x => $xrow) {
            $from = date('Y-m-d H:i:s', strtotime($xrow->from));
            $to = date('Y-m-d H:i:s', strtotime($xrow->to));

            if (new \Datetime($to) < new \Datetime(date('Y-m-d H:i:s'))) {
                $update = DB::table('survey_nama')
                    ->where('id', $xrow->id)
                    ->update([
                        'status' => 'expired'
                    ]);

                $up = true;
            }else{
                if (new \Datetime($from) < new \Datetime(date('Y-m-d H:i:s'))) {
                    $update = DB::table('survey_nama')
                        ->where('id', $xrow->id)
                        ->update([
                            'status' => 'active'
                        ]);

                    $up = true;
                }
            }
        }

        return $up;
    }
}

