<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyConnect;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class SurveyConnectRepositories extends SurveyConnect
{
    public static function findByUnsurId($unsur_id)
    {
        return DB::table('survey_connect')
            ->where('survey_connect.survey_unsur_id', $unsur_id)
            ->whereNull('survey_connect.deleted_at')
            ->first();
    }

    public static function findByPertanyaanId($pertanyaan_id)
    {
        return DB::table('survey_connect')
            ->where('survey_connect.survey_pertanyaan_id', $pertanyaan_id)
            ->whereNull('survey_connect.deleted_at')
            ->first();
    }

    /**
     * list survey
     */
    public static function getSurveyActive()
    {
        return DB::table('survey_connect')
            ->select(
                'survey_connect.survey_nama_id',
                'survey_connect.survey_unsur_id', 
                'survey_connect.survey_pertanyaan_id',
                'survey_pertanyaan.type as type_pertanyaan',
                'survey_pertanyaan.nama as nama_pertanyaan'
            )
            ->join('survey_nama', function ($join)
            {
                $join->on('survey_nama.id', '=', 'survey_connect.survey_nama_id');
                $join->whereNull('survey_nama.deleted_at');
                $join->where('survey_nama.status', '=', 'active');
            })
            ->join('survey_unsur', function ($join)
            {
                $join->on('survey_unsur.id', '=', 'survey_connect.survey_unsur_id');
                $join->whereNull('survey_unsur.deleted_at');
            })
            ->join('survey_pertanyaan', function ($join)
            {
                $join->on('survey_pertanyaan.id', '=', 'survey_connect.survey_pertanyaan_id');
                $join->whereNull('survey_pertanyaan.deleted_at');
            })
            ->where(function ($q)
            {
                $q->orWhereNotNull('survey_connect.survey_nama_id');
                $q->orWhereNotNull('survey_connect.survey_unsur_id');
                $q->orWhereNotNull('survey_connect.survey_pertanyaan_id');
            })
            ->whereNull('survey_connect.deleted_at')
            ->orderby('survey_pertanyaan.id','asc')
            ->get();
    }

    /**
     * survey id active
     */
    public static function findSurveyIDActive()
    {
        return DB::table('survey_connect')
            ->select(
                'survey_connect.survey_nama_id',
                'survey_connect.survey_unsur_id', 
                'survey_connect.survey_pertanyaan_id',
                'survey_pertanyaan.type as type_pertanyaan',
                'survey_pertanyaan.nama as nama_pertanyaan'
            )
            ->leftjoin('survey_nama', function ($join)
            {
                $join->on('survey_nama.id', '=', 'survey_connect.survey_nama_id');
                $join->whereNull('survey_nama.deleted_at');
                $join->where('survey_nama.status', '=', 'active');
            })
            ->leftjoin('survey_unsur', function ($join)
            {
                $join->on('survey_unsur.id', '=', 'survey_connect.survey_unsur_id');
                $join->whereNull('survey_unsur.deleted_at');
            })
            ->leftjoin('survey_pertanyaan', function ($join)
            {
                $join->on('survey_pertanyaan.id', '=', 'survey_connect.survey_pertanyaan_id');
                $join->whereNull('survey_pertanyaan.deleted_at');
            })
            ->where(function ($q)
            {
                $q->orWhereNotNull('survey_connect.survey_nama_id');
                $q->orWhereNotNull('survey_connect.survey_unsur_id');
                $q->orWhereNotNull('survey_connect.survey_pertanyaan_id');
            })
            ->whereNull('survey_connect.deleted_at')
            ->orderby('survey_pertanyaan.id','asc')
            ->groupby('survey_connect.survey_nama_id')
            ->first();
    }

    /**
     * find pertanyaan by unsur 
     */
    public static function getPertanyaanByUnsur($unsur_id)
    {
        return DB::table('survey_connect')
            ->select('survey_pertanyaan.id','survey_pertanyaan.type','survey_pertanyaan.nama as pertanyaan_survey')
            ->leftjoin('survey_pertanyaan', function ($join)
            {
                $join->on('survey_pertanyaan.id', '=', 'survey_connect.survey_pertanyaan_id');
                $join->whereNull('survey_pertanyaan.deleted_at');
            })
            ->where(function ($q)
            {
                $q->orWhereNotNull('survey_connect.survey_pertanyaan_id');
            })
            ->whereNull('survey_connect.deleted_at')
            ->get();
    }
}
