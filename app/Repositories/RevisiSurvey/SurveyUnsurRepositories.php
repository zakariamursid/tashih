<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyUnsur;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class SurveyUnsurRepositories extends SurveyUnsur
{
    public static function getListLaporan($nama_survey_id = null)
    {
        return DB::table('survey_unsur')
                ->select(
                    'survey_unsur.*'
                )
                ->leftjoin('survey_connect', function ($join) use ($nama_survey_id)
                {
                    $join->on('survey_connect.survey_unsur_id', '=', 'survey_unsur.id');
                    $join->whereNull('survey_connect.deleted_at');
                    if (!empty($nama_survey_id)) {
                        $join->where('survey_connect.survey_nama_id', $nama_survey_id);
                    }
                })
                ->where(function ($q) use ($nama_survey_id)
                {
                    if (!empty($nama_survey_id)) {
                        $q->orWhereNotNull('survey_connect.survey_unsur_id');                    
                    }
                })
                ->whereNull('survey_unsur.deleted_at')
                ->groupBy('survey_unsur.id')
                ->orderby('survey_unsur.id', 'asc')
                ->get();
    }

    public static function getAll()
    {
        return DB::table('survey_unsur')
            ->select('survey_unsur.id', 'survey_unsur.nama')
            ->whereNull('survey_unsur.deleted_at')
            ->get();
    }

    public static function sumNilai($nama_survey_id = null, $unsur_id = null)
    {
        return (float) DB::table('survey_answer')
            ->where(function ($q) use ($nama_survey_id, $unsur_id) {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
                if (!empty($unsur_id)) {
                    $q->where('survey_answer.survey_unsur_id', $unsur_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->sum('survey_answer.point');
    }

    public static function averageNilai($nama_survey_id = null, $unsur_id = null)
    {
        return DB::table('survey_answer')
            ->where(function ($q) use ($nama_survey_id, $unsur_id) {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
                if (!empty($unsur_id)) {
                    $q->where('survey_answer.survey_unsur_id', $unsur_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->avg('survey_answer.point');
    }

    public static function countNilai($nama_survey_id = null, $unsur_id = null)
    {
        return DB::table('survey_answer')
            ->where(function ($q) use ($nama_survey_id, $unsur_id) {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
                if (!empty($unsur_id)) {
                    $q->where('survey_answer.survey_unsur_id', $unsur_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->count('survey_answer.point');
    }

    public static function countUsers($nama_survey_id = null, $unsur_id = null)
    {
        return DB::table('survey_answer')
            ->where(function ($q) use ($nama_survey_id, $unsur_id) {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
                if (!empty($unsur_id)) {
                    $q->where('survey_answer.survey_unsur_id', $unsur_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->groupBy('survey_answer.users_id')
            ->count('survey_answer.users_id');
    }
}
