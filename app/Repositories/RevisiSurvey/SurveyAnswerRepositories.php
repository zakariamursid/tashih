<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyAnswer;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class SurveyAnswerRepositories extends SurveyAnswer
{
    /**
     * check answer users by type
     */
    public static function checkUserAnswerByType($users_id, $pertanyaan_id, $type)
    {
        return DB::table('survey_answer')
            ->where(function ($q) use ($users_id, $pertanyaan_id, $type)
            {
                if (!empty($type)) {
                    $q->where('survey_answer.type_answer', $type);
                }
                if (!empty($users_id)) {
                    $q->where('survey_answer.users_id', $users_id);
                }
                if (!empty($pertanyaan_id)) {
                    $q->where('survey_answer.survey_pertanyaan_id', $pertanyaan_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->first();
    }

    /**
     * check is already answer this survey
     */ 
    public static function checkIsAnswer($users_id, $nama_survey_id, $type)
    {
        return DB::table('survey_answer')
        ->where(function ($q) use ($users_id, $nama_survey_id, $type)
        {
            if (!empty($type)) {
                $q->where('survey_answer.type_answer', $type);
            }
            if (!empty($users_id)) {
                $q->where('survey_answer.users_id', $users_id);
            }
            if (!empty($pertanyaan_id)) {
                $q->where('survey_answer.survey_nama_id', $nama_survey_id);
            }
        })
        ->whereNull('survey_answer.deleted_at')
        ->first();
    }

    /**
     * export laporan survey
     */
    public static function getListLaporanSurvey($nama_survey_id = null)
    {
        return DB::table('survey_answer')
            ->select(
                'cms_users.id as id_user',
                'cms_users.name as user_name',
                'survey_pertanyaan.id as id_question',
                'survey_unsur.id as id_unsur',
                'survey_pertanyaan.nama as question',
                'survey_unsur.nama as name_unsur',
                'survey_answer.option as opsi_text',
                'survey_answer.point as opsi_value'
            )
            ->leftjoin('cms_users', function ($join)
            {
                $join->on('cms_users.id', '=', 'survey_answer.users_id');
            })
            ->leftjoin('survey_nama', function ($join)
            {
                $join->on('survey_nama.id', '=', 'survey_answer.survey_nama_id');
                $join->whereNull('survey_nama.deleted_at');
            })
            ->leftjoin('survey_unsur', function ($join)
            {
                $join->on('survey_unsur.id', '=', 'survey_answer.survey_unsur_id');
                $join->whereNull('survey_unsur.deleted_at');
            })
            ->leftjoin('survey_pertanyaan', function ($join)
            {
                $join->on('survey_pertanyaan.id', '=', 'survey_answer.survey_pertanyaan_id');
                $join->whereNull('survey_pertanyaan.deleted_at');
            })
            ->where(function ($q)
            {
                $q->orWhereNotNull('cms_users.id');
                $q->orWhereNotNull('survey_nama.id');
                $q->orWhereNotNull('survey_unsur.id');
                $q->orWhereNotNull('survey_pertanyaan.id');
            })
            ->where(function ($q) use ($nama_survey_id)
            {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_nama.id', $nama_survey_id);
                }
            })
            ->get();
    }

    /**
     * list essay laporan 
     */
    public static function getEssayLaporan($nama_survey_id = null)
    {
        return DB::table('survey_answer')
            ->select('survey_pertanyaan.nama as pertanyaan', 'survey_answer.answer')
            ->join('survey_pertanyaan', function ($join)
            {
                $join->on('survey_pertanyaan.id', '=', 'survey_answer.survey_pertanyaan_id');
                $join->whereNull('survey_pertanyaan.deleted_at');
                $join->where('survey_pertanyaan.type', 'Essay');
            })
            ->where(function ($q) use ($nama_survey_id)
            {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
            })
            ->whereNull('survey_answer.deleted_at')
            ->orderby('survey_answer.id', 'desc')
            ->paginate(10);
    }

    /**
     * jumlah responden survey
     */
    public static function countUsersSurvey($nama_survey_id = null)
    {
        return DB::table('survey_answer')
            ->where(function ($q) use ($nama_survey_id)
            {
                if (!empty($nama_survey_id)) {
                    $q->where('survey_answer.survey_nama_id', $nama_survey_id);
                }
            })
            ->groupby('survey_answer.users_id')
            ->get();
    }
}