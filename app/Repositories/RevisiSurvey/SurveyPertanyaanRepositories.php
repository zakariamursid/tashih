<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyPertanyaan;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class SurveyPertanyaanRepositories extends SurveyPertanyaan
{
    public static function findData($id)
    {
        return DB::table('survey_pertanyaan')->select('type', 'nama')->where('id', $id)->first();
    }
}
