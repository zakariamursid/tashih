<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyNama;
use Illuminate\Support\Facades\DB;
use CRUDBooster;

class SurveyNamaRepositories extends SurveyNama
{
    public static function getAll()
    {
        return DB::table('survey_nama')
            ->select('id', 'nama')
            ->orderby('id', 'asc')
            ->whereNull('deleted_at')
            ->get();
    }
}
