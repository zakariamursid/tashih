<?php

namespace App\Repositories\RevisiSurvey;

use App\Models\RevisiSurvey\SurveyOpsiPertanyaan;
use Illuminate\Support\Facades\DB;
use CRUDBooster;
use Kemenag;

class SurveyOpsiPertanyaanRepositories extends SurveyOpsiPertanyaan
{
    public static function saveOpsiPertanyaan($pertanyaan_id, $table_id, $type, $name)
    {
        if (!empty($table_id)) {    
            foreach ($table_id as $x => $row) {
                $list_id = $row;
                $list_type = $type[$x];
                $list_name = $name[$x];

                if (empty($list_id)) {
                    $save = new SurveyOpsiPertanyaan();
                    $save->setSurvey_pertanyaan_id($pertanyaan_id);
                    $save->setType($list_type);
                    $save->setNama($list_name);
                    $save->setPoint(static::typePoint($list_type));
                    $save->add();
                } else {
                    $save = new SurveyOpsiPertanyaan();
                    $save->setId($list_id);
                    $save->setSurvey_pertanyaan_id($pertanyaan_id);
                    $save->setType($list_type);
                    $save->setNama($list_name);
                    $save->setPoint(static::typePoint($list_type));
                    $save->edit();

                    static::deleteSaveOpsiPertanyaan($pertanyaan_id, $table_id);
                }
            }
        }
    }

    public static function deleteSaveOpsiPertanyaan($pertanyaan_id, $id)
    {
        $data = DB::table('survey_opsi_pertanyaan')
            ->where('survey_pertanyaan_id', $pertanyaan_id)
            ->whereNull('deleted_at')
            ->get();
            
        foreach ($data as $x => $row) {
            if (!in_array($row->id, $id)) {
                $save['deleted_at'] = Kemenag::dateTime();
                DB::table('survey_opsi_pertanyaan')->where('id', $row->id)->update($save);
            }
        }
    }

    public static function typePoint($type)
    {
        switch ($type) {
            case 'A':
                $point = 4;
                break;

            case 'B':
                $point = 3;
                break;

            case 'C':
                $point = 2;
                break;
            
            case 'D':
                $point = 1;
                break;

            default:
                $point = 0;
                break;
        }

        return $point;
    }

    public static function getByPertanyanId($pertanyaan_id)
    {
        return DB::table('survey_opsi_pertanyaan')
            ->select(
                'id as pilihan_id',
                'type as opsi',
                'nama as nama'
            )
            ->where('survey_pertanyaan_id', $pertanyaan_id)
            ->whereNull('deleted_at')
            ->orderby('id', 'desc')
            ->get();
    }

    public static function deleteByPertanyaanId($pertanyaan_id)
    {
        return DB::table('survey_opsi_pertanyaan')
            ->where('survey_pertanyaan_id', $pertanyaan_id)
            ->whereNull('deleted_at')
            ->delete();
    }

    public static function getByPertanyan($pertanyaan_id)
    {
        return DB::table('survey_opsi_pertanyaan')
            ->select(
                'id as pilihan_id',
                'type as opsi',
                'nama as nama'
            )
            ->where('survey_pertanyaan_id', $pertanyaan_id)
            ->whereNull('deleted_at')
            ->orderby('type', 'asc')
            ->get();
    }
}
