<?php

namespace App\Repositories;

use App\Models\Survey;
use Illuminate\Support\Facades\DB;
use App\Repositories\SurveyOpsiJawabanRepositories;
use CRUDBooster;

class SurveyRepositories extends Survey
{
    public static function getListSurvey($id_users)
    {
        $listKategori = DB::table('jawaban_survey')
            ->select('survey.*')
            ->join('survey','survey.id','=','jawaban_survey.id_survey')
            ->where('jawaban_survey.id_cms_users',$id_users)
            ->whereNull('jawaban_survey.deleted_at')
            ->whereNull('survey.deleted_at')
            ->pluck('survey.id')
            ->toArray();
            
        $data = DB::table('survey')
            ->select(
                "survey.id",
                "survey.id_survey_unsur",
                "kategori_survey.id as id_kategori_survey",
                "survey_unsur.nama",
                "survey.type",
                "survey.pertanyaan_survey as question_survey"
            )
            ->leftjoin('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->leftjoin('kategori_survey', 'kategori_survey.id', '=', 'survey_unsur.id_kategori_survey')
            ->where('kategori_survey.status', 'active')
            ->whereNull('survey.deleted_at')
            ->whereNull('kategori_survey.deleted_at')
            ->orderby('survey.id', 'desc')
            ->get();

        $ret = [];
        foreach ($data as $x => $xrow) {
            $option = SurveyOpsiJawabanRepositories::getListOpsiBySurveyId($xrow->id);
            $xrow->option = $option;

            if (in_array($xrow->id, $listKategori)) {
                continue;
            }else{
                $ret[] = $xrow;
            }
        }

        return $ret;
    }

    public static function checkSurveyActive()
    {
        $data = DB::table('survey')
            ->select(
                "survey.id",
                "survey.id_survey_unsur",
                "kategori_survey.id as id_kategori_survey",
                "survey_unsur.nama",
                "survey.type",
                "survey.pertanyaan_survey as question_survey"
            )
            ->leftjoin('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->leftjoin('kategori_survey', 'kategori_survey.id', '=', 'survey_unsur.id_kategori_survey')
            ->where('kategori_survey.status', 'active')
            ->whereNull('survey.deleted_at')
            ->whereNull('kategori_survey.deleted_at')
            ->orderby('survey.id', 'desc')
            ->first();

        return ($data?$data->id_survey_unsur:0);
    }

    public static function listSurvey()
    {
        $data = DB::table('survey')
            ->select(
                "survey.id",
                "survey.id_survey_unsur",
                "kategori_survey.id as id_kategori_survey",
                "survey_unsur.nama",
                "survey.type",
                "survey.pertanyaan_survey as question_survey"
            )
            ->leftjoin('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->leftjoin('kategori_survey', 'kategori_survey.id', '=', 'survey_unsur.id_kategori_survey')
            ->where('kategori_survey.status', 'active')
            ->whereNull('survey.deleted_at')
            ->whereNull('kategori_survey.deleted_at')
            ->orderby('survey.id', 'desc')
            ->get();

        return $data;
    }


    // export survey
    public static function listAnswerPenerbit() {
        $first_date = g("first_date");
        $last_date = g('last_date');

        return DB::table('jawaban_survey')
            ->select(
                'cms_users.id as id_user',
                'cms_users.name as user_name',
                'survey.id as id_question',
                'survey_unsur.id as id_unsur',
                'survey.pertanyaan_survey as question',
                'survey_unsur.nama as name_unsur',
                // 'jawaban_survey.jawaban as opsi_value'
                'survey_opsi_jawaban.type_option as opsi_text'
                // 'survey_opsi_jawaban.nama'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'jawaban_survey.id_cms_users')
            ->leftjoin('survey', 'survey.id', '=', 'jawaban_survey.id_survey')
            ->leftjoin('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->leftjoin('survey_opsi_jawaban', 'survey_opsi_jawaban.nama', '=', 'jawaban_survey.jawaban')
            ->where(function ($q) use ($first_date, $last_date)
            {
                if (!empty($first_date)) {
                    $q->whereDate("jawaban_survey.created_at",">=",$first_date);
                }
                if (!empty($last_date)) {
                    $q->whereDate("jawaban_survey.created_at","<=",$last_date);
                }
            })
            ->whereNull('jawaban_survey.deleted_at')
            ->whereNull('survey.deleted_at')
            ->get();
    }

    public static function listUnsurSurvey() {
        return DB::table('survey_unsur')
            ->select(
                'id',
                'nama'
            )
            ->get();
    }

    public static function listSurveyPenerbit($id_unsur) {
        return DB::table('survey')
            ->select('survey.id','survey.type','survey.pertanyaan_survey')
            ->where('survey.id_survey_unsur', $id_unsur)
            ->whereNull('survey.deleted_at')
            ->get();
    }


    // report survey
    public static function listUnsurReport() {
        return DB::table("survey_unsur")
            ->select(
                'id',
                'nama'
            )
            ->orderby('id', 'asc')
            ->get();
    }

    public static function listAsnwerReport($id_unsur, $id_users = null) {
        $first_date = g("first_date");
        $last_date = g('last_date');

        return DB::table('jawaban_survey')
            ->selectRaw('sum(survey_opsi_jawaban.nilai) AS sumnilai, count(survey_opsi_jawaban.nilai) as countnilai, avg(survey_opsi_jawaban.nilai) as avgnilai')
            ->leftjoin('cms_users', 'cms_users.id', '=', 'jawaban_survey.id_cms_users')
            ->leftjoin('survey', 'survey.id', '=', 'jawaban_survey.id_survey')
            ->leftjoin('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->leftjoin('survey_opsi_jawaban', 'survey_opsi_jawaban.nama', '=', 'jawaban_survey.jawaban')
            ->where(function ($q) use ($id_unsur, $id_users) {
                if ($id_unsur) {
                    $q->where('survey_unsur.id', $id_unsur);
                }
                if ($id_users) {
                    $q->where('jawaban_survey.id_cms_users', $id_users);
                }
            })
            ->where(function ($q) use ($first_date, $last_date)
            {
                if (!empty($first_date)) {
                    $q->whereDate("jawaban_survey.created_at",">=",$first_date);
                }
                if (!empty($last_date)) {
                    $q->whereDate("jawaban_survey.created_at","<=",$last_date);
                }
            })
            ->whereNull('jawaban_survey.deleted_at')
            ->whereNull('survey.deleted_at')
            ->first();
    }

    public static function countPenerbitReport($id_unsur, $id_users = null) {
        $first_date = g("first_date");
        $last_date = g('last_date');

        return DB::table('jawaban_survey')
            ->join('cms_users', 'cms_users.id', '=', 'jawaban_survey.id_cms_users')
            ->join('survey', 'survey.id', '=', 'jawaban_survey.id_survey')
            ->join('survey_unsur', 'survey_unsur.id', '=', 'survey.id_survey_unsur')
            ->where(function ($q) use ($id_unsur, $id_users) {
                if ($id_unsur) {
                    $q->where('survey_unsur.id', $id_unsur);
                }
                if ($id_users) {
                    $q->where('jawaban_survey.id_cms_users', $id_users);
                }
            })
            ->where(function ($q) use ($first_date, $last_date)
            {
                if (!empty($first_date)) {
                    $q->whereDate("jawaban_survey.created_at",">=",$first_date);
                }
                if (!empty($last_date)) {
                    $q->whereDate("jawaban_survey.created_at","<=",$last_date);
                }
            })
            ->whereNull('jawaban_survey.deleted_at')
            ->whereNull('survey.deleted_at')
            ->groupby('cms_users.id')
            ->groupby('jawaban_survey.id_cms_users')
            ->count();
    }
}
