<?php

namespace App\Repositories;

use App\Models\KategoriSurvey;
use Illuminate\Support\Facades\DB;

class KategoriSurveyRepositories extends KategoriSurvey
{
    public static function joinUnsur($kategori_id)
    {
        return DB::table('survey_unsur')
            ->select(
                'survey_unsur.id  as id_survey_unsur',
                'survey_unsur.nama as unsur',
                'survey.pertanyaan_survey as pertanyaan',
                'survey.type as type_pertanyaan'
            )
            ->where('survey_unsur.id_kategori_survey', $kategori_id)
            ->join('survey', function ($join)
            {
                $join->on('survey.id_survey_unsur', '=', 'survey_unsur.id');
            })
            ->orderby('survey.id', 'desc')
            ->get();
    }

    public static function surveyOpsi($id_unsur)
    {
        return DB::table('survey_opsi_jawaban')
            ->select(
                'survey_opsi_jawaban.nama',
                'survey_opsi_jawaban.type_option as type'
            )
            ->where('survey_opsi_jawaban.id_survey', $id_unsur)
            ->orderby('survey_opsi_jawaban.id', 'desc')
            ->get();
    }

    public static function listSurvey()
    {
        return DB::table('kategori_survey')
            ->where('kategori_survey.status', 'active')
            ->whereNull('kategori_survey.deleted_at')
            ->orderby('kategori_survey.id','asc')
            ->get();
    }
}
