<?php

namespace App\Repositories;

use App\Models\DownloadMateri;
use Illuminate\Support\Facades\DB;

class DownloadMateriRepositories extends DownloadMateri
{
    public static function getListDownloadMateri($search, $total_record)
    {
        return DB::table('download_materi')
            ->select(
                'download_materi.*',
                'kategori_download.nama_kategori'
            )
            ->join('kategori_download','kategori_download.id','=','download_materi.id_kategori_download')
            ->where(function ($q) use ($search)
            {
                if ($search) {
                    $q->where('download_materi.judul', 'LIKE', '%'.$search.'%');
                }
            })
            ->whereNull('download_materi.deleted_at')
            ->limit($total_record)
            ->paginate($total_record);
    }

    public static function getListMateriUpload($id_download_materi)
    {
        return DB::table('materi_upload')
            ->where('id_download_materi', $id_download_materi)
            ->get();
    }
}
