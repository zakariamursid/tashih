<?php

namespace App\Repositories;

use App\Models\ListSiaranPers;
use Illuminate\Support\Facades\DB;

class ListSiaranPersRepositories extends ListSiaranPers
{
    public static function getListSiaranPers($search, $total_record)
    {
        return DB::table('list_siaran_pers')
            ->select(
                'list_siaran_pers.*',
                'cms_users.name as users_name',
                'cms_users.photo as users_image'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'list_siaran_pers.id_cms_user')
            ->where(function ($q) use ($search)
            {
                if ($search) {
                    $q->where('list_siaran_pers.title', 'LIKE', '%'.$search.'%');
                }
            })
            ->whereNull('list_siaran_pers.deleted_at')
            ->orderby('list_siaran_pers.id','desc')
            ->limit($total_record)
            ->paginate($total_record);
    }

    public static function getListSiaranPersOthers()
    {
        return DB::table('list_siaran_pers')
            ->select(
                'list_siaran_pers.*',
                'cms_users.name as users_name',
                'cms_users.photo as users_image'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'list_siaran_pers.id_cms_user')
			->whereNull('list_siaran_pers.deleted_at')
			->orderBy(DB::raw('RAND()'))
			->limit(5)
			->get();
    }

    public static function findListSiaranPers($slug)
    {
        return DB::table('list_siaran_pers')
            ->select(
                'list_siaran_pers.*',
                'cms_users.name as users_name',
                'cms_users.photo as users_image'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'list_siaran_pers.id_cms_user')
            ->where('list_siaran_pers.slug','LIKE', '%'.$slug.'%')
            ->whereNull('list_siaran_pers.deleted_at')
            ->first();
    }

    public static function listBannerInfoLayanan()
    {
        return DB::table('gallery_mushaf')
            ->select(
                'gallery_mushaf.id',
                'gallery_mushaf.title',
                'gallery_mushaf.slug',
                'gallery_mushaf.image',
                'gallery_mushaf.content',
                'gallery_mushaf.created_at',
                DB::raw('"infolayanan" AS type')
            )
            ->where('gallery_mushaf.flag_showhome', 'Yes')
            ->whereNull('gallery_mushaf.deleted_at')
            ->orderby('gallery_mushaf.id', 'DESC');
    }

    public static function listBannerSeputarLajnah()
    {
        return DB::table('info_seputar_lajnah')
            ->select(
                'info_seputar_lajnah.id',
                'info_seputar_lajnah.title',
                'info_seputar_lajnah.slug',
                'info_seputar_lajnah.image',
                'info_seputar_lajnah.content',
                'info_seputar_lajnah.created_at',
                DB::raw('"seputarlajnah" AS type')
            )
            ->where('info_seputar_lajnah.flag_showhome', 'Yes')
            ->whereNull('info_seputar_lajnah.deleted_at')
            ->orderby('info_seputar_lajnah.id', 'DESC');
    }

    public static function listBannerSiaranPers()
    {
        return DB::table('list_siaran_pers')
            ->select(
                'list_siaran_pers.id',
                'list_siaran_pers.title',
                'list_siaran_pers.slug',
                'list_siaran_pers.image',
                'list_siaran_pers.content',
                'list_siaran_pers.created_at',
                DB::raw('"siaranpers" AS type')
            )
            ->where('list_siaran_pers.flag_showhome', 'Yes')
            ->whereNull('list_siaran_pers.deleted_at')
            ->orderby('list_siaran_pers.id', 'DESC');
    }

    public static function searchListSiaranPers($search) {
        return DB::table('list_siaran_pers')
                ->select(
                    'list_siaran_pers.id',
                    'list_siaran_pers.created_at',
                    'list_siaran_pers.title',
                    'list_siaran_pers.slug',
                    'list_siaran_pers.image',
                    'list_siaran_pers.url_videos',
                    'list_siaran_pers.content',
                    'cms_users.name as users_name'
                )
                ->leftjoin('cms_users', 'cms_users.id', '=', 'list_siaran_pers.id_cms_user')
                // ->where('list_siaran_pers.flag_showhome', 'Yes')
                ->whereNull('list_siaran_pers.deleted_at')
                ->where(function ($q) use ($search) {
                    if ($search) {
                        $q->where('list_siaran_pers.title', 'LIKE', '%'.$search.'%')
                        ->orWhere('list_siaran_pers.content', 'LIKE', '%'.$search.'%');
                    }
                })
                ->orderby('list_siaran_pers.id', 'DESC')
                ->get();
    }

    public static function searchListInfoLayananTashih($search) {
        return DB::table('gallery_mushaf')
            ->select(
                'gallery_mushaf.id',
                'gallery_mushaf.created_at',
                'gallery_mushaf.title',
                'gallery_mushaf.slug',
                'gallery_mushaf.image',
                'gallery_mushaf.content',
                'cms_users.name as users_name'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'gallery_mushaf.id_cms_users')
            // ->where('gallery_mushaf.flag_showhome', 'Yes')
            ->whereNull('gallery_mushaf.deleted_at')
            ->where(function ($q) use ($search) {
                if ($search) {
                    $q->where('gallery_mushaf.title', 'LIKE', '%'.$search.'%')
                    ->orWhere('gallery_mushaf.content', 'LIKE', '%'.$search.'%');
                }
            })
            ->orderby('gallery_mushaf.id', 'DESC')
            ->get();
    }

    public static function searchListInfoSeputarLajnah($search) {
        return DB::table('info_seputar_lajnah')
            ->select(
                'info_seputar_lajnah.id',
                'info_seputar_lajnah.title',
                'info_seputar_lajnah.created_at',
                'info_seputar_lajnah.slug',
                'info_seputar_lajnah.image',
                'info_seputar_lajnah.content',
                'cms_users.name as users_name'
            )
            ->leftjoin('cms_users', 'cms_users.id', '=', 'info_seputar_lajnah.id_cms_users')
            // ->where('info_seputar_lajnah.flag_showhome', 'Yes')
            ->whereNull('info_seputar_lajnah.deleted_at')
            ->where(function ($q) use ($search) {
                if ($search) {
                    $q->where('info_seputar_lajnah.title', 'LIKE', '%'.$search.'%')
                    ->orWhere('info_seputar_lajnah.content', 'LIKE', '%'.$search.'%');
                }
            })
            ->orderby('info_seputar_lajnah.id', 'DESC')
            ->get();
    }

    public static function searchInfoPenerbit($search) {
        return DB::table('cms_users')
                ->select(
                    'id',
                    'name',
                    'email',
                    'pic',
                    'phone',
                    'address'
                )
                ->where(function ($q) use ($search) {
                    if ($search) {
                        $q->where('name', 'LIKE', '%'.$search.'%');
                    }
                })
                ->where("status","Aktif")
                ->where('id_cms_privileges',2)
                ->get();
    }

    public static function listMushafPenerbit($users_id) {
        return DB::table('proses_pentashihan')
            ->select(
                'nama_produk'
            )
            ->where('status', 'Selesai')
            ->where("id_cms_users", $users_id)
            ->whereNull("deleted_at")
            ->groupby("nama_produk")
            ->get();
    }
}
