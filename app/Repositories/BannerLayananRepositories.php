<?php

namespace App\Repositories;

use App\Models\BannerLayanan;
use Illuminate\Support\Facades\DB;

class BannerLayananRepositories extends BannerLayanan
{
    public static function listBannerLayanan()
    {
        return DB::table('banner_layanan')
            ->where(function ($q)
            {
                $q->where('status', 'Aktif')
                ->whereNull('deleted_at');
            })
            ->orderby('id','DESC')
            ->get();
    }

    public static function findBannerLayanan($slug)
    {
        return DB::table('banner_layanan')
            ->where(function ($q) use ($slug)
            {
                $q->where('slug', $slug)
                ->where('status', 'Aktif')
                ->whereNull('deleted_at');
            })
            ->orderby('id','DESC')
            ->first();
    }

    public static function listRandomBannerLayanan($id)
    {
        return DB::table('banner_layanan')
            ->where(function ($q) use ($id)
            {
                $q->where('id', '<>', $id)
                ->where('status', 'Aktif')
                ->whereNull('deleted_at');
            })
            ->inRandomOrder()
            ->limit(5)
            ->orderby('id','DESC')
            ->get();
    }
}
