<?php

namespace App\Repositories;

use App\Models\SurveyJawaban;
use Illuminate\Support\Facades\DB;

class SurveyJawabanRepositories extends SurveyJawaban
{
    public static function isUserAnswer($id_kategori_survey, $id_users)
    {
        $listKategori = DB::table('jawaban_survey')
            ->select('survey.*')
            ->leftjoin('survey','survey.id','=','jawaban_survey.id_survey')
            ->where('jawaban_survey.id_cms_users', $id_users)
            ->where('survey.id_survey_unsur', $id_kategori_survey)
            ->whereNull('jawaban_survey.deleted_at')
            ->whereNull('survey.deleted_at')
            ->pluck('survey.id_survey_unsur')
            ->toArray();

        return $listKategori;
    }

    public static function isUserAnswerSurvey($id_kategori_survey, $id_users)
    {
        $data = DB::table('jawaban_survey')
            ->leftjoin('survey','survey.id','=','jawaban_survey.id_survey')
            ->where('survey.id_survey_unsur',$id_kategori_survey)
            ->where('jawaban_survey.id_cms_users',$id_users)
            ->whereNull('jawaban_survey.deleted_at')
            ->count();

        return ($data > 0 ? true : false);
    }

    public static function answerSurvey($id_users, $id_survey)
    {
        $data = DB::table('jawaban_survey')
            ->where('jawaban_survey.id_cms_users', $id_users)
            ->where('jawaban_survey.id_survey', $id_survey)
            ->whereNull('jawaban_survey.deleted_at')
            ->first();

        return $data;
    }
}
