<?php

namespace App\Repositories;

use App\Models\EmailNotif;
use Illuminate\Support\Facades\DB;

class EmailNotifRepositories extends EmailNotif
{
    public static function listPenerbit()
    {
        $search = request('search');
        $total_record = (request('totalRecord') == '' ? 5 : request('totalRecord'));

        $data = DB::table("cms_users")
            ->where('id_cms_privileges', 2)
            ->where('status', 'Aktif')
            ->where(function ($q) use ($search)
            {
                if ($search) {
                    $q->where("name", "LIKE", "%".$search."%")
                    ->orWhere("email", "LIKE", "%".$search."%")
                    ->orWhere("address", "LIKE", "%".$search."%")
                    ->orWhere("pic", "LIKE", "%".$search."%");
                }
            })
            ->orderby('id', 'desc')
            ->limit($total_record)
            ->paginate($total_record);

        return $data;
    }
}
