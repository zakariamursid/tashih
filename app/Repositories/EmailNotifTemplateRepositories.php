<?php

namespace App\Repositories;

use App\Models\EmailNotifTemplate;
use Illuminate\Support\Facades\DB;

class EmailNotifTemplateRepositories extends EmailNotifTemplate
{
    public static function getList()
    {
        $data = DB::table('email_notif_template')
            ->orderby('id', 'desc')
            ->get();

        return $data;
    }

    public static function generateHtmlEmail($template)
    {
        $template = self::first('email_notif_template', ['slug' => $template]);
        $html = $template->content;

        return $html;
    }

    public static function parseSqlTable($table)
    {

        $f = explode('.', $table);

        if (count($f) == 1) {
            return ["table" => $f[0], "database" => env('DB_DATABASE')];
        } elseif (count($f) == 2) {
            return ["database" => $f[0], "table" => $f[1]];
        } elseif (count($f) == 3) {
            return ["table" => $f[0], "schema" => $f[1], "table" => $f[2]];
        }

        return false;
    }
    public static function pk($table)
    {
        return self::findPrimaryKey($table);
    }
    public static function findPrimaryKey($table)
	{
		if(!$table)
		{
			return 'id';
		}
		
		$pk = DB::getDoctrineSchemaManager()->listTableDetails($table)->getPrimaryKey();
		if(!$pk) {
            return null;
		}
		return $pk->getColumns()[0];	
	}
    public static function first($table, $id)
    {
        $table = self::parseSqlTable($table)['table'];
        if (is_array($id)) {
            $first = DB::table($table);
            foreach ($id as $k => $v) {
                $first->where($k, $v);
            }

            return $first->first();
        } else {
            $pk = self::pk($table);

            return DB::table($table)->where($pk, $id)->first();
        }
    }
}
