<?php

namespace App\Repositories;

use App\Models\SurveyOpsiJawaban;
use Illuminate\Support\Facades\DB;
use App\Services\SurveyOpsiJawabanServices;

class SurveyOpsiJawabanRepositories extends SurveyOpsiJawaban
{
    public static function getListOpsiBySurveyId($id_survey)
    {
        $data = DB::table('survey_opsi_jawaban')
            ->select(
                'survey_opsi_jawaban.id',
                'survey_opsi_jawaban.nama',
                'survey_opsi_jawaban.type_option',
                'survey_opsi_jawaban.nilai'
            )
            ->where('survey_opsi_jawaban.id_survey', $id_survey)
            ->orderby('survey_opsi_jawaban.type_option', 'asc')
            ->get();

        return $data;
    }

    public static function findById($id_option)
    {
        $data = DB::table('survey_opsi_jawaban')
            ->where('id', $id_option)
            ->first();

        return $data;
    }

    public static function exportOpsiJawaban($limit = 100, $name_survey_id = null)
    {
        return DB::table('jawaban_survey')
            ->select(
                'survey_two.id_survey_unsur',
                'survey_two.pertanyaan_survey as survey_name',
                'cms_users.name as name_users',
                'jawaban_survey.*',
                'survey_unsur.nama as nama_unsur',
                'survey_opsi_jawaban.type_option as option_survey'
            )
            ->join('cms_users', function ($join)
            {
                $join->on('cms_users.id', '=', 'jawaban_survey.id_cms_users');
            })
            ->join('survey as survey_two', function ($join)
            {
                $join->on('survey_two.id', '=', 'jawaban_survey.id_survey');
            })->leftjoin('survey_unsur', function ($join)
            {
                $join->on('survey_unsur.id', '=', 'survey_two.id_survey_unsur');
            })
            ->leftjoin('survey_opsi_jawaban', function ($join)
            {
                $join->on('survey_opsi_jawaban.id', '=', 'jawaban_survey.id_survey_opsi_jawaban');
            })
            ->leftjoin('kategori_survey', function ($join) use ($name_survey_id)
            {
                $join->on('kategori_survey.id', '=', 'survey_unsur.id_kategori_survey');
                if ($name_survey_id) {
                    $join->where('kategori_survey.id', $name_survey_id);
                }
            })
            ->where(function ($q) use ($name_survey_id)
            {
                if ($name_survey_id) {
                    $q->WhereNotNull('kategori_survey.id');
                    $q->WhereNotNull('survey_unsur.id');
                    $q->WhereNotNull('survey_two.id');
                    $q->WhereNotNull('survey_opsi_jawaban.id');
                }
            })
            ->orderby('survey_opsi_jawaban.id', 'desc')
            ->limit($limit)
            ->get();
    }
}
