<!DOCTYPE html>
<html>
<head>
	<title>Bukti Pendaftaran</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		*,h1,h2,h3,h4,h5,h6,p,span,b,strong{
  		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
  		font-weight: normal;
		  /*box-sizing: border-box;*/
		}
		.row::after {
		  content: "";
		  clear: both;
		  /*display: table;*/
		}
		[class*="col-"] {
		  /*float: left;*/
		  padding: 15px;
		}
		.col-1 {width: 8.33%;}
		.col-2 {width: 16.66%;}
		.col-3 {width: 25%;}
		.col-4 {width: 33.33%;}
		.col-5 {width: 41.66%;}
		.col-6 {width: 50%;}
		.col-7 {width: 58.33%;}
		.col-8 {width: 66.66%;}
		.col-9 {width: 75%;}
		.col-10 {width: 83.33%;}
		.col-11 {width: 91.66%;}
		.col-12 {width: 100%;}
		table{
			border-collapse: collapse;
			width: 100%;
		}
		

		.title{
			font-weight: bold !important;
		}
		.title,.sub-title{
			padding: 20px;
			margin: 0;
		}
		p{
			font-size: 11pt !important;
		}
		table p {
			margin: 0;
			padding: 2px 15px;
			font-size: 10pt !important;
		}
		img,iframe{
			width: auto;
			height: 1.2cm;
		}
		/* table, tr {border:hidden;}
td, th {border:hidden;} */
		table,td
		{
			font-size:11pt !important;
		}
table :empty{border:none; height:20px;}


		#table-bordered thead
		{
		  border: 1px solid black;
		  padding:2px;
		}
		#table-bordered tr
		{
			padding:2px;
			  border: 1px solid black;
		}
		#table-bordered td
		{
			padding:2px;
			border: 1px solid black;
		}
		#table-bordered th
		{
			padding:2px;
			border: 1px solid black;
		}


	</style>
</head>
<body>
<div class="container" style="margin: -1cm 0 -1cm;">
	<div class="row">
		<div class="col-12" style="padding: 0;margin:0;border-bottom: 3px solid #000;">
			<div class="col-1" style="float: left;height: 2.5cm;">
				<img src="https://kemenag.go.id/myadmin/public/data/files/users/3/images/Logo%20kemenag.png" style="width: 2cm; height: 2cm;padding-top: 15px;">
			</div>
			<div class="col-12" style="height: 2.5cm;text-align: center;">
				<p style="font-weight: bold;margin-bottom: 0;">
					KEMENTERIAN AGAMA REPUBLIK INDONESIA 
					<br> LAJNAH PENTASHIHAN MUSHAF AL-QUR'AN
				</p>
				<p style="margin-top: 0;margin-bottom: 0;font-size: 10px;">
					Gedung Bayt Al-Qur'an &amp; Museum Istiqlal, Jalan Raya TMII Pintu I Jakarta Timur 13560 
					<br> Telp: (021) 87798807, 8416466, 8416467, 8416468  Fax: (021) 87798807 
					<br> Website: http://lajnah.kemenag.go.id Email : lajnah@kemenag.go.id
				</p>
			</div>
		</div>
		<div class="col-12" style="padding: 0;margin: 0;">
			<div class="col-12" style="height: .5cm;text-align: center;">
				<p style="font-weight: bold;margin-bottom: 0;">
					Bukti Pendaftaran Mushaf Al-Qur’an
				</p>
			</div>

			<table style="border:hidden;padding-top:50px;">
				<tbody>
					<!-- Data Pendaftar -->

					<tr>
						<td><p>Nomor Pendaftaran mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nomor_registrasi}}</p></td>
					</tr>
					<tr>
						<td><p>Jenis Pendaftaran mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->jenis_permohonan}}</p></td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td colspan="3">
							<p style="font-weight: bold;">I. Informasi Data Mushaf</p>
						</td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td><p>Nama Penerbit</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$cms_users->name}}</p></td>
					</tr>
					<tr>
						<td width="40%"><p>ID User</p></td>
						<td width="2%"><p>:</p></td>
						<td><p> {{($cms_users->id_penerbit == '' ? '-' : $cms_users->id_penerbit)}}</p></td>
					</tr>
					<tr>
						<td><p>Nama Produk/Mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nama_produk}}</p></td>
					</tr>
					<tr>
						<td><p>Nama Penanggung Jawab Produk/Mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$cms_users->pic}}</p></td>
					</tr>
					<tr>
						<td><p>Jenis Naskah</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->jenis_naskah}}</p></td>
					</tr>
					<tr>
						<td><p>Deskripsi Mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->keterangan}}</p></td>
					</tr>
					@if($proses_pentashihan->jenis_permohonan == "Surat Izin Edar")
					<tr>
						<td><p>Negara asal mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->countries_name}}</p></td>
					</tr>
					<tr>
						<td><p>Penerbit asal mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nama_penerbit_asal}}</p></td>
					</tr>
					<tr>
						<td><p>Lembaga pentashih asal mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nama_lembaga_pentashih_asal}}</p></td>
					</tr>
					<tr>
						<td><p>Bukti tashih Lembaga pentashih asal mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nama_penerbit_asal}}</p></td>
					</tr>
					@endif
					
					<tr>
						<td><p>Ukuran</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->ukuran}}</p></td>
					</tr>
					<tr>
						<td><p>Oplah</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->oplah}}</p></td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td colspan="3">
							<p style="font-weight: bold;">II. Informasi Data Materi Tambahan pada Mushaf</p>
						</td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>

					<tr>
						<td><p>Materi tambahan pada mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->materi_tambahan}}</p></td>
					</tr>

					<tr>
						<td><p>Penanggung Jawab materi tambahan</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{($proses_pentashihan->penanggung_jawab_materi_tambahan == '' ? '-' : $proses_pentashihan->penanggung_jawab_materi_tambahan)}}</p></td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					
					<tr>
						<td colspan="3">
							<p style="font-weight: bold;">III. Informasi Dokumen Mushaf</p>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><p>Surat permohonan izin edar*</p></td>
						<td width="2%"><p>:</p></td>
						<td>
							<p>
								@if($proses_pentashihan->surat_permohonan != '')
								<?php 
									$extension = explode('.', $proses_pentashihan->surat_permohonan);
									$total     = count($extension);
									$extension = $extension[$total-1];
									$extension = strtolower($extension);
								?>
									@if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'GIF')
										<img src="{{storage_path('app/'.$proses_pentashihan->surat_permohonan)}}">
									@else
										<a href="{{asset($proses_pentashihan->surat_permohonan)}}" target="_blank">{{asset($proses_pentashihan->surat_permohonan)}}</a>
									@endif
								@else
									-
								@endif
							</p>
						</td>
					</tr>

					<tr>
						<td><p>Gambar cover mushaf *</p></td>
						<td width="2%"><p>:</p></td>
						<td>
							<p>
								@if($proses_pentashihan->gambar_cover != '')
								<?php 
									$extension = explode('.', $proses_pentashihan->gambar_cover);
									$total     = count($extension);
									$extension = $extension[$total-1];
									$extension = strtolower($extension);
								?>
									@if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'GIF')
										<img src="{{storage_path('app/'.$proses_pentashihan->gambar_cover)}}">
									@else
										<a href="{{asset($proses_pentashihan->gambar_cover)}}" target="_blank">{{asset($proses_pentashihan->gambar_cover)}}</a>
									@endif
								@else
									-
								@endif
							</p>										
						</td>
					</tr>

					<tr>
						<td><p>Gambar dokumen naskah *</p></td>
						<td width="2%"><p>:</p></td>
						<td>
							<p>
								@if($proses_pentashihan->dokumen_naskah != '')
								<?php 
									$extension = explode('.', $proses_pentashihan->dokumen_naskah);
									$total     = count($extension);
									$extension = $extension[$total-1];
									$extension = strtolower($extension);
								?>
									@if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'GIF')
										<img src="{{storage_path('app/'.$proses_pentashihan->dokumen_naskah)}}">
									@else
										<a href="{{asset($proses_pentashihan->dokumen_naskah)}}" target="_blank">{{asset($proses_pentashihan->dokumen_naskah)}}</a>
									@endif
								@else
									-
								@endif
							</p>					
						</td>
					</tr>
					<tr>
						<td><p>Apakah Sudah dilakukan Tashih Internal? *</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->tashih_internal}}</p></td>
					</tr>
					<tr>
						<td><p>Bukti Tashih Internal</p></td>
						<td width="2%"><p>:</p></td>
						<td>
							<p>
								@if($proses_pentashihan->dokumen_naskah != '')
								<?php 
									$extension = explode('.', $proses_pentashihan->dokumen_naskah);
									$total     = count($extension);
									$extension = $extension[$total-1];
									$extension = strtolower($extension);
								?>
									@if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'GIF')
										<img src="{{storage_path('app/'.$proses_pentashihan->dokumen_naskah)}}">
									@else
										<a href="{{asset($proses_pentashihan->dokumen_naskah)}}" target="_blank">{{asset($proses_pentashihan->dokumen_naskah)}}</a>
									@endif
								@else
									-
								@endif
							</p>					
						</td>
					</tr>

					<!-- Data Mushaf Yang Didaftarkan -->
				</tbody>
			</table>
		</div>
		<div class="col-12" style="padding: 0;margin: 0;">
			<p style="text-align: justify; margin-top:50px;">			
			Dengan ini penerbit menyatakan bahwa data-data yang tertera pada <span style="font-weight: bold;">BUKTI PENDAFTARAN
			{{ucfirst($proses_pentashihan->jenis_permohonan)}}</span>ini adalah benar.
			Jika data yang diisikan ternyata tidak benar, terbukti palsu, dan atau ternyata
			tidak memenuhi persyaratan maka LPMQ dapat membatalkan proses pentashihan
			dan atau mencabut Tanda Tashih mushaf yang didaftarkan ini.
			
			</p>
	
			<?php 
				$tanggal_indonesia = explode(',',Kemenag::dateIndonesia($proses_pentashihan->created_at));
			?>
				<span style="float: left;">{{$tanggal_indonesia[0]}}</span>
				<br><br><br><br><br>
				<span style="font-size: 9pt !important; float: left;">(..................................................................................)
				<p>Tandatangani, nama jelas dan stempel penerbit</p>
				</span>
				<br>
				<!-- <span style="font-size: 8pt !important;position: absolute;bottom: -0.3cm;">
					<b style="font-weight: bold;">* BUKTI PENDAFTARAN MUSHAF </b> <br> ini wajib dilampirkan ketika mengirim naskah fisik mushaf yang telah di daftarkan.
				</span> -->
		</div>
		</div>

			<div class="col-12" style="height: .5cm;text-align: justify;">
				<p style="float:left;">BERITA ACARA VERIFIKASI NASKAH MASTER (DIISI OLEH PENTASHIH LPMQ)</p>
				<br>			
				<br>			
				<br>			
				<p style="font-weight: bold;margin-bottom: 0;margin-top:50px;text-align: center;">
				BERITA ACARA <br> HASIL VERIFIKASI NASKAH MASTER
				</p>

				<p style="text-align: justify; margin-top:25px;">
					Pada hari ini ……….………., tanggal……….bulan……….………. tahun………. telah
					dilaksanakan pemeriksaan dan telaah terhadap naskah master Mushaf Al-Qur&#39;an
					sebagai berikut:					
				</p>

				<table style="border:hidden;padding-top:25px;">
				<tbody>
					<!-- Data Pendaftar -->

					<tr>
						<td><p>Nama Penerbit</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$cms_users->name}}</p></td>
					</tr>
					<tr>
						<td><p>ID Penerbit</p></td>
						<td width="2%"><p>:</p></td>
						<td><p> {{($cms_users->id_penerbit == '' ? '-' : $cms_users->id_penerbit)}}</p></td>
					</tr>
					<tr>
						<td><p>Nama Produk/Mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nama_produk}}</p></td>
					</tr>
					<tr>
						<td><p>Nomor pendaftaran mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->nomor_registrasi}}</p></td>
					</tr>
					<tr>
						<td><p>Jenis pendaftaran mushaf</p></td>
						<td width="2%"><p>:</p></td>
						<td><p>{{$proses_pentashihan->jenis_permohonan}}</p></td>
					</tr>
				</tbody>
				</table>

				<p style="text-align: justify;">
				Dengan hasil pemeriksaan dan telaah sebagai berikut:
				</p>

				<table class="table" id="table-bordered">
					<thead>
						<tr>
							<td>No</td>
							<td>Jenis Pemeriksaan</td>
							<td>Ceklis</td>
							<td>Keterangan</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1.</td>
							<td>Kelengkapan Juz</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Kelengkapan Surah</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Urutan Halaman</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Kesesuaian Dengan Kaidah Penulisan Mushaf Standar Indonesia</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Kesuaian dengan kaidah tajwid warna</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>6.</td>
							<td>Kesesuaian dengan terjemah kemenag</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>7.</td>
							<td>Kesesuaian dengan pedoman penulisan Al-Qur`an Braille Kemenag.</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>8.</td>
							<td>Kesesuaian Dengan Pedoman Transliterasi</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<p style="text-align: justify;">
				 Naskah tersebut dinyatakan
				 </p>
				<input type="checkbox" name="lolos" value="lolos">
				<label for="lolos"> Lolos verifikasi</label>
				<input type="checkbox" name="tidak" value="tidak">
				<label for="tidak"> Tidak lolos verifikasi</label><br>				 
				<br>

				<p>
				Rencana tindak lanjut Pelayanan Pentashihan Mushaf Al-Qur’an sebagai berikut:
				</p>
				<input id="checkbox" type="checkbox"/><label for="checkbox">Dikembalikan ke penerbit</label> <br>
				<input id="checkbox" type="checkbox"/><label for="checkbox">Dilakukan pentashihan dengan ketentuan sebagai berikut:</label> <br>
				<ul style="list-style-type: none;">
					<li>Waktu Layanan Pentashihan : ..... hari kerja</li>
					<li>Jumlah Surat Tanda Tashih : ..... Surat Tanda Tashih</li>
					<li>Ketentuan PNBP layanan pentashihan sebagai berikut:</li>
				</ul>

				<table>
					<tbody>

					@for($i = 1; $i <= 6; $i++)
					<tr>
						<td  colspan="2">PNBP Konten .................................................................. : Rp .............................................................</td>
					</tr>
					@endfor

					<tr>
						<td colspan="2">_______________________________________________________________________________</td>
					</tr>
					
					<tr>
						<th style="float:right;">Total Biaya PNBP </th>
						<th> &nbsp; &nbsp;&nbsp; <b style="padding-left:12px;">: Rp</b> <b style="padding-left:120px;">per surat tanda tashih </b></th>
					</tr>

					</tbody>
					
				</table>
				 <br>
				 <br>
				<p style="text-align: justify;">
				Demikian Berita Acara ini dibuat agar dapat dipergunakan sebagaimana mestinya.
				</p>

				<p>
					Jakarta, ……………….,................................. <br>
					Mengetahui : <br>
					Verifikator		 <br>			
				</p>

				<br>
				<div style="padding-top:50px;">
				<span>
					(
				</span>
				<span style="margin-left:150px;">
					)
				</sp>
				</div>
					Nip. 

				<p style="padding-top:100px;">* BUKTI PENDAFTARAN MUSHAF AL-QUR’AN INI WAJIB DILAMPIRKAN KETIKA MENGIRIM <br> NASKAH FISIK MUSHAF AL-QUR’AN YANG TELAH DIDAFTARKAN.</p>
			</div>

	</div>
</div>
</body>
</html>