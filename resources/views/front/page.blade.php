@extends('front/template/mastertemplate')

@section('content')
  <section id="newsDetail" class="container">
		<div class="content-breadcrumb">
			<p>PENTASHIHAN/{{$breadcrumb}}</p>
		</div>

		<div class="row">
			<div id="content" class="col-sm-8 animated fadeInLeft">
				<h1 style="font-size: 26px;">{{$title}}</h1>
				<p class="info-news" style="font-size: 14px;">
					<i class="fa fa-user"></i> {{strtoupper($name)}}
					<span> | </span>
					<i class="fa fa-book"></i> LAJNAH
					<span> | </span>
					<i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia(($page->updated_at == '' ? $page->created_at : $page->updated_at))}}
				</p>
				<div style="padding: 0 60px 36px">
					{!! $page->content !!}
				</div>
			</div>

			<!-- info -->
	    <div id="info" class="col-md-4 order-md-2">
				<div class="col-sm-12">
					<a href="{{asset('persyaratan-pengajuan-tashih')}}">
	      		<img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
	      	</a>
	      	@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
	      	<a href="{{asset('pendaftaran-penerbit')}}">
	      		<img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
	      	</a>
	      	@endif
	      	@if(CRUDBooster::myId() != '')
	      	<a href="{{asset('pengajuan-tanda-tashih')}}">
	      		<img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
	      	</a>
	      	@endif
	      	<a href="{{asset('petunjuk-teknis-pencetakan')}}">
	      		<img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
	      	</a>
	      	<a href="{{asset('pelaporan-mushaf-bermasalah')}}">
	      		<img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
	      	</a>
				</div>
			</div><!-- ./ info -->
		</div>
	</section>
@endsection

@push('js')
<script type="text/javascript">
	
</script>
@endpush