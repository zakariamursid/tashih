@extends('front/template/mastertemplate')

@section('content')

<section id="newsDetail" class="container">
    <div class="content-breadcrumb">
        <p>HOME / SEARCH RESULT</p>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 border-right">
            <h2 class="medium-bold-black">Hasil Pencarian “{{ $search_text }}” kategori “{{ $type_search }}”</h2>
            @if(in_array(Kemenag::gSession('type_search'), ['all', 'info_seputar']))
            <h4 class="title-content">HASIL DARI INFO SEPUTAR LAJNAH</h4>
            <div class="list-news-div">
                @if(count($info_seputar) > 0)
                @foreach($info_seputar as $i => $irow)
                <a href="{{asset('info-seputar-lajnah/read/'.$irow->slug)}}">
                    <div class="item-news-div border-bottom pb-4">
                        <div class="image-news-div">
                            <img src="{{ asset($irow->image) }}" alt="Gambar">
                        </div>
                        <div class="content-news-div">
                            <h4>{{ $irow->title }}</h4>
                            <p>{{ $irow->users_name }}, {{Kemenag::dateIndonesia($irow->created_at)}}</p>
                            <span>{{ $irow->content }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
                @else
                <h4 class="title-content" style="color: #dd2233 !important;">INFO SEPUTAR LAJNAH TIDAK DITEMUKAN</h4>
                @endif
            </div>
            @endif

            @if(in_array(Kemenag::gSession('type_search'), ['all', 'info_layanan']))
            <h4 class="title-content">HASIL DARI INFO LAYANAN PENTASIHAN</h4>
            <div class="list-news-div">
                @if(count($info_layanan) > 0)
                @foreach($info_layanan as $y => $yrow)
                <a href="{{asset('info-layanan-pentashihan/read/'.$yrow->slug)}}">
                    <div class="item-news-div border-bottom pb-4">
                        <div class="image-news-div">
                            <img src="{{ asset($yrow->image) }}" alt="Gambar">
                        </div>
                        <div class="content-news-div">
                            <h4>{{ $yrow->title }}</h4>
                            <p>{{ $yrow->users_name }}, {{Kemenag::dateIndonesia($yrow->created_at)}}</p>
                            <span>{{ $yrow->content }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
                @else
                <h4 class="title-content" style="color: #dd2233 !important;">INFO LAYANAN PENTASIHAN TIDAK DITEMUKAN</h4>
                @endif
            </div>
            @endif

            @if(in_array(Kemenag::gSession('type_search'), ['all', 'siaran_pers']))
            <h4 class="title-content">HASIL DARI SIARAN PERS</h4>
            <div class="list-news-div">
                @if(count($siaran_pers) > 0)
                @foreach($siaran_pers as $x => $xrow)
                <a href="{{asset('list-siaran-pers/read/'.$xrow->slug)}}">
                    <div class="item-news-div border-bottom pb-4">
                        <div class="image-news-div">
                            <img src="{{ asset($xrow->image) }}" alt="Gambar">
                        </div>
                        <div class="content-news-div">
                            <h4>{{ $xrow->title }}</h4>
                            <p>{{ $xrow->users_name }}, {{Kemenag::dateIndonesia($xrow->created_at)}}</p>
                            <span>{{ $xrow->content }}</span>
                        </div>
                    </div>
                </a>
                @endforeach
                @else
                <h4 class="title-content" style="color: #dd2233 !important;">SIARAN PERS TIDAK DITEMUKAN</h4>
                @endif
            </div>
            @endif

            @if(in_array(Kemenag::gSession('type_search'), ['all', 'info_penerbit']))
            <h4 class="title-content">HASIL DARI INFORMASI PENERBIT</h4>
            <div class="list-info-penerbit row">
                @if(count($info_penerbit) > 0)
                @foreach($info_penerbit as $z => $zrow)
                <div class="col-md-10">
                    <div class="item-info-penerbit">
                        <div class="content-info-penerbit">
                            <h4>Nama Penerbit</h4>
                            <p>{{ $zrow->name }}</p>
                        </div>
                        <div class="content-info-penerbit-last">
                            <h4>Nama Mushaf</h4>
                            <p>{!! $zrow->mushaf !!}</p>
                        </div>
                        <div class="content-info-penerbit">
                            <h4>Penanggung Jawab Penerbit</h4>
                            <p>{{ $zrow->pic }}</p>
                        </div>
                        <div class="content-info-penerbit-last">
                            <h4>Alamat</h4>
                            <p>{{ $zrow->address }}</p>
                        </div>
                        <div class="content-info-penerbit">
                            <h4>Telepon</h4>
                            <p>{{ $zrow->phone }}</p>
                        </div>
                        <div class="content-info-penerbit-last">
                            <h4>Email</h4>
                            <p>{{ $zrow->email }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="col-sm-12">
                    <h4 class="title-content" style="color: #dd2233 !important;">PENERBIT TIDAK DITEMUKAN</h4>
                </div>
                @endif
            </div>
            @endif
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 side-menu">
            <div class="list-news-other row">
                @foreach($banner_layanan as $x => $xrow)
                <div class="col-sm-12 mb-3">
                    <a href="{{ $xrow->link }}">
                        <img src="{{ asset($xrow->image) }}" class="img-fluid">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection