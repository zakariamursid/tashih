@extends('front/template/mastertemplate')

@section('content')

<form action="{{asset(Request::segment(1).'/save')}}" method="POST" enctype="multipart/form-data">
	<section class="container tashih" style="padding-left: 0; padding-right: 0;">
		<div class="row no-margin">
			<div class="content-breadcrumb col-sm-12 wow animated fadeInLeft no-padding">
				<p>ADUAN MUSHAF BERMASALAH</p>
			</div>
			
			<div id="contentInformation" class="col-sm-10 mr-auto ml-auto wow animated fadeInBottom" style="">
				<p align="center" class="no-margin" style="">
					Halaman ini berfungsi untuk melakukan pelaporan Mushaf AL Quran yang terdapat kesalahan cetak.
				</p>
			</div>
			<div id="content" class="col-sm-12 form-shadow" style="margin-top: 0;z-index: 1;">
				<div id="formMushafBermasalah" class="row no-margin">
					<!-- identitas pelapor -->
					<div class="col-sm-12">
						<h4 class="big-bold-black">FORM ADUAN MUSHAF BERMASALAH</h4>
						<p class="medium-black">Silakan isi form di bawah ini!</p>

						<div class="row">
							<div class="col-sm-12">
								<h5 class="medium-bold-green">Identitas Pelapor</h5>
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Nama Pelapor *</label>
								<input type="text" name="nama_pelapor" placeholder="Tulis nama pelapor" class="form-control" required="">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Email Pelapor *</label>
								<input type="email" name="email_pelapor" placeholder="Tulis email pelapor" class="form-control" required="">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Nomor Telepon Pelapor *</label>
								<input type="text" name="phone_pelapor" placeholder="Tulis nomor telepon pelapor" class="form-control" required="">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Pekerjaan Pelapor *</label>
								<input type="text" name="pekerjaan_pelapor" placeholder="Tulis pekerjaan pelapor" class="form-control" required="">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Alamat Pelapor *</label>
								<textarea name="alamat_pelapor" placeholder="Tulis alamat pelapor" class="form-control" required="" rows="4"></textarea>
							</div>
						</div>
					</div><!-- ./ identitas pelapor -->
					
					<!-- separator -->
					<div class="col-sm-12 no-padding">
						<div class="row">
							<hr class="hr-grey" style="margin:24px 0;">
						</div>
					</div><!-- ./ separator -->
					
					<!-- data penerbit dan laporan -->
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<h5 class="medium-bold-green">Data Penerbit dan Laporan</h5>
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Nama Penerbit</label>
								<input type="text" name="nama_penerbit" placeholder="Tulis nama penerbit" class="form-control">

								<label class="verysmall-green">Judul Mushaf</label>
								<input type="text" name="judul_mushaf" placeholder="Tulis judul mushaf" class="form-control">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">Gambar Cover Mushaf<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="" data-original-title="Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 2 mb"></i></label><br>
								<input type="file" name="scan_cover_mushaf" class="file-forum" accept="application/pdf, image/*" onchange="ValidateExtension(this)"><br>
								
								<label class="verysmall-green">Gambar Surat Tanda Tashih<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="" data-original-title="Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 2 mb"></i></label><br>
								<input type="file" name="scan_surat_tanda_tashih" class="file-forum" accept="application/pdf, image/*" onchange="ValidateExtension(this)">
							</div>
							<div class="col-sm-6 box-form">
								<label class="verysmall-green">No. Surat Tanda Tashih</label>
								<input type="text" name="no_surat_tanda_tashih" placeholder="Tulis no. surat tanda tashih" class="form-control">
							</div>
						</div>
					</div><!-- data penerbit dan laporan -->
					
					<!-- separator -->
					<div class="col-sm-12 no-padding">
						<div class="row">
							<hr class="hr-grey" style="margin:24px 0;">
						</div>
					</div><!-- ./ separator -->
					
					<!-- data penerbit dan laporan -->
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-12">
								<h5 class="medium-bold-green">
									Gambar Dokumen Bermasalah 
									<button type="button" class="btn-wrapper btn btn-success btn-sm" style="position: relative;width: 20px;height: 20px;">
										<i class="fa fa-plus" style="font-size: 8px;position: absolute;top: 5px;left: 5px;"></i>
									</button>
								</h5>
							</div>
						</div>
						<div id="wrapper">

							<div id="boxWrapper" class="row">
								<div class="col-sm-5">
									<div class="row">
										<div class="col-sm-4 box-form">
											<label class="verysmall-green">Surah dan ayat</label>
											<input type="text" name="halaman[]" placeholder="Surah dan ayat" class="form-control" required="">
										</div>
										<div class="col-sm-7 ml-auto box-form">
											<label class="verysmall-green">Gambar Halaman<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="" data-original-title="Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 2 mb"></i></label><br>
											<input type="file" name="gambar_halaman[]" class="file-forum" required="" accept="application/pdf, image/*" onchange="ValidateExtension(this)">
										</div>
									</div>
								</div>
								<div class="col-sm-6 row">
									<div class="col-sm-6 box-form">
										<label class="verysmall-green">Jenis Kesalahan</label>
										<select name="jenis_kesalahan[]" class="form-control select2" required="">
											<option value="">Pilih jenis Kesalahan</option>
											@foreach($jenis_kesalahan as $row)
											<option value="{{$row->id}}">{{$row->name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-6">
										<label class="verysmall-green">Deskripsi Kesalahan</label>
										<textarea name="deskripsi_kesalahan[]" class="form-control" cols="30" rows="3"></textarea>
									</div>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-sm-2 ml-auto" style="padding-top: 50px;">
								<button id="kirim" type="submit" class="btn btn-green">KIRIM</button>
								{{-- <button id="submitLaporan" type="submit"></button> --}}
							</div>
						</div>
					</div><!-- data penerbit dan laporan -->

				</div>
			</div>
		</div>
	</section>
	<input type="hidden" name="_token" value="{{csrf_token()}}">
</form>

@endsection

@push('js')

	<script type="text/javascript">
		function ValidateExtension(e) {
			var allowedFiles = [".pdf", ".png", ".jpg", ".jpeg", ".gif"];
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
			if (!regex.test($(e).val().toLowerCase())) {
				alert("Please upload files having extensions: " + allowedFiles.join(', ') + " only.");
				$(e).val("");
				$(e).val(null);
				return false;
			}
			return true;
		}

		$('.select2').select2();

		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})

		$('.btn-wrapper').click(function(){
			let append = '';
			append += '<div id="boxWrapper" class="row">'

			let wrapperLeft = `
				<div class="col-sm-5">
					<div class="row">
						<div class="col-sm-4 box-form">
							<label class="verysmall-green">Surah dan ayat</label>
							<input type="text" name="halaman[]" placeholder="Surah dan ayat" class="form-control" required="">
						</div>
						<div class="col-sm-7 ml-auto box-form">
							<label class="verysmall-green">Gambar Halaman</label>
							<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="" data-original-title="Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 2 mb"></i>
							<br>
							<input type="file" name="gambar_halaman[]" class="file-forum" required="">
						</div>
					</div>
				</div>
			`;

			let wrapperRight = `
				<div class="col-sm-6 row">
					<div class="col-sm-6 box-form">
						<label class="verysmall-green">Jenis Kesalahan</label>
						<select name="jenis_kesalahan[]" class="form-control select2" required="">
							<option value="">Pilih jenis Kesalahan</option>
							@foreach($jenis_kesalahan as $row)
							<option value="{{$row->id}}">{{$row->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-6">
						<label class="verysmall-green">Deskripsi Kesalahan</label>
						<textarea name="deskripsi_kesalahan[]" class="form-control" cols="30" rows="3"></textarea>
					</div>
				</div>
			`;

			let wrapperClose = `
				<div class="col-sm-1 box-form">
					<button type="button" class="btn btn-info btn-sm btn-remove-wrapper" style="margin-top: 40px;">
						<i class="fa fa-times"></i>
					</button>
				</div>
			`;

			append += wrapperLeft;
			append += wrapperRight;
			append += wrapperClose;
			append += '</div>';

			$('#wrapper').append(append);

			$('.select2').select2();
		})

		$('#wrapper').on('click','.btn-remove-wrapper',function(){
			$(this).parent().parent().remove();
		})

		@if(Kemenag::message() != '')
	  	@if(Kemenag::messageType() == 'warning')
				$('#contentInformation').find("p").css('background-color','#f39c12');
	  	@endif
	  	@if(Kemenag::messageType() == 'danger')
				$('#contentInformation').find("p").css('background-color','#dd4b39');
	  	@endif

	  	$('#contentInformation').find("p").html("{{Kemenag::message()}}");
	  	$('#contentInformation').show(500);
	  @endif
	</script>

@endpush