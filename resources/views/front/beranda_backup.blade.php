@extends('front/template/mastertemplate')

@section('content')
<div id="contentInformation" class="col-sm-10 mr-auto ml-auto wow animated fadeInBottom animated" style="display: none;margin-bottom:-20px;">
	<p align="center" class="no-margin" style="">
		{{Kemenag::message()}}
	</p>
</div>
  <section id="content" class="container">
  	<!-- news-mini -->
		<div class="row">
			<?php $loop_new_info = 0; ?>
			@foreach($new_info as $row_info)
			<div class="col-sm-4 animated infinite fadeInUp">
				<a href="{{$row_info->href}}" class="news-mini">
					<div class="media">
						<div class="thumbnail">
							<img src="{{$row_info->image}}">
						</div>
						<div class="media-body">
							<label>{{$row_info->title}}</label>
							<p>{{$row_info->admin_name}} | {{Kemenag::dateIndonesia($row_info->created_at)}}</p>
						</div>
					</div>
				</a>
			</div>
			<?php 
				$loop_new_info++; 
				if($loop_new_info == 3) break;
			?>
			@endforeach
  	</div><!-- ./ news-mini -->

		<!-- slider -->
  	<div class="row">
  		<div id="slider" class="col-sm-12 animated infinite fadeInUp">
  			<div id="swiperSlider" class="swiper-container">
					<div class="swiper-wrapper">
						@foreach($banner as $row_banner)
						@if(!empty($row_banner->url_videos))
						@php
						$video_id_baner = explode("?v=", $row_banner->url_videos);
						if (empty($video_id_baner[1]))
							$video_id_baner = explode("/v/", $link);

						$video_id_baner = explode("&", $video_id_baner[1]);
						$video_id_baner = $video_id_baner[0];
						@endphp
						<div class="swiper-slide">
							<iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{$video_id_baner}}?controls=0&playlist={{$video_id_baner}}" frameborder="0" allowfullscreen style="width:100%;height:100%;"></iframe>
						</div>
						@else
						@php
						    $slug = url('info-seputar-lajnah/read/'.$row_banner->slug);
						    $url_banner = (empty($row_banner->url)) ? $slug : $row_banner->url;
						@endphp
						<div class="swiper-slide">
							<a href="{{$url_banner}}">
								<img src="{{$row_banner->image}}">
								<h1>{{$row_banner->title}}</h1>
								<p style="display: none;">
									<i class="fa fa-user"></i> {{strtoupper($row_banner->admin_name)}}
									<span> | </span>
									<i class="fa fa-book"></i> {{strtoupper($row_banner->admin_name)}}
									<span> | </span>
									<i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia($row_banner->created_at)}}
								</p>
							</a>
						</div>
						@endif
						@endforeach
					</div>
					<i id="sliderPrev" class="fa fa-arrow-left"></i>
					<div class="swiper-pagination float-right"></div>
					<i id="sliderNext" class="fa fa-arrow-right"></i>
				</div>
  		</div>
  	</div><!-- ./ slider -->
		
	<!-- news & info -->
	<div class="row">
		<!-- info -->
		<div id="info" class="col-md-4 order-md-2">
			<div class="col-sm-12">
				{{--<a href="{{asset('persyaratan-pengajuan-tashih')}}">
					<img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
				</a>
				@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
				<a href="{{asset('pendaftaran-penerbit')}}">
					<img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
				</a>
				@endif
				@if(CRUDBooster::myId() != '')
				<a href="{{asset('pengajuan-tanda-tashih')}}">
					<img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
				</a>
				@endif
				<a href="{{asset('petunjuk-teknis-pencetakan')}}">
					<img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
				</a>
				<a href="{{asset('pelaporan-mushaf-bermasalah')}}">
					<img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
				</a>--}}

				@foreach($banner_layanan as $x => $xrow)
				<a href="{{ $xrow->link }}">
					<img src="{{ asset($xrow->image) }}" class="img-fluid animated infinite flipInX">
				</a>
				@endforeach
			</div>
		</div>
		<!-- ./ info -->
			
		<!-- news -->
		<div id="news" class="col-md-8 order-md-1">
			<div class="row list-news">
				<div class="label col-sm-12">
					<a href="{{asset('info-seputar-lajnah')}}" class="float-right">
						<p style="position: relative;padding: 0;color: #3B3B3B;font-weight: 500;font-size: 14px;bottom: 0;">LIHAT SEMUA <i class="fa fa-angle-right"></i></p>
					</a>
					<h4>INFO SEPUTAR LAJNAH</h4>
				</div>
				<?php 
					$big   = 1; 
					$small = 0;
				?>
				@foreach($info_lainnya as $row_info_lainnya)
					@if($big < 2)
						<div class="col-sm-8 list-news list-news-big wow animated infinite fadeInLeft">
							<div class="thumbnail">
								<a href="{{$row_info_lainnya->href}}">
									<img src="{{$row_info_lainnya->image}}">
									<p>{{$row_info_lainnya->title}}</p>
								</a>
							</div>
						</div>
					@else
						<div onclick="hrefUrl('{{asset($row_info_lainnya->href)}}')" class="col-sm-4 list-news list-news-small wow animated infinite fadeInRight">
							<div class="thumbnail">
								<a href="{{$row_info_lainnya->href}}">
									<img src="{{$row_info_lainnya->image}}">
									<p>{{$row_info_lainnya->title}}</p>
								</a>
							</div>
						</div>
					@endif
					<?php 
						if ($big < 2) {
                            $big   = $big+1;
                            $small = ($big == 2 ? 0 : $small);
						}else{
                            $small = $small+1;
                            $big   = ($small == 2 ? 0 : $big);
						}
					?>
				@endforeach
			</div>
		</div>
	</div>
  </section>

  <section id="galeryMushaf" class="container">
  	    <div id="swipperMushaf" class="swiper-container">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="label">
                        INFO LAYANAN PENTASHIHAN
                        <a href="{{asseT('info-layanan-pentashihan')}}" class="float-right">LIHAT SEMUA <i class="fa fa-angle-right"></i></a>
                    </h4>
                </div>
                <div class="col-md-4">
                    <i id="galleryMushafNext" class="fa fa-arrow-right float-right"></i>
                    <div id="paginationGallery" class="swiper-pagination float-right" style="text-align: right;"></div>
                    <i id="galleryMushafPrev" class="fa fa-arrow-left float-right" style="margin-right: {{5+(count($gallery_mushaf)*5)}}%;"></i>
                </div>
            </div>

            <div class="swiper-wrapper">
                @foreach($gallery_mushaf as $row_gallery_mushaf)
                <div class="swiper-slide">
                    <div class="row">
                        <div class="col-sm-8 mushaf-banner thumbnail" data-link="0">
                            <a href="{{$row_gallery_mushaf->href}}" style="color: #FFF;">
                                <img src="{{$row_gallery_mushaf->image}}" class="img-fluid">
                                <div class="content-gallery-mushaf">
                                    <h1>{{$row_gallery_mushaf->admin_name}}</h1>
                                    <p>
                                        <i class="fa fa-user"></i> {{strtoupper($row_gallery_mushaf->admin_name)}}
                                        <span> | </span>
                                        <i class="fa fa-book"></i> {{strtoupper($row_gallery_mushaf->admin_name)}}
                                        <span> | </span>
                                        <i class="fa fa-calendar"></i> {{$row_gallery_mushaf->created_at}}
                                    </p>
                                    <content>
                                        {{$row_gallery_mushaf->content}}
                                    </content>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-4 mushaf-list">
                            @foreach($row_gallery_mushaf->mini as $row_mini_gallery_mushaf)
                            <a href="{{$row_mini_gallery_mushaf->href}}" style="cursor: pointer;">
                                <div class="list-gallery thumbnail">
                                    <img src="{{$row_mini_gallery_mushaf->image}}" class="img-fluid">
                                    <label>{{$row_mini_gallery_mushaf->title}}</label>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
		</div>
        {{--<div class="row">
            <div class="col-md-8">
                <h4 class="label">
                    GALERI MUSHAF AL-QUR'AN
                    <span class="float-right">LIHAT SEMUA <i class="fa fa-angle-right"></i></span>
                </h4>
            </div>
        </div>--}}
  </section>
  
   <section id="galeryMushaf" class="container">
  	    <div class="row">
  	        <div class="col-sm-12">
  	            <h4 class="label">PETA PENERBIT MUSHAF AL-QUR'AN INDONESIA</h4>
            	  <div class="mapsFooter" id="mapsPenerbit"></div>
  	        </div>
  	    </div>
  </section>
  
  <style>
  #mapsPenerbit{
      height:450px;
  }
  
  @media screen(max-width: 500px){
      #mapsPenerbit{
          height:350px;
      }   
  }
  </style>
@endsection

@section('js')
	<script type="text/javascript">
		/**
		 * link href
		 */
		function hrefUrl(href){
			document.location.href=href;
		}

		/**
		 * slider for index
		 */
		var swiper = new Swiper('#swiperSlider', {
			loop: true,
			slidesPerView: 'auto',
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				dynamicMainBullets: 1
			},
			navigation: {
	      nextEl: '#sliderNext',
	      prevEl: '#sliderPrev',
	    },
	  });

	  function sliderArrowLeft(){
	  	var width = 0;
	  	$('.swiper-pagination-bullet').each(function(){
	  		width += 13;
	  	});
	  	width += 100;

	  	$('#sliderPrev').css('right',''+ width + 'px');
	  }
	  sliderArrowLeft();


	  var swipperMushaf = new Swiper('#swipperMushaf', {
	    spaceBetween: 30,
	    pagination: {
	      el: '.swiper-pagination',
	      clickable: true
	    },
			navigation: {
	      nextEl: '#galleryMushafNext',
	      prevEl: '#galleryMushafPrev'
	    }
	  });

	@if(Kemenag::message() != '')
		@if(Kemenag::messageType() == 'warning')
				$('#contentInformation').find("p").css('background-color','#f39c12');
		@endif
		@if(Kemenag::messageType() == 'danger')
				$('#contentInformation').find("p").css('background-color','#dd4b39');
		@endif

		$('#contentInformation').find("p").html("{{Kemenag::message()}}");
		$('#contentInformation').show(500);
	@endif
	</script>
@endsection