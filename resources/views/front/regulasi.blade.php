@extends('front/template/mastertemplate')

@section('content')

<!--	<section class="container tashih animated infinite fadeInUp" style="padding-left: 0; padding-right: 0;">-->
    <section class="container tashih" style="padding-left: 0; padding-right: 0;">
		<div class="row no-margin">
			<div class="content-breadcrumb col-sm-12 animated fadeInLeft">
				<p>PENTASIHAN/REGULASI PENTASHIHAN</p>
			</div>

			<div id="content" class="col-sm-12 form-shadow">
				<div id="listRegulasi" class="row no-margin">
					<div class="col-sm-12">
						<h4 class="medium-bold-black">REGULASI PENTASIHAN</h4>
					</div>
					
					@foreach($regulasi as $row)
					<div class="col-md-6 col-sm-12" style="margin-top: 15px;margin-bottom: 15px;">
						<div class="box-thumbnail">
							<p class="medium-bold-green">{{$row->title}}</p>
							<table>
								<tr>
									<td class="verysmall-bold-black">Kategori</td>
									<td width="15px" align="center" class="verysmall-bold-black">:</td>
									<td class="verysmall-bold-black">{{$row->kategori}}</td>
								</tr>
								<tr>
									<td class="verysmall-bold-black">Nomor</td>
									<td width="15px" align="center" class="verysmall-bold-black">:</td>
									<td class="verysmall-bold-black">{{$row->nomor}}</td>
								</tr>
							</table>
							<hr class="hr-grey">
							<div class="row">
								<div class="col-sm-6" align="center">
									<a href="javascript:void(0)" class="small-bold-blue open-modal" data-url="{{url($row->file)}}" data-title="{{$row->title}}"><i class="fa fa-eye"></i> LIHAT DETAIL</a>
								</div>
								<div class="col-sm-6" align="center">
									<a href="{{url($row->file)}}" target="_blank" class="small-bold-green" download=""><i class="fa fa-download"></i> DOWNLOAD FILE</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</section>

	<!-- Modal -->
	<div id="modalViewer" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">

	      <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
	      
	      <div class="modal-body">
	        <iframe src=""></iframe>
	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>

@endsection

@push('js')

	<script type="text/javascript">
		$('.open-modal').on('click',function($row){
			let src = $(this).data("url");
			let title = $(this).data("title");

			$('#modalViewer').find(".modal-title").html(title);
			$('#modalViewer').find("iframe").attr("src",src);
			$('#modalViewer').modal("show");
		})
	</script>

@endpush