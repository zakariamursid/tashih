<!-- <div class=""> -->
	<h3 class="tashih-title" style="margin-bottom: 25px;">HASIL PENCARIAN</h3>
	{{-- <p class="tashih-label label-pencarian">
		Penerbit : <b>Lentera Abadi</b> atau Kode Tashih : <b>P.VI/1/TL.02.1/164/2011</b> atau Nama Mushaf :
	</p> --}}
	@if(count($proses_pentashihan)<=0)
	<div class="col">
		<label class="tashih-pencarian">Search again .....</label>
	</div>
	@else
	@foreach($proses_pentashihan as $row)
	<div class="row">
		<div class="col">
			<label class="tashih-pencarian">Penerbit</label>
			<p class="tashih-detail">
				{{$row->users_name}}
			</p>
		</div>
		<div class="col">
			<label class="tashih-pencarian">Nama Mushaf</label>
			<p class="tashih-detail">
				{{$row->nama_produk}}
			</p>
		</div>
		{{-- <div class="col">
			<label class="tashih-pencarian">Tahun Terbit</label>
			<p class="tashih-detail">
				-
			</p>
		</div>
		<div class="col">
			<label class="tashih-pencarian">Cetakan Ke</label>
			<p class="tashih-detail">
				-
			</p>
		</div> --}}
	</div>
	<div class="row">
		<div class="col">
			<label class="tashih-pencarian">Deskripsi Mushaf</label>
			<p class="tashih-detail">
				{{$row->keterangan}}
			</p>
		</div>
		<!-- <div class="col">
			<label class="tashih-pencarian">Link Tashih</label>
			<p class="tashih-detail">
				<a href="{{ url('cek-tanda-tashih/scan/'.$row->id) }}">Link Tanda Tashih</a>
			</p>
		</div> -->
	</div>
	<div class="row">
		<div class="col">
			<label class="tashih-pencarian">Status Pentashihan</label>
			<p class="tashih-detail">
				{{$row->status}}
			</p>
		</div>
		<div class="col">
			<label class="tashih-pencarian">Image Dokumen Naskah</label>
			<p class="tashih-detail">
				<img src="{{ ($row->cover?:'---') }}" width="65px">
			</p>
		</div>
		{{--<div class="col">
			<label class="tashih-pencarian">Nomor Tashih</label>
			<p class="tashih-detail">
				{{$row->nomor_tashih}}
			</p>
		</div>--}}
	</div>
	<div class="row">
		<div class="col">
			<label class="tashih-pencarian">Kode Tashih</label>
		</div>
		<div class="col">
			<label class="tashih-pencarian">Nomor Tashih</label>
		</div>
		<div class="col">
			<label class="tashih-pencarian">Ukuran</label>
		</div>
		{{-- <div class="col">
			<label class="tashih-pencarian">Lihat Tanda Tashih</label>
		</div> --}}
	</div>
	@if(count($row->ukurantashih) <= 0)
	<div class="col-md-12">
		<h5 class="tashih-pencarian">Belum ada produk yang ditashih.</h5>
	</div>
	@else
	@foreach($row->ukurantashih as $y => $yrow)
	<div class="row">
		<div class="col">
			<p class="tashih-detail">
				{{ ($yrow->kode?:'---') }}
			</p>
		</div>
		<div class="col">
			<p class="tashih-detail">
				{{ ($yrow->nomor?:'---') }}
			</p>
		</div>
		<div class="col">
			<p class="tashih-detail">
				{{ ($yrow->ukuran?:'---') }}
			</p>
		</div>
		{{-- <div class="col">
			<p class="tashih-detail">
				@if(file_exists(asset($yrow->scan_tanda_tashih)))
				<a href="{{ asset($yrow->scan_tanda_tashih) }}" class="btn btn-success btn-sm" download>Link Tanda Tashih</a>
				@else
				<h6 style="margin:0;">Tidak ada file tashih.</h6>
				@endif
			</p>
		</div> --}}
	</div>
	@endforeach
	@endif
	<div class="border-bottom mb-5"></div>
	@endforeach
	@endif
<!-- </div> -->

{!! $proses_pentashihan->links() !!}

@push('js')

    <script>
        function addClassPaging() {
            $('.pagination').find('li').addClass('page-item');
            $('.pagination').find('a').addClass('page-link');
            $('.pagination').find('span').addClass('page-link');
            $('.pagination').addClass('justify-content-center');
        }

        $(window).ready(function(){
            addClassPaging();
        });
    </script>

@endpush