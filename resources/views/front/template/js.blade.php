
    <!-- Optional JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!--    <script src="{{asset('vendor/crudbooster/assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>-->
    <script src='{{asset('vendor/crudbooster/assets/select2/dist/js/select2.full.min.js')}}'></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset("js/bootstrap.min.js")}}"></script>
    <script src="{{asset("js/wow.js")}}"></script>
    <script src="{{asset("js/swiper.min.js")}}"></script>
    <script src="{{ asset('vendor/crudbooster/assets/lightbox/dist/js/lightbox.min.js') }}"></script>
    <script src="{{asset('star/src/jquery.star-rating-svg.js')}}"></script>
    <script src="{{asset('vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script type="text/javascript">
        /**
         * [Make same heigt footer]
         * var heightdiv [default 0] for declare if max height next is on looping
         * set navigation stay on top
         */
        $(document).ready(function(){
            var heightdiv = 0;
            $('.footer-border').each(function(){
                var height = $(this).height();
                if (heightdiv < height) {
                    heightdiv = height;
                }
            });
            $('.footer-border').height(heightdiv);

            let scroll = $('#navigation').scrollTop();
        });

        /**
         * [myMap description]
         * for callback google maps location in center Gedung Bayt Al-Qur`an & Museum Istiqlal
         */
        function myMap() {
            var myCenter = new google.maps.LatLng(-6.3030329,106.887909);
            var mapCanvas = document.getElementById("mapsFooter");
            var mapOptions = {center: myCenter, zoom: 15};
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({position:myCenter});
            marker.setMap(map);
            map.setOptions({
                draggable: false,
                zoomControl: false,
                crollwheel: false,
                disableDoubleClickZoom: false
            });

            $.ajax({
                url:"{{ CRUDBooster::mainpath('location') }}",
                method:"GET",
                data:{
                    "_token":"{{csrf_token()}}",
                },
                success:function (data) {
                    kok=[];
                    kok=data;
                    // console.log(data);
                    var myLatLng = {lat: -1.7593717, lng: 113.4055126};
                    var map = new google.maps.Map(document.getElementById('mapsPenerbit'), {
                        zoom: 5,
                        center: myLatLng
                    });

                    var count;

                    for (count = 0; count < data.length; count++) {
                        var infowindow=new google.maps.InfoWindow();
                        id=data[count]['id'];
                        var id =new google.maps.Marker({
                            position: new google.maps.LatLng(Number(data[count]['lat']), Number(data[count]['lng'])),
                            map: map,
                            title: data[count]['name']
                        });

                        google.maps.event.addListener(id, 'click', (function(id, count) {
                            return function() {

                            var map = new google.maps.Map(document.getElementById('mapsPenerbit'), {
                                zoom: 17,
                                center: {
                                        lat:Number(data[count]['lat']),
                                        lng:Number(data[count]['lng']),
                                }
                            });
                            var marker = new google.maps.Marker({
                                position: {
                                        lat:Number(data[count]['lat']),
                                        lng:Number(data[count]['lng']),
                                },
                                map: map,
                                title: data[count]['name'],
                                pixelOffset: new google.maps.Size(100,140),
                            });

                            infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['phone']+"</small>");
                            infowindow.open(map, marker);

                            google.maps.event.addListener(marker, 'mouseover', (function(marker, count) {
                                return function() {
                                infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['[phone]']+"</small>");
                                infowindow.open(map, marker);
                                }
                            })(marker, count));

                            }
                        })(id, count));

                        google.maps.event.addListener(id, 'mouseover', (function(id, count) {
                            return function() {
                            infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['phone']+"</small>");
                            infowindow.open(map, id);
                            }
                        })(id, count));
                    }

                },
                error:function (data) {
                    console.log(data);
                }
            });
        }

        /**
         * [make animation navbar fixed top]
         */
        $(window).on("scroll",function(){
            var wn = $(window).scrollTop();
            if(wn > 50){
                $("#navigation").css("margin-top","0");
            }
            else{
                $("#navigation").css("margin-top","0px");
            }
        });

        /**
         * [make animation animate.css]
         */
        wow = new WOW({
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
                // console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        });
        wow.init();

        /**
         * [switch modal]
         */
        $('.switch-modal').on('click',function(){
            let target = $(this).data('target');
            $(target).modal('show');
        })

        $(function () {
            $(".my-rating-9").starRating({
                starShape: 'rounded',
                starSize: 40,
                initialRating: 0,
                disableAfterRate: false,
                emptyColor: '#DEDEDE',
                hoverColor: '#EDBD19',
                strokeColor: '#EDBD19',
                activeColor: '#EDBD19',
                disableAfterRate: false,
                onHover: function(currentIndex, currentRating, $el){
                    // $('.live-rating').text(currentIndex);
                    if (currentIndex < 1) {
                        $(".text-rating").text("Tidak Puas");
                    }else if(currentIndex >2 &&  currentIndex <=3){
                        $(".text-rating").text("Kurang Puas");
                    }else if(currentIndex > 3){
                        $(".text-rating").text("Puas");
                    }
                    $("#jumlah_rating").val(currentIndex);
                },
                onLeave: function(currentIndex, currentRating, $el){
                    // $('.live-rating').text(currentRating);
                    if (currentRating < 1) {
                        $(".text-rating").text("Tidak Puas");
                    }else if(currentRating >2 &&  currentRating <=3){
                        $(".text-rating").text("Kurang Puas");
                    }else if(currentRating > 3){
                        $(".text-rating").text("Puas");
                    }
                    $("#jumlah_rating").val(currentRating);

                }
            });
        });

        $("#simpan_rating").click(function () {
            if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
                return swal("Gagal","Harap untuk memilih rating dahulu","warning");
            }else{
                $.ajax({
                    url:"{{url('postrating')}}",
                    method:"POST",
                    data:{
                        "_token":"{{csrf_token()}}",
                        "id_cms_users": $("#id_cms_users_rating").val(),
                        "jumlah_rating": $("#jumlah_rating").val(),
                    },
                    success:function (params) {
                        if (params.status=="ok") {
                            swal("Success","Trima kasih atas masukkannya","success");
                        }else{
                            swal("Gagal","Mohon coba beberapa saat lagi","error");
                        }
                        $("#modalRating").modal("hide");
                        $("#jumlah_rating").val("");
                        location.reload();
                    },
                    error:function (params) {
                        $("#modalRating").modal("hide");
                        console.log(params);
                    }
                })
            }
        })

        function CekRatingStar() {
            if ($("#jumlah_rating").val()=="" || $("#jumlah_rating").val()==0.0) {
                return swal("Gagal","Harap untuk memilih rating dahulu","warning");
                return false;
            }
            return true;
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{Config::get('kemenag.google_apikey')}}&callback=myMap"></script>
