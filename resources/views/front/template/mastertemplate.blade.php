<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{asset('image/kemenag.ico')}}">

    @include('front/template/css')

    <?php
      $segment = (Request::segment(1) == '' ? 'Beranda' : ucwords(str_replace('-', ' ', Request::segment(1))));
    ?>
    <title>{{Config::get('kemenag.web_name')}} - {{$segment}}</title>

    @stack('css')

  </head>
  <body>

    @include('front/template/header')

    @yield('content')

    @include('front/template/footer')

    @include('front/template/js')

    @stack('js')

    <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '75004205-45e7-4cfb-bcdb-49492c0e0396', f: true }); done = true; } }; })();</script>
  </body>
</html>