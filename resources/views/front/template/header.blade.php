<header class="animated infinite slideInDown" style="padding-top: 10px;">
	<div class="container">
		
	</div>
</header>

<nav id="navigation" class="navbar navbar-expand-lg navbar-light bg-light fixed-top animated infinite slideInDown">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-lg-5 col-md-12 col-sm-12 title-header">
						<img src="{{asset('image/kemenag_logo.png')}}" class="float-left img-header">
						<h4>Layanan Tashih Online</h4>
						<h5>Lajnah Pentashihan Mushaf Al-Qur'an</h5>
						<h5>Badan Litbang dan Diklat Kementerian Agama Republik Indonesia</h5>
					</div>
                    <div class="col-lg-4 col-md-12 col-sm-12 ml-auto pt-4" id="box-search-res">
                        <form action="{{ url('search/category') }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            <div class="row">
                                <div class="col-md-4 pl-0">
                                    <input type="text" name="text_search" id="text_search" class="form-control" placeholder="Kata Kunci Pencarian" value="{{ Kemenag::gSession('search_text') }}" required>
                                </div>
                                <div class="col-md-5 pl-0 pr-0">
                                    <div class="form-group">
                                        <select name="type_search" id="type_search" class="form-control" required>
                                            <option value="">Kategori Pencarian</option>
                                            <option value="Umum" @if(Kemenag::gSession('type_search') == 'all') selected @endif>Umum (Seluruh Kategori)</option>
                                            <option value="Info Seputar Lajnah" @if(Kemenag::gSession('type_search') == 'info_seputar') selected @endif>Info Seputar Lajnah</option>
                                            <option value="Info Layanan Pentashihan" @if(Kemenag::gSession('type_search') == 'info_layanan') selected @endif>Info Layanan Pentashihan</option>
                                            <option value="Siaran Pers" @if(Kemenag::gSession('type_search') == 'siaran_pers') selected @endif>Siaran Pers</option>
                                            <option value="Informasi Penerbit" @if(Kemenag::gSession('type_search') == 'info_penerbit') selected @endif>Informasi Penerbit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pl-0">
                                    <button type="submit" class="btn btn-cari">CARI</button>
                                </div>
                            </div>
                        </form>
                    </div>
					<div class="col-lg-3 col-md-12 col-sm-12 ml-auto" id="kotak-login-res">
						@if(CRUDBooster::myId() == '')
							<button id="btnLogin" class="btn float-right" data-toggle="modal" data-target="#modalLogin" aria-haspopup="true" aria-expanded="false" style="margin-top: 20px;">LOGIN</button>
							<a id="btnRegistrasi" class="btn float-right" href="{{(Request::segment(1) != 'pendaftaran-penerbit' ? url('pendaftaran-penerbit') : 'javascript:vid(0)')}}" style="margin-top: 20px;">REGISTRASI</a>
						@else
							<a href="{{asset('admin')}}" target="_blank" onclick="return confirm('Ingin masuk ke panel admin?')">
								<div class="row">
									<div class="col-sm-7 ml-auto">
										<label class="hy-login float-right">
											Halo,<br>
											<span class="name-user" style="text-align: right;">{{substr(CRUDBooster::myName(), 0, 20)}}</span>
										</label>
									</div>
									<div class="col-sm-4 box-user-image">
										<span class="float-left">
											{{-- <i class="fa fa-sign-in-alt float-right"></i> --}}
											<div class="thumbnail float-right img-user" style="">
												<img src="{{CRUDBooster::myPhoto()}}">
											</div>
										</span>
										
									</div>
								</div>
							</a>
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-12">

                <div class="d-flex mt-3">
                    <div class="wrapper">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <button class="navbar-toggler ml-auto" type="button" data-toggle="modal" data-target="#modalCategorySearch">
                        <span class="fa fa-search"></span>
                    </button>
                </div>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item @if(Kemenag::getMenu() == 'beranda') active @endif">
							<a class="nav-link" href="{{url('/')}}">BERANDA</a>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'pentashihan') active @endif dropdown">
							<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pentashihanNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							PENTASHIHAN
							</a>
							<div class="dropdown-menu" aria-labelledby="pentashihanNavigation">
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'regulasi') active @endif" href="{{url('regulasi')}}">REGULASI PENTASHIHAN</a>
								<div class="dropdown-divider"></div>
								@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'pendaftaranPenerbit') active @endif" href="{{url('pendaftaran-penerbit')}}">PENDAFTARAN PENERBIT AL-QUR’AN</a>
								@if(CRUDBooster::myId() != '')
								<div class="dropdown-divider"></div>
								@endif
								@endif
								{{--<a class="dropdown-item" href="{{url('persyaratan-pengajuan-tashih')}}">PERSYARATAN PENGAJUAN TASHIH</a>
								<div class="dropdown-divider"></div>--}}
								@if(CRUDBooster::myId() != '')
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'pengajuanTashih') active @endif" href="{{url('pengajuan-tashih')}}">PERMOHONAN SURAT TANDA TASHIH/IZIN EDAR</a>
								{{--<div class="dropdown-divider"></div>--}}
                                {{--@php
									$kueriasd=DB::table('jawaban_survey')
									->where('id_cms_users',CRUDBooster::myId())
									->first();
								@endphp
								@if(empty($kueriasd->id))
								<a class="dropdown-item" href="{{url('survey')}}">SURVEY</a>
								<div class="dropdown-divider"></div>
								@endif--}}
								@endif
								{{--<a class="dropdown-item" href="{{url('petunjuk-teknis-pencetakan')}}">PETUNJUK TEKNIS PENCETAKAN</a>--}}
							</div>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'pelaporanmushafbermasalah') active @endif">
							<a class="nav-link" href="{{url('aduan-mushaf-bermasalah')}}">ADUAN MUSHAF BERMASALAH</a>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'cektandatashih') active @endif">
							<a class="nav-link" href="{{url('cek-tanda-tashih')}}">CEK TANDA TASHIH</a>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'listsiaranpers') active @endif">
							<a class="nav-link" href="{{url('list-siaran-pers')}}">SIARAN PERS</a>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'info') active @endif dropdown">
							<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="infoNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								INFO
							</a>
							@php
								$cek_login=DB::table('cms_users')
								->where('id',CRUDBooster::myId())
								->first();
							@endphp
							<div class="dropdown-menu" aria-labelledby="infoNavigation">
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'info-lajnah') active @endif" href="{{url('info-seputar-lajnah')}}">INFO SEPUTAR LAJNAH</a>
								<div class="dropdown-divider"></div>
								{{--@if($cek_login->id_cms_privileges==2)--}}
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'info-penerbit') active @endif" href="{{url('info-penerbit')}}">INFO PENERBIT</a>
								<div class="dropdown-divider"></div>
								{{--@endif--}}
								<!--<a class="dropdown-item" href="{{url('proses-pentasihan')}}">PROSES PENTASHIHAN</a>-->
								<!--<div class="dropdown-divider"></div>-->
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'info-layanan') active @endif" href="{{url('info-layanan-pentashihan')}}">INFO LAYANAN PENTASHIHAN</a>
							</div>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'forum') active @endif dropdown">
							<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pertanyaanNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								FORUM
							</a>
							<div class="dropdown-menu" aria-labelledby="pertanyaanNavigation">
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'forum-pertanyaan') active @endif" href="{{url('forum/pertanyaan')}}">PERTANYAAN</a>
								@if(CRUDBooster::myId() != '')
								<div class="dropdown-divider"></div>
								<a class="dropdown-item @if(Kemenag::getMenuSub() == 'forum-konsultasi') active @endif" href="{{url('forum/ ')}}">KONSULTASI</a>
								@endif
							</div>
						</li>
						<li class="nav-item @if(Kemenag::getMenu() == 'faq') active @endif">
							<a class="nav-link" {{--href="#"--}} href="{{url('faq')}}">FAQ</a>
						</li>
						{{--<li class="nav-item @if(Kemenag::getMenu() == 'faq') active @endif dropdown">
							<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pertanyaanNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								FAQ
							</a>
							<div class="dropdown-menu" aria-labelledby="pertanyaanNavigation">
								<a class="dropdown-item" href="{{url('faq/faq-pertanyaan')}}">PERTANYAAN</a>
								@if(CRUDBooster::myId() != '')
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{url('faq/faq-konsultasi')}}">KONSULTASI</a>
								@endif
							</div>
						</li>--}}
						<li class="nav-item @if(Kemenag::getMenu() == 'downloadmateri') active @endif">
							<a class="nav-link" href="{{url('download-materi')}}">DOWNLOAD MATERI</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</nav>

<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  		<form action="{{asset('admin/login')}}" method="POST">
	      	<div class="row">
	      		<div class="col-md-4 ml-auto">
	  					<button type="button" class="close modal-close pull-right" data-dismiss="modal">&times;</button>
	      		</div>
      			<div class="col-md-12">
      				<h3 class="label-modal" style="">LOGIN</h3>
      			</div>
	      	</div>
	        <div class="row">
	        	<div class="col-sm-10 mr-auto ml-auto form-modal">
	        		<div class="form-group">
	        			<input type="text" class="form-control" name="email" placeholder="Email / Kode ID" required="">
	        		</div>
	        		<div class="form-group">
	        			<input type="password" class="form-control" name="password" placeholder="Password" required="">
	        		</div>
	        	</div>
	        </div>
	        <center>
	        	<button type="submit" class="btn btn-success" name="submit" value="LOGIN">LOGIN</button>
	        	<p class="action-modal">
	        		Belum memiliki akun Terdaftar ? 
	        		<a href="{{(Request::segment(1) != 'pendaftaran-penerbit' ? url('pendaftaran-penerbit') : 'javascript:vid(0)')}}" class="switch-modal" @if(Request::segment(1) == 'pendaftaran-penerbit') data-dismiss="modal" @endif data-target="">DAFTAR</a>
	        	</p>
	        	<p class="action-modal"><a href="javascript:void(0)" class="switch-modal" data-dismiss="modal" data-target="#modalForgot">Lupa Password?</a></p>
	        </center>
	        <input type="hidden" name="_token" value="{{csrf_token()}}">
	  		</form>
      </div>
    </div>
  </div>
</div><!-- ./ Modal Login -->

<!-- Modal Forgot -->
<div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<form action="{{url('auth/forgot-password')}}" method="POST">
	      	<div class="row">
	      		<div class="col-md-4 ml-auto">
	  					<button type="button" class="close modal-close pull-right" data-dismiss="modal">&times;</button>
	      		</div>
      			<div class="col-md-12">
      				<h3 class="label-modal" style="">LUPA PASSWORD</h3>
      			</div>
	      	</div>
	        <div class="row">
	        	<div class="col-sm-10 mr-auto ml-auto form-modal">
	        		<div class="form-group">
	        			<input type="email" class="form-control" name="email" placeholder="Email" required="">
	        		</div>
	        	</div>
	        </div>
	        <center>
	        <button type="submit" class="btn btn-success" name="submit" value="SUBMIT">SUBMIT</button>
	        	<p class="action-modal">Sudah memiliki Akun ? 
	        		<a href="javascript:void(0)" class="switch-modal" data-dismiss="modal" data-target="#modalLogin">LOGIN</a>
	        	</p>
	        </center>

	        <input type="hidden" name="_token" value="{{csrf_token()}}">
	      </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Kategori Search -->
<div class="modal fade" id="modalCategorySearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xs" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="row">
                        <div class="col-md-4 ml-auto">
                            <button type="button" class="close modal-close pull-right" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="col-md-12">
                            <h3 class="label-modal" style="">Pencarian Kategori</h3>
                        </div>
                    </div>
                    <div class="row col-sm-10 mr-auto ml-auto form-modal">
                        <div class="col-md-4 mb-3">
                            <input type="text" name="text_search" id="text_search" class="form-control" placeholder="Kata Kunci Pencarian" value="{{ Kemenag::gSession('search_text') }}" required>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="type_search" id="type_search" class="form-control" required>
                                    <option value="">Kategori Pencarian</option>
                                    <option value="Umum" @if(Kemenag::gSession('type_search') == 'all') selected @endif>Umum</option>
                                    <option value="Info Seputar Lajnah" @if(Kemenag::gSession('type_search') == 'info_seputar') selected @endif>Info Seputar Lajnah</option>
                                    <option value="Info Layanan Pentashihan" @if(Kemenag::gSession('type_search') == 'info_layanan') selected @endif>Info Layanan Pentashihan</option>
                                    <option value="Siaran Pers" @if(Kemenag::gSession('type_search') == 'siaran_pers') selected @endif>Siaran Pers</option>
                                    <option value="Informasi Penerbit" @if(Kemenag::gSession('type_search') == 'info_penerbit') selected @endif>Informasi Penerbit</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <center>
                        <button type="submit" class="btn btn-success" name="submit">CARI</button>
                    </center>
                </form>
            </div>
        </div>
    </div>
</div>


<br><br><br>