<div class="container footerWeb pl-0 pr-0">
	<div class="row">
		<div class="col-lg-4 col-md-12 col-sm-12 border-right pl-0 pr-0">
			<div class="footer-content">
    		    <div class="mapsFooter" id="mapsFooter"></div>
    		    {{--<h3 class="footer-title">PETA PENERBIT MUSHAF AL-QUR'AN INDONESIA</h3>
    		    <div class="mapsFooter" id="mapsPenerbit"></div>--}}
				<p class="line-height-footer-bold">{{Config::get('kemenag.web_name')}}</p>
  			    <p class="line-height-footer">{!! nl2br(CRUDBooster::getSetting('alamat')) !!}</p>
			</div>
		</div>
		<div class="col-lg-5 col-md-12 col-sm-12 border-right pl-0 pr-0">
			<h3 class="footer-title">TENTANG KAMI</h3>
			<div class="footer-content">
  			    <p style="text-align: justify;">{!! nl2br(CRUDBooster::getSetting('tentang_kami')) !!}</p>
			</div>
		</div>
		<div class="col-lg-3 col-md-12 col-sm-12 pl-0 pr-0">
			<h3 class="footer-title">KONTAK KAMI</h3>
			<div class="footer-content">
				<form action="{{asset('auth/contact-us')}}" method="POST" enctype="multipart/form-data" onsubmit="return submitUserForm()">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" name="nama" placeholder="Tulis nama Anda di sini" value="{{old('nama')}}" required="" class="form-control">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" placeholder="Tulis email Anda di sini" value="{{old('email')}}" required="" class="form-control">
					</div>
					<div class="form-group">
						<label>Pesan</label>
						<textarea rows="5" name="pesan" required="" class="form-control"  placeholder="Tulis pesan Anda di sini">{{old('pesan')}}</textarea>
					</div>
					<div class="form-group">
    					<div class="g-recaptcha" data-sitekey="6Lc7qq0UAAAAAG46EDUrpyzMU2kczIYG065psVYI"></div>
    					<div id="g-recaptcha-error"></div>
            		</div>
					<button id="btnFooter" type="submit" class="btn btn-success">KIRIM</button>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
				</form>
			</div>
		</div>
	</div>
</div>
<div id="footerCopyRight">
    <h6>Lajnah Pentashihan Mushaf Al-Qur’an. Badan Litbang dan Diklat. Kementerian Agama Republik Indonesia © {{date('Y')}}</h6>
</div>

<div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <br><br>
    <br><br>
    <br><br>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body" style="font-family: 'Comic Sans MS', arial;text-align:center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="text-green">Apakah anda terbatu dengan adanya aplikasi ini ?</label>
                            <br><br>
                            <span class="my-rating-9"></span>
                            <br>
                            <span class="text-rating verysmall-green"></span>
                            <br>
                            <input type="hidden" name="id_cms_users_rating" id="id_cms_users_rating" value="{{CRUDBooster::myId()}}">
                            <input type="hidden" name="jumlah_rating" id="jumlah_rating">
                        </div>
                        <div class="form-group">
                            <button type="submit" id="simpan_rating" class="btn btn-success btn-green">Rate Us</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(!empty(CRUDBooster::myId()))
@php
$tablerating=DB::table("rating")
->where("id_cms_users",CRUDBooster::myId())
->first();
@endphp
@if(empty($tablerating->id_cms_users))
<!-- <div id="rating">
	<span id="rating-text" class="animated infinite-satu tada slow delay-1s">
		Rating Us
		<div id="arrow-down"></div>
	</span>
	<button id="btn-rating-us" data-toggle="modal" data-target="#modalRating">
		<span class="fa fa-star"></span>
	</button>
</div> -->
@endif
@else
<a href="{{(Request::segment(1) != 'pendaftaran-penerbit' ? url('pendaftaran-penerbit') : 'javascript:vid(0)')}}" class="animated fadeIn slow" id="myBtn2" title="Registrasi">DAFTAR</a>
<a href="#" id="myBtn" class="animated fadeIn slow" class="btn float-right" data-toggle="modal" data-target="#modalLogin" aria-haspopup="true" aria-expanded="false">LOGIN</a>
@endif

@push('js')
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<script>
    function submitUserForm() {
        var response = grecaptcha.getResponse();
        if(response.length == 0) {
            document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">This field is required.</span>';
            return false;
        }
        return true;
    }

    function verifyCaptcha() {
        document.getElementById('g-recaptcha-error').innerHTML = '';
    }
</script>
@if(Session::get('auth_message') != '')
<script type="text/javascript">
	alert('{{Session::get('auth_message')}}')
</script>
@endif
@endpush

<style>
    #arrow-down {
        width: 0;
        height: 0;
        border-left: 16px solid transparent;
        border-right: 16px solid transparent;
        border-top: 16px solid #00923F;
        margin:auto;
    }
    #rating-text{
        font-family: 'Comic Sans MS', arial;
        position: absolute;
        width: 100px;
        height: 33px;
        bottom: 67px;
        right: -20px;
        background-color: #00923F;
        border-radius: 13px;
        color: #fff;
        text-align: center;
        font-size: 12px;
        font-weight: 700;
        padding: 5px;
    }
    #rating{
        position: fixed;
        bottom: 20px;
        right: 100px;
        z-index: 99;
    }
    #btn-rating-us {
        font-size: 24px;
        border: none;
        outline: none;
        background-color: #00923F;
        color: #fff;
        cursor: pointer;
        padding: 13px 17px;
        /*border-radius: 50%;*/
        font-weight:700;
        overflow:none;
    }
    #myBtn {
        position: fixed;
        bottom: 25px;
        right: 330px;
        z-index: 99;
        font-size: 14px;
        border: none;
        outline: none;
        background-color: #00923F;
        color: #FFF;
        cursor: pointer;
        padding: 10px 15px;
        border-radius: 10px;
        border: solid 3px #00923F;
        font-weight:700;
        display:none;
    }

    #myBtn2 {
        position: fixed;
        bottom: 25px;
        right: 200px;
        z-index: 99;
        font-size: 14px;
        border: none;
        outline: none;
        background-color: #FFF;
        color: #3B3B3B;
        cursor: pointer;
        padding: 10px 15px;
        border-radius: 10px;
        border: solid 3px #00923F;
        font-weight:700;
        display:none;
    }

    @media only screen and (max-width: 992px) {
        #myBtn,#myBtn2 {
            display:block;
        }

        #kotak-login-res{
            display:none;
        }

        #box-search-res {
            display:none;
        }
    }
</style>