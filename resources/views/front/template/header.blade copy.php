<header class="animated infinite slideInDown" style="padding-top: 10px;">
	
</header>

<nav id="navigation" class="navbar navbar-expand-lg navbar-light bg-light fixed-top animated infinite slideInDown">
<div class="container">
		<div class="row">
			<div class="col-sm-8 title-header">
				<img src="{{asset('image/kemenag_logo.png')}}" class="float-left img-header">
				<h4>Layanan Tashih Online</h4>
				<h5>Lajnah Pentashihan Mushaf Al-Qur'an</h5>
				<h5>Badan Litbang dan Diklat Kementerian Agama Republik Indonesia</h5>
			</div>
			<div class="col-sm-4 ml-auto">
				@if(CRUDBooster::myId() == '')
					<button id="btnLogin" class="btn float-right" data-toggle="modal" data-target="#modalLogin" aria-haspopup="true" aria-expanded="false" style="margin-top: 20px;">LOGIN</button>
					<a id="btnRegistrasi" class="btn float-right" href="{{(Request::segment(1) != 'pendaftaran-penerbit' ? url('pendaftaran-penerbit') : 'javascript:vid(0)')}}" style="margin-top: 20px;">REGISTRASI</a>
				@else
					<a href="{{asset('admin')}}" target="_blank" onclick="return confirm('Ingin masuk ke panel admin?')">
						<div class="row">
							<div class="col-sm-7 ml-auto">
								<label class="hy-login float-right">
									Halo,<br>
									<span class="name-user" style="text-align: right;">{{substr(CRUDBooster::myName(), 0, 20)}}</span>
								</label>
							</div>
							<div class="col-sm-4 box-user-image">
								<span class="float-left">
									{{-- <i class="fa fa-sign-in-alt float-right"></i> --}}
									<div class="thumbnail float-right img-user" style="">
										<img src="{{CRUDBooster::myPhoto()}}">
									</div>
								</span>
								
							</div>
						</div>
					</a>
				@endif
			</div>
		</div>
	</div>
	<div class="container">
		{{-- <a class="navbar-brand" href="javascript:void(0)"><i class="fa fa-bars"></i></a> --}}
  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
    	<ul class="navbar-nav mr-auto">
      	<li class="nav-item @if(Kemenag::getMenu() == 'beranda') active @endif ">
        	<a class="nav-link" href="{{url('/')}}">BERANDA</a>
      	</li>
      	<li class="nav-item @if(Kemenag::getMenu() == 'pentashihan') active @endif dropdown">
        	<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pentashihanNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          	PENTASHIHAN
        	</a>
	        <div class="dropdown-menu" aria-labelledby="pentashihanNavigation">
						<a class="dropdown-item" href="{{url('regulasi')}}">REGULASI</a>
						<div class="dropdown-divider"></div>
						@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
						<a class="dropdown-item" href="{{url('pendaftaran-penerbit')}}">PENDAFTARAN PENERBIT</a>
						<div class="dropdown-divider"></div>
						@endif
						<a class="dropdown-item" href="{{url('persyaratan-pengajuan-tashih')}}">PERSYARATAN PENGAJUAN TASHIH</a>
						<div class="dropdown-divider"></div>
						@if(CRUDBooster::myId() != '')
						<a class="dropdown-item" href="{{url('pengajuan-tashih')}}">PENGAJUAN TASHIH</a>
						<div class="dropdown-divider"></div>
						@endif
						<a class="dropdown-item" href="{{url('petunjuk-teknis-pencetakan')}}">PETUNJUK TEKNIS PENCETAKAN</a>
	        </div>
      	</li>
				<li class="nav-item @if(Kemenag::getMenu() == 'pelaporanmushafbermasalah') active @endif ">
					<a class="nav-link" href="{{url('aduan-mushaf-bermasalah')}}">ADUAN MUSHAF BERMASALAH</a>
				</li>
				<li class="nav-item @if(Kemenag::getMenu() == 'cektandatashih') active @endif ">
					<a class="nav-link" href="{{url('cek-tanda-tashih')}}">CEK TANDA TASHIH</a>
				</li>
      	<li class="nav-item @if(Kemenag::getMenu() == 'info') active @endif  dropdown">
        	<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="infoNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          	INFO
	        </a>
	        <div class="dropdown-menu" aria-labelledby="infoNavigation">
						<a class="dropdown-item" href="{{url('info-seputar-lajnah')}}">INFO SEPUTAR LAJNAH</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="{{url('galeri-mushaf-alquran')}}">GALERI MUSHAF AL-QUR'AN</a>
	        </div>
		    </li>
		    <li class="nav-item @if(Kemenag::getMenu() == 'forum') active @endif  dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:void(0)" id="pertanyaanNavigation" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						FORUM
					</a>
					<div class="dropdown-menu" aria-labelledby="pertanyaanNavigation">
						<a class="dropdown-item" href="{{url('forum/pertanyaan')}}">PERTANYAAN</a>
						@if(CRUDBooster::myId() != '')
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="{{url('forum/konsultasi')}}">KONSULTASI</a>
						@endif
					</div>
				</li>
	    </ul>
		</div>
	</div>
</nav>

<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body">
	  		<form action="{{asset('admin/login')}}" method="POST">
	      	<div class="row">
	      		<div class="col-md-4 ml-auto">
	  					<button type="button" class="close modal-close pull-right" data-dismiss="modal">&times;</button>
	      		</div>
      			<div class="col-md-12">
      				<h3 class="label-modal" style="">LOGIN</h3>
      			</div>
	      	</div>
	        <div class="row">
	        	<div class="col-sm-10 mr-auto ml-auto form-modal">
	        		<div class="form-group">
	        			<input type="text" class="form-control" name="email" placeholder="Email / Kode ID" required="">
	        		</div>
	        		<div class="form-group">
	        			<input type="password" class="form-control" name="password" placeholder="Password" required="">
	        		</div>
	        	</div>
	        </div>
	        <center>
	        	<button type="submit" class="btn btn-success" name="submit" value="LOGIN">LOGIN</button>
	        	<p class="action-modal">
	        		Belum memiliki akun Terdaftar ? 
	        		<a href="{{(Request::segment(1) != 'pendaftaran-penerbit' ? url('pendaftaran-penerbit') : 'javascript:vid(0)')}}" class="switch-modal" @if(Request::segment(1) == 'pendaftaran-penerbit') data-dismiss="modal" @endif data-target="">REGISTER</a>
	        	</p>
	        	<p class="action-modal"><a href="javascript:void(0)" class="switch-modal" data-dismiss="modal" data-target="#modalForgot">Forgot Password?</a></p>
	        </center>
	        <input type="hidden" name="_token" value="{{csrf_token()}}">
	  		</form>
      </div>
    </div>
  </div>
</div><!-- ./ Modal Login -->

<!-- Modal Forgot -->
<div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body">
      	<form action="{{url('auth/forgot-password')}}" method="POST">
	      	<div class="row">
	      		<div class="col-md-4 ml-auto">
	  					<button type="button" class="close modal-close pull-right" data-dismiss="modal">&times;</button>
	      		</div>
      			<div class="col-md-12">
      				<h3 class="label-modal" style="">FORGOT PASSWORD</h3>
      			</div>
	      	</div>
	        <div class="row">
	        	<div class="col-sm-10 mr-auto ml-auto form-modal">
	        		<div class="form-group">
	        			<input type="email" class="form-control" name="email" placeholder="Email" required="">
	        		</div>
	        	</div>
	        </div>
	        <center>
	        <button type="submit" class="btn btn-success" name="submit" value="SUBMIT">SUBMIT</button>
	        	<p class="action-modal">Sudah memiliki Akun ? 
	        		<a href="javascript:void(0)" class="switch-modal" data-dismiss="modal" data-target="#modalLogin">LOGIN</a>
	        	</p>
	        </center>

	        <input type="hidden" name="_token" value="{{csrf_token()}}">
	      </form>
      </div>
    </div>
  </div>
</div>