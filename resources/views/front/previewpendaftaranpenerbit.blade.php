@extends('front/template/mastertemplate')

@section('content')
<style>
	.form-check-label{
		font-size: 12px;
	}

	.medium-bold-green{
	color: #000000;
	font-size: 16px;
	font-weight: bold;
	}


</style>


	<section id="pentashihan" class="master container">
		<div class="row">
			<!-- <div class="content-breadcrumb col-sm-12 wow animated fadeInLeft">
				<p>PENTASIHAN/PENDAFTARAN PENERBIT</p>
			</div> -->
			
			<div id="contentInformation" class="col-sm-10 mr-auto ml-auto wow animated fadeInBottom animated" style="display: none;">
				<p align="center" class="no-margin" style="">
					{{Kemenag::message()}}
				</p>
			</div> 

			<div id="content" class="col-sm-12 form-shadow">
						<center>
						<p class="medium-bold-green" style="margin-top:25px;font-size:26px;">Preview Pendaftaran Penerbit Al-Qur’an</p>
						</center>

					<div class="col-12">
						<h3 style="margin-bottom:50px;margin-top:50px;">Informasi Data Penerbit</h3>
					</div>	

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Penerbit</label>
						<div class="col-sm-10">:
							{{$row->name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Alamat Penerbit</label>
						<div class="col-sm-10">:
							{{$row->address}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Provinsi</label>
						<div class="col-sm-10">:
							{{$row->province_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Kabupaten / Kota</label>
						<div class="col-sm-10">:
							{{$row->city_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Kecamatan</label>
						<div class="col-sm-10">:
							{{$row->district_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Kode Pos</label>
						<div class="col-sm-10">:
							{{$row->kode_pos}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">No. Telp Penerbit</label>
						<div class="col-sm-10">:
							{{$row->phone}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Email Penerbit</label>
						<div class="col-sm-10">:
							{{$row->email}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Jenis Lembaga</label>
						<div class="col-sm-10">:
							{{$row->jenis_lembaga_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">NIB</label>
						<div class="col-sm-10">:
							{{$row->nib_value}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">NPWP</label>
						<div class="col-sm-10">:
							{{$row->npwp_value}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Password</label>
						<div class="col-sm-10">:
							{{$row->password_value}}
						</div>
					</div>

					<div class="col-12">
						<h3 style="margin-bottom:50px;margin-top:50px;">Informasi Penanggung jawab Penerbit</h3>
					</div>	

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Penanggung Jawab</label>
						<div class="col-sm-10">:
							{{$row->pic}}
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Alamat Penanggung Jawab</label>
						<div class="col-sm-10">:
							{{$row->pic_address}}
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Jabatan Penanggung Jawab</label>
						<div class="col-sm-10">:
							{{$row->pic_jabatan}}
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Telp Penanggung Jawab</label>
						<div class="col-sm-10">:
							{{$row->pic_phone}}
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Email Penanggung Jawab</label>
						<div class="col-sm-10">:
							{{$row->pic_email}}
						</div>
					</div>	

					<div class="col-12">
						<h3 style="margin-bottom:50px;margin-top:50px;">Informasi Dokumen Legalitas Penerbit</h3>
					</div>	

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">SIUP dan TDP</label>
						<div class="col-sm-10">
							@if($row->siup != '')
									<a href="{{asset($row->siup)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Akta Notaris</label>
						<div class="col-sm-10">
							@if($row->akte_notaris != '')
									<a href="{{asset($row->akte_notaris)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">NPWP</label>
						<div class="col-sm-10">
							@if($row->npwp != '')
									<a href="{{asset($row->npwp)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">NIB</label>
						<div class="col-sm-10">
							@if($row->nib != '')
									<a href="{{asset($row->nib)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">KTP Penanggung Jawab Penerbit</label>
						<div class="col-sm-10">
							@if($row->scan_ktp != '')
									<a href="{{asset($row->scan_ktp)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	
	
					<div class="col-12">
						<h3 style="margin-bottom:50px;margin-top:50px;">Surat Pendaftaran Penerbit</h3>
					</div>	

	
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Surat pendaftaran akun penerbit </label>
						<div class="col-sm-10">
							@if($row->surat_permohonan != '')
									<a href="{{asset($row->surat_permohonan)}}" target="_blank" class="btn btn-sm btn-success pull-left" style="margin-right: 10px;">Lihat</a>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>	

					<div class="col-12">

				<h4 style="margin-top:50px;"><b>Pernyataan</b></h4> <br>
				<p>
				Saya menyatakan bahwa seluruh data yang terisi dalam formulir pendaftaran penerbit Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.
				</p>

				<b>
				Lihat dan dan tinjau ulang data yang Anda isikan.
				<br>
				Jika seluruh data sudah benar, klik Registrasi Penerbit. Jika ada yang salah, klik Edit Data
				</b>

<br>
						<div class="float-right" style="margin-top:50px;">
									<a href="{{url('pendaftaran-penerbit/ralat?uid='.$_GET['uid'])}}" class="btn btn-primary">
										<i class="fa fa-pencil"></i> Edit Data
									</a>
									<a title="Simpan" href="javascript:;" onclick="insertData()" class="btn btn-success">Lanjutkan Registrasi Penerbit</a>
									<input style="display:none;" id="addBtn" type="button" value="Save" class="btn btn-success">
						</div>						
					</div>
	
			</div>
		</div>
	</section>

@endsection
@push('js')

<script>
	function insertData()
    {
        swal({
            title: "Apakah anda yakin?",
            text: "",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
					showCancelButton: true,
					confirmButtonText: "Ya",
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
        },
		function(isConfirm){

           if (isConfirm){
				$("#addBtn").click();
            } else {
                swal("Dibatalkan", "", "error"); 
            }
         });
    }

	function successInsertData(text,redirect)
    {
        swal({
            title: "",
            text: text,
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
					showCancelButton: false,
					showCancelButton: false,
					showConfirmButton: true,
					confirmButtonText: "Baik",
					// cancelButtonText: "Tutup",
					closeOnConfirm: false,
					closeOnCancel: false
        },
		function(isConfirm){
				window.location = "{{url('')}}";
         });
    }


	$('#addBtn').click(function(event)
	{
        $.ajax({
            url:"{{CRUDBooster::mainpath('insert-save')}}",
            method:"POST",
            data:
            {
                '_token': "{{ csrf_token() }}",
				'id':"{{$row->id}}"
            },

            success:function(response)
            {
                if(response.status == '200')
                {
					successInsertData(response.message,response.redirect);
                }
                else if(response.status == '500' && response.flag == 'empty_temp')
                {
					window.location = "{{CRUDBooster::mainpath('')}}";
                }
                else
                {
					swal(response.message, "", "error"); 
                }
            }
       })
	});

</script>

@endpush