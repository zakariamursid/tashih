@extends('front/template/mastertemplate')

@section('content')

<section id="newsDetail" class="container">
    <div class="content-breadcrumb">
        <p>HOME / INFO LAYANAN PENTASHIHAN</p>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 border-right">
            <h2 class="medium-bold-black">{{$gallery_mushaf->title}}</h2>
            <p class="info-news m-0 p-0">
                {{strtoupper($gallery_mushaf->admin_name)}}, {{Kemenag::dateIndonesia($gallery_mushaf->created_at)}}
            </p>
            <img src="{{$gallery_mushaf->image}}" class="img-fluid img-content-read">
            <div id="detailContent">
                {!! $gallery_mushaf->content !!}
            </div>

            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <b>Share Link</b>
                </div>

                <div class="col-lg-1 col-md-1">
                    <div style="cursor:pointer" id="facebook-share" class="share"><img class="img-responsive" src="{{asset('image/facebook.png')}}" style="width: 40px; height: auto;"></div>
                    <script type="text/javascript">
                        // Facebook Share
                        $('#facebook-share').click(function () {
                            url = location.href;
                            title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
                            window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');

                        });
                    </script>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-1 share-sosmed" style="">
                    <div style="cursor:pointer" id="google-share" class="share"><img class="img-responsive" src="{{asset('image/google-plus.png')}}" style="width: 40px; height: auto;"></div>
                    <script type="text/javascript">
                        // google Share
                        $('#google-share').click(function () {
                            url = location.href;
                            title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
                            window.open('https://plus.google.com/share?url=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                        });
                    </script>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-1 share-sosmed" style="">
                    <div style="cursor:pointer" id="twitter-share" class="share"><img class="img-responsive" src="{{asset('image/twitter.png')}}" style="width: 40px; height: auto;"></div>
                    <script type="text/javascript">
                        // google Share
                        $('#twitter-share').click(function () {
                            url = location.href;
                            title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
                            window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 side-menu">
            <h4 class="title-content">INFO LAYANAN PENTASHIHAN LAINNYA</h4>

            <div class="list-news-other">
                @foreach($info_lainnya as $row)
                <a href="{{asset('info-layanan-pentashihan/read/'.$row->slug)}}">
                    <div class="item-news-other">
                        <div class="image-news-other">
                            <img src="{{$row->image}}"" alt="Gambar">
                        </div>
                        <div class="content-news-other">
                            <h4>{{$row->title}}</h4>
                            <p>{{Kemenag::dateIndonesia($row->created_at)}}</p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="list-news-other row">
                @foreach($banner_layanan as $x => $xrow)
                <div class="col-sm-12 mb-3">
                    <a href="{{ $xrow->link }}">
                        <img src="{{ asset($xrow->image) }}" class="img-fluid">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection

@push('js')
<script type="text/javascript">
	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
	  resizeHr()
	});
</script>
@endpush
@push('css')
<style>
    @font-face {
        font-family: FontArabTashih;
        src: url('{{ asset("font/font_arab_tashih.OTF") }}');
    }
</style>
@endpush