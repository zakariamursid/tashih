@extends('front/template/mastertemplate')

@section('content')

<section id="newsDetail" class="container">
    <div class="content-breadcrumb">
        <p>HOME / INFO SEPUTAR LAJNAH</p>
    </div>

    <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 border-right">
            <h2 class="medium-bold-black">{{$info_seputar_lajnah->title}}</h2>
            <p class="info-news m-0 p-0">
                {{strtoupper($info_seputar_lajnah->admin_name)}}, {{Kemenag::dateIndonesia($info_seputar_lajnah->created_at)}}
            </p>
            <img src="{{$info_seputar_lajnah->image}}" alt="Gambar" class="img-fluid img-content-read">
            <div id="detailContent">
                {!! $info_seputar_lajnah->content !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 side-menu">
            <h4 class="title-content">INFO SEPUTAR LAJNAH LAINNYA</h4>

            <div class="list-news-other">
                @foreach($info_lainnya as $row)
                <a href="{{asset('info-seputar-lajnah/read/'.$row->slug)}}">
                    <div class="item-news-other">
                        <div class="image-news-other">
                            <img src="{{$row->image}}" alt="Gambar">
                        </div>
                        <div class="content-news-other">
                            <h4>{{$row->title}}</h4>
                            <p>{{Kemenag::dateIndonesia($row->created_at)}}</p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="list-news-other row">
                @foreach($banner_layanan as $x => $xrow)
                <div class="col-sm-12 mb-3">
                    <a href="{{ $xrow->link }}">
                        <img src="{{ asset($xrow->image) }}" class="img-fluid">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

@endsection

@push('js')

<script type="text/javascript">
	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
	  resizeHr()
	});
</script>

@endpush

@push('css')
<style>
    @font-face {
        font-family: FontArabTashih;
        src: url('{{ asset("font/font_arab_tashih.OTF") }}');
    }
</style>
@endpush