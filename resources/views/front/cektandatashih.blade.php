@extends('front/template/mastertemplate')

@section('content')

<!--	<section id="content" class="container tashih wow animated infinite fadeInUp">-->
    <section id="content" class="container tashih form-shadow mt-5 mb-5">
		<div class="row">
			<div class="col-sm-7">
				<h3 class="tashih-title">CEK TANDA TASHIH</h3>

				<form action="#" method="GET">
				  <div class="row">
				  	<div class="col-sm-9">
				  		<div class="row">
				  			<div class="col-sm-6">
							  	<label class="form-tashih-label">Cari Berdasarkan</label>
							  	<select class="form-control select2" name="berdasarkan" required="">
							  		<option value="" class="disabled"></option>
							  		<option @if(Request::get('berdasarkan') == 'Nama Penerbit') selected="" @endif value="Nama Penerbit">Nama Penerbit</option>
							  		<option @if(Request::get('berdasarkan') == 'Nama Mushaf') selected="" @endif value="Nama Mushaf">Nama Mushaf</option>
							  		<option @if(Request::get('berdasarkan') == 'Nomor Tanda Tashih') selected="" @endif value="Nomor Tanda Tashih">Nomor Tanda Tashih</option>
							  	</select>
                            </div>
						  	<div class="col-sm-6">
							  	<label class="form-tashih-label">Kata Kunci</label>
							  	<input type="text" name="kata_kunci" class="form-control" required="" value="{{Request::get('kata_kunci')}}">
                            </div>
				  		</div>
				  	</div>
				  	<div class="col-sm-3">
					  	<button type="submit" id="submitTashih" class="btn btn-success" style="">KIRIM</button>
                    </div>
				  </div>
				</form>
			</div>
		</div>
	</section>

	@php
	Session::put("berdasarkan",Request::get("berdasarkan"));
	Session::put("kata_kunci",Request::get("kata_kunci"));
	@endphp
	@if(Session::get('berdasarkan') != '' || Session::get('kata_kunci') != '')
	<section id="hasilPencarian" class="container wow animated infinite fadeInUp">

        @include("front.tabelcektandatansih")

	</section>
	@elseif($scan!="")
	<section id="hasilPencarian" class="container wow animated infinite fadeInUp">

        @include("front.tabelcektandatansih")

	</section>
	@endif

@endsection

@push('js')

	<script type="text/javascript">
	    $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                }else{
                    getData(page);

                    addClassPaging();
                }
            }
        });

        $(document).ready(function()
        {
            $(document).on('click', '.pagination a',function(event)
            {
                event.preventDefault();

                $('li').removeClass('active');
                $(this).parent('li').addClass('active');
                var myurl = $(this).attr('href');
                var page=$(this).attr('href').split('page=')[1];
                getData(page);

                addClassPaging();
            });
        });

        function getData(page){
            $.ajax({
                url: '?page=' + page,
                type: "get",
                datatype: "html"
            }).done(function(data){
                $("#hasilPencarian").empty().html(data);
                location.hash = page;
            }).fail(function(jqXHR, ajaxOptions, thrownError){
                alert('No response from server');
            });
		}
		
		$('.select2').select2({});

        function addClassPaging() {
            $('.pagination').find('li').addClass('page-item');
            $('.pagination').find('a').addClass('page-link');
            $('.pagination').find('span').addClass('page-link');
            $('.pagination').addClass('justify-content-center');
        }

        $(window).ready(function(){
            addClassPaging();
		})
	</script>

@endpush