@extends('front/template/mastertemplate')

@section('content')
<!--	<section id="hasilPencarian" class="container wow animated infinite fadeInUp">-->
    <section id="hasilPencarian" class="container mt-4">

        {{-- <div class="row"> --}}
            <div class="col-sm-12">
                @foreach($proses_pentashihan as $row)
                <div class="row border-bottom mb-5">
                    <div class="col-md-6">
                        <h5 class="tashih-pencarian">Nama Penerbit</h5>
                        <p class="tashih-detail">
                            {{ ($row->name?:'---') }}
                        </p>
                        <h5 class="tashih-pencarian">Penanggung Jawab</h5>
                        <p class="tashih-detail">
                            {{ ($row->pic?:'---') }}
                        </p>
                        <h5 class="tashih-pencarian">Telepon Penerbit</h5>
                        <p class="tashih-detail">
                            {{ ($row->phone?:'---') }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <h5 class="tashih-pencarian">Alamat</h5>
                        <p class="tashih-detail">
                            {{ ($row->address?:'---') }}
                        </p>
                        <h5 class="tashih-pencarian">Email</h5>
                        <p class="tashih-detail">
                            {{ ($row->email?:'---') }}
                        </p>
                    </div>
                    @if(count($row->datamushaf) <=0)
                    <div class="col-md-12">
                        <h5 class="tashih-pencarian">Belum ada produk yang dirilis.</h5>
                    </div>
                    @else
                    <!-- <div class="col-md-6">
                        <h5 class="tashih-pencarian">Nama Mushaf</h5>
                    </div>
                    <div class="col-md-6">
                        <h5 class="tashih-pencarian">Image Dokumen Naskah</h5>
                    </div> -->
                    {{--@foreach($row->datamushaf as $datas)
                    <div class="col-md-6 mb-2">
                        <p class="tashih-detail">
                            {{ ($datas->nama_produk?:'--') }}
                        </p>
                    </div>
                    <div class="col-md-6 mb-2">
                        <a data-lightbox="roadtrip"
                            title="{{$datas->nama_produk}}"
                            href="{{ ($datas->cover?:'') }}">
                            <img src="{{ ($datas->cover?:'') }}" width="65px">
                        </a>
                    </div>
                    @endforeach--}}
                    @endif
                    {{--<div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td style="border:none">
                                        <label class="tashih-pencarian">Nama Penerbit</label>
                                    </td>
                                    <td style="border:none">
                                        <label class="tashih-pencarian">Alamat</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border:none">
                                        <p class="tashih-detail">
                                            {{ ($row->name?:'---') }}
                                        </p>
                                    </td>
                                    <td style="border:none">
                                        <p class="tashih-detail">
                                            {{ ($row->address?:'---') }}
                                        </p>
                                    </td>
                                </tr>
                                @if(count($datamushaf) <=0)
                                <tr>
                                    <td colspan="2">
                                        <div class="col">
                                            <label class="tashih-pencarian">Belum ada produk yang dirilis</label>
                                        </div>
                                    </td>
                                </tr>
                                @else
                                <tr>
                                    <td style="border:none">
                                        <label class="tashih-pencarian">Nama Mushaf</label>
                                    </td>
                                    <td style="border:none">
                                        <label class="tashih-pencarian">Image Cover</label>
                                    </td>
                                </tr>
                                @foreach($datamushaf as $datas)
                                <tr>
                                    <td style="border:none">
                                        {{$datas->nama_produk}}
                                    </td>
                                    <td style="border:none">
                                        <a data-lightbox="roadtrip"
                                        title="{{$datas->nama_produk}}"
                                        href="{{asset($datas->gambar_cover)}}">
                                            <img src="{{asset($datas->gambar_cover)}}" width="65px">
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </table>
                        </div>
                    </div>--}}
                </div>
                <!-- <hr class="hr-pencarian"> -->
                @endforeach
            </div>
    {{-- </div> --}}

    {!! $proses_pentashihan->links() !!}

	</section>
@endsection

@push('js')
    <script type="text/javascript">
        $('.select2').select2({});

        $(window).ready(function(){
            $('.pagination').find('li').addClass('page-item');
            $('.pagination').find('a').addClass('page-link');
            $('.pagination').find('span').addClass('page-link');
            $('.pagination').addClass('justify-content-center');
        })
    </script>
@endpush