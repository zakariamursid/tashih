@extends('front/template/mastertemplate')

@section('content')
	<section class="master container">
		<div class="row">
			<div class="content-breadcrumb col-sm-12 wow animated fadeInLeft">
				<p>HOME / FAQ</p>
			</div>

			<div class="col-sm-4 wow animated fadeInLeft">
				<div id="listForum" class="form-shadow">
					<label class="small-bold-black">LIST FAQ</label>
					
					<?php $no = 0; ?>
					@foreach($forum as $row_forum)
						<?php 
							if(Request::get('id') == $row_forum->id){
								$active = 'active';
								$index = $no;
							}elseif ($no == 0 && Request::get('id') == '') {
								$active = 'active';
								$index = $no;
							}else{
								$active = '';
								$index = 0;
							}
						?>
						<div data-id="{{$row_forum->id}}" class="box-list-forum {{$active}}">
							<i class="fa fa-chevron-right"></i>
							<label class="verysmall-green">Nama Kategori</label>
							<p class="medium-black">{{$row_forum->value}}</p>
							{{--<p class="verysmall-grey" style="text-align: right;">{{Kemenag::dateIndonesia($row_forum->created_at)}}</p>--}}
						</div>
						<?php 
							$no++; 
						?>
					@endforeach

					{{$forum->render()}}
				</div>
			</div>

			<div class="col-sm-8 wow animated fadeInRight">
				<div id="content">
					<div id="Forum">
					    <div id="listJawaban">
		                @if(count($forum[$index]->jawaban) <= 0)
		                    
		                    <h2 style="text-align: center;" class="big-bold-black">List Faq masih kosong</h2>
		                @else
						
						@foreach($forum[$index]->jawaban as $row_jawaban)
						<div class="box-grey">
								<div class="head-jawaban"  data-toggle="collapse" data-target="#collapseExample{{$row_jawaban->id}}" aria-expanded="false" aria-controls="collapseExample{{$row_jawaban->id}}">
									{{--<span class="float-right small-grey">{{Kemenag::dateIndonesia($row_jawaban->created_at)}}</span>--}}
									<h4 class="medium-bold-black">{{$row_jawaban->judul}}</h4>
								</div>
								<div class="body-jawaban collapse" id="collapseExample{{$row_jawaban->id}}">
									<p class="small-grey-black">
										{!! nl2br($row_jawaban->isi) !!}
									</p>
									<p class="small-grey-black">
										Lampiran : 
										@if($row_jawaban->lampiran == '')
											<span>-</span>
										@else
											<span>
												<a href="{{asset($row_jawaban->lampiran)}}" class="text-info" target="_blank">
													<i class="fa fa-download"></i>
													Download
												</a>
											</span>
										@endif
									</p>
								</div>
							</div>
						@endforeach
						
						@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection

@push('js')
<script type="text/javascript">
generatePaginate();
	function generatePaginate(){
		$('.pagination').find('li').addClass('page-item');
		$('.pagination').find('a').addClass('page-link');
		$('.pagination').find('span').addClass('page-link');
		$('.pagination').addClass('justify-content-center');
	}

	function wrapData(page){
		var loadData = $.ajax({
		  url: "{{url('faq/json-faq')}}",
		  cache: false,
		  data:{
		  	page : page
		  }
		});

		loadData.done(function(response){
			var data = response;

			$('.box-list-forum').on('click',function(){
				let id            = parseInt($(this).data('id'));
				let data          = callData(id); 
				let total_jawaban = data[0].total_jawaban;
				let jawaban       = data[0].jawaban;
				$('#totalJawaban').find('span').html(total_jawaban+' Jawaban');

				removeActive();
				$(this).addClass('active');
				
				if(jawaban==""){
				    let box = '';
				    box +='<h2 style="text-align: center;" class="big-bold-black">List Faq masih kosong</h2>';
				    $('#listJawaban').empty().append(box);
				}else{
				    // alert(jawaban.id);
				    generateJawaban(jawaban)   
				}
			})

			function callData(id){
				return data.filter(d => d.id == id);
			}

			function removeActive(){
				$('.box-list-forum.active').removeClass('active');
			}

			function generateJawaban(jawaban){
				$('#listJawaban').html("");
				let box = '';
				 $.each(jawaban,function(key, value){
			        
			        
    			    url = '{{url("faq/download")}}/'+value.id;
    			  
    		        box += '<div class="box-grey">'
    				box += '<div class="head-jawaban" data-toggle="collapse" data-target="#collapseExample'+value.id+'" aria-expanded="false" aria-controls="collapseExample'+value.id+'">'
    				// box += '<span class="float-right small-grey">'+value.tanggal+'</span>'
    				box += '<h4 class="medium-bold-black">'+value.name+'</h4>'
    				box += '</div>'
    				box += '<div class="body-jawaban collapse" id="collapseExample'+value.id+'">'
    				box += '<p class="small-grey-black">'
    				box += value.content
    				box += '</p>'
    				box += '<p class="small-grey-black"> Lampiran : '
    				if (value.lampiran == '-' || value.lampiran == '') {
    					box += '<span>-</span>'
    				}else{
    					box += '<span><a href="'+url+'" class="text-info"><i class="fa fa-download"></i>Download</a></span>'
    				}
    				box += '</p>'
    				box += '</div>'
    				box += '</div>'; 
			    
			    });
			
				$('#listJawaban').empty().append(box);
			}

			function appendJawaban(data){
			    
			    $.each(data,function(key, value){
			        
			        
    			    url = '{{url("faq/download")}}/'+value.id;
      				let box = '';
    			  
    		        box += '<div class="box-grey">'
    				box += '<div class="head-jawaban">'
    				box += '<span class="float-right small-grey">'+value.tanggal+'</span>'
    				box += '<h4 class="medium-bold-black">'+value.name+'</h4>'
    				box += '</div>'
    				box += '<div class="body-jawaban">'
    				box += '<p class="small-grey-black">'
    				box += value.content
    				box += '</p>'
    				box += '<p class="small-grey-black"> Lampiran : '
    				if (value.lampiran == '-' || value.lampiran == '') {
    					box += '<span>-</span>'
    				}else{
    					box += '<span><a href="'+url+'" class="text-info"><i class="fa fa-download"></i>Download</a></span>'
    				}
    				box += '</p>'
    				box += '</div>'
    				box += '</div>'; 
			    
			    });
			
				$('#listJawaban').empty().append(box);
			}
		})
	}
	wrapData({{(Request::get('page') == '' ? 1 : Request::get('page'))}})
</script>
@endpush