@extends('front/template/mastertemplate')

@section('content')
	<section class="master container">
		<div class="row">
			<div class="content-breadcrumb col-sm-12 wow animated fadeInLeft">
				<p>FORUM/KONSULTASI</p>
			</div>

			<div class="col-sm-4 wow animated fadeInLeft">
				<div id="listForum" class="form-shadow">
					@if(CRUDBooster::myId() != '')
					<button class="btn btn-success btn-sm btn-green float-right" data-toggle="modal" data-target="#modalForum">
						<i class="fa fa-plus"></i>
						KONSULTASI
					</button>
					@endif
					<label class="small-bold-black">LIST KONSULTASI</label>
					<?php $no = 0; ?>
					@foreach($forum as $row_forum)
						<?php 
							if(Request::get('id') == $row_forum->id){
								$active = 'active';
								$index = $no;
							}elseif ($no == 0 && Request::get('id') == '') {
								$active = 'active';
								$index = $no;
							}else{
								$active = '';
								$index = 0;
							}
						?>
						<div data-id="{{$row_forum->id}}" class="box-list-forum {{$active}}">
							<i class="fa fa-chevron-right"></i>
							<label class="verysmall-green">Judul Konsultasi</label>
							<p class="medium-black">{{$row_forum->judul}}</p>
							<label class="verysmall-green">Oleh</label>
							<p class="medium-black">{{$row_forum->name}}</p>
							<p class="verysmall-grey" style="text-align: right;">{{Kemenag::dateIndonesia($row_forum->created_at)}}</p>
						</div>
						<?php 
							$no++; 
						?>
					@endforeach

					{{$forum->render()}}
				</div>
			</div>

			<div class="col-sm-8 wow animated fadeInRight">
				<div id="content" class="form-shadow">
					<div id="Forum">
						@if(isset($forum[$index]))
							<div id="infoForum">
								<p class="medium-bold-black">Detail Konsultasi</p>
								<label id="namaPenanya" class="small-bold-black">{{$forum[$index]->name}}</label>
							<span class="float-right small-grey" id="tanggalForum">{{Kemenag::dateIndonesia($forum[$index]->created_at)}}</span>
							</div>

							<div id="detailForum">
								<div id="judulForum" class="big-bold-black">{{$forum[$index]->judul}}</div>
								<div id="isiForum">
									<p id="contentForum" class="small-grey">
										{!! nl2br($forum[$index]->isi) !!}
									</p>
									<p id="lampiranForum" class="small-grey">
										Lampiran : 
										@if($forum[$index]->lampiran == '')
											<span>-</span>
										@else
											<span>
												<a href="{{asset($forum[$index]->lampiran)}}" class="text-info" target="_blank">
													<i class="fa fa-download"></i>
													Download
												</a>
											</span>
										@endif
									</p>
								</div>
								<div id="totalJawaban">
									<span class="small-bold-black">{{$forum[$index]->total_jawaban}} Jawaban</span>
								</div>
							</div>

							<div id="listJawaban">
								@foreach($forum[$index]->jawaban as $row_jawaban)
									<div class="box-grey">
										<div class="head-jawaban">
											<span class="float-right small-grey">{{Kemenag::dateIndonesia($row_jawaban->created_at)}}</span>
											<h4 class="medium-bold-black">{{$row_jawaban->name}}</h4>
										</div>
										<div class="body-jawaban">
											<p class="small-grey-black">
												{!! nl2br($row_jawaban->isi) !!}
											</p>
											<p class="small-grey-black">
												Lampiran : 
												@if($row_jawaban->lampiran == '')
													<span>-</span>
												@else
													<span>
														<a href="{{asset($row_jawaban->lampiran)}}" class="text-info" target="_blank">
															<i class="fa fa-download"></i>
															Download
														</a>
													</span>
												@endif
											</p>
										</div>
									</div>
								@endforeach
							</div>

							@if(CRUDBooster::myId() != '')
								<div id="kirimJawaban">
									<form action="{{asset('forum/konsultasi-jawaban')}}" method="POST" enctype="multipart/form-data">
										<div class="box-grey">
											<div class="head-jawaban" style="border-bottom: none;">
												<h4 class="medium-bold-black">{{CRUDBooster::myName()}}</h4>
											</div>
											<div class="body-jawaban">
												<textarea class="form-control" name="isi" placeholder="Tulis jawaban Anda di sini" rows="4" required=""></textarea>
											</div>
											<div class="footer-jawaban">
												<p class="verysmall-green">Lampiran</p>
												<input type="file" name="lampiran" class="file-forum">
												<button type="submit" class="btn btn-success btn-green float-right">Kirim</button>
											</div>
										</div>
										<input id="idForum" type="hidden" name="id_forum" value="{{$forum[$index]->id}}">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										<input type="hidden" name="page" value="{{Request::get('page')}}">
									</form>
								</div>
							@elseif(CRUDBooster::myId() == '' && $forum[$index]->total_jawaban == 0)
								<div id="kirimJawaban">
									<p class="big-bold-black" style="text-align: center;">Belum ada jawaban</p>
								</div>
							@endif
						@else
							<h2 style="text-align: center;" class="big-bold-black">List konsultasi masih kosong</h2>
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>

	@if(CRUDBooster::myId() != '')
		<form action="{{asset('forum/konsultasi')}}" method="POST" enctype="multipart/form-data">
			<!-- Modal Forum -->
			<div class="modal fade" id="modalForum" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-lg" role="document">
			    <div class="modal-content">
			      <div class="modal-body">
			      	<div class="row">
		      			<div class="col-md-12">
		      				<h3 class="medium-bold-black" style="text-align: left;">MENGAJUKAN KONSULTASI BARU</h3>
		      			</div>
			      	</div>
			        <div class="row">
			        	<div class="col-sm-12 form-modal">
			        		<div class="form-group">
			        			<label class="verysmall-green">Judul Konsultais</label>
			        			<input type="text" class="form-control" name="judul" placeholder="Tulis Judul Konsultasi" required="">
			        		</div>
			        	</div>
			        	<div class="col-sm-12 form-modal">
			        		<div class="form-group">
			        			<label class="verysmall-green">Isi Konsultasi</label>
			        			<textarea class="form-control" name="isi" placeholder="Tulis Isi Konsultasi" rows="5" required=""></textarea>
			        		</div>
			        	</div>
			        	<div class="col-sm-6 form-modal">
			        		<div class="form-group">
			        			<label class="verysmall-green">Lampiran</label><br>
										<input type="file" name="lampiran" class="file-forum">
			        		</div>
			        	</div>
			        	<div class="col-sm-6 form-modal">
			        		<button type="submit" class="btn btn-success btn-green float-right">KIRIM</button>
			        		<button type="button" class="btn btn-success btn-transparent-green float-right" data-dismiss="modal" value="TUTUP">TUTUP</button>
			        	</div>
			        </div>
			      </div>
			    </div>
			  </div>
			</div>
			<input type="hidden" name="_token" value="{{csrf_token()}}">
		</form>
	@endif
@endsection

@section('js')
<script type="text/javascript">
	generatePaginate();
	function generatePaginate(){
		$('.pagination').find('li').addClass('page-item');
		$('.pagination').find('a').addClass('page-link');
		$('.pagination').find('span').addClass('page-link');
		$('.pagination').addClass('justify-content-center');
	}

	function wrapData(page){
		var loadData = $.ajax({
		  url: "{{url('forum/konsultasi-json')}}",
		  cache: false,
		  data:{
		  	page : page
		  }
		});

		loadData.done(function(response){
			var data = response;

			$('.box-list-forum').on('click',function(){
				let id            = parseInt($(this).data('id'));
				let data          = callData(id);
				let nama          = data[0].nama; 
				let tanggal       = data[0].tanggal; 
				let judul         = data[0].judul; 
				let content       = data[0].content; 
				let total_jawaban = data[0].total_jawaban;
				let jawaban       = data[0].jawaban;
				let lampiran      = data[0].lampiran;

				if (lampiran == '-' || lampiran == '') {
					$('#lampiranForum').find('span').html('-');
				}else{
					$('#lampiranForum').find('span').html('<a href="'+lampiran+'" class="text-info" target="_blank"><i class="fa fa-download"></i>Download</a>');
				}

				$('#idForum').val(id);
				$('#namaPenanya').html(nama);
				$('#tanggalForum').html(tanggal);
				$('#judulForum').html(judul);
				$('#isiForum').find('#contentForum').html(content);
				$('#totalJawaban').find('span').html(total_jawaban+' Jawaban');

				removeActive();
				$(this).addClass('active');
				generateJawaban(jawaban)
			})

			function callData(id){
				return data.filter(d => d.id == id);
			}

			function removeActive(){
				$('.box-list-forum.active').removeClass('active');
			}

			function generateJawaban(jawaban){
				$('#listJawaban').html("");
				for (i in jawaban) {
					appendJawaban(jawaban[i])
				}
			}

			function appendJawaban(data){
				let box = '';
				box += '<div class="box-grey">'
				box += '<div class="head-jawaban">'
				box += '<span class="float-right small-grey">'+data.tanggal+'</span>'
				box += '<h4 class="medium-bold-black">'+data.name+'</h4>'
				box += '</div>'
				box += '<div class="body-jawaban">'
				box += '<p class="small-grey-black">'
				box += data.content
				box += '</p>'
				box += '<p class="small-grey-black"> Lampiran : '
				if (data.lampiran == '-' || data.lampiran == '') {
					box += '<span>-</span>'
				}else{
					box += '<span><a href="'+data.lampiran+'" class="text-info" target="_blank"><i class="fa fa-download"></i>Download</a></span>'
				}
				box += '</p>'
				box += '</div>'
				box += '</div>';

				$('#listJawaban').append(box);
			}
		})
	}
	wrapData({{(Request::get('page') == '' ? 1 : Request::get('page'))}})
</script>
@endsection