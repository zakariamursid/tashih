@extends('front/template/mastertemplate')

@section('content')

@php 
	if(old('id_province') != null)
	{
		$code_province = old('id_province');
	}
	else
	{
		$code_province = "undefined";
	}
	if(old('id_city') != null)
	{
		$code_city = $code_city->code;
	}
	else
	{
		$code_city = "undefined";
	}
	if(old('id_district') != null)
	{
		$code_district = $code_district->code;
	}
	else
	{
		$code_district = "undefined";
	}
	if(old('id_jenis_lembaga') != null)
	{
		$id_jenis_lembaga = old('id_jenis_lembaga');
	}
	else
	{
		$id_jenis_lembaga = "undefined";
	}

@endphp

	<section id="pentashihan" class="master container">
		<div class="row">
			<div class="content-breadcrumb col-sm-12 wow animated fadeInLeft">
				<center>
				<h4 class="medium-bold-black"  style="padding-bottom:50px;">Formulir Pendaftaran Penerbit Al-Qur’an</h4>
				</center>
			</div>
			
			<div id="contentInformation" class="col-sm-10 mr-auto ml-auto wow animated fadeInBottom animated" style="display: none;">
				<p align="center" class="no-margin" style="">
					{{Kemenag::message()}}
				</p>
			</div>


					<div class="col-sm-12"  style="padding-bottom:50px;">
							<div class="alert alert-success" role="alert">
								<h4 class="alert-heading">Perhatian!</h4>
								{!!$message->content!!}
							</div>
							<div class="alert alert-success" role="alert">
									Harap mengisi informasi penerbit dengan data terbaru. <br>
									Data ini akan menjadi rujukan utama pada sistem layanan tashih online. <br>
									Setelah melakukan registrasi, LPMQ akan melakukan verifikasi pendaftaran. <br> <br>
									Jika lolos verifikasi, informasi id penerbit dan password akan dikirimkan ke alamat email yang diisikan.
							</div>

						</div>
					</div>

			<div id="content" class="col-sm-12 form-shadow" style="padding-bottom:0px;">
				<form action="{{ asset('pendaftaran-penerbit/temp-insert') }}" method="POST" enctype="multipart/form-data">
					<div id="formPenerbit" class="row">
						<div class="col-sm-12">
							<h4 class="alert-heading">Informasi Data Penerbit</h4>
							<label class="col-form-label verysmall-green-default">Harap mengisi informasi penerbit dengan data terbaru.
							<br>	
							Data ini akan menjadi rujukan utama pada sistem layanan tashih online.
							<br>
							Setelah melakukan registrasi, LPMQ akan melakukan verifikasi pendaftaran.
							<br>
							Jika lolos verifikasi, informasi id penerbit dan password akan dikirimkan ke alamat email yang diisikan.</label>
						</div>

						<div class="col-12 form-group row" style="margin-top:50px;">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Nama Penerbit *</label><br>
								<label class="col-form-label verysmall-green-default">Isi nama penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="text" name="name" value="{{old('name')}}" placeholder="Tulis nama penerbit (Contoh: PT. Abadi Sejahtera.)" required="" class="form-control">
							</div>
						</div>

						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Alamat Penerbit *</label><br>
								<label class="col-form-label verysmall-green-default">Isi alamat penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<textarea name="address" placeholder="Tulis alamat penerbit (Contoh: Gedung Rajawali 2. Jl. Elang No. 12 Kel. Merpati Kec. Puyuh Kab. Merak.)" required="" class="form-control">{{old('address')}}</textarea>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Propinsi Penerbit *</label><br>
								<label class="col-form-label verysmall-green-default">Pilih propinsi alamat penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<select required name="id_province" id="id_province" required="" class="form-control select2">
									<option class="select-empty-value" value="">Pilih Provinsi</option>
								@foreach($province as $row)
									@if(old('id_province') == $row->code)
										<option value="{{$row->code}}" selected="">{{$row->name}}</option>
									@else
										<option value="{{$row->code}}">{{$row->name}}</option>
									@endif
								@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Kabupaten / Kota *</label><br>
								<label class="col-form-label verysmall-green-default">Pilih kabupaten / kota alamat penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<select required name="id_city" id="id_city" required="" class="form-control select2">
									<option selected disabled  class="select-empty-value" value="">Pilih Kabupaten / Kota</option>
								</select>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Kecamatan *</label><br>
								<label class="col-form-label verysmall-green-default">Pilih kecamatan alamat penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<select required name="id_district" id="id_district" required="" class="form-control select2">
									<option selected disabled class="select-empty-value" value="">Pilih Kecamatan</option>
								</select>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Kode Pos *</label> <br>
								<label class="col-form-label verysmall-green-default">Pilih kode pos alamat penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input require type="number" name="kode_pos" value="{{old('kode_pos')}}" placeholder="Tulis kode pos alamat penerbit" required="" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">No. Telp. Penerbit *</label> <br>
								<label class="col-form-label verysmall-green-default">Isi nomor telpon penerbit (hanya menerima angka)</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="number" start="0" name="phone" value="{{old('phone')}}" placeholder="Tulis No. Telp penerbit (Contoh: 0218416468)" required="" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Email Penerbit *</label><br>
								<label class="col-form-label verysmall-green-default">Isi email penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="email" name="email" value="{{old('email')}}" placeholder="Tulis alamat email penerbit (contoh: pta.abadisejahtera@gmail.com)" required="" class="form-control">
							</div>
						</div>

						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Jenis Lembaga</label><br>
								<label class="col-form-label verysmall-green-default">Pilih jenis lembaga penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<select name="id_m_jenis_lembaga" id="id_m_jenis_lembaga" class="form-control select2">
									<option selected disabled class="select-empty-value" value="">Pilih Jenis Lembaga</option>
									@foreach($jenis_lembaga as $row)
										@if(old('id_m_jenis_lembaga') == $row->id)
											<option value="{{$row->id}}" selected="">{{$row->name}}</option>
										@else
											<option value="{{$row->id}}">{{$row->name}}</option>
										@endif
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">NIB</label><br>
								<label class="col-form-label verysmall-green-default">Isi nomor induk berusaha penerbit </label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input require type="number" name="nib_value" value="{{old('nib_value')}}" placeholder="Tulis Nomor Induk Berusaha penerbit (contoh: 0708320005784)" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">NPWP</label><br>
								<label class="col-form-label verysmall-green-default">Isi nomor NPWP penerbit </label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input require type="number" name="npwp_value" value="{{old('npwp_value')}}" placeholder="Tulis NPWP penerbit (Contoh: 44.308.934.8.555.000)" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Password *</label><br>
								<label class="col-form-label verysmall-green-default">Buat password penerbit Anda</label>
							</div>
							<div class="col-lg-8 col-sm-12">
                            <div class="input-group">
								<input type="password" id="password" name="password" value="{{old('password')}}" required="" class="form-control" placeholder="Tulis password penerbit Anda">
                                <span class="input-group-text showPwdBtn" style="background-color:white; border-left:0;"><i id="password_icon" class="fa fa-eye fa-fw"></i></span>
                            </div> 
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Konfirmasi Password *</label><br>
								<label class="col-form-label verysmall-green-default">Konfirmasi password penerbit Anda</label>
							</div>
							<div class="col-lg-8 col-sm-12">
                            <div class="input-group">
								<input type="password" id="password_confirmation" name="password_confirmation" value="{{old('password_confirmation')}}" required="" class="form-control"  placeholder="Tulis ulang password penerbit Anda">
                                <span class="input-group-text showConfPwdBtn"  style="background-color:white; border-left:0;"><i id="password_confirmation_icon" class="fa fa-eye fa-fw"></i></span>
                            </div> 								
							</div>
						</div>
						</div>
						</div>
						<br>
						<div id="content" class="col-sm-12 form-shadow">
						<div class="col-sm-12"  style="padding-bottom:25px;">
							<h4 class="alert-heading">Informasi Penanggung jawab Penerbit</h4>
							<label class="col-form-label verysmall-green-default">Penanggung jawab Penerbit adalah salah satu direksi atau pimpinan yang terdapat pada akta notaris.</label>
						</div>

						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Nama Penanggung Jawab *</label><br>
								<label class="col-form-label verysmall-green-default">Isi nama penanggung jawab penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="text" name="pic" value="{{old('pic')}}" placeholder="Tulis nama penanggung jawab penerbit" required="" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Alamat Penanggung Jawab *</label><br>
								<label class="col-form-label verysmall-green-default">Isi alamat penanggung jawab penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<textarea name="pic_address" placeholder="Tulis alamat penanggung jawab penerbit" required="" class="form-control">{{old('pic_address')}}</textarea>
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Jabatan Penanggung Jawab *</label><br>
								<label class="col-form-label verysmall-green-default">Isi jabatan penanggung jawab penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="text" name="pic_jabatan" value="{{old('pic_jabatan')}}" placeholder="Tulis jabatan penanggung jawab penerbit" required="" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">No. Telp Penanggung Jawab *</label><br>
								<label class="col-form-label verysmall-green-default">Isi no. telp penanggung jawab penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="number" name="pic_phone" value="{{old('pic_phone')}}" placeholder="Tulis no telp. penanggung jawab penerbit" required="" class="form-control">
							</div>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">E-mail Penanggung Jawab *</label><br>
								<label class="col-form-label verysmall-green-default">Isi email penanggung jawab penerbit</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="email" name="pic_email" value="{{old('pic_email')}}" placeholder="Tulis alamat email penanggung jawab penerbit" required="" class="form-control">
							</div>
						</div>
					</div>
					<br>

					<div id="legalitas" class="col-sm-12 form-shadow"  style="border-radius:12px;">
						<div class="col-sm-12"  style="padding-bottom:25px;">
							<h4 class="alert-heading">Informasi Dokumen Legalitas Penerbit</h4>
							<label class="col-form-label verysmall-green-default">Unggah dokumen legalitas penerbit</label>
						</div>
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">SIUP dan TDP</label><br>
								<label class="col-form-label verysmall-green-default">Unggah SIUP dan TDP dengan format file pdf. Ukuran masimal 500kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="file" name="siup" id="siup" class="file-forum" data-target="2" accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="2" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Profile Penerbit</label><br>
								<label class="col-form-label verysmall-green-default">Unggah Profil Penerbit dengan format file pdf. Ukuran masimal 500 kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="file" name="company_profile" id="company_profile" class="file-forum" data-target="3" accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="3" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">Akta Notaris</label><br>
								<label class="col-form-label verysmall-green-default">Unggah Akta Notaris dengan format file pdf. Ukuran masimal 500 kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="file" name="akte_notaris" id="akte_notaris" class="file-forum" data-target="4" accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="4" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">NPWP</label><br>
								<label class="col-form-label verysmall-green-default">Unggah NPWP dengan format file pdf. Ukuran masimal 500 kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input type="file" name="npwp" id="npwp" class="file-forum" data-target="5"  accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="5" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">NIB</label><br>
								<label class="col-form-label verysmall-green-default">Unggah NIB dengan format file pdf. Ukuran masimal 500 kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input require type="file" name="nib" id="nib" class="file-forum" data-target="6" accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="6" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
						<div class="col-12 form-group row">
							<div class="col-lg-4 col-sm-12">
								<label class="col-form-label">KTP Penanggung Jawab Penerbit</label><br>
								<label class="col-form-label verysmall-green-default">Unggah KTP Penanggung Jawab dengan format file pdf. Ukuran masimal 500 kb</label>
							</div>
							<div class="col-lg-8 col-sm-12">
								<input require type="file" name="scan_ktp" id="scan_ktp" class="file-forum" data-target="7" accept="application/pdf" onchange="ValidateExtension(this)">
								<a href="" id="7" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
							</div>
						</div>						
					</div>

						<br>
						<div id="content" class="col-sm-12 form-shadow">
							<div class="col-sm-12"  style="padding-bottom:25px;">
								<h4 class="alert-heading">Unggah surat pendaftaran akun penerbit</h4>
								<label class="col-form-label verysmall-green-default">Unggah surat pendaftaran akun penerbit mushaf Al-Qur'an. Contoh surat dapat diunduh pada menu download materi.</label>
							</div>
							<div class="col-12 form-group row">
								<div class="col-lg-4 col-sm-12">
									<label class="col-form-label">Surat pendaftaran akun penerbit*</label><br>
									<label class="col-form-label verysmall-green-default">Unggah Surat pendaftaran akun penerbit dengan format file pdf. Ukuran masimal 500 kb</label>
								</div>
								<div class="col-lg-8 col-sm-12">
									<input type="file" name="surat_permohonan" id="surat_permohonan" class="file-forum" data-target="1" accept="application/pdf" onchange="ValidateExtension(this)">
									<a href="" id="1" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
								</div>
							</div>						
						</div>
						<br>

					<div id="content" class="row form-shadow"  style="padding:25px;">
					
					<div class="col-12">
					<h4 class="alert-heading">Pernyataan</h4>
						Baca dan beri tanda centang pada pernyataan berikut: <br> <br>
					</div>

					<div class="col-lg-1 col-2">
						<input class="form-check-input" type="checkbox" value="" id="agreement" style="margin-left:15px;">
					</div>

					<div class="col-lg-11 col-10" >
						<label class="form-check-label" for="check" id="agreementPendaftaran">
						Saya menyatakan bahwa seluruh data yang terisi dalam formulir pendaftaran penerbit Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.
						</label>
					</div>

					<div class="col-12" style="padding-top: 50px;">
							<div class="float-right">
								<input id="resetBtn" type="button" value="Reset" class="btn btn-red">
								<button id="btnSave" type="button" class="btn btn-green" disabled>Registrasi Penerbit</button>
								<button id="btnSubmit" type="submit" class="btn btn-green" value="KIRIM" style="display:none;">KIRIM</button>
							</div>
						</div>
					</div>
					</div>

					<input type="hidden" name="_token" value="{{csrf_token()}}">
				</form>
			</div>
		</div>
	</section>

@endsection

@push('js')

    <script type="text/javascript">
        // $('.select2').select2();
		var jenisLembagaSelect = $('select[name=id_m_jenis_lembaga]').select2()
		var provinceSelect = $('select[name=id_province]').select2()
		var citySelect = $('select[name=id_city]').select2()
		var districtSelect = $('select[name=id_district]').select2()

provinceSelect.on("change", function (e) {
	if(this.value != '')
	{
    $.ajax({
        url: '{{url('api/list_city')}}',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
		var options = []
			res.data.forEach(e => {
				options.push(new Option(e.name, e.code, true, true))
			})
			citySelect.val(null).trigger('change')
			citySelect.append(options).trigger('change');

				if('{{$code_city}}' != 'undefined')
				{
                	citySelect.val({{$code_city}}).trigger("change");
				}
			
		});
	}
})

citySelect.on("change", function (e) {
	if(this.value != '')
	{
    $.ajax({
        url: '{{url('api/list_district')}}',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
        var options = []
        res.data.forEach(e => {
            options.push(new Option(e.name, e.code, true, true))
        })
        districtSelect.val(null).trigger('change')
        districtSelect.append(options).trigger('change');

				if('{{$code_district}}' != 'undefined')
				{
                	districtSelect.val('{{$code_district}}').trigger("change");
				}

    });
	}
})

$('.showPwdBtn').click(function()
{
	showPassword('password');
});
$('.showConfPwdBtn').click(function()
{
	showPassword('password_confirmation');
});

function showPassword(id) {
  var x = document.getElementById(id);
  if (x.type === "password") {
    x.type = "text";

	$("#"+id+"_icon").removeClass("fa-eye");
	$("#"+id+"_icon").addClass("fa-eye-slash");
  } else {
    x.type = "password";
	$("#"+id+"_icon").addClass("fa-eye");
	$("#"+id+"_icon").removeClass("fa-eye-slash");
  }
}
    document.addEventListener('readystatechange', function(event) {
            if (event.target.readyState === "complete") {
				if('{{$code_province}}' != 'undefined')
				{
                	provinceSelect.val({{$code_province}}).trigger("change");
				}
				if('{{$id_jenis_lembaga}}' != 'undefined')
				{
	                jenisLembagaSelect.val('{{$id_jenis_lembaga}}').trigger("change");
				}
            }
        });

		$('#btnSave').click(function()
		{
			if($('#id_m_jenis_lembaga').val() == null)
			{
				// alert("Anda belum memilih jenis lembaga");
				// $('#id_m_jenis_lembaga').focus();
				// return false;
			}


			if($('#password').val() != $('#password_confirmation').val())
			{
				alert('Konfirmasi password tidak sama dengan password');
				$("#password_confirmation").focus();
				return false;               
			}

			if($('#id_province').val() == "")
			{
				alert("Anda belum memilih provinsi");
				$('#id_province').focus();
				return false;
			}
			if($('#id_city').val() == "")
			{
				alert("Anda belum memilih kabupaten / kota");
				$('#id_city').focus();
				return false;
			}
			if($('#id_district').val() == "")
			{
				alert("Anda belum memilih Kecamatan");
				$('#id_district').focus();
				return false;
			}
			

			if ($('#surat_permohonan').get(0).files.length === 0) {
				alert("Surat Pendaftaran Akun Penerbit belum dilampirkan");
				$('#surat_permohonan').focus();
				return false;
			}

			// if ($('#company_profile').get(0).files.length === 0) {
			// 	alert("Scan Company Profile belum dilampirkan");
			// 	$('#company_profile').focus();
			// 	return false;
			// }
			// if ($('#npwp').get(0).files.length === 0) {
			// 	alert("Scan NPWP belum dilampirkan");
			// 	$('#npwp').focus();
			// 	return false;
			// }

			// if ($('#nib').get(0).files.length === 0) {
			// 	alert("Scan NIB belum dilampirkan");
			// 	$('#nib').focus();
			// 	return false;
			// }
			// if ($('#scan_ktp').get(0).files.length === 0) {
			// 	alert("Scan KTP Penanggung Jawab belum dilampirkan");
			// 	$('#scan_ktp').focus();
			// 	return false;
			// }

			// if($('#id_m_jenis_lembaga').val() == 2)
			// {
			// 	if ($('#siup').get(0).files.length === 0) {
			// 		alert("Scan SIUP dan TDP belum dilampirkan");
			// 		$('#siup').focus();
			// 		return false;
			// 	}
			// 	if ($('#akte_notaris').get(0).files.length === 0) {
			// 		alert("Scan Akte Notaris belum dilampirkan");
			// 		$('#siup').focus();
			// 		return false;
			// 	}
			// }

			$('#btnSubmit').click();
		});

        // $("#id_m_jenis_lembaga").change(function () {
		jenisLembagaSelect.on("change", function (e) {
          a = $(this).val();
        //   if (a=="1") {
		// 	$("#label_surat_permohonan").html('Scan Surat Pendaftaran Akun Penerbit  *');
		// 	$("#label_npwp").html('Scan NPWP *');
		// 	$("#label_company_profile").html('Scan Company Profile *');
            
		// 	$("#surat_permohonan").attr("required","required");
        //     $("#npwp").attr("required","required");
        //     $("#company_profile").attr("required","required");
        //   }else if(a=="2"){
		// 	$("#label_surat_permohonan").html('Scan Surat Pendaftaran Akun Penerbit  *');
		// 	$("#label_siup").html('Scan SIUP dan TDP *');
		// 	$("#label_company_profile").html('Scan Company Profile *');
		// 	$("#label_akte_notaris").html('Scan Akte Notaris *');
		// 	$("#label_npwp").html('Scan NPWP *');

        //     $("#surat_permohonan").attr("required","required");
        //     $("#siup").attr("required","required");
        //     $("#company_profile").attr("required","required");
        //     $("#akte_notaris").attr("required","required");
        //     $("#npwp").attr("required","required");
        //     // $("#tdp").attr("required","required");
        //   }else if(a=="3"){
		// 	$("#label_surat_permohonan").html('Scan Surat Pendaftaran Akun Penerbit  *');
		// 	$("#label_npwp").html('Scan NPWP *');
		// 	$("#label_company_profile").html('Scan Company Profile *');

        //     $("#surat_permohonan").attr("required","required");
        //     $("#npwp").attr("required","required");
        //     $("#company_profile").attr("required","required");
        //   }
        });

        $(function () {
            // $('[data-toggle="tooltip"]').tooltip()
        })

        @if(Kemenag::message() != '')
        @if(Kemenag::messageType() == 'warning')
            $('#contentInformation').find("p").css('background-color','#f39c12');
        @endif
        @if(Kemenag::messageType() == 'danger')
            $('#contentInformation').find("p").css('background-color','#dd4b39');
        @endif

        $('#contentInformation').find("p").html("{{Kemenag::message()}}");
        $('#contentInformation').show(500);
        @endif

		function ValidateExtension(e) {
			var allowedFiles = [".pdf"];
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
			if (!regex.test($(e).val().toLowerCase())) {
				alert("Please upload files having extensions: " + allowedFiles.join(', ') + " only.");
				$(e).val("");
				$(e).val(null);
				return false;
			}
			return true;
		}

		$('.file-forum').on("change", function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			const datatarget = $(this).data('target');
			$('#'+datatarget).attr('href',tmppath);
			$('#'+datatarget).show();
		});
		$("#resetBtn").click(function()
		{
			location.reload();
		});

	


		$("#agreement").click(function()
		{
			$("#btnSave").attr("disabled", !this.checked);
		});
	
    </script>

@endpush

@push('css')

    <style>
        .select2 {
            border: 0.7px solid #ced4da !important;
        }
		.btn-red
		{
			background-color: #dc3545;
			border: 1.6px solid #dc3545;
			border-radius: 10px;
			color: #FFFFFF;
			min-width: 110px;
		}		
    </style>

@endpush