@extends('front/template/mastertemplate')

@section('content')

    <div id="contentInformation" class="col-sm-10 mr-auto ml-auto" style="display: none;margin-bottom:-20px;">
        <p align="center" class="no-margin" style="">
            {{Kemenag::message()}}
        </p>
    </div>
    <section id="content" class="container">

        <div class="swiper-container" id="swiperBannerTop">
            <div class="swiper-wrapper">
                @foreach($new_info as $row_info)
                <div class="swiper-slide">
                    <a href="{{$row_info->link}}" class="news-mini">
                        <div class="media">
                            <div class="thumbnail">
                                <img src="{{$row_info->image}}">
                            </div>
                            <div class="media-body">
                                <label>{{$row_info->title}}</label>
                                <p>{{$row_info->admin_name}} | {{Kemenag::dateIndonesia($row_info->created_at)}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>

        <div class="row">
            <div id="slider" class="col-sm-12 animated infinite fadeInUp">
                <div id="swiperSlider" class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($banner as $row_banner)
                        @if(!empty($row_banner->url_videos))
                        @php
                        $video_id_baner = explode("?v=", $row_banner->url_videos);
                        if (empty($video_id_baner[1]))
                        $video_id_baner = explode("/v/", $link);

                        $video_id_baner = explode("&", $video_id_baner[1]);
                        $video_id_baner = $video_id_baner[0];
                        @endphp
                        <div class="swiper-slide">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{$video_id_baner}}?controls=0&playlist={{$video_id_baner}}" frameborder="0" allowfullscreen style="width:100%;height:100%;"></iframe>
                        </div>
                        @else
                        @php
                        $slug = url('info-seputar-lajnah/read/'.$row_banner->slug);
                        $url_banner = (empty($row_banner->url)) ? $slug : $row_banner->url;
                        @endphp
                        <div class="swiper-slide">
                            <a href="{{$url_banner}}">
                                <img src="{{$row_banner->image}}">
                                <h1>{{$row_banner->title}}</h1>
                                <p style="display: none;">
                                    <i class="fa fa-user"></i> {{strtoupper($row_banner->admin_name)}}
                                    <span> | </span>
                                    <i class="fa fa-book"></i> {{strtoupper($row_banner->admin_name)}}
                                    <span> | </span>
                                    <i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia($row_banner->created_at)}}
                                </p>
                            </a>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <i id="sliderPrev" class="fa fa-arrow-left"></i>
                    <div class="swiper-pagination float-right"></div>
                    <i id="sliderNext" class="fa fa-arrow-right"></i>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="info" class="col-md-4 order-md-2">
                <div class="col-sm-12">
                    {{--<a href="{{asset('persyaratan-pengajuan-tashih')}}">
                        <img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
                    </a>
                    @if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
                    <a href="{{asset('pendaftaran-penerbit')}}">
                        <img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
                    </a>
                    @endif
                    @if(CRUDBooster::myId() != '')
                    <a href="{{asset('pengajuan-tanda-tashih')}}">
                        <img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
                    </a>
                    @endif
                    <a href="{{asset('petunjuk-teknis-pencetakan')}}">
                        <img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
                    </a>
                    <a href="{{asset('pelaporan-mushaf-bermasalah')}}">
                        <img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
                    </a>--}}

                    @foreach($banner_layanan as $x => $xrow)
                    <a href="{{ $xrow->link }}" target="_blank">
                        <img src="{{ asset($xrow->image) }}" class="img-fluid">
                    </a>
                    @endforeach
                </div>
            </div>

            <div id="news" class="col-md-8 order-md-1">
                <div class="row list-news">
                    <div class="col-sm-12">
                        <h4 class="title-content">
                            INFO SEPUTAR LAJNAH
                            <a href="{{asseT('info-seputar-lajnah')}}" class="float-right">LIHAT SEMUA <i class="fa fa-angle-right"></i></a>
                        </h4>
                    </div>
                </div>
                <div class="row list-news border-right">
                    <?php
                    $big   = 1;
                    $small = 0;
                    ?>
                    @foreach($info_lainnya as $row_info_lainnya)
                    @if($big < 2)
                    <div class="col-md-7 col-sm-12">
                        <a href="{{$row_info_lainnya->href}}">
                            <div class="item-image">
                                <img src="{{$row_info_lainnya->image}}">
                                <div class="text-left">{{$row_info_lainnya->title}}</div>
                            </div>
                        </a>
                    </div>
                    @else
                    <div class="col-md-5 col-sm-12">
                        <a href="{{$row_info_lainnya->href}}">
                            <div class="item-image">
                                <img src="{{$row_info_lainnya->image}}">
                                <div class="text-left">{{$row_info_lainnya->title}}</div>
                            </div>
                        </a>
                    </div>
                    @endif
                    <?php
                    if ($big < 2) {
                        $big   = $big+1;
                        $small = ($big == 2 ? 0 : $small);
                    }else{
                        $small = $small+1;
                        $big   = ($small == 2 ? 0 : $big);
                    }
                    ?>
                    @endforeach
                </div>
            </div>
        </div>

    </section>

    <section id="galeryMushaf" class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="title-content">
                    INFO LAYANAN PENTASHIHAN
                    <a href="{{asseT('info-layanan-pentashihan')}}" class="float-right">LIHAT SEMUA <i class="fa fa-angle-right"></i></a>
                </h4>
            </div>
        </div>
        <div class="row">
            @foreach($gallery_mushaf as $row_gallery_mushaf)
            <div class="col-md-4 col-sm-12">
                <a href="{{ url('info-layanan-pentashihan/read/'.$row_gallery_mushaf->slug) }}">
                    <div class="item-image">
                        <img src="{{ asset($row_gallery_mushaf->image) }}">
                        <div class="text-left">{{ $row_gallery_mushaf->title }}</div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </section>

    <section id="galeryMushaf" class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="title-content">PETA PENERBIT MUSHAF AL-QUR'AN INDONESIA</h4>
                <div class="mapsFooter" id="mapsPenerbit"></div>
            </div>
        </div>
    </section>

@endsection

@push('js')

<script type="text/javascript">
    /**
     * link href
     */
    function hrefUrl(href){
        document.location.href=href;
    }

    /**
     * slider for index
     */
    var swiper = new Swiper('#swiperSlider', {
        loop: true,
        slidesPerView: 'auto',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicMainBullets: 1
        },
        navigation: {
          nextEl: '#sliderNext',
          prevEl: '#sliderPrev',
        },
    });

    var swiper = new Swiper('#swiperBannerTop', {
        slidesPerView: 4,
        spaceBetween: 10,
        // centeredSlides: true,
        loop: {{ (count($new_info) > 4 ? 'true' : 'false') }},
        loopFillGroupWithBlank: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                    spaceBetween: 20,
            },
            768: {
                slidesPerView: 2,
                    spaceBetween: 40,
            },
            1024: {
                slidesPerView: 4,
                    spaceBetween: 50,
            },
        }
    });

    function sliderArrowLeft(){
        var width = 0;
        $('.swiper-pagination-bullet').each(function(){
            width += 13;
        });
        width += 100;

        $('#sliderPrev').css('right',''+ width + 'px');
    }
    sliderArrowLeft();


    var swipperMushaf = new Swiper('#swipperMushaf', {
        spaceBetween: 30,
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
        navigation: {
          nextEl: '#galleryMushafNext',
          prevEl: '#galleryMushafPrev'
        }
    });

    @if(Kemenag::message() != '')
    @if(Kemenag::messageType() == 'warning')
    $('#contentInformation').find("p").css('background-color','#f39c12');
    @endif
    @if(Kemenag::messageType() == 'danger')
    $('#contentInformation').find("p").css('background-color','#dd4b39');
    @endif
    $('#contentInformation').find("p").html("{{Kemenag::message()}}");
    $('#contentInformation').show(500);
    @endif
</script>

@endpush

@push('css')
<style>
    #swiperBannerTop {
        margin-bottom: 30px;
    }
    .swiper-button-next, .swiper-container-rtl .swiper-button-prev {
        background: #FFF !important;
        color: #575757 !important;
        font-size: 33px;
        text-align: center;
        color: #575757;
        box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.25);
    }
    .swiper-button-next::after, .swiper-container-rtl .swiper-button-prev::after {
        content: ' \276F';
    }
    .swiper-button-prev, .swiper-container-rtl .swiper-button-next {
        background: #FFF !important;
        color: #575757 !important;
        font-size: 33px;
        text-align: center;
        color: #575757;
        box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.25);
    }
    .swiper-button-prev::after, .swiper-container-rtl .swiper-button-next::after {
        content: ' \276E';
    }
    .swiper-button-next, .swiper-button-prev {
        width: 54px;
        height: 54px;
        border-radius: 50%;
    }

    #mapsPenerbit{
        height:450px;
    }
    @media screen(max-width: 500px){
        #mapsPenerbit{
            height:350px;
        }
    }
</style>
@endpush
