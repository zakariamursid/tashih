@extends('front/template/mastertemplate')

@section('content')

<!--	<section class="container tashih animated infinite fadeInUp" style="padding-left: 0; padding-right: 0;">-->
    <section class="container tashih" style="padding-left: 0; padding-right: 0;">
		<div class="row no-margin">
			<div class="content-breadcrumb col-sm-12 animated fadeInLeft">
				<p>PENTASIHAN / DOWNLOAD MATERI PENTASHIHAN</p>
			</div>

			<div id="content" class="col-sm-12">
				<div id="listRegulasi" class="row no-margin">
    				<div class="row">
    					<div class="col-sm-8">
    						<h4 class="medium-bold-black">DOWNLOAD MATERI PENTASIHAN</h4>
    					</div>
    					<div class="col-sm-4">
    					    <!--<div class="form-group ">-->
    					        <!--<label>Pilih Kategori</label>-->
    					        <select class="form-control pull-right" name="kategori_download" id="kategori_download" onchange="location = this.value;">
    					            <option value="#">Pilih Kategori</option>
    					            @foreach($kategori_download as $data)
    					            <option @if($_GET['category']==$data->id) selected @endif value="{{request()->fullUrlWithQuery(['category'=>$data->id])}}">{{$data->nama_kategori}}</option>
    					            @endforeach
    					        </select>
    					    <!--</div>-->
    					<!--</div>-->
					</div>
					
					@if(count($regulasi ) <= 0 )
					<div class="col-md-12">
					<h4 class="medium-bold-green">Tidak ada materi ditemukan.</h4>
					</div>
					@else
					@foreach($regulasi as $row)
					<div class="col-sm-12" style="margin-top: 15px;margin-bottom: 15px;">
						<div class="box-thumbnail open-modal" data-id="{{$row->id}}" data-url="{{url($row->file)}}" data-title="{{$row->judul}}" data-deskripsi="{{$row->deskripsi}}">
						    <div class="row">
						        <div class="col-sm-6">
						            <p class="medium-bold-green">{{$row->judul}}</p>
        							<table>
        								<tr>
        									<td class="verysmall-bold-black">Kategori</td>
        									<td width="15px" align="center" class="verysmall-bold-black">:</td>
        									<td class="verysmall-bold-black">{{$row->nama_kategori}}</td>
        								</tr>
        							</table>
						        </div>
						        <div class="col-sm-6" >
						            
						        </div>
						        
						    </div>
							
							<!--<hr class="hr-grey">-->
							
						</div>
					</div>
					@endforeach
					@endif

				</div>
			</div>

			{!! $regulasi->links() !!}


		</div>
	</section>

	<!-- Modal -->
	<div id="modalViewer" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">
	    <!-- Modal content-->
	    <div class="modal-content">

	      <div class="modal-header">
          <h4 id="modal-title" class="medium-bold-green"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
	      
	      <div class="modal-body">
	        <p class="medium-bold-green">Deskripsi</p>
	        <br>
	        <div class="form-group konten-deskripsi"></div>
	        <hr class="hr-grey">
	        <iframe src=""></iframe>
	        <br>
	        <p>Materi materi download</p>
	        <div id="download_materi"></div>
	      </div>

	      <div class="modal-footer">
	        
	        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
	      </div>

	    </div>
	  </div>
	</div>

@endsection

@push('js')

    <script type="text/javascript">
        $('.open-modal').on('click',function($row){
            let src = $(this).data("url");
            let id_satu = $(this).data("id");
            let title = $(this).data("title");

            let url_download="{{url('download-list-file')}}/"+id_satu;

            $.ajax({
                url:"{{url('list-file')}}/"+id_satu,
                type:"GET",
                data:{
                    id:id_satu,
                },
                success:function(data){
                    let content = '';
                    let item = data.item;

                    content += '<table class="table">';
                    content += '<tr>';
                    content += '<th>Nama File</th>';
                    content += '<th>Download</th>';
                    content += '</tr>';

                    content +='<tr>';
                    content +='<td> '+title+' </td>';
                    content +='<td>';
                    content +='<a href="'+url_download+'" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Download File</a>';
                    content +='</td>';
                    content +='<tr>';

                    $.each(item,function(key,value){
                        url = "{{url('download-list-file')}}/"+value.id;
                        if(value.name_file!=null){
                        content += '<tr>';
                        content += '<td>Materi '+value.name_file+'</td>';
                        content += '<td><a href="'+url+'" class="btn btn-xs btn-success"><span class="fa fa-download"></span> Download File</a></td>';
                        content += '</tr>';
                        }
                    });

                    content += '</table>';

                    $("#download_materi").html(content);
                },
                error:function(data){
                    alert(data)
                }
            });

            $("#modal-title").text(title);
            $('#modalViewer').find("iframe").attr("src",src);
            $("#download-materi").attr("href",src);
            $(".konten-deskripsi").html($(this).data("deskripsi"));
            $('#modalViewer').modal("show");
        })

        $(window).ready(function(){
            $('.pagination').find('li').addClass('page-item');
            $('.pagination').find('a').addClass('page-link');
            $('.pagination').find('span').addClass('page-link');
            $('.pagination').addClass('justify-content-center');
        })
    </script>

@endpush
@push('css')

    <style>
        .box-thumbnail:hover{
            cursor:pointer;
            background:#f8f9fa;
        }
    </style>

@endpush
