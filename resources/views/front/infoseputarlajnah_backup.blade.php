@extends('front/template/mastertemplate')

@section('content')
  <section id="newsDetail" class="container">
		<div class="content-breadcrumb">
			<p>HOME/INFO SEPUTAR LAJNAH</p>
		</div>

		<div class="row">
			<div id="content" class="col-sm-8 animated fadeInLeft">
				
				@foreach($info_seputar_lajnah as $row)
				<a href="{{asset('info-seputar-lajnah/read/'.$row->slug)}}">
					<div class="row no-margin" style="padding: 24px 24px 0">
						<div class="col-sm-7">
							<h2 class="medium-bold-black">{{$row->title}}</h2>
							<p class="small-grey">{{$row->content}}</p>
							<hr class="hr-grey" style="margin-top: 6px;margin-bottom: 6px;">
							<p class="info-news no-margin" style="padding: 0;">
								<i class="fa fa-user"></i> {{strtoupper($row->admin_name)}}
								<span> | </span>
								<i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia($row->created_at)}}
							</p>
						</div>
						<div class="col-sm-5">
							<div class="thumbnail" style="width: 100%;height: 170px;">
								<img src="{{$row->image}}" class="img-fluid">
							</div>
						</div>
					</div>
				</a>
				@endforeach
				<div id="paginateBox" align="center">
					{!! $info_seputar_lajnah->render() !!}
				</div>
			</div>

			<div class="col-sm-4 relation animated fadeInRight">
				<label class="title">INFO SEPUTAR LAJNAH LAINNYA</label>
				
				@foreach($info_lainnya as $row)
				<hr>
				<a href="{{asset('info-seputar-lajnah/read/'.$row->slug)}}">
				<div class="row">
					<div class="col-sm-3">
						<div class="thumbnail">
							<img src="{{$row->image}}">
						</div>
					</div>
					<div class="col-sm-9">
						<label>{{$row->title}}</label>
						<p>
							{{strtoupper($row->admin_name)}}
							<span> | </span>
							{{Kemenag::dateIndonesia($row->created_at)}}
						</p>
					</div>
				</div>
				</a>
				@endforeach
				
				<!-- info -->
		    <div id="info">
					<div class="col-sm-12">
						<a href="{{asset('persyaratan-pengajuan-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
		      	<a href="{{asset('pendaftaran-penerbit')}}">
		      		<img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	@if(CRUDBooster::myId() != '')
		      	<a href="{{asset('pengajuan-tanda-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	<a href="{{asset('petunjuk-teknis-pencetakan')}}">
		      		<img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	<a href="{{asset('pelaporan-mushaf-bermasalah')}}">
		      		<img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
					</div>
				</div><!-- ./ info -->
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript">
	$(window).ready(function(){
		$('.pagination').find('li').addClass('page-item');
		$('.pagination').find('a').addClass('page-link');
		$('.pagination').find('span').addClass('page-link');
		$('.pagination').addClass('justify-content-center');
	})

	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
	  resizeHr()
	});
</script>
@endsection