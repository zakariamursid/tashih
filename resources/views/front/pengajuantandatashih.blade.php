@extends('front/template/mastertemplate')

@section('content')
	<section id="pentashihan" class="master container">
		<div class="row">
			<div class="content-breadcrumb col-sm-12 wow animated fadeInLeft">
				<p>PENTASIHAN/PENGAJUAN TASHIH</p>
			</div>
			
			<div id="contentInformation" class="col-sm-10 mr-auto ml-auto wow animated fadeInBottom animated" style="margin-top: 0;">
				<p align="center" class="no-margin" style="">
					Halaman ini berfungsi untuk mengirim naskah mushaf Al-Qur'an untuk ditashih.
				</p>
			</div>

			<div id="content" class="col-sm-12 wow animated fadeInLeft" style="padding-top: 0;">
				@if(CRUDBooster::myId() == '')
					<center>
						<h2 class="medium-bold-black" style="margin-top: 25px;">Anda harus login terlebih dahulu untuk mengakses halaman ini</h2>
					</center>
				@else
					<form action="{{ asset('pengajuan-tanda-tashih/save') }}" method="POST" enctype="multipart/form-data">
						<div id="infoDokumen" class="row" style="border-bottom: 1.6px solid #DCDCDC;">
							<div class="col-sm-12">
								<h4 class="medium-bold-black">FORM PENGAJUAN TANDA TASHIH</h4>
								<h5 class="medium-black">Silakan isi form di bawah ini!</h5>
								<p class="medium-bold-green">Info Dokumen</p>
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">Nama Penerbit *</label>
								<input type="text" value="{{CRUDBooster::myName()}}" required="" class="form-control" disabled="true">
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">Penanggung Jawab Produk *</label>
								<input type="text" name="penanggung_jawab_produk" placeholder="Tulis penanggung jawab produk" required="" class="form-control">
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Nama Produk * 
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dengan judul kitab yang akan di daftarkan"></i>
								</label>
								<input type="text" name="nama_produk" placeholder="Tulis nama produk" required="" class="form-control">
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Scan Surat Pendaftaran Akun Penerbit *
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>
								</label><br>
								<input type="file" name="surat_permohonan" class="file-forum" required="">
							</div>

							<div class="col-sm-6 form-group">
								<div class="row">
									<div class="col-sm-6">
										<label class="verysmall-green-default">
											Ukuran (cm)  *
											<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dalam ukuran centimeter, dan format panjang X tinggi. misal:15cm X 20cm"></i>
										</label>
										<input type="text" name="ukuran" placeholder="Tulis ukuran" required="" class="form-control">
									</div>
									<div class="col-sm-6">
										<label class="verysmall-green-default">
											Oplah *
											<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dengan angka dan jml unit. misal:100000 eksemplar"></i>
										</label>
										<input type="text" name="oplah" placeholder="Tulis oplah" required="" class="form-control">
									</div>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Nama Percetakan 
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Opsional. Hanya diisi jika pihak penerbit terpisah dengan pihak percetakan"></i>
								</label>
								<input type="text" name="nama_percetakan" placeholder="Tulis nama percetakan" class="form-control">
							</div>

							<div class="col-sm-12 form-group">
								<div class="row">
									<div class="col-sm-4 mr-auto">
										<div class="form-group">
											<label class="verysmall-green-default">
												Jenis Naskah *
												<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Jika jenis naskah digital, maka harus mengupload file aplikasi"></i>
											</label>
											<select id="id_m_jenis_naskah" name="id_m_jenis_naskah" required="" class="form-control select2">
									  		<option class="select-empty-value" value="">Pilih Jenis Naskah</option>
									  		@foreach($jenis_naskah as $row)
									  			<option value="{{$row->id}}">{{$row->name}}</option>
									  		@endforeach
									  	</select>
									  	<div style="padding-top: 15px;display: none;">
									  		<input id="fileNaskah" type="file" name="file_naskah" class="file-forum" required="false">
									  	</div>
										</div>
										
										<div class="form-group">
											<label class="verysmall-green-default">
												Jenis Mushaf *
											</label>
											<select name="id_m_jenis_mushaf" required="" class="form-control select2">
									  		<option class="select-empty-value" value="">Pilih Jenis Mushaf</option>
									  		@foreach($jenis_mushaf as $row)
									  			<option value="{{$row->id}}">{{$row->name}}</option>
									  		@endforeach
									  	</select>
									  </div>
									</div>

									<div class="col-sm-6 ml-auto">
										<label class="verysmall-green-default">
											Keterangan *
										</label>
										<textarea name="keterangan" placeholder="Tulis keterangan" required="" class="form-control" rows="5"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div id="dataNaskah" class="row">
							<div class="col-sm-12">
								<h4 class="medium-bold-green">Data Naskah</h4>
							</div>
							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Scan Gambar Cover *
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>
								</label><br>
								<input type="file" name="gambar_cover" required="" class="file-forum">
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Apakah Sudah dilakukan Tashih Internal? *
								</label><br>
								<div class="form-check form-check-inline">
								  <input class="form-check-input" name="tashih_internal" type="radio" name="inlineRadioOptions" id="sudah" value="Sudah">
								  <label class="form-check-label" for="sudah">Sudah</label>
								</div>
								<div class="form-check form-check-inline">
								  <input class="form-check-input" name="tashih_internal" type="radio" name="inlineRadioOptions" id="belum" value="Belum" checked="">
								  <label class="form-check-label" for="belum">Belum</label>
								</div>
							</div>

							<div class="col-sm-6 form-group">
								<label class="verysmall-green-default">
									Scan Contoh Dokumen Naskah *
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="File harus dalam format PDF"></i>
								</label><br>
								<input type="file" name="dokumen_naskah" required="" class="file-forum">
							</div>

							<div class="col-sm-6 form-group">
								<label id="labelBuktiTashihInternal" class="verysmall-green-default">
									Scan Bukti Tashih Internal
									<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>
								</label><br>
								<input id="buktiTashihInternal" type="file" name="bukti_tashih_internal" class="file-forum">
							</div>

							<div class="col-sm-12">
								<div id="accordion">
								  <div class="card">
								    <div class="card-header" id="headingOne">
								      <h5 class="mb-0">
								        <button type="button" class="btn btn-link text-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none;">
								          Materi Tambahan Lain (*Opsional tidak harus diisi) <i class="fa fa-arrow-circle-down"></i>
								        </button>
								      </h5>
								    </div>

								    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
								      <div class="card-body">
								        <div class="row">
								        	<div class="col-sm-7 form-group">
														@foreach($materi_tambahan as $row)
														<div class="form-check form-check-inline">
														  <input class="form-check-input" name="materi_tambahan_lain[]" type="checkbox" name="inlineRadioOptions" value="{{$row->name}}">
														  <label class="form-check-label" for="{{$row->name}}">{{$row->name}}</label>
														</div>
														@endforeach

														<div class="form-check form-check-inline">
														  <input id="lainnya" class="form-check-input" name="lainnya" type="checkbox" name="inlineRadioOptions" value="Lainnya">
														  <label class="form-check-label" for="Lainnya">Lainnya</label>
														</div>
														<div id="wrapper" class="row" style="margin-top: 25px; display: none;">
															<div class="col-sm-1">
																<button type="button" id="buttonWrapper" class="btn btn-sm btn-success" data-toggle="tooltip" title="Jika form disamping dikosongkan tidak akan masuk kedalam data">
																	<i class="fa fa-plus"></i>
																</button>
															</div>
															<div id="wrapperAppend" class="col-sm-11 row">
																<div class="col-sm-6 form-group relative">
																	<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control lainnya">
																	<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>
																</div>
																<div class="col-sm-6 form-group relative">
																	<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control lainnya">
																	<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>
																</div>
															</div>
														</div>
								        	</div>

													<div class="col-sm-5 ml-auto">
														<label id="labelPenanggungJawabMateriTambahan" class="verysmall-green-default">
															Penanggung Jawab Materi Tambahan
															<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Dapat diisi lebih dari 1 orang, dan dipisahkan oleh tanda koma. misal: Andi Fulan, Ilham Fulan, Ridwan Fulan"></i>
														</label>
														<textarea id="penanggungJawabMateriTambahan" name="penanggung_jawab_materi_tambahan" placeholder="Tulis penanggung jawab materi tambahan" required="" class="form-control" rows="3"></textarea>
													</div>
								        </div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{csrf_token()}}">

						<div class="row">
							<div class="col-sm-2 ml-auto">
								<button type="submit" class="btn btn-green" value="KIRIM">KIRIM</button>
							</div>
						</div>
					</form>
				@endif
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript">
	$('#id_m_jenis_naskah').change(function(){
		let text = $(this).find('option:selected').text();
		let array = text.split(" ");

		$.each(array,function(index,value){
			if (value == 'Digital' || value == 'digital' || value == 'DIGITAL') {
				$('#fileNaskah').parent().show(600);
				$('#fileNaskah').attr('required',true);
			}else{
				$('#fileNaskah').parent().hide(600);
				$('#fileNaskah').attr('required',false);
			}
		})
	})

	$('input[name="tashih_internal"]').change(function(){
		let val = $(this).val();
		if (val == 'Sudah') {
			$('#labelBuktiTashihInternal').html('Scan Bukti Tashih Internal * <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>');
			$('#buktiTashihInternal').attr('required',true)
		}else{
			$('#labelBuktiTashihInternal').html('Scan Bukti Tashih Internal <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file PDF,JPEG,JPG,PNG,GIF"></i>');
			$('#buktiTashihInternal').attr('required',false)
		}
	})

	$('#lainnya').change(function(){
		if (this.checked) {
			$("#wrapper").show(500);
			$('#wrapperAppend').find('.lainnya').each(function(){
				$(this).attr('required',true);
			})
		}else{
			$("#wrapper").hide(500);
			$('#wrapperAppend').find('.lainnya').each(function(){
				$(this).attr('required',false);
			})
		}
	})

	$('input[type="checkbox"]').change(function(){
		let checkedNum = $('input[type="checkbox"]:checked').length;

		if (checkedNum === 0) {
			$('#penanggungJawabMateriTambahan').attr('required',false);
			$('#labelPenanggungJawabMateriTambahan').html('Penanggung Jawab Materi Tambahan <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Dapat diisi lebih dari 1 orang, dan dipisahkan oleh tanda koma. misal: Andi Fulan, Ilham Fulan, Ridwan Fulan"></i>');
		}else{
			$('#labelPenanggungJawabMateriTambahan').html('Penanggung Jawab Materi Tambahan * <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Dapat diisi lebih dari 1 orang, dan dipisahkan oleh tanda koma. misal: Andi Fulan, Ilham Fulan, Ridwan Fulan"></i>');
			$('#penanggungJawabMateriTambahan').attr('required',true);
		}
	})

	$('#buttonWrapper').click(function(){
		let view = '';
		view += '<div class="col-sm-6 form-group relative">'
		view += '<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control">'
		view += '<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>'
		view += '</div>'

		$('#wrapperAppend').append(view);
	})
	$('#wrapperAppend').on('click','.btn-remove',function(){
		$(this).parent().remove();
	})

	$('.select2').select2({
  });

	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})

  @if(Kemenag::message() != '')
  	@if(Kemenag::messageType() == 'warning')
			$('#contentInformation').find("p").css('background-color','#f39c12');
  	@endif
  	@if(Kemenag::messageType() == 'danger')
			$('#contentInformation').find("p").css('background-color','#dd4b39');
  	@endif

  	$('#contentInformation').find("p").html("{{Kemenag::message()}}");
  	$('#contentInformation').show(500);
  @endif
</script>
@endsection