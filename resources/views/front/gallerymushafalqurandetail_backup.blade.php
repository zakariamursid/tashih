@extends('front/template/mastertemplate')

@section('content')
  <section id="newsDetail" class="container">
		<div class="content-breadcrumb">
			<p>HOME/INFO LAYANAN PENTASHIHAN</p>
		</div>

		<div class="row">
			<div id="content" class="col-sm-8 wow animated fadeInLeft">
				<h1>{{$gallery_mushaf->title}}</h1>
				<p class="info-news">
					<i class="fa fa-user"></i> {{strtoupper($gallery_mushaf->admin_name)}}
					<span> | </span>
					<i class="fa fa-book"></i> {{strtoupper($gallery_mushaf->admin_name)}}
					<span> | </span>
					<i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia($gallery_mushaf->created_at)}}
				</p>
				<div id="imgNews" class="thumbnail">
					<img src="{{$gallery_mushaf->image}}" class="img-fluid">
				</div>
				<div id="detailContent">
					<p>
						{!! $gallery_mushaf->content !!}
					</p>
					<p>
						<div class="row">
							<div class="col-lg-2 col-md-2">
							  <b>Share Link</b>
							</div>
							<div class="col-lg-1 col-md-1">
								<div style="cursor:pointer" id="facebook-share" class="share"><img class="img-responsive" src="{{asset('image/facebook.png')}}" style="width: 40px; height: auto;"></div>
								<script type="text/javascript">
									// Facebook Share
									$('#facebook-share').click(function () {
										url = location.href;
										title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
										window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');

									});
								</script>
							</div>

							<div class="col-md-1 col-sm-1 col-xs-1 share-sosmed" style="">
								<div style="cursor:pointer" id="google-share" class="share"><img class="img-responsive" src="{{asset('image/google-plus.png')}}" style="width: 40px; height: auto;"></div>
								<script type="text/javascript">
									// google Share
									$('#google-share').click(function () {
										url = location.href;
										title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
										window.open('https://plus.google.com/share?url=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');
									});
								</script>
							</div>

							<div class="col-md-1 col-sm-1 col-xs-1 share-sosmed" style="">
								<div style="cursor:pointer" id="twitter-share" class="share"><img class="img-responsive" src="{{asset('image/twitter.png')}}" style="width: 40px; height: auto;"></div>
								<script type="text/javascript">
									// google Share
									$('#twitter-share').click(function () {
										url = location.href;
										title = '{{url("galeri-mushaf-alquran/".$gallery_mushaf->slug)}}';
										window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(url) + '&t=' + encodeURIComponent(title), 'sharer', 'toolbar=0,status=0,width=626,height=436');
									});
								</script>
							</div>
						</div>
					</p>
				</div>
			</div>

			<div class="col-sm-4 relation wow animated fadeInRight">
				<label class="title">INFO LAYANAN PENTASHIHAN LAINNYA</label>
				
				@foreach($info_lainnya as $row)
				<hr>
				<a href="{{asset('info-layanan-pentashihan/read/'.$row->slug)}}">
				<div class="row">
					<div class="col-sm-3">
						<div class="thumbnail">
							<img src="{{$row->image}}">
						</div>
					</div>
					<div class="col-sm-9">
						<label>{{$row->title}}</label>
						<p>
							{{strtoupper($row->admin_name)}}
							<span> | </span>
							{{Kemenag::dateIndonesia($row->created_at)}}
						</p>
					</div>
				</div>
				</a>
				@endforeach

				<!-- info -->
		    <div id="info">
					<div class="col-sm-12">
						<a href="{{asset('persyaratan-pengajuan-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
		      	<a href="{{asset('pendaftaran-penerbit')}}">
		      		<img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	@if(CRUDBooster::myId() != '')
		      	<a href="{{asset('pengajuan-tanda-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	<a href="{{asset('petunjuk-teknis-pencetakan')}}">
		      		<img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	<a href="{{asset('pelaporan-mushaf-bermasalah')}}">
		      		<img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
					</div>
				</div><!-- ./ info -->
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript">
	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
	  resizeHr()
	});
</script>
@endsection