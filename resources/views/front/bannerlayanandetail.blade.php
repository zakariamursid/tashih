@extends('front/template/mastertemplate')

@section('content')
  <section id="newsDetail" class="container">
		<div class="content-breadcrumb">
			<p>HOME/LAYANAN</p>
		</div>

		<div class="row">
			<div id="content" class="col-sm-8 wow animated fadeInLeft">
				<h1>{{ $detail->title }}</h1>
				<div id="imgNews" class="thumbnail" style="margin-left: 0px;">
					<img src="{{ asset($detail->image) }}" class="img-fluid">
				</div>
				<div id="detailContent">
					<p>
						{!! $detail->content !!}
					</p>
				</div>
			</div>

			<div class="col-sm-4 relation wow animated fadeInRight">
				<label class="title">INFO PELAYANAN LAINNYA</label>
				@foreach($random as $x => $xrow)
				<div class="col-sm-12 row">
					<a href="{{ $xrow->link }}" class="mb-3">
						<img src="{{ asset($xrow->image) }}" class="img-fluid animated infinite flipInX">
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript">
	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
		resizeHr()
	});
</script>
@endsection
@push('css')
<style>
    @font-face {
        font-family: FontArabTashih;
        src: url('{{ asset("font/font_arab_tashih.OTF") }}');
    }
</style>
@endpush