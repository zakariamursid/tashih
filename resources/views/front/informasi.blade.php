@extends('front/template/mastertemplate')

@section('content')
  <section id="newsDetail" class="container">
		<div class="content-breadcrumb">
			<p>HOME/INFORMASI</p>
		</div>

		<div class="row">
			<div id="content" class="col-sm-8 wow animated fadeInLeft">
				<h1>{{$banner->title}}</h1>
				<p class="info-news">
					<i class="fa fa-user"></i> {{strtoupper($banner->admin_name)}}
					<span> | </span>
					<i class="fa fa-book"></i> {{strtoupper($banner->admin_name)}}
					<span> | </span>
					<i class="fa fa-calendar"></i> {{Kemenag::dateIndonesia($banner->created_at)}}
				</p>
				<div id="imgNews" class="thumbnail">
					<img src="{{$banner->image}}" class="img-fluid">
				</div>
				<div id="detailContent">
					<p>
						{!! $banner->content !!}
					</p>
				</div>
			</div>

			<div class="col-sm-4 relation wow animated fadeInRight">
				<label class="title">INFO SEPUTAR LAJNAH LAINNYA</label>
				
				@foreach($info_lainnya as $row)
				<hr>
				<a href="{{asset('info-seputar-lajnah/'.$row->slug)}}">
				<div class="row">
					<div class="col-sm-3">
						<div class="thumbnail">
							<img src="{{$row->image}}">
						</div>
					</div>
					<div class="col-sm-9">
						<label>{{$row->title}} {{strlen($row->title)}}</label>
						<p>
							{{strtoupper($row->admin_name)}}
							<span> | </span>
							{{Kemenag::dateIndonesia($row->created_at)}}
						</p>
					</div>
				</div>
				</a>
				@endforeach

				<!-- info -->
		    <div id="info">
					<div class="col-sm-12">
						<a href="{{asset('persyaratan-pengajuan-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_tashih.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@if((CRUDBooster::myId() != '' && !CRUDBooster::isSuperadmin()) || CRUDBooster::myId() == '' || CRUDBooster::isSuperadmin())
		      	<a href="{{asset('pendaftaran-penerbit')}}">
		      		<img src="{{asset('image/banner/img_daftar_penerbit.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	@if(CRUDBooster::myId() != '')
		      	<a href="{{asset('pengajuan-tanda-tashih')}}">
		      		<img src="{{asset('image/banner/img_pengajuan_naskah.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	@endif
		      	<a href="{{asset('petunjuk-teknis-pencetakan')}}">
		      		<img src="{{asset('image/banner/img_petunjuk_teknis.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
		      	<a href="{{asset('pelaporan-mushaf-bermasalah')}}">
		      		<img src="{{asset('image/banner/img_report.png')}}" class="img-fluid animated infinite flipInX">
		      	</a>
					</div>
				</div><!-- ./ info -->
			</div>
		</div>
	</section>
@endsection

@push('js')
<script type="text/javascript">
	function resizeHr(){
		var marginRight = parseInt($('#newsDetail').css("marginRight"));
		var paddingRight = parseInt($('#newsDetail').css("paddingRight"));
		var margin = marginRight+paddingRight;

		$('#newsDetail .relation hr').each(function(){
			$(this).css("marginRight","-"+margin+"px");
		})
	}
	$(document).ready(function(){
		resizeHr()
	})
	$( window ).resize(function() {
	  resizeHr()
	});
</script>
@endpush