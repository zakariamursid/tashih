<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<div class="panel panel-default">
    <div class="panel-body p-3">
        <div class="row">
            <form action="" type="GET">
                <div class="col-md-5">
                    <div class="form-group">
						<label>Nama Survei</label>
						<select name="select_survey" id="select_survey" class="form-control" onchange="location = this.value;">
							<option value="{{ request()->fullUrlWithQuery(['nama_survey'=>'', 'name' => '']) }}" @if(g('nama_survey') == '') selected @endif>Pilih Nama Survei**</option>
							@foreach($survey as $y => $yrow)
                            <option value="{{ request()->fullUrlWithQuery(['nama_survey' => $yrow->id, 'name' => $yrow->nama]) }}" @if(g('nama_survey') == $yrow->id) selected @endif>{{ $yrow->nama }}</option>
                            @endforeach
						</select>
					</div>
                </div>
            </form>
            <div class="col-md-1">
                <a href="{{ CRUDBooster::mainpath('export-survey?nama_survey='.g('nama_survey')) }}" class="btn btn-md btn-success mt-3"><span class="fa fa-file-excel-o"></span> Export Survey</a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body p-3">

        <div class="header-report">
            <h4>{{ (g('name') ? g('name') : '') }}</h4>
            <h4>LAYANAN PENTASHIHAN MUSHAF AL-QUR'AN</h4>
            <h4>LAJNAH PENTASHIHAN MUSHAF AL-QUR’AN</h4>
            <h4>BADAN LITBANG DAN DIKLAT KEMENTERIAN AGAMA RI</h4>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Unsur</th>
                        <th>Rata Rata</th>
                        <th>Index</th>
                        <th>Mutu Pelayanan</th>
                        <th>Kinerja Pelayanan</th>
                    </tr>
                </thead>
                <tbody>
                    @php $unsurUse = 0;$sumAvg = 0; @endphp
                    @foreach($result as $x => $row)
                    <tr>
                        <td>{{ $row->nama }}</td>
                        <td>{{ $row->average_nilai }}</td>
                        <td>{{ $row->index }}</td>
                        <td>{{ $row->mutu }}</td>
                        <td>{{ $row->kinerja }}</td>
                    </tr>
                    @php
                        if($row->average_nilai) {
                            $unsurUse++;

                            $sumAvg += $row->average_nilai;
                        }
                    @endphp
                    @endforeach
                    <tr>
                        <th>Jumlah Unsur Digunakan :</th>
                        <td colspan="4">{{$unsurUse}}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <th>{{ ($sumAvg > 0 ? $sumAvg/$unsurUse : 0) }}</th>
                        <th>{{ ($sumAvg > 0 ? Kemenag::indexReportSurvey($sumAvg/$unsurUse) : 0) }}</th>
                        <th>{{ ($sumAvg > 0 ? Kemenag::mutuPelayananReportSurvey($sumAvg/$unsurUse) : 0) }}</th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Jumlah responden</th>
                        <th colspan="4">{{ $users }}</th>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered" style="width: 60%;">
                <tr>
                    <th>Nilai Persepsi</th>
                    <th>Nilai Interval</th>
                    <th>Nilai Interval Konversi (NIK)</th>
                    <th>Mutu Pelayanan</th>
                    <th>Kinerja Unit Pelayanan</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>1.00 - 2.5996</td>
                    <td>25.00 - 64.99</td>
                    <td>D</td>
                    <td>Tidak baik</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>2.60 - 3.064</td>
                    <td>65.00 - 76.60</td>
                    <td>C</td>
                    <td>Kurang baik</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>3.0644 - 3.532</td>
                    <td>76.61 - 88.30</td>
                    <td>B</td>
                    <td>Baik</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>3.5324 - 4.00</td>
                    <td>88.31 - 100.00</td>
                    <td>A</td>
                    <td>Sangat baik</td>
                </tr>
            </table>
        </div>

        <div class="header-middle">
            <h4>JAWABAN ESSAY</h4>
        </div>

        <div class="table-responsive">
            <table class="table table-striped table-bordered" style="width: 60%;">
                <tr>
                    <th style="min-width: 400px;max-width: 400px;">Pertanyaan Survey</th>
                    <th style="min-width: 300px;max-width: 300px;">Jawaban</th>
                </tr>
                @foreach($essay as $row)
                <tr>
                    <td>{{ $row->pertanyaan }}</td>
                    <td>{{ $row->answer }}</td>
                </tr>
                @endforeach
            </table>
        </div>
        {{ $essay->links() }}


    </div>
</div>

@endsection
@push('bottom')
@endpush
@push('head')
<style>
    .header-report {
        overflow-wrap: anywhere;
        text-align: center;
        margin-bottom: 30px;
        margin-top: 35px;
    }
    .header-report h4 {
        font-weight: bold;
        line-height: 13px;
    }
    .mt-3 {
        margin-top: 25px;
    }
    .header-middle {
        overflow-wrap: anywhere;
        margin-bottom: 30px;
        margin-top: 35px;
    }
    .header-middle h4 {
        font-weight: bold;
        line-height: 13px;
    }
</style>
@endpush