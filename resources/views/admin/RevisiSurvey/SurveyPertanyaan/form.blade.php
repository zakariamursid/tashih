@extends('crudbooster::admin_template')
@section('content')
<section id="appMain">
<p>
    <a title="Return" href="{{ CRUDBooster::mainpath() }}">
        <i class="fa fa-chevron-circle-left "></i>&nbsp; Back To List Data Isi Survey
    </a>
</p>
<form action="{{ $link }}" method="post" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('POST') }}
<input type="hidden" name="type_form" value="{{ $type }}">
<div class='panel panel-default'>
    <div class='panel-body'>
        <div class="box-body form-horizontal">
            
            <div class="form-group header-group-0">
                <label class="control-label col-sm-2">
                    Type<span class="text-danger" title="This field is required">*</span>
                </label>
                <div class="col-sm-10">
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="type" value="Essay" v-model="form.type">
                                Essay
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" id="type" value="Pilihan" v-model="form.type">
                                Pilihan
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group header-group-0">
                <label class="control-label col-sm-2">
                    Nama<span class="text-danger" title="This field is required">*</span>
                </label>
                <div class="col-sm-10">
                    <input type="text" title="Nama" required="" placeholder="You can only enter the letter only" class="form-control" name="nama" id="nama" value="" v-model="form.nama">
                </div>
            </div>
            <div class="form-group header-group-0" v-if="form.type == 'Pilihan'">
                <label class="control-label col-sm-2">
                    Opsi Type Pilihan<span class="text-danger" title="This field is required">*</span>
                </label>
                <div class="table-responsive col-sm-10">
                    <div class="m-2">
                        <div class="row">
                            <div class="col-sm-4">
                                <select name="opsi_pilihan" id="opsi_pilihan" class="form-control" v-model="form.opsi_pilihan">
                                    <option value="">Pilih Opsi**</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="C">C</option>
                                    <option value="D">D</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="nama_pilihan" id="nama_pilihan" class="form-control" placeholder="Nama Pilihan" v-model="form.nama_pilihan">
                            </div>
                            <div class="col-sm-3">
                                <button type="button" class="btn btn-sm btn-primary" @click="form.type_form_pilihan == 'Add' ? addPilihan() : editPilihan()" style="margin: 0.2rem 0;">@{{ form.text_btn_pilihan }}</button>
                            </div>
                        </div>
                    </div>
                    
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 75px; max-width: 75px; min-width: 75px;">Opsi</th>
                            <th style="width: 200px; max-width: 200px; min-width: 200px;">Nama</th>
                            <th style="width: 50px; max-width: 50px; min-width: 50px;">#</th>
                        </tr>
                        <tr v-for="(row, i) in form.list_pilihan" v-if="form.list_pilihan.length > 0">
                            <td>@{{ row.opsi }}</td>
                            <td>@{{ row.nama }}</td>
                            <td>
                                <input type="hidden" name="pilihan_id_table[]" :value="row.pilihan_id">
                                <input type="hidden" name="opsi_table[]" :value="row.opsi">
                                <input type="hidden" name="nama_table[]" :value="row.nama">
                                <div class="btn-group">
                                    <a href="javascript:void(0)" @click="editRowPilihan(row, i)" class="btn btn-xs btn-warning"><span class="fa fa-edit"></span></a>
                                    <a href="javascript:void(0)" @click="deletePilihan(i)" class="btn btn-xs btn-danger"><span class="fa fa-trash"></span></a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <div class="box-footer">
            <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <a href="{{ CRUDBooster::mainpath() }}" class="btn btn-default">
                        <i class="fa fa-chevron-circle-left"></i> Back
                    </a>
                    @if(Request::segment(3) == 'add')
                    <input type="submit" name="submit" value="Save &amp; Add More" class="btn btn-success">
                    @endif
                    <input type="submit" name="submit" value="Save" class="btn btn-success">
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</section>
@endsection
@push('bottom')

<script>
    conf = {
        base_url : "{{ CRUDBooster::adminpath('survey-pertanyaan') }}",
        token : "{{ csrf_token() }}",
        base_asset : "{{ asset('/') }}",
        data: {!! json_encode($data) !!},
        list: {!! json_encode($list) !!}
    };
</script>
<script src="https://unpkg.com/vue@2.6.0/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
<script src="{{ asset('js/admin/RevisiSurvey/SurveyPertanyaan/form.js') }}"></script>

@endpush
@push('head')
<style>
    [v-cloak] > * { display:none }
    [v-cloak]::before { content: "loading…" }
    .m-2 {
        margin: 20px auto;
    }
</style>
@endpush