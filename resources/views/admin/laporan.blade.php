@extends('crudbooster::admin_template')
@section('content')

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="row">
	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="penerbitPerTahun" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>

	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="pendaftaranMushafPertahun" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>

	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="rekapTandaTashih" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>

	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="aduanMushafBermasalah" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
	
	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="penerbitDitolak" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
	
	<div class="col-sm-6">
		<div class="box">
			<div class="box-header">
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<div id="totalOplah" style="min-width: 100%; height: 300px; margin: 0 auto"></div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
</div>

<script>
	Highcharts.chart('penerbitPerTahun', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Pendaftaran Penerbit Pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_penerbit as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Penerbit'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Jumlah Penerbit',
			data: [0,@foreach($total_penerbit as $row) {{$row->total}}, @endforeach 0]
		}]
	});

	Highcharts.chart('pendaftaranMushafPertahun', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Pendaftaran Mushaf Pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_mushaf as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Mushaf'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Jumlah Mushaf',
			data: [0,@foreach($total_mushaf as $row) {{$row->total}}, @endforeach 0]
		}]
	});

	Highcharts.chart('rekapTandaTashih', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Rekap Tanda Tashih Pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_rekap as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Rekap'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Rekap Tanda Tashih Pertahun',
			data: [0,@foreach($total_rekap as $row) {{$row->total}}, @endforeach 0]
		}]
	});

	Highcharts.chart('aduanMushafBermasalah', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Aduan Mushaf Bermasalah Pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_aduan as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Aduan'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Aduan Mushaf Bermasalah Pertahun',
			data: [0,@foreach($total_aduan as $row) {{$row->total}}, @endforeach 0]
		}]
	});
	
	Highcharts.chart('penerbitDitolak', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Penerbit ditolak pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_penerbit_ditolak as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Ditolak'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Penerbit ditolak pertahun',
			data: [0,@foreach($total_penerbit_ditolak as $row) {{$row->total}}, @endforeach 0]
		}]
	});
	
	Highcharts.chart('totalOplah', {
		chart: {
			type: 'line'
		},
		title: {
			text: 'Total Oplah Pertahun'
		},
		xAxis: {
			categories: ['',@foreach($total_oplah as $row) {{$row->year}}, @endforeach '']
		},
		credits: {
			enabled: false
		},
		yAxis: {
			labels: {
				format: '{value:,.0f}'
			},
			title: {
				text: 'Jumlah Oplah'
			}
		},
		plotOptions: {
		  line: {
				dataLabels: {
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			name: 'Total Oplah Pertahun',
			data: [0,@foreach($total_oplah as $row) {{$row->total}}, @endforeach 0]
		}]
	});
</script>


@endsection