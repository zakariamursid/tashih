<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<style>
	.form-check-label{
		font-size: 12px;
	}

	.medium-bold-green{
	color: #000000;
	font-size: 16px;
	font-weight: bold;
	}


</style>

<p>
	<a title="Return" href="{{CRUDBooster::mainpath()}}">
		<i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Tashih
	</a>
</p>

<div class="panel panel-default">
	<div class="panel-heading">
		<strong>
			<i class="fa fa-book"></i> Preview Pendaftaran Mushaf Al-Qur’an ({{$row->jenis_permohonan}})
		</strong>
	</div>
	
	<div class="panel-body">
			<div class="box-body">
					<div class="col-sm-12">
						<center>
						<p class="medium-bold-green" style="margin-top:25px;font-size:26px;">Preview Pendaftaran Mushaf Al-Qur’an ({{$row->jenis_permohonan}})</p>
						</center>
					</div>

					<div class="col-sm-12 form-group row" style="margin-top:75px;">
						<label class="col-sm-2 col-form-label verysmall-green-default">Jenis pendaftaran mushaf</label>
						<div class="col-sm-10">: 
							{{$row->jenis_permohonan}}
						</div>
					</div>

					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">I. Informasi Data Mushaf</h3>
					</div>

					@if($row->jenis_permohonan == "Pembaruan/perpanjangan Surat Tanda Tashih")

					<div class="col-sm-12 form-group row" style="margin-top:25px;">
						<label class="col-sm-2 col-form-label verysmall-green-default">No. Pendaftaran mushaf lama *</label>
						<div class="col-sm-10">:
							{{$row->mushaf_lama_nomor_registrasi}}
						</div>
					</div>

					@endif

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Penerbit</label>
						<div class="col-sm-10">:
							{{$row->cms_users_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Produk/Mushaf</label>
						<div class="col-sm-10">:
							{{$row->nama_produk	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Penanggung Jawab Produk</label>
						<div class="col-sm-10">:
							{{$row->penanggung_jawab_produk	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Jenis Naskah</label>
						<div class="col-sm-10">:
							@php
								$jenis_naskah  = explode(';', $row->id_m_jenis_naskah); 
								$max = max($jenis_naskah);
							@endphp

							@foreach ($jenis_naskah as $val)
								{{$val}} @if($max != $val) , @endif
							@endforeach

						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Nama Percetakan</label>
						<div class="col-sm-10">:
							{{$row->nama_percetakan	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Jenis Mushaf</label>
						<div class="col-sm-10">:
							{{$row->m_jenis_naskah_name	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Deskripsi Mushaf</label>
						<div class="col-sm-10">:
							{{$row->keterangan	}}
						</div>
					</div>

					@if($row->jenis_permohonan == "Surat Izin Edar")

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Negara asal mushaf</label>
						<div class="col-sm-10">:
							{{$row->countries_name	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Penerbit asal mushaf</label>
						<div class="col-sm-10">:
							{{$row->nama_penerbit_asal	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Lembaga pentashih asal mushaf </label>
						<div class="col-sm-10">:
							{{$row->nama_lembaga_pentashih_asal	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Bukti tashih Lembaga pentashih asal mushaf </label>
						<div class="col-sm-10">

							@if($row->bukti_tashih_lembaga_pentashih_asal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->bukti_tashih_lembaga_pentashih_asal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>

					@endif


					<div class="col-sm-12 form-group row">
					<label class="col-sm-2 col-form-label verysmall-green-default">Ukuran & Oplah</label>
					<div class="col-sm-10">

					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<td>Ukuran</td>
								<td>Oplah</td>
							</tr>
						</thead>
						<tbody>
							@foreach ($row->proses_pentashihan_ukuran as $val)
									<tr>
										<td>{{$val->ukuran}}</td>
										<td>{{$val->oplah}}</td>
									</tr>
							@endforeach
						</tbody>
					</table>

					</div>
					</div>


					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">II. Informasi Data Materi Tambahan pada Mushaf</h3>
					</div>


					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Materi Tambahan Lainnya</label>
						<div class="col-sm-10">:
							@foreach ($row->materi_tambahan as $val)
								<button class="btn btn-xs btn-success">{{$val->name}}</button>
							@endforeach				
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Penanggung Jawab Materi Tambahan</label>
						<div class="col-sm-10">:
							{{$row->penanggung_jawab_materi_tambahan}}
						</div>
					</div>


					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">III. Informasi Dokumen Mushaf</h3>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Scan Surat Permohonan Pentashihan</label>
						<div class="col-sm-10">
							@php
								$surat_permohonan_ext = pathinfo($row->surat_permohonan, PATHINFO_EXTENSION);
							@endphp

							@if ($surat_permohonan_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->surat_permohonan)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($surat_permohonan_ext)}}" alt="Scan Surat Permohonan Pentashihan" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->surat_permohonan != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->surat_permohonan)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif

						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Scan Gambar Cover</label>
						<div class="col-sm-10">
							@php
								$gambar_cover_ext = pathinfo($row->gambar_cover, PATHINFO_EXTENSION);
							@endphp

							@if ($gambar_cover_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->gambar_cover)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->gambar_cover)}}" alt="Scan Gambar Cover" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->gambar_cover != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->gambar_cover)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif							
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Scan Contoh Dokumen Naskah</label>
						<div class="col-sm-10">
							@php
								$dokumen_naskah_ext = pathinfo($row->dokumen_naskah, PATHINFO_EXTENSION);
							@endphp

							@if ($dokumen_naskah_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->dokumen_naskah)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->dokumen_naskah)}}" alt="Scan Contoh Dokumen Naskah" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->dokumen_naskah != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->dokumen_naskah)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif

						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Apakah Sudah dilakukan Tashih Internal?</label>
						<div class="col-sm-10">:
							{{$row->tashih_internal	}}
						</div>
					</div>


					<div class="col-sm-12 form-group row">
						<label class="col-sm-2 col-form-label verysmall-green-default">Scan Bukti Tashih Internal</label>
						<div class="col-sm-10">
							@php
								$bukti_tashih_internal_ext = pathinfo($row->bukti_tashih_internal, PATHINFO_EXTENSION);
							@endphp

							@if($row->bukti_tashih_internal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->bukti_tashih_internal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
									<b>Tidak ada file yang dilampirkan</b>
							@endif

							@if ($bukti_tashih_internal_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->bukti_tashih_internal)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->bukti_tashih_internal)}}" alt="Scan Bukti Tashih Internal" class="img-fluid" style="margin-top:25px;"> -->
							@endif
						</div>
					</div>



			<div class="col-sm-12">
				<h4 style="margin-top:50px;"><b>Pernyataan</b></h4> <br>
				<p>
				Saya menyatakan bahwa seluruh data yang terisi dalam formulir pendaftaran mushaf Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.					
				</p>
			</div>


			</div> <!-- ./box-body -->
			
			<div class="box-footer">

			<div class="pull-right">
						<a href="{{CRUDBooster::mainpath('ralat/'.$row->id)}}" class="btn btn-primary">
							<i class="fa fa-pencil"></i> Edit
						</a>
                        <a title="Simpan" href="javascript:;" onclick="insertData()" class="btn btn-success">Lanjutkan Registrasi Mushaf</a>
						<input style="display:none;" id="addBtn" type="button" value="Save" class="btn btn-success">
			</div>

			</div> <!-- ./box-footer -->
	</div>

</div>

@endsection
@push('bottom')

<script>
	function insertData()
    {
        swal({
            title: "Apakah anda yakin?",
            text: "",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
					showCancelButton: true,
					confirmButtonText: "Ya",
					cancelButtonText: "Batal",
					closeOnConfirm: false,
					closeOnCancel: false
        },
		function(isConfirm){

           if (isConfirm){
				$("#addBtn").click();
            } else {
                swal("Dibatalkan", "", "error"); 
            }
         });
    }

	function successInsertData(text,redirect)
    {
        swal({
            title: "",
            text: text,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
					showCancelButton: true,
					confirmButtonText: "Unduh bukti pendaftaran mushaf",
					cancelButtonText: "Tutup",
					closeOnConfirm: false,
					closeOnCancel: false
        },
		function(isConfirm){

           if (isConfirm){

			window.location = redirect;

            } else {
				window.location = "{{CRUDBooster::mainpath('')}}";
            }
         });
    }


	$('#addBtn').click(function(event)
	{
        $.ajax({
            url:"{{CRUDBooster::mainpath('insert-save')}}",
            method:"POST",
            data:
            {
                '_token': "{{ csrf_token() }}",
            },

            success:function(response)
            {
                if(response.status == '200')
                {
					successInsertData(response.message,response.redirect);
                }
                else if(response.status == '500' && response.flag == 'empty_temp')
                {
					window.location = "{{CRUDBooster::mainpath('')}}";
                }
                else
                {
					swal(response.message, "", "error"); 
                }
            }
       })
	});

</script>

@endpush