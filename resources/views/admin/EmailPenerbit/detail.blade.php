<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<p>
    <a title='Return' href='{{ CRUDBooster::adminpath("email-penerbit") }}'>
        <i class='fa fa-chevron-circle-left'></i>
        &nbsp; Back To List Data Email Masuk
    </a>
</p>

<div class="panel panel-default" id="appForm">
    <div class="panel-heading">
        <strong><i class='fa fa-envelope-o'></i> Detail Email Masuk</strong>
    </div>

    <div class="panel-body p-3">
        {!! $email !!}

        @if($file)
        <p class="t-2">Download File : &nbsp;<a href="{{ $file }}" download class="btn btn-xs btn-success"><span class="fa fa-download"></span> Download File</a></p>
        @endif
    </div>
    <div class="box-footer" style="background: #F5F5F5">
        <div class="form-group">
            <a href="{{ CRUDBooster::adminpath('email-penerbit') }}" class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> Back</a>
        </div>
    </div>

</div>
@endsection
@push('bottom')


@endpush
@push('head')

<style>
    .t-2 {
        margin-top: 20px !important;
    }
</style>

@endpush