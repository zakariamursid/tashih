<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<?php 
function time_since($original)
{
	date_default_timezone_set('Asia/Jakarta');
	$chunks = array(
		array(60 * 60 * 24 * 365, 'tahun'),
		array(60 * 60 * 24 * 30, 'bulan'),
		array(60 * 60 * 24 * 7, 'minggu'),
		array(60 * 60 * 24, 'hari'),
		array(60 * 60, 'jam'),
		array(60, 'menit'),
	);
	
	$today = time();
	$since = $today - $original;
	
	if ($since > 604800)
	{
		$print = date("M jS", $original);
		if ($since > 31536000)
		{
		$print .= ", " . date("Y", $original);
		}
		return $print;
	}
	
	for ($i = 0, $j = count($chunks); $i < $j; $i++)
	{
		$seconds = $chunks[$i][0];
		$name = $chunks[$i][1];
	
		if (($count = floor($since / $seconds)) != 0)
		break;
	}
	
	$print = ($count == 1) ? '1 ' . $name : "$count {$name}";
	return $print . ' yang lalu';
}
?>
@if(CRUDBooster::myPrivilegeId() != 2)
    <div class="row">
    	@if(CRUDBooster::isSuperadmin())
    	<div class="col-lg-3 col-xs-6">
    		<!-- small box -->
    		<div class="small-box bg-green">
    			<div class="inner">
    				<h3>{{$total_penerbit}}</h3>
    				<p>Total Penerbit</p>
    			</div>
    			<div class="icon">
    				<i class="fa fa-user-md"></i>
    			</div>
    			<a target="_blank" href="{{CRUDBooster::adminPath('penerbit')}}" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
    		</div>
    	</div>
    	@endif
    	<div class="col-lg-3 col-xs-6">
    		<!-- small box -->
    		<div class="small-box bg-green">
    			<div class="inner">
    				<h3>{{$total_pentashihan}}</h3>
    				<p>Total Pentashihan</p>
    			</div>
    			<div class="icon">
    				<i class="fa fa-book"></i>
    			</div>
    			<a target="_blank" href="{{CRUDBooster::adminPath('proses-pentashihan')}}" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
    		</div>
    	</div>
    	<div class="col-lg-3 col-xs-6">
    		<!-- small box -->
    		<div class="small-box bg-green">
    			<div class="inner">
    				<h3>{{$total_info_lajnah}}</h3>
    				<p>Total Info Lajnah</p>
    			</div>
    			<div class="icon">
    				<i class="fa fa-newspaper-o"></i>
    			</div>
    			<a target="_blank" href="{{CRUDBooster::adminPath('info-seputar-lajnah')}}" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
    		</div>
    	</div>
    	<div class="col-lg-3 col-xs-6">
    		<!-- small box -->
    		<div class="small-box bg-green">
    			<div class="inner">
    				<h3>{{$total_gallery_mushaf}}</h3>
    				<p>Total Informasi Layanan Pentashihan</p>
    			</div>
    			<div class="icon">
    				<i class="fa fa-image"></i>
    			</div>
    			<a target="_blank" href="{{CRUDBooster::adminPath('info_layanan_pentashihan')}}" class="small-box-footer">Lihat Data <i class="fa fa-arrow-circle-right"></i></a>
    		</div>
    	</div>
    </div>
    
    <div class="row">
	{{--<div class="col-sm-12">
    		<div class="box">
    			<div class="box-header">
    				<h3 class="box-title">Data Pentashihan</h3>
    			</div>
    			<!-- /.box-header -->
    			<div class="box-body no-padding">
    				<table class="table table-condensed">
    					<tbody>
    						<tr>
    							<th>Nomor Urut</th>
    							<th>Nomor Tanda Tashih</th>
    							<th>Kode Tanda Tashih</th>
    							<th>Nama Penerbit</th>
    							<th>Ukuran</th>
    							<th>Nama Mushaf</th>
								<th>Tanda Tashih</th>
    							<th>Status</th>
    							<th style="width: 40px">Action</th>
    						</tr>
    						@if(empty($proses_pentashihan))
    						<tr><td colspan="5">Tashih masih kosong</td></tr>
    						@else
    							@foreach($proses_pentashihan as $row)
    								@if($row->status!="Selesai")
    								<tr>
    									<td>{{$row->nomor_registrasi}}</td>
    									<td>{{$row->ukuran_nomor}}</td>
    									<td>{{$row->ukuran_kode}}</td>
    									<td>{{$row->nama_penerbit}}</td>
    									<td>{{$row->ukuran_size}}</td>
    									<td>{{$row->nama_produk}}</td>
										<td>
											<a href="{{ url('cek-tanda-tashih/scan/'.$row->id) }}" class="btn btn-xs btn-danger">Tanda Tashih</a>
										</td>
    									<td><button class="btn btn-xs {{$row->color}}">{{($row->status=='Lolos Verifikasi') ? 'Lolos Verifikasi (Proses Pentashihan)' : $row->status}}</button></td>
    									<td><a target="_blank" href="{{CRUDBooster::adminPath('proses-pentashihan?id='.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a></td>
    								</tr>
    								@endif
    							@endforeach
    						@endif
    					</tbody>
    				</table>
    			</div>
    			<!-- /.box-body -->
    			<div class="box-footer">
    			{{$proses_pentashihan->links()}}
    			</div>
    			<!-- /.box-footer -->
    		</div>
    	</div>
    	<div class="col-md-12">
    		<div class="box" >
    			<div class="box-header">
    				<h3>Deadline Terdekat</h3>
    			</div>
    			<div class="box-body" style="max-height: 100vh;min-height:100vh;overflow-y:overlay;">
    				<ul class="timeline">
    				    
    				    @if(count($db_status_tansih) <= 0)
    				    <li>
    						<i class="fa fa-clock-o bg-gray"></i>
    						<div class="timeline-item">
    						    <h3 class="timeline-header">tidak ada data pengajuan tashih</h3>
    						</div>
    					</li>
    				    @else
    					@foreach($db_status_tansih as $data_row)

    					<!-- <li class="time-label">
    						<span class="bg-red">
    						{{ date('d M. Y',strtotime($data->tanggal)) }}
    						</span>
    					</li> -->

    					<li>
    						<i class="fa fa-user bg-aqua"></i>
    
    						<div class="timeline-item">
    						    
    						@if(new \Datetime(date('Y-m-d')) < new \Datetime(date('Y-m-d',strtotime($data_row['tanggal_deadline']))) )
    						@else
    						<span class="time"><i class="fa fa-clock-o"></i> {{time_since(strtotime($data_row['tanggal_deadline']))}}</span>
    						@endif
    
    						<h3 class="timeline-header no-border"><!--<button class="btn btn-xs {{$color}}">{{$data->status}}</button> -->
    						    <b>
    						    {{$data_row['nama_produk']}} - {{$data_row['name']}} - {{$data_row['nomor_registrasi']}} - {{date('d M Y',strtotime($data_row['tanggal_deadline']))}}
    						    </b>
    						</h3>
    						</div>
    					</li>

    					@endforeach
    					@endif
    
    					<li>
    						<i class="fa fa-clock-o bg-gray"></i>
    					</li>
    				</ul>
    			</div>
    			<div class="box-footer">

    			<br><br>
    			</div>
    		</div>
    	</div>--}}
		<div class="col-sm-6">
    		<div class="box">
    			<div class="box-header">
    				<h3 class="box-title">Data Pentashihan</h3>
    			</div>
    			<!-- /.box-header -->
    			<div class="box-body no-padding" style="min-height:100vh;overflow:hidden;overflow-y:overlay;">
    				<table class="table table-condensed">
    					<tbody>
    						<tr>
    							<th>Nomor Registrasi</th>
    							<th>Nama Mushaf</th>
    							<th>Nama Penerbit</th>
    							<th>Status</th>
    							<th style="width: 40px">Action</th>
    						</tr>
    						@if(empty($proses_pentashihan))
    						<tr><td colspan="5">Tashih masih kosong</td></tr>
    						@else
    							@foreach($proses_pentashihan as $row)
    								@php
    								switch ($row->status) {
    									case 'Registrasi Berhasil':
    										$color = 'btn-success';
    										break;
    
    									case 'Naskah Diterima':
    										$color = 'btn-warning';
    										break;
    
    									case 'Tidak Lolos Verifikasi':
    										$color = 'btn-default';
    										break;
    
    									case 'Lolos Verifikasi':
    										$color = 'btn-success';
    										break;
    
    									case 'Proses Perbaikan Naskah':
    										$color = 'btn-warning';
    										break;
    
    									case 'Pentashihan Tahap Selanjutnya':
    										$color = 'btn-primary';
    										break;
    
    									case 'Selesai':
    										$color = 'btn-success';
    										break;
    									
    									default:
    										$color = 'btn-default';
    										break;
    								}
    								@endphp
    								@if($row->status!="Selesai")
    								<tr>
    									<td>{{$row->nomor_registrasi}}</td>
    									<td>{{$row->nama_produk}}</td>
    									<td>{{$row->nama_penerbit}}</td>
    									<td><button class="btn btn-xs {{$color}}">{{($row->status=='Lolos Verifikasi') ? 'Lolos Verifikasi (Proses Pentashihan)' : $row->status}}</button></td>
    									<td><a target="_blank" href="{{CRUDBooster::adminPath('proses-pentashihan?id='.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a></td>
    								</tr>
    								@endif
    							@endforeach
    						@endif
    					</tbody>
    				</table>
    			</div>
    			<!-- /.box-body -->
    			<div class="box-footer">
    			{{$proses_pentashihan->links()}}
    			</div>
    			<!-- /.box-footer -->
    		</div>
    	</div>
    	<div class="col-md-6">
    		<div class="box" >
    			<div class="box-header">
    				<h3>Deadline Terdekat</h3>
    			</div>
    			<div class="box-body" style="max-height: 100vh;min-height:100vh;overflow-y:overlay;">
    				<ul class="timeline">
    				    
    				    @if(count($db_status_tansih) <= 0)
    				    <li>
    						<i class="fa fa-clock-o bg-gray"></i>
    						<div class="timeline-item">
    						    <h3 class="timeline-header">tidak ada data pengajuan tashih</h3>
    						</div>
    					</li>
    				    @else
    					@foreach($db_status_tansih as $data_row)

    					{{--<li class="time-label">
    						<span class="bg-red">
    						{{ date('d M. Y',strtotime($data->tanggal)) }}
    						</span>
    					</li>--}}

    					<li>
    						<i class="fa fa-user bg-aqua"></i>
    
    						<div class="timeline-item">
    						    
    						@if(new \Datetime(date('Y-m-d')) < new \Datetime(date('Y-m-d',strtotime($data_row['tanggal_deadline']))) )
    						@else
    						<span class="time"><i class="fa fa-clock-o"></i> {{time_since(strtotime($data_row['tanggal_deadline']))}}</span>
    						@endif
    
    						<h3 class="timeline-header no-border"><!--<button class="btn btn-xs {{$color}}">{{$data->status}}</button> -->
    						    <b>
    						    {{$data_row['nama_produk']}} - {{$data_row['name']}} - {{$data_row['nomor_registrasi']}} - {{date('d M Y',strtotime($data_row['tanggal_deadline']))}}
    						    </b>
    						</h3>
    						</div>
    					</li>

    					@endforeach
    					@endif
    
    					<li>
    						<i class="fa fa-clock-o bg-gray"></i>
    					</li>
    				</ul>
    			</div>
    			<div class="box-footer">

    			<br><br>
    			</div>
    		</div>
    	</div>
    </div>
@elseif(CRUDBooster::myPrivilegeId() == 2)
    
	@if(!empty($checkAnswerSurveyPenerbit))
	<div class="alert alert-danger">
		<h4><i class="icon fa fa-info"></i>  Mohon Perhatiaannya...</h4>
		Mohon mengisi pertanyaan survey pada menu di samping untuk membantu kami meningkatkan pelayanan.
	</div>
	@endif
    <div class="row">
        <div class="col-md-12">
    		<div class="box" >
    			<div class="box-header">
    				<h3>Deadline Terdekat</h3>
    			</div>
    			<div class="box-body" style="min-height:100vh;overflow:hidden;overflow-y:overlay;">
    				<ul class="timeline">
    				    
    					@foreach($history_tashih_penerbit as $data)
							<li class="time-label">
								<span class="bg-red">
								{{ date('d M. Y',strtotime($data->tanggal)) }}
								</span>
							</li>
							@foreach($data->proses_tashih as $data_row)
							<li>
								<i class="fa fa-user bg-aqua"></i>
		
								<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> {{time_since(strtotime($data->tanggal_deadline))}}</span>
		
								<h3 class="timeline-header no-border"><button class="btn btn-xs {{ $data->color }}">{{$data->status}}</button> {{$data_row->nama_produk}}- {{$data_row->cms_users_name}} - {{$data_row->nomor_registrasi}}</h3>
								</div>
							</li>
							@endforeach
    					@endforeach
                        @if(count($history_tashih_penerbit) <= 0)
							<li>
								<i class="fa fa-clock-o bg-gray"></i>
								<div class="timeline-item">
									<h3 class="timeline-header">tidak ada data pengajuan tashih</h3>
								</div>
							</li>
    					@else
							<li>
								<i class="fa fa-clock-o bg-gray"></i>
							</li>
                        @endif
    					
    				</ul>
    			</div>
    			<div class="box-footer">
    			<br><br>
    			</div>
    		</div>
    	</div>
    </div>
@endif

@endsection