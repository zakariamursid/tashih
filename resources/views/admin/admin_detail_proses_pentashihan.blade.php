<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<style>
	.form-check-label{
		font-size: 12px;
	}

	.medium-bold-green{
	color: #000000;
	font-size: 16px;
	font-weight: bold;
	}


</style>

<p>
	<a title="Return" href="{{CRUDBooster::mainpath()}}">
		<i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Tashih
	</a>
</p>

<div class="panel panel-default">
	<div class="panel-heading">
		<strong>
			<i class="fa fa-book"></i> Detail Pendaftaran Mushaf Al-Qur’an ({{$row->jenis_permohonan}})
		</strong>
	</div>
	
	<div class="panel-body">
			<div class="box-body">
                <div class="table-responsive">
					<div class="col-sm-12">
						<center>
						<p class="medium-bold-green" style="margin-top:25px;font-size:26px;">Detail Pendaftaran Mushaf Al-Qur’an ({{$row->jenis_permohonan}})</p>
						</center>
					</div>

					<div class="col-sm-12 form-group row" style="margin-top:75px;">
						<label class="col-sm-4 col-form-label verysmall-green-default">Jenis pendaftaran mushaf</label>
						<div class="col-sm-8">: 
							{{$row->jenis_permohonan}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nomor Registrasi	</label>
						<div class="col-sm-8">: 
							{{$row->nomor_registrasi}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Tanggal pendaftaran mushaf</label>
						<div class="col-sm-8">: 
							{{$row->created_at}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Tanggal Verifikasi</label>
						<div class="col-sm-8">: 
							{{$row->tanggal_verifikasi}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Tanggal Deadline </label>
						<div class="col-sm-8">: 
							{{$row->tanggal_deadline}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">File Verifikasi </label>
						<div class="col-sm-8">: 
							{{$row->bukti_verifikasi}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Status</label>
						<div class="col-sm-8">: 
							{{$row->status}}
						</div>
					</div>

					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;margin-top:50px;">I. Informasi Data Mushaf</h3>
					</div>

					@if($row->jenis_permohonan == "Pembaruan/perpanjangan Surat Tanda Tashih")

					<div class="col-sm-12 form-group row" style="margin-top:25px;">
						<label class="col-sm-4 col-form-label verysmall-green-default">No. Pendaftaran mushaf lama *</label>
						<div class="col-sm-8">:
							{{$row->mushaf_lama_nomor_registrasi}}
						</div>
					</div>

					@endif

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nama Penerbit</label>
						<div class="col-sm-8">:
							{{$row->cms_users_name}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nama Produk/Mushaf</label>
						<div class="col-sm-8">:
							{{$row->nama_produk	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nama Penanggung Jawab Produk</label>
						<div class="col-sm-8">:
							{{$row->penanggung_jawab_produk	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Jenis Naskah</label>
						<div class="col-sm-8">:
							@php
								$jenis_naskah  = explode(';', $row->id_m_jenis_naskah); 
								$max = max($jenis_naskah);
							@endphp

							@foreach ($jenis_naskah as $val)
								{{$val}} @if($max != $val) , @endif
							@endforeach

						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nama Percetakan</label>
						<div class="col-sm-8">:
							{{$row->nama_percetakan	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Jenis Mushaf</label>
						<div class="col-sm-8">:
							{{$row->m_jenis_naskah_name	}}
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Deskripsi Mushaf</label>
						<div class="col-sm-8">:
							{{$row->keterangan	}}
						</div>
					</div>

					@if($row->jenis_permohonan == "Surat Izin Edar")

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Negara asal mushaf</label>
						<div class="col-sm-8">:
							{{$row->countries_name	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Penerbit asal mushaf</label>
						<div class="col-sm-8">:
							{{$row->nama_penerbit_asal	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Lembaga pentashih asal mushaf </label>
						<div class="col-sm-8">:
							{{$row->nama_lembaga_pentashih_asal	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Bukti tashih Lembaga pentashih asal mushaf </label>
						<div class="col-sm-8">

							@if($row->bukti_tashih_lembaga_pentashih_asal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->bukti_tashih_lembaga_pentashih_asal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif
						</div>
					</div>

					@endif

					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">II. Informasi Data Materi Tambahan pada Mushaf</h3>
					</div>


					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Materi Tambahan Lainnya</label>
						<div class="col-sm-8">
							@foreach ($row->materi_tambahan as $val)
								<button class="btn btn-xs btn-success">{{$val->name}}</button>
							@endforeach				
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Penanggung Jawab Materi Tambahan</label>
						<div class="col-sm-8">:
							{{$row->penanggung_jawab_materi_tambahan}}
						</div>
					</div>


					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">III. Informasi Dokumen Mushaf</h3>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Scan Surat Permohonan Pentashihan</label>
						<div class="col-sm-8">
							@php
								$surat_permohonan_ext = pathinfo($row->surat_permohonan, PATHINFO_EXTENSION);
							@endphp

							@if ($surat_permohonan_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->surat_permohonan)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($surat_permohonan_ext)}}" alt="Scan Surat Permohonan Pentashihan" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->surat_permohonan != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->surat_permohonan)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif

						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Scan Gambar Cover</label>
						<div class="col-sm-8">
							@php
								$gambar_cover_ext = pathinfo($row->gambar_cover, PATHINFO_EXTENSION);
							@endphp

							@if ($gambar_cover_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->gambar_cover)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->gambar_cover)}}" alt="Scan Gambar Cover" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->gambar_cover != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->gambar_cover)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif							
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Scan Contoh Dokumen Naskah</label>
						<div class="col-sm-8">
							@php
								$dokumen_naskah_ext = pathinfo($row->dokumen_naskah, PATHINFO_EXTENSION);
							@endphp

							@if ($dokumen_naskah_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->dokumen_naskah)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->dokumen_naskah)}}" alt="Scan Contoh Dokumen Naskah" class="img-fluid" style="margin-top:25px;"> -->
							@endif

							@if($row->dokumen_naskah != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->dokumen_naskah)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
								<b>Tidak ada file yang dilampirkan</b>
							@endif

						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Apakah Sudah dilakukan Tashih Internal?</label>
						<div class="col-sm-8">
							{{$row->tashih_internal	}}
						</div>
					</div>


					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Scan Bukti Tashih Internal</label>
						<div class="col-sm-8">
							@php
								$bukti_tashih_internal_ext = pathinfo($row->bukti_tashih_internal, PATHINFO_EXTENSION);
							@endphp

							@if($row->bukti_tashih_internal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($row->bukti_tashih_internal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Lihat</a>
								</p>
							@else
									<b>Tidak ada file yang dilampirkan</b>
							@endif

							@if ($bukti_tashih_internal_ext == "pdf")
								<!-- <iframe src="https://docs.google.com/gview?url={{url($row->bukti_tashih_internal)}}&embedded=true" width="640" height="480" allow="autoplay"></iframe> -->
							@else
								<!-- <img src="{{asset($row->bukti_tashih_internal)}}" alt="Scan Bukti Tashih Internal" class="img-fluid" style="margin-top:25px;"> -->
							@endif
						</div>
					</div>


					<div class="col-sm-12" style="margin-bottom:25px;">
						<h3>IV. Informasi PNBP Layanan tashih</h3>
					</div>

					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Nominal PNPB</label>
						<div class="col-sm-8">:
							{{$row->pnpb	}}
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">No Resi</label>
						<div class="col-sm-8">:
							{{$row->no_resi	}}
						</div>
					</div>
					<!-- <div class="col-sm-12 form-group row">
						<label class="col-sm-4 col-form-label verysmall-green-default">Disposisi Nomor Agenda</label>
						<div class="col-sm-8">:
							{{$row->disposisi_nomor_agenda	}}
						</div>
					</div> -->


					<div class="col-sm-12">
						<h3 style="margin-bottom:25px;">V. Informasi Riwayat Bukti Penerimaan</h3>
					</div>


					<table class="table table-bordered table-striped" style="margin-bottom:50px;">
						<thead>
							<tr>
								<td>Nama Penerima</td>
								<td>Tanggal Penerimaan</td>
								<td>Tanggal Deadline</td>
							</tr>
						</thead>
						<tbody>
                            @if(count($row->proses_pentashihan_bukti_penerimaan) > 0)
                                @foreach ($row->proses_pentashihan_bukti_penerimaan as $val)
                                        <tr>
                                            <td>{{$val->nama_penerima}}</td>
                                            <td>{{$val->tanggal_penerimaan}}</td>
                                            <td>{{$val->tanggal_deadline}}</td>
                                        </tr>
                                @endforeach
                            @else
                                    <td colspan="6">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
						</tbody>
					</table>

					<div class="col-sm-12">
						<h3 style="margin-bottom:25px;">VI. List Tashih (Admin)</h3>
					</div>

					<table class="table table-bordered table-striped" style="margin-bottom:50px;">
						<thead>
							<tr>
								<td>User</td>
								<td>Juz</td>
							</tr>
						</thead>
						<tbody>
                            @if(count($row->proses_pentashihan_user) > 0)
                                @foreach ($row->proses_pentashihan_user as $val)
                                        <tr>
                                            <td>{{$val->cms_users_name}}</td>
                                            <td>{{$val->juz}}</td>
                                        </tr>
                                @endforeach
                            @else
                                    <td colspan="2">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
						</tbody>
					</table>

					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">VII. Informasi Riwayat Koreksi</h3>
					</div>


					<table class="table table-bordered table-striped" style="margin-bottom:50px;">
						<thead>
							<tr>
								<td>Admin</td>
								<td>Tanggal Koreksi</td>
								<td>Pesan</td>
								<td>Lampiran</td>
								<td>Revisi</td>
								<td>Lampiran Revisi</td>
								<td>Tanggal Revisi</td>
							</tr>
						</thead>
						<tbody>
                            @if(count($row->proses_pentashihan_koreksi) > 0)
                                @foreach ($row->proses_pentashihan_koreksi as $val)
                                        <tr>
                                            <td>{{$val->cms_users_name}}</td>
                                            <td>{{$val->created_at}}</td>
                                            <td>{{$val->pesan}}</td>
                                            <td>{{$val->lampiran}}</td>
                                            <td>{{$val->revisi}}</td>
                                            <td>{{$val->revisi_lampiran}}</td>
                                            <td>{{$val->revisi_date}}</td>
                                        </tr>
                                @endforeach
                            @else
                                    <td colspan="7">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
						</tbody>
					</table>

					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">VIII. Informasi Surat Pernyataan</h3>
					</div>

					<table class="table table-bordered table-striped" style="margin-bottom:50px;">
						<thead>
							<tr>
								<td>*Nama</td>
								<td>*Jabatan</td>
								<td>*Alamat</td>
								<td>*Nomor Telepon</td>
								<td>*Email</td>
								<td>*Nama Percetakan</td>
								<td>*Alamat Percetakan</td>
								<td>*Rencana Pelaksanaan</td>
							</tr>
						</thead>
						<tbody>
                            @if(count($row->proses_pentashihan_surat_pernyataan) > 0)
                                @foreach ($row->proses_pentashihan_surat_pernyataan as $val)
                                        <tr>
                                            <td>{{$val->nama}}</td>
                                            <td>{{$val->jabatan}}</td>
                                            <td>{{$val->alamat}}</td>
                                            <td>{{$val->phone}}</td>
                                            <td>{{$val->email}}</td>
                                            <td>{{$val->nama_percetakan}}</td>
                                            <td>{{$val->alamat_percetakan}}</td>
                                            <td>{{$val->rencana_pelaksanaan}}</td>
                                        </tr>
                                @endforeach
                            @else
                                    <td colspan="8">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
						</tbody>
					</table>
                    
					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">IX. Informasi Ukuran & Oplah</h3>
					</div>


					<div class="col-sm-12">

					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<td>Ukuran</td>
								<td>Oplah</td>
							</tr>
						</thead>
						<tbody>
                            @if(count($row->proses_pentashihan_ukuran) > 0)
                            @foreach ($row->proses_pentashihan_ukuran as $val)
                            <tr>
										<td>{{$val->ukuran}}</td>
										<td>{{$val->oplah}}</td>
							</tr>

							@endforeach
                            @else
                                    <td colspan="8">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
                        </tbody>
					</table>

					</div>



					<div class="col-sm-12">
						<h3 style="margin-bottom:50px;">X. Laporan Pencetakan</h3>
					</div>
					<table class="table table-bordered table-striped" style="margin-bottom:50px;">
						<thead>
							<tr>
								<td>Pencetakan ke</td>
								<td>Tanggal Pencetakan</td>
								<td>Ukuran</td>
								<td>Oplah</td>
								<td>Nama Percetakan</td>
								<td>Alamat Percetakan</td>
							</tr>
						</thead>
						<tbody>
                            <tr>
                            @if(count($row->proses_pentashihan_laporan_percetakan) > 0)
                                @foreach ($row->proses_pentashihan_laporan_percetakan as $val)
                                            <td>{{$val->cetakan_ke}}</td>
                                            <td>{{$val->tanggal_cetak}}</td>
                                            <td>{{$val->ukuran}}</td>
                                            <td>{{$val->nama_percetakan}}</td>
                                            <td>{{$val->alamat_percetakan}}</td>
                                @endforeach
                            @else
                                    <td colspan="6">
                                    <center>
                                    No Data Avaliable
                                    </center>    
                                </td>
                            @endif
                            </tr>
                        </tbody>
					</table>


			</div> <!-- ./box-body -->
			
			<div class="box-footer">

			</div> <!-- ./box-footer -->
	</div>
	</div>

</div>

@endsection