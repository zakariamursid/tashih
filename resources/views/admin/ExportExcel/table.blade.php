<?php
if($fileformat=="xls"){
    header("Pragma: public");
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . $filename . '.' . $fileformat . '"');
}
?>

<style>
    table,tr,th,td{
        white-space:no-wrap;
    }
</style>

@foreach($survey as $i => $row)
<table style="width:100%" border="1" style="white-space:no-wrap">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Responden</th> 
            <th colspan="{{ count($row['survey'])+2 }}">{{ $row['name'] }}</th>
        </tr>
        <tr>
            @foreach($row['survey'] as $x => $xrow)
            <th>{{ $xrow['pertanyaan_survey'] }}</th>
            @endforeach
            <th>Jumlah Pertanyaan</th>
            <th>Rata-Rata</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 1;$push = []; @endphp
        @foreach($row['answer'] as $x => $xrow)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $xrow['name'] }}</td>
            @php $value = 0; @endphp
            @foreach($row['survey'] as $y => $yrow)
                <td>@if(isset($xrow[$y])) {{$xrow[$y]['opsi_value']}} @endif</td>
                @php 
                    if(isset($xrow[$y])) {
                        $value += $xrow[$y]['opsi_value'];
                    }
                @endphp
            @endforeach
            <td>{{ count($row['survey']) }}</td>
            <td>{{ $value/count($row['survey']) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
@endforeach