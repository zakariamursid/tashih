<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<div id="appForm">

    <p>
        <a title='Return' href='{{ CRUDBooster::adminpath("siaran-pers-penerbit") }}'>
            <i class='fa fa-chevron-circle-left'></i>
            &nbsp; Back To List Siaran Pers Penerbit
        </a>
    </p>
    <div class="panel panel-default">

        <div class="panel-body p-3 container">
            <h3 class="card-title">{{ $find->title }}</h3>
            <img src="{{ asset('$find->image') }}" class="img-responsive " alt="Gambar">

            <div class="justify-content-md-center">
                {!! $find->content !!}
            </div>
        </div>

    </div>

</div>
@endsection
@push('bottom')

@endpush
@push('head')


@endpush