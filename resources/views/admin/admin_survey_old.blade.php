<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

    @php
    $tabel=DB::table('jawaban_survey')
    ->select('survey.*')
    ->join('survey','survey.id','=','jawaban_survey.id_survey')
    ->whereNull('jawaban_survey.deleted_at')
    ->where('jawaban_survey.id_cms_users',CRUDBooster::myId())
    ->pluck('survey.id_kategori_survey')
    ->toArray();
    
    //dd($tabel);
    @endphp
    @if(!empty($tabel->id))
    <div class="alert alert-success">
        <h4><i class="icon fa fa-info"></i> Wow, good job...</h4>
        Trima kasih telah memberi kami pendapat anda
    </div>
    @else
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'>Survey Form</div>
        <div class='panel-body'>      
            <form action="{{ CRUDBooster::adminPath('jawaban_survey44/save') }}" method="post">
            {{csrf_field()}}
            {{method_field('POST')}}
            @foreach($result as $data)
    
                @if(in_array($data->id_kategori_survey,$tabel))
                <?php continue; ?>
                @else
                <div class="panel panel-info">
                    <div class="panel-heading">{{$data->pertanyaan_survey}}</div>
                    <div class="panel-body">
                        <div class="radio">
                            <label>
                            <input type="radio" name="pertanyaan{{$data->id}}" id="pertanyaan{{$data->id}}" value="Sangat Baik"  required>
                            Sangat Baik
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            <input type="radio" name="pertanyaan{{$data->id}}" id="pertanyaan{{$data->id}}" value="Baik" required>
                            Baik
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            <input type="radio" name="pertanyaan{{$data->id}}" id="pertanyaan{{$data->id}}" value="Kurang Baik" required>
                            Kurang Baik
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                            <input type="radio" name="pertanyaan{{$data->id}}" id="pertanyaan{{$data->id}}" value="Tidak Baik" required>
                            Tidak Baik
                            </label>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id_pertayaan{{$data->id}}" value="{{$data->id}}">
                @endif
            @endforeach
                <div class="form-group pull-right">
                    <button type="reset" class="btn btn-sm btn-danger">Reset</button>
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection