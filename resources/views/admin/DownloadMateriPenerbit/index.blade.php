<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<div id="appForm">
    
    <div class="row">
        <div class="search-table col-sm-3 pull-right mb-2">
            <div class="input-group">
                <input type="text" class="form-control pull-right" id="formSearch" v-model="search" @input="searchInput">
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-body p-3">
            <div class="row" v-if="data.data.length > 0">
                <div class="col-sm-6" v-for="(row,i) in data.data">
                    <div class="siaranpers-item" @click="findMateri(i)">
                        <img src="{{ asset('image/icon/img_folders_fill.png') }}" alt="Gambar">
                        <div class="siaranpers-content">
                            <h4>@{{ row.judul }}</h4>
                            <p><b>Kategori</b> : @{{ row.nama_kategori }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" v-if="data.data.length <= 0">
                <div class="col-sm-6">
                    <div class="siaranpers-item">
                        <img src="https://via.placeholder.com/450x450?text=Image+Not+Found">
                        <div class="siaranpers-content">
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="siaranpers-item">
                        <img src="https://via.placeholder.com/450x450?text=Image+Not+Found">
                        <div class="siaranpers-content">
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="siaranpers-item">
                        <img src="https://via.placeholder.com/450x450?text=Image+Not+Found">
                        <div class="siaranpers-content">
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="siaranpers-item">
                        <img src="https://via.placeholder.com/450x450?text=Image+Not+Found">
                        <div class="siaranpers-content">
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>

            <vue-pagination :pagination="data"
                @paginate="listData()"
                :offset="4">
            </vue-pagination>
        </div>

    </div>

    <div class="modal fade" id="modalDownload">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">@{{ dataModal.title }}</h4>
                </div>
                <div class="modal-body">
                    <div class="container mb-2 d-inline" v-html="dataModal.content"></div>
                    <iframe :src="dataModal.file" frameborder="0" style="border:0;width: 100%;" height="350" id="iframe" onerror="return alert('FIle not load')"></iframe>
                    <table class="table table-striped">
                        <tr>
                            <th>Nama File</th>
                            <th>Download</th>
                        </tr>
                        <tr v-for="(row, i) in dataModal.list_materi">
                            <td>@{{ row.name_file }}</td>
                            <td>
                                <a :href="row.file" download class="btn btn-xs btn-success"><span class="fa fa-download"></span>  Download File</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('bottom')

<script>
    conf = {
        base_url : "{{ CRUDBooster::adminpath('download-materi-penerbit') }}",
        token : "{{ csrf_token() }}",
        base_asset : "{{ asset('/') }}"
    };
</script>
<script src="https://unpkg.com/vue@2.6.0/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
<script src="{{ asset('js/admin/DownloadPenerbit/Index.js') }}"></script>

@endpush
@push('head')

<style>
    .siaranpers-item {
        background-color: #FFFFFF;
        border-radius: 12px;
        box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.08);
        display: flex;
        padding: 20px 30px;
        margin-bottom: 30px;
    }
    .siaranpers-item img {
        height:40px;
    }
    .siaranpers-item .siaranpers-content {
        display: grid;
        padding-left: 23px;
        overflow-wrap: anywhere;
    }
    .siaranpers-item .siaranpers-content h4,.siaranpers-item .siaranpers-content a{
        color: #0C3157;
        font-size: 18px;
        text-align: left;
        font-weight: bold;
    }
    .siaranpers-item:hover, .siaranpers-item:focus {
        background: #f1f1f1;
        cursor: pointer;
    }
    .siaranpers-item .siaranpers-content p{
        color: #0C3157;
        font-size: 14px;
        line-height: 22px;
        text-align: left;
    }
    .siaranpers-item .siaranpers-content h4:empty{
        width: 200px;
        background: #ddd;
        padding: 10px 20px;
        border-radius: 12px;
    }
    .siaranpers-item .siaranpers-content p:empty{
        width: 350px;
        background: #ddd;
        padding: 10px 20px;
        border-radius: 12px;
    }

    .mb-2 {
        margin-bottom: 15px !important;
    }

    .input-group .input-group-addon {
        border-left-color: transparent;
    }
    .form-control:focus, .form-control:hover {
        border-color: #d2d6de;
        border-right-color: transparent;
    }
    .form-control {
        border-right-color: transparent;
    }

    .pagination {
        display: table;
        margin: auto;
    }

    .d-inline {
        display: inline !important;
    }
</style>

@endpush