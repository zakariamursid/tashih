<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<p>
    <a title='Return' href='{{ CRUDBooster::adminpath("email") }}'>
        <i class='fa fa-chevron-circle-left'></i>
        &nbsp; Back To List Data Send Email
    </a>
</p>

<div class="panel panel-default" id="appForm">
    <div class="panel-heading">
        <strong><i class='fa fa-envelope-o'></i> Add Email</strong>
    </div>

    <form v-on:submit.prevent="saveData"  method="post" enctype="multipart/form-data">
        <div class="panel-body p-3">

            <div class="form-group m-5">
                <label class="control-label">Pilih Template Email <code>*</code></label>
                <select name="select_template" v-model="select_template" id="select_template" class="form-control" required>
                    <option value="">**Pilih Template Email</option>
                    @foreach($listTemplate as $x => $xrow)
                    <option value="{{ $xrow->id }}">{{ $xrow->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group m-5">
                <label class="control-label">
                    Pilih Penerbit <code>*</code>
                    <a href="javascript:void(0)" @click="showModal" class="btn btn-xs btn-success ml-2"><span class="fa fa-plus"></span></a>
                </label>
                <div class="row-penerbit">
                    <div class="item-penerbit" v-for="(row, i) in listPenerbit">
                        <img :src="row.image" alt="Gambar Penerbit">
                        <div class="content-penerbit">
                            <h4>@{{ row.name }}</h4>
                            <p>@{{ row.email }}</p>
                        </div>
                        <div class="row-btn ml-auto px-1 py-1">
                            <a href="javascript:void(0)" class="btn btn-xs btn-danger" @click="remove(i)"><span class="fa fa-trash"></span></a>
                        </div>
                    </div>
                    <div class="item-penerbit" v-if="listPenerbit.length <= 0">
                        <img src="https://via.placeholder.com/430x300?text=Not+Found" alt="Gambar Penerbit">
                        <div class="content-penerbit">
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="box-footer" style="background: #F5F5F5">
            <div class="form-group">
                <a href="{{ CRUDBooster::adminpath('email') }}" class='btn btn-default'><i class='fa fa-chevron-circle-left'></i> Back</a>
                <input type="submit" name="submit" value='Save' class='btn btn-success'>
            </div>
        </div>
    </form>

    <div class="modal fade" id="modalPenerbit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Data Penerbit</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="table-responsive">
                        <div class="col-sm-7 pull-left">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="checkall" id="checkall"> Pilih semua
                                </label>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-xs btn-danger ml-2" @click="selectPenerbitAll"><i class="fa fa-check"></i> Select All</a>
                        </div>
                        <div class="search-table col-sm-3 pull-right">
                            <div class="input-group">
                                <input type="text" class="form-control pull-right" id="formSearch">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Penanggung Jawab</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody v-if="isNull === true">
                                <tr>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                </tr>
                                <tr>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                    <td><div class="strip-table"></div></td>
                                </tr>
                            </tbody>
                            <tbody v-if="data.length <= 0 && isNull !== true">
                                <tr>
                                    <td colspan="6" class="text-center">No data found.</td>
                                </tr>
                            </tbody>
                            <tbody v-if="data.length > 0 ">
                                <tr v-for="(row, i) in data">
                                    <td>
                                        <input type="checkbox" name="penerbit[]" id="penerbit" :data-id="i">
                                    </td>
                                    <td>@{{ row.name }}</td>
                                    <td>@{{ row.email }}</td>
                                    <td>@{{ row.address }}</td>
                                    <td>@{{ row.pic }}</td>
                                    <td>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary" @click="selectPenerbit(i)"><i class="fa fa-check"></i> Select</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <button id="btnPrev" class="btn btn-default" :class="page >= totalPage && page != 1 || page > 1 ? 'active' : ''" @click="prev()">
                            <i class="fa fa-chevron-left"></i>
                        </button>
                        <button id="btnNext" class="btn btn-default" :class="page <= totalPage && page !== totalPage ? 'active' : ''" @click="next()">
                            <i class="fa fa-chevron-right"></i>
                        </button>
                        <span>&nbsp; Page &nbsp;</span>
                        <input id="page" type="number" v-model="page" :max="totalPage" min="1" @input="changePage">
                        <span>&nbsp; of  @{{ totalPage }}</span>
                    </div>
                    
                    <div class="pull-right">
                        <p class="pull-left" style="padding-top: 5px;margin-bottom: 0;">Jumlah Baris &nbsp;</p>
                        <select v-model="totalRecord" @change="getList" name="limit" style="width: 56px;" class="form-control input-sm">
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="200">200</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('bottom')

<script src='<?php echo asset("vendor/crudbooster/assets/select2/dist/js/select2.full.min.js")?>'></script>
<script>
    conf = {
        base_url : "{{ CRUDBooster::adminpath('email') }}",
        token : "{{ csrf_token() }}",
        base_asset : "{{ asset('/') }}"
    };
</script>
<script src="https://unpkg.com/vue@2.6.0/dist/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.2/axios.js"></script>
<script src="{{ asset('js/admin/SendMail/AddForm.js') }}"></script>

@endpush
@push('head')

<link rel='stylesheet' href='<?php echo asset("vendor/crudbooster/assets/select2/dist/css/select2.min.css")?>'/>
<style type="text/css">
    .select2-container {
        /* margin: 0 10px; */
    }
    .select2-container--default .select2-selection--single {
        border-radius: 0px !important
    }
    .select2-container .select2-selection--single {
        height: 40px
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #3c8dbc !important;
        border-color: #367fa9 !important;
        color: #fff !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        color: #fff !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 36px;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px;
        right: 10px;
    }
    .select2-container--open .select2-dropdown {
        /* left: -10px; */
    }

    .strip-table:empty {
        padding: 5px 15px;
        background: #ddd;
        border-radius: 12px;
    }
    .ml-2 {
        margin-left: 20px !important;
    }
    .row-penerbit {
        margin-top: 15px;
    }
    .item-penerbit {
        background-color: #FFFFFF;
        border-radius: 12px;
        box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.08);
        display: flex;
        padding: 20px 30px;
        margin-bottom: 30px;
    }
    .item-penerbit img {
        height:50px;
    }
    .item-penerbit .content-penerbit {
        display: grid;
        padding-left: 23px;
        overflow-wrap: anywhere;
    }
    .item-penerbit .content-penerbit h4{
        color: #0C3157;
        font-size: 20px;
        text-align: left;
        font-weight: bold;
    }
    .item-penerbit .content-penerbit p{
        color: #0C3157;
        font-size: 14px;
        line-height: 22px;
        text-align: left;
    }
    .item-penerbit .content-penerbit h4:empty{
        width: 200px;
        background: #ddd;
        padding: 10px 20px;
        border-radius: 12px;
    }
    .item-penerbit .content-penerbit p:empty{
        width: 350px;
        background: #ddd;
        padding: 10px 20px;
        border-radius: 12px;
    }

    .ml-auto, .mx-auto {
        margin-left: auto !important;
    }
    .pl-1, .px-1 {
        padding-left: 0.25rem !important;
    }
    .pb-1, .py-1 {
        padding-bottom: 0.25rem !important;
    }
    .pr-1, .px-1 {
        padding-right: 0.25rem !important;
    }
    .pt-1, .py-1 {
        padding-top: 0.25rem !important;
    }

    .input-group .input-group-addon {
        border-left-color: transparent;
    }
    .form-control:focus, .form-control:hover {
        border-color: #d2d6de;
        border-right-color: transparent;
    }
    .form-control {
        border-right-color: transparent;
    }

    #modalPenerbit .btn-default, #modalPenerbit .btn-default:hover, #modalPenerbit .btn-default:focus {
        background-color: #F5F5F5;
        color: #4D4D58;
        border-color: #F5F5F5;
        font-size: 12px;
        padding: 6px 9px;
        pointer-events: none;
        user-select: none;
        cursor: not-allowed;
    }
    #modalPenerbit .btn-default.active, #modalPenerbit .btn-default.active:focus, #modalPenerbit .btn-default.active:hover, #modalPenerbit .btn-default:active.focus, #modalPenerbit .btn-default:active:focus, #modalPenerbit .btn-default:active:hover, .open>.dropdown-toggle#modalPenerbit .btn-default.focus, .open>.dropdown-toggle#modalPenerbit .btn-default:focus, .open>.dropdown-toggle#modalPenerbit .btn-default:hover {
        background-color: #0C3157;
        color: #FFF;
        border-color: #0C3157;
        cursor: pointer;
        pointer-events: all;
        user-select: all;
    }
    #modalPenerbit span, #modalPenerbit p {
        color: #20202E;
        font-size: 12px;
        line-height: 15px;
    }
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
        -moz-appearance: textfield;
        text-align: center;
        outline: none !important;
        background-color: #FFFFFF;
        border: 1px solid #C7C7C7;
        border-radius: 5px;
        width: 30px;
    }
    select.input-sm {
        background-color: #FFFFFF;
        border: 1px solid #C7C7C7;
        border-radius: 5px;
    }
    .checkbox {
        display: inline !important;
    }

    .disabled-form {
        opacity: 0.5;
        user-select: none;
        pointer-events: none;
    }
    .enabled-form {
        opacity: 1;
        user-select: all;
        pointer-events: all;
    }
</style>

@endpush