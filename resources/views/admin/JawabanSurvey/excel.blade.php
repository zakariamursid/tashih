<table border='1' width='100%' cellpadding='3' cellspacing="0" style='border-collapse: collapse;font-size:12px'>

        <tr>
            <th>Responden</th>
            <th>Unsur</th>
            <th>Pertanyaan Survey</th>
            <th>Jawaban</th>
            <th>Pilihan Jawaban</th>
            <th>Nilai</th>
        </tr>

    @if(count($result)==0)
        <tr class='warning'>
            <td colspan='{{count($columns)+1}}' align="center">No Data Avaliable</td>
        </tr>
    @else
        @foreach($result as $row)
        <tr>
            <td>{{ $row->name_users }}</td>
            <td>{{ $row->nama_unsur }}</td>
            <td>{{ $row->survey_name }}</td>
            <td>{{ $row->jawaban }}</td>
            <td>{{ $row->option_survey }}</td>
            <td>{{ $row->nilai }}</td>
        </tr>
        @endforeach
    @endif

</table>