<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
@push('head')

<style>
	.form-check-label{
		color: #000000;
		/* font-size: 12px; */
	}
	.verysmall-green-default
	{
		color: #000000;
		font-size: 16px;
	}
	.flex {
     -webkit-box-flex: 1;
     -ms-flex: 1 1 auto;
     flex: 1 1 auto
 }
	
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"></link>

<link rel="stylesheet" href="{{asset('vendor/select2/select2.min.css')}}">
@endpush

<script type="text/javascript">
	var token = "'.csrf_token().'";
	var asset = "'.asset('/').'";
	var mainpath = "'.CRUDBooster::mainpath().'";
</script>

<div class="panel panel-default">
	<div class="panel-body">
		<canvas id="myChart" style="width:100%;max-width:600px"></canvas>
	</div>
</div>


  <div class="panel panel-default">
    <div class="panel-body">

      <h1>Tidak Puas</h1>
      <br>

      <table class="table datatable">
        <thead>
          <tr>
            <th style="width:auto;">Nomor Registrasi</th>
            <th style="width:auto;">Penerbit</th>
            <th style="width:auto;">Nama Mushaf</th>
            <th style="width:auto;">Tingkat kepuasan</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data['tidak_puas']))
          @foreach($data['tidak_puas'] as $val)
          <tr>
            <td>{{$val->nomor_registrasi}}</td>
            <td>{{$val->cms_users_name}}</td>
            <td>{{$val->nama_produk}}</td>
            <td>{{$val->jumlah_rating}}</td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-body">
      

      <h1>Kurang / Cukup Puas</h1>
      <br>

      <table class="table datatable">
        <thead>
          <tr>
            <th style="width:auto;">Nomor Registrasi</th>
            <th style="width:auto;">Penerbit</th>
            <th style="width:auto;">Nama Mushaf</th>
            <th style="width:auto;">Tingkat kepuasan</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data['kurang_puas']))
          @foreach($data['kurang_puas'] as $val)
          <tr>
            <td>{{$val->nomor_registrasi}}</td>
            <td>{{$val->cms_users_name}}</td>
            <td>{{$val->nama_produk}}</td>
            <td>{{$val->jumlah_rating}}</td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
      
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-body">
      <h1>Puas</h1>
      <br>
      <table class="table datatable">
        <thead>
          <tr>
            <th style="width:auto;">Nomor Registrasi</th>
            <th style="width:auto;">Penerbit</th>
            <th style="width:auto;">Nama Mushaf</th>
            <th style="width:auto;">Tingkat kepuasan</th>
          </tr>
        </thead>
        <tbody>
          @if(count($data['puas']))
          @foreach($data['puas'] as $val)
          <tr>
            <td>{{$val->nomor_registrasi}}</td>
            <td>{{$val->cms_users_name}}</td>
            <td>{{$val->nama_produk}}</td>
            <td>{{$val->jumlah_rating}}</td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>

<script type="text/javascript">
	var segment = "{{Request::segment(3)}}";
</script>


@endsection
@push('bottom')

<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>

const tidakpuas = '{{count($data['tidak_puas'])}}';
const kurangpuas = '{{count($data['kurang_puas'])}}';
const puas = '{{count($data['puas'])}}';

var xValues = ["Tidak Puas", "Kurang / Cukup Puas", "Puas"];
var yValues = [tidakpuas,kurangpuas,puas];
var barColors = [
  "#4472c3",
  "#ed7d31",
  "#a5a5a5"
];

new Chart("myChart", {
  type: "pie",
  data: {
    labels: xValues,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
	responsive: true,
    title: {
      display: true,
      text: "Rating Penerbit"
    },
    legend: {
		position: 'bottom'
        }
    }
});

$(document).ready( function () {
    $('.datatable').DataTable();
} );

</script>

@endpush
