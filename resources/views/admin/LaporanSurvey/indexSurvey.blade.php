<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

<div class="panel panel-default">
    <div class="panel-body p-3">
        <div class="row">
            <form action="" type="GET">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Tangal Awal</label>
                        <input type="text" name="first_date" id="first_date" class="form-control datePicker-Form">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Tangal Akhir</label>
                        <input type="text" name="last_date" id="last_date" class="form-control datePicker-Form">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group mt-3">
                        <button type="submit" class="btn btn-md btn-primary">Submit</button>
                    </div>
                </div>
            </form>
            <div class="col-md-1">
                <a href="{{ CRUDBooster::mainpath('export-survey') }}" class="btn btn-md btn-success mt-3"><span class="fa fa-file-excel-o"></span> Export Survey</a>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body p-3">

        <div class="header-report">
            <h4>SURVEI LAYANAN PENTASHIHAN MUSHAF AL-QUR'AN</h4>
            <h4>LAJNAH PENTASHIHAN MUSHAF AL-QUR’AN</h4>
            <h4>BADAN LITBANG DAN DIKLAT KEMENTERIAN AGAMA RI</h4>
        </div>

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Unsur</th>
                    <th>Rata Rata</th>
                    <th>Index</th>
                    <th>Mutu Pelayanan</th>
                    <th>Kinerja Pelayanan</th>
                </tr>
            </thead>
            <tbody>
                @php $unsurUse = 0;$sumAvg = 0;$sumPenerbit = 0; @endphp
                @foreach($unsur as $x => $xrow)
                <tr>
                    <td>{{$xrow->nama}}</td>
                    <td>{{$xrow->avg}}</td>
                    <td>{{$xrow->index}}</td>
                    <td>{{$xrow->mutu}}</td>
                    <td>{{$xrow->kinerja}}</td>
                </tr>
                @php
                    if($xrow->avg) {
                        $unsurUse++;

                        $sumAvg += $xrow->avg;
                    }
                    if($xrow->penerbit) {
                        $sumPenerbit += $xrow->penerbit;
                    }
                @endphp
                @endforeach
                <tr>
                    <th>Jumlah Unsur Digunakan :</th>
                    <td>{{$unsurUse}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th>Total</th>
                    <th>{{$sumAvg/$unsurUse}}</th>
                    <th>{{ Kemenag::indexReportSurvey($sumAvg/$unsurUse) }}</th>
                    <th>{{ Kemenag::mutuPelayananReportSurvey($sumAvg/$unsurUse) }}</th>
                    <td></td>
                </tr>
                <tr>
                    <th>Jumlah responden</th>
                    <th>{{ $sumPenerbit }}</th>
                    <th></th>
                    <th></th>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table class="table table-striped table-bordered" style="width: 60%;">
            <tr>
                <th>Nilai Persepsi</th>
                <th>Nilai Interval</th>
                <th>Nilai Interval Konversi (NIK)</th>
                <th>Mutu Pelayanan</th>
                <th>Kinerja Unit Pelayanan</th>
            </tr>
            <tr>
                <td>1</td>
                <td>1.00 - 2.5996</td>
                <td>25.00 - 64.99</td>
                <td>D</td>
                <td>Tidak baik</td>
            </tr>
            <tr>
                <td>2</td>
                <td>2.60 - 3.064</td>
                <td>65.00 - 76.60</td>
                <td>C</td>
                <td>Kurang baik</td>
            </tr>
            <tr>
                <td>3</td>
                <td>3.0644 - 3.532</td>
                <td>76.61 - 88.30</td>
                <td>B</td>
                <td>Baik</td>
            </tr>
            <tr>
                <td>4</td>
                <td>3.5324 - 4.00</td>
                <td>88.31 - 100.00</td>
                <td>A</td>
                <td>Sangat baik</td>
            </tr>
        </table>

    </div>
</div>

@endsection
@push('bottom')

<script>
    $(document).ready(function () {
        $('.datePicker-Form').flatpickr({
            dateFormat: 'Y-m-d',
            locale: {
                'firstDayOfWeek': 1
            },
        });
    });
</script>

@endpush
@push('head')

<style>
    .header-report {
        overflow-wrap: anywhere;
        text-align: center;
        margin-bottom: 30px;
        margin-top: 35px;
    }
    .header-report h4 {
        font-weight: bold;
        line-height: 13px;
    }
    .mt-3 {
        margin-top: 25px;
    }
</style>

@endpush