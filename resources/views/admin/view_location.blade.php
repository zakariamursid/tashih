@extends('crudbooster::admin_template')
@section('content')
<section id="content_section" class="content">
    <div class="box" style="height:100vh">
        <div class="box-header">
            <a href="{{ CRUDBooster::mainpath('all-address') }}" class="btn btn-sm btn-danger pull-right" style="top:0px;"><span class="fa fa-refresh"></span>Generate Alamat</a>
            <button class="btn btn-sm btn-primary pull-right" onclick="return initMap()" style="top:0px;"><span class="fa fa-refresh"></span> Load Map</button>
            <h3><i class="fa fa-location-arrow"></i>&nbsp; Lokasi Penerbit</h3>
        </div>
        <div class="box-body table-responsive no-padding">
            <div id="map"></div>
        </div>
    </div>
 </section>

<script>

</script>
 <style>
    #map{
    width:100%;height:100vh;
}
 </style>

<script>
function initMap() {
    $.ajax({
        url:"{{ CRUDBooster::mainpath('location') }}",
        method:"GET",
        data:{
            "_token":"{{csrf_token()}}",
        },
        success:function (data) {  
            kok=[];
            kok=data;

            var myLatLng = {lat: -1.7593717, lng: 113.4055126};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 5,
                    center: myLatLng
                });

                var count;

                for (count = 0; count < data.length; count++) {
                    var infowindow=new google.maps.InfoWindow();
                    id=data[count]['id'];
                    var id =new google.maps.Marker({
                        position: new google.maps.LatLng(Number(data[count]['lat']), Number(data[count]['lng'])),
                        map: map,
                        title: data[count]['name']
                    });
                    
                    google.maps.event.addListener(id, 'click', (function(id, count) {
                        return function() {
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 17,
                            center: {
                                    lat:Number(data[count]['lat']),
                                    lng:Number(data[count]['lng']),
                            }
                        });
                        var marker = new google.maps.Marker({
                            position: {
                                    lat:Number(data[count]['lat']),
                                    lng:Number(data[count]['lng']),
                            },
                            map: map,
                            title: data[count]['name'],
                            pixelOffset: new google.maps.Size(100,140),
                        });

                        infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['phone']+"</small>");
                        infowindow.open(map, marker);

                        google.maps.event.addListener(marker, 'mouseover', (function(marker, count) {
                            return function() {
                            infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['[phone]']+"</small>");
                            infowindow.open(map, marker);
                            }
                        })(marker, count));
                        
                        }
                    })(id, count));

                    google.maps.event.addListener(id, 'mouseover', (function(id, count) {
                        return function() {
                        infowindow.setContent("<b>"+data[count]['name']+"</b>"+" <br> "+"<small>"+data[count]['address']+"</small>"+"<br>"+"<small>"+data[count]['phone']+"</small>");
                        infowindow.open(map, id);
                        }
                    })(id, count));
                }
                

        },
        error:function (data) {
            console.log(data);
        }
    }); 
    
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ CRUDBooster::getsetting('google_api_key')}}&callback=initMap"
    async defer></script>
@endsection