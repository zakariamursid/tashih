@extends('crudbooster::admin_template')
@section('content')
<p>
    <a title="Return" href="{{ CRUDBooster::adminpath('kategori_survey') }}">
        <i class="fa fa-chevron-circle-left "></i>&nbsp; Back To List Data Nama Survey
    </a>
</p>
<div class='panel panel-default'>
    <div class='panel-heading'>
        <strong>{{ $page_title }}</strong>
    </div>
    <div class='panel-body'>
        <div class="table-responsive mb-5">
            <table class="table table-striped">
                <tr>
                    <th>Nama Survey</th>
                    <td>{{ $data->jenis_kategori }}</td>
                </tr>
                <tr>
                    <th>From</th>
                    <td>{{ $data->from }}</td>
                </tr>
                <tr>
                    <th>To</th>
                    <td>{{ $data->to }}</td>
                </tr>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Unsur</th>
                    <th>Pertanyaan</th>
                    <th>Tipe Pertanyaan</th>
                    <th>Daftar Jawaban</th>
                    <th>Nilai Jawaban</th>
                </tr>
                @foreach($unsur as $row)
                <tr>
                    <td>{{ $row->unsur }}</td>
                    <td>{{ $row->pertanyaan }}</td>
                    <td>{{ $row->type_pertanyaan }}</td>
                    <td>
                        @foreach($row->opsi as $yrow)
                        {{ $yrow->type }}<br>
                        @endforeach
                    </td>
                    <td>
                        @foreach($row->opsi as $yrow)
                        @if($yrow->type == 'A')
                        4
                        @elseif($yrow->type == 'B')
                        3
                        @elseif($yrow->type == 'C')
                        2
                        @elseif($yrow->type == 'D')
                        1
                        @endif
                        <br>
                        @endforeach
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection