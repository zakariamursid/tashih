<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
@php 
	if(old('id_m_jenis_mushaf') != null)
	{
		$id_m_jenis_mushaf = old('id_m_jenis_mushaf');
	}
	else
	{
		$id_m_jenis_mushaf = $proses_pentashihan->id_m_jenis_mushaf;
	}
	if(old('id_mushaf_lama') != null)
	{
		$id_mushaf_lama = old('id_mushaf_lama');
	}
	else
	{
		$id_mushaf_lama = $proses_pentashihan->id_mushaf_lama;
	}
	if(old('id_proses_pentashihan_ukuran') != null)
	{
		$id_proses_pentashihan_ukuran = old('id_proses_pentashihan_ukuran');
	}
	else
	{
		$id_proses_pentashihan_ukuran = $proses_pentashihan->id_proses_pentashihan_ukuran;
	}
@endphp 

<style>
	.form-check-label{
		color: #000000;
		/* font-size: 12px; */
	}
	.verysmall-green-default
	{
		color: #000000;
		font-size: 16px;
	}
	.text-success
	{
		color: #00923F;
		font-size: 12px;
	}
</style>
<link rel="stylesheet" href="{{asset('vendor/select2/select2.min.css')}}">

<script type="text/javascript">
	var token = "'.csrf_token().'";
	var asset = "'.asset('/').'";
	var mainpath = "'.CRUDBooster::mainpath().'";
</script>

<p>
	<a title="Return" href="{{CRUDBooster::mainpath()}}">
		<i class="fa fa-chevron-circle-left "></i> &nbsp; Back To List Tashih
	</a>
</p>

<div class="panel panel-default">
	<div class="panel-heading">
		<strong>
			Formulir Pendaftaran Mushaf Al-Qur’an
		</strong>
	</div>
	
	<?php $form = (Request::segment(3) == 'ralat' ? 'temp-edit' : 'edit/'.Request::segment(4)); ?>
	<div class="panel-body">
		<form action="{{CRUDBooster::mainpath($form)}}" method="POST" enctype="multipart/form-data">
			<input type="number" value="{{$proses_pentashihan->id}}" name="temp_id" style="display:none;">
			<div class="box-body">
				<div id="infoDokumen" class="row">
					<div class="col-sm-12">
						
							<div class="alert alert-success" role="alert">
								<h4 class="alert-heading">Perhatian!</h4>
								{!!$message->content!!}
							</div>
					</div>

					<div class="col-sm-12">
						<h3 style="margin-bottom:25px;margin-top:25px;"><b>Pendaftaran Mushaf Al-Qur’an</b></h3>
					</div>

					<div class="col-sm-12 form-group row" style="margin-top:25px;">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Jenis pendaftaran mushaf*</label>
						<div class="col-lg-8 col-sm-12">
						<select name="jenis_permohonan" id="jenis_permohonan" class="form-control" required="required">
							<option value="">**Pilih Jenis Permohonan</option>
							<option value="Surat Tanda tashih Baru" @if($proses_pentashihan->jenis_permohonan == 'Surat Tanda tashih Baru') selected @endif>Tanda tashih mushaf baru</option>
							<option value="Pembaruan/perpanjangan Surat Tanda Tashih" @if($proses_pentashihan->jenis_permohonan == 'Pembaruan/perpanjangan Surat Tanda Tashih') selected @endif>Perpanjangan tanda tashih</option>
							<option value="Surat Izin Edar" @if($proses_pentashihan->jenis_permohonan == 'Surat Izin Edar') selected @endif>Izin edar mushaf luar negeri</option>
						</select>
						</div>
					</div>

					<div class="col-sm-12">
						<p style="margin-bottom:25px;margin-top:0px;">Keterangan: <br> <br>
						1. Tanda tashih mushaf baru. Untuk pendaftaran mushaf yang baru pertama kali akan diterbitkan oleh penerbit Anda. <br>
						2. Perpanjangan tanda tashih. Atau disebut juga pembaruan tanda tashih. Yaitu untuk pendaftaran mushaf yang tanda tashihnya sudah memasuki akhir masa berlaku (2 tahun) dan penerbit Anda akan mencetaknya lagi. <br>
						3. Izin edar mushaf luar negeri. Untuk pendaftaran mushaf luar negeri yang diimpor oleh penerbit Anda dan akan diedarkan di Indonesia.
						</p>
					</div>
					<div class="col-sm-12">
						<h3 style="margin-bottom:25px;margin-top:25px;"><b>I. Informasi Data Mushaf</b></h3>
						<p>Harap mengisi informasi data mushaf dengan sebenar-benarnya. Data ini akan menjadi data utama mushaf yang beredar di Indonesia.</p>
					</div>
					
					<div class="col-sm-12 form-group row" style="margin-top:25px;">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Nama Penerbit *</label>
						<div class="col-lg-8 col-sm-12">
							<input type="text" value="{{CRUDBooster::myName()}}" required="required" class="form-control" disabled="true">
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Nama Produk/Mushaf *
						<br>
						<small class="text-success font-weight-bold">Diisi dengan nama/brand mushaf al-qur’an yang akan didaftarkan pentashihan.</small>
							<small class="text-success font-weight-bold">Misal: Mushaf Al-Qur’an, Al-Qur’an dan Terjemahnya, Mushaf Alkabir, dll</small>

						</label>
						<div class="col-lg-8 col-sm-12">
							<input type="text" name="nama_produk" placeholder="Tulis Nama Produk/Mushaf" required="required" class="form-control" value="{{$proses_pentashihan->nama_produk}}">
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Nama Penanggung Jawab Produk *<br>
						<small class="text-success font-weight-bold">Isi nama penanggung jawab Produk/Mushaf</small>
					</label>
						<div class="col-lg-8 col-sm-12">
							<input type="text" name="penanggung_jawab_produk" placeholder="Tulis nama penanggung jawab Produk/Mushaf" required="required" class="form-control" value="{{$proses_pentashihan->penanggung_jawab_produk}}">
						</div>
					</div>

					<div class="col-lg-4 col-sm-12 form-group" >
					</div>
					<div class="col-lg-8 col-sm-12 form-group" >
						<div class="row">
							<div class="col-sm-12" style="margin-bottom:25px;">
								<div id="wrapOplah" class="row">
									@if($proses_pentashihan_ukuran)
										<?php $loop_ukuran = 0; ?>
										@foreach($proses_pentashihan_ukuran as $row)
											<div class="col-sm-6" @if($loop_ukuran > 0) style="margin-top:10px;" @endif>
												<label class="verysmall-green-default">
													Ukuran (cm) *
													@if($loop_ukuran == 0)
													<br>
													<small class="text-success font-weight-bold" style="white-space: pre-line;min-height: 40px; display: block;">Diisi dengan format panjang x lebar (dalam satuan cm). Misal: 29,7 x 20 (ditulis tanpa cm)</small>
													@else
													<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dalam ukuran centimeter, dan format panjang X tinggi. misal:15 X 20"></i>
													@endif
												</label>
												<input type="text" name="ukuran[]" placeholder="Tulis ukuran" required="required" class="form-control" value="{{$row->ukuran}}">
											</div>
											<div class="col-sm-6" @if($loop_ukuran > 0) style="margin-top:10px;" @endif>
												<label class="verysmall-green-default">
													Oplah *
													@if($loop_ukuran == 0)
													<br>
													<small class="text-success font-weight-bold" style="white-space: pre-line;min-height: 40px; display: block;">Diisi dengan rencana oplah. Misal: 100000</small>
													@else
													<i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Diisi dengan angka dan jml unit. misal:100000 eksemplar"></i>
													@endif
												</label>
												@if($loop_ukuran == 0)
												<button id="btnOplah" type="button" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus"></i></button>
												@else
												<button type="button" class="btn btn-xs btn-warning pull-right btn-remove-oplah"><i class="fa fa-times"></i></button>
												@endif
												<input type="text" name="oplah[]" placeholder="Tulis oplah" required="required" class="form-control" value="{{$row->oplah}}">
											</div>
											<?php $loop_ukuran++; ?>
										@endforeach
									@else
										<div class="col-sm-6">
											<label class="verysmall-green-default">
												Ukuran (cm) * <br>
												<small class="text-success font-weight-bold" style="white-space: pre-line;min-height: 40px; display: block;">Diisi dengan format panjang x lebar (dalam satuan cm). Misal: 29,7 x 20 (ditulis tanpa cm)</small>
											</label>
											<input type="text" name="ukuran[]" placeholder="Tulis ukuran" required="required" class="form-control" value="">
										</div>
										<div class="col-sm-6">
											<label class="verysmall-green-default">
												Oplah * <br>
												<small class="text-success font-weight-bold" style="white-space: pre-line;min-height: 40px; display: block;">Diisi dengan rencana oplah. Misal: 100000</small>
											</label>
											<button id="btnOplah" type="button" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus"></i></button>
											<input type="text" name="oplah[]" placeholder="Tulis oplah" required="required" class="form-control" value="">
										</div>
									@endif
								</div>	
							</div>	
						</div>	
					</div>	

					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Jenis Naskah *
						<br>
						<small class="text-success font-weight-bold">Pilih jenis naskah yang sesuai dengan mushaf yang akan Anda terbitkan. <br> Pilihan boleh lebih dari satu. Jika mushaf yang akan diterbitkan adalah mushaf digital, maka wajib mengunggah file apk</small>

						</label>
						<div class="col-lg-8 col-sm-12">
						  				<?php  
															$slice            = explode(' ', $row->name);
															$show_file_naskah = false;
									  					for ($i=0; $i < count($slice) ; $i++) { 
									  						($slice == 'digital' || $slice == 'DIGITAL' || $slice == 'Digital' ? $show_file_naskah == true : '');
									  					}
									  				?>

						<div class="row">
										  	@foreach($jenis_naskah as $row)
									  		<?php
									  			$slice            = explode(' ', $row->name);
													$show_file_naskah = false;
							  					for ($i=0; $i < count($slice) ; $i++) { 
							  						($slice[$i] == 'digital' || $slice[$i] == 'DIGITAL' || $slice[$i] == 'Digital' ? $show_file_naskah = true : '');
							  					}

												  $jenis_naskah = explode(';', $proses_pentashihan->id_m_jenis_naskah);

												  $m_jenis_naskah = [];

												  foreach($jenis_naskah as $val)
												  {
													$m_jenis_naskah[$val] = "checked";
												  }
									  		?>
									  		<div class="col-sm-6">
									  			<div class="radio-inline pointer" style="padding-left: 0;">
													<input id="{{'jenisNaskah-'.$row->id}}" class="form-check-input @if($show_file_naskah) show-file @endif " name="id_m_jenis_naskah[]" type="checkbox" @if(array_key_exists($row->name, $m_jenis_naskah)) checked @endif value="{{$row->name}}">
													<label for="{{'jenisNaskah-'.$row->id}}" class="form-check-label" for="{{$row->name}}">{{$row->name}}</label>
												</div>
									  		</div>
											@endforeach

									  	</div>
									  	<div style="padding-top: 15px;display: @if($show_file_naskah) block @else none @endif;">
											@if(Request::segment('3') == 'ralat')
												@if($proses_pentashihan->file_naskah != '')
													<p id="fileNaskah" class="text-muted" style="margin-bottom: 40px;">
														<a href="{{asset($proses_pentashihan->file_naskah)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
														<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->file_naskah.'&amp;id='.Request::segment(4).'&amp;column=file_naskah')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
														<em class="pull-left">*Hapus untuk mengganti file ini</em>
													</p>
												@else
												<input type="file" id="fileNaskah" name="new_file_naskah" class="file-forum">
												@endif
											@else
												<input type="file" id="fileNaskah" name="new_file_naskah" class="file-forum">
											@endif
									  	</div>
						</div>
					</div>
					
					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Jenis Mushaf *
							<br>
							<small class="text-success font-weight-bold">Pilih jenis mushaf yang sesuai dengan mushaf yang akan Anda terbitkan.</small>
						</label>
						<div class="col-lg-8 col-sm-12">
											<select name="id_m_jenis_mushaf" required="required" class="form-control select2">
									  		<option class="select-empty-value" value="">Pilih Jenis Mushaf</option>
									  		@foreach($jenis_mushaf as $row)
									  			<option @if($row->id == $proses_pentashihan->id_m_jenis_mushaf) selected="" @endif value="{{$row->id}}">{{$row->name}}</option>
									  		@endforeach
									  	</select>
						</div>
					</div>


					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Nama Percetakan *
							<br>
							<small class="text-success font-weight-bold">Diisi dengan nama percetakan tempat mushaf yang didaftarkan akan dicetak. Misal: Gramedia, Bekasi.</small>
						</label>
						<div class="col-lg-8 col-sm-12">
								<input type="text" name="nama_percetakan" placeholder="Tulis nama percetakan" class="form-control" value="{{$proses_pentashihan->nama_percetakan}}">
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Deskripsi Mushaf *	<br>
						<small class="text-success font-weight-bold">Diisi dengan deskripsi mushaf yang didaftarkan.</small>
					</label>
						<div class="col-lg-8 col-sm-12">
							<textarea name="keterangan" placeholder="Tulis deskripsi mushaf" required="required" class="form-control" rows="5">{{$proses_pentashihan->keterangan}}</textarea>
						</div>
					</div>

					<div id="divIzinEdar">

					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Negara asal mushaf *
							<br>
							<small class="text-success font-weight-bold">Pilih negara asal mushaf yang didaftarkan.</small>

						</label>
						<div class="col-lg-8 col-sm-12">
											<select name="id_countries" required="required" class="form-control select2">
									  		<option class="select-empty-value" value="">Pilih negara asal mushaf</option>
									  		@foreach($countries as $row)
									  			<option @if($row->id == $proses_pentashihan->id_countries) selected="" @endif value="{{$row->id}}">{{$row->name}}</option>
									  		@endforeach
									  	</select>
						</div>
					</div>
					

					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Penerbit asal mushaf *
							<br>
							<small class="text-success font-weight-bold">Diisi nama lembaga pentashih mushaf asal.</small>
						</label>
						<div class="col-lg-8 col-sm-12">
								<input type="text" name="nama_penerbit_asal" placeholder="Tulis Nama penerbit asal Mushaf" class="form-control" value="{{$proses_pentashihan->nama_penerbit_asal}}">
								<small class="text-success font-weight-bold">Diisi nama penerbit mushaf asal.</small>
						</div>
					</div>

					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Lembaga pentashih asal mushaf *</label>
						<div class="col-lg-8 col-sm-12">
								<input type="text" name="nama_lembaga_pentashih_asal" placeholder="Tulis Nama lembaga pentashih asal Mushaf" class="form-control" value="{{$proses_pentashihan->nama_lembaga_pentashih_asal}}">
						</div>
					</div>

					<div class="col-12 form-group">
						<label class="col-lg-4 col-sm-12 verysmall-green-default">
						Bukti tashih Lembaga pentashih asal mushaf * <br>
						<small class="text-success font-weight-bold"> Unggah tanda tashih dari lembaga pentashih mushaf asal dengan format file pdf. Ukuran masimal 500 kb</small>

						</label>
						<div class="col-lg-8 col-sm-12">
						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->bukti_tashih_lembaga_pentashih_asal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->bukti_tashih_lembaga_pentashih_asal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->bukti_tashih_lembaga_pentashih_asal.'&amp;id='.Request::segment(4).'&amp;column=bukti_tashih_lembaga_pentashih_asal')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							 <input type="file" name="bukti_tashih_lembaga_pentashih_asal" class="file-forum"  data-target="2" accept="application/pdf" onchange="ValidateExtensionPDF(this)">
							@endif
						@else
							<input type="file" name="bukti_tashih_lembaga_pentashih_asal" class="file-forum" data-target="2" accept="application/pdf" onchange="ValidateExtensionPDF(this)">
						@endif
						<a href="" id="2" target="_blank" class="medium-bold-green" style="display:none">Preview</a>

						</div>
					</div> 
					
					</div> 


				</div>

					<div class="col-sm-12 form-group row" id="divNoMushafLama">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">No. Pendaftaran mushaf lama *
							<br>
						<small class="text-success font-weight-bold">Pilih No. pendaftaran mushaf lama yang ingin diperpanjang tanda tashihnya.</small>

						</label>
						<div class="col-lg-8 col-sm-12">
						<select required name="id_mushaf_lama" id="id_mushaf_lama" required="" class="form-control select2">
							<option selected disabled  class="select-empty-value" value="">Pilih No. Pendaftaran mushaf lama</option>
						</select>
						</div>
					</div>
		
						<div class="col-sm-12 form-group row" id="divKodeMushafLama">
							<label class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Nomor, kode dan ukuran tanda tashih mushaf lama *
								<br>
								<small class="text-success font-weight-bold">Pilih Nomor, kode dan ukuran tanda tashih mushaf lama yang ingin diperpanjang tanda tashihnya.</small>

							</label>
							<div class="col-lg-8 col-sm-12">
								<select required name="id_proses_pentashihan_ukuran" id="id_proses_pentashihan_ukuran" required="" class="form-control select2">
								<option selected disabled  class="select-empty-value" value="">Pilih Nomor, kode dan ukuran tanda tashih mushaf lama</option>
							  	</select>
							</div>
						</div>

					<div class="col-12 form-group" id="divPembaruan">
						<label class="col-lg-4 col-sm-12 verysmall-green-default">
							Scan Surat Pernyataan tidak ada perubahan pada master naskah * <br>
							<small class="text-success font-weight-bold">Format file : pdf. Ukuran masimal : 500 kb</small>

						</label>
						<div class="col-lg-8 col-sm-12">
						<p class="verysmall-green-default">Contoh Surat Pernyataan tidak ada perubahan pada master naskah dapat diunduh <a href="{{ url('download-materi')  }}">di sini</a></p>

						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->file_surat_pernyataan != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->file_surat_pernyataan)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->file_surat_pernyataan.'&amp;id='.Request::segment(4).'&amp;column=file_surat_pernyataan')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							 <input type="file" name="new_file_surat_pernyataan" class="file-forum"  data-target="1"  accept="application/pdf" onchange="ValidateExtensionImage(this)">
							@endif
						@else
							<input type="file" name="new_file_surat_pernyataan" class="file-forum"  data-target="1"  accept="application/pdf" onchange="ValidateExtensionImage(this)">
						@endif

						<a href="" id="1" target="_blank" class="medium-bold-green" style="display:none">Preview</a>

						</div>
					</div>
					


					<div class="col-sm-12" style="margin-bottom:50px;;margin-top:50px;">
						<h3 style="margin-bottom:25px;"><b>II. Informasi Data Materi Tambahan pada Mushaf</b></h3>
						<p>
							Isikan data materi tambahan pada mushaf yang Anda daftarkan.
						</p>
					</div>
	
					<div class="col-sm-12 form-group row">
						<label for="name" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">
							Materi tambahan pada mushaf <br>
							<small class="text-success font-weight-bold">Pilih materi tambahan yang terdapat pada mushaf yang akan Anda terbitkan. Pilihan boleh lebih dari satu.</small>					

						</label>
						<div class="col-lg-8 col-sm-12" style="padding-left:30px;">
						<div class="col-sm-12 form-group">
										<?php 
											$materi_tambahan_lain = explode(';', $proses_pentashihan->id_m_materi_tambahan);
										?>
										@foreach($materi_tambahan as $row)
										<div class="form-check radio-inline pointer" style="padding-left: 0;">
											<input id="{{'materi_tambahan-'.$row->id}}" class="form-check-input" @if(in_array($row->name, $materi_tambahan_lain)) checked="" @endif name="materi_tambahan_lain[]" type="checkbox" value="{{$row->name}}">
											<label for="{{'materi_tambahan-'.$row->id}}" class="form-check-label" for="{{$row->name}}">{{$row->name}}</label>
										</div>
										@endforeach
										<div class="form-check radio-inline pointer" style="padding-left: 0;">
											<input id="lainnya" class="form-check-input" name="lainnya" type="checkbox" value="Lainnya">
											<label class="form-check-label" for="lainnya">Lainnya</label>
										</div>
										<div id="wrapper" class="row" style="margin-top: 25px; @if(Request::segment(3) == 'add') display: none; @else display: block; @endif">
											<div class="col-sm-1">
												<button type="button" id="buttonWrapper" class="btn btn-sm btn-success" data-toggle="tooltip" title="Jika form disamping dikosongkan tidak akan masuk kedalam data">
													<i class="fa fa-plus"></i>
												</button>
											</div>
											<div id="wrapperAppend" class="col-sm-11 row">
												@if(Request::segment(3) == 'ralat')
													<div class="col-sm-6 form-group relative">
														<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control lainnya">
														<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>
													</div>
													<div class="col-sm-6 form-group relative">
														<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control lainnya">
														<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>
													</div>
												@else
													@foreach($proses_pentashihan_materi_tambahan as $row)
														<div class="col-sm-6 form-group relative">
															<input type="text" name="materi_lainnya[]" placeholder="Tulis materi lainnya" class="form-control lainnya" value="{{$row->name}}">
															<button type="button" class="btn btn-remove btn-default btn-sm"><i class="fa fa-times"></i></button>
														</div>
													@endforeach
												@endif
											</div>
										</div>
									</div>
		
						</div>
					</div>

					<div class="col-12 form-group">
						<label id="labelPenanggungJawabMateriTambahan" class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">
								Penanggung Jawab Materi Tambahan  <br>
								<small class="text-success font-weight-bold">Isi penanggung jawab materi tambahan</small>
						</label>
						<div class="col-lg-8 col-sm-12">
							<textarea id="penanggungJawabMateriTambahan" name="penanggung_jawab_materi_tambahan" placeholder="Tulis penanggung jawab materi tambahan" class="form-control" rows="3">{{$proses_pentashihan->penanggung_jawab_materi_tambahan}}</textarea>
						</div>
					</div>

				<div id="dataNaskah" class="row">
					<div class="col-sm-12" style="margin-bottom:50px;">
						<h3 style="margin-top:50px;"><b>III. Informasi Dokumen Mushaf</b></h3>
						<p>Unggah dokumen mushaf</p>
					</div>

					<div class="col-12 form-group">
						<label class="col-lg-4 col-sm-12 col-form-label verysmall-green-default">Scan Surat Permohonan Pentashihan *
						<br>
						<small class="text-success font-weight-bold">Format file : pdf. Ukuran masimal : 500 kb</small>

						</label>
						<div class="col-lg-8 col-sm-12">
						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->surat_permohonan != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->surat_permohonan)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->surat_permohonan.'&amp;id='.Request::segment(4).'&amp;column=surat_permohonan')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							 <input type="file" name="new_surat_permohonan" class="file-forum" data-target="3" accept="application/pdf" onchange="ValidateExtensionPDF(this)">
							@endif
						@else
							<input type="file" name="new_surat_permohonan" class="file-forum" data-target="3" accept="application/pdf" onchange="ValidateExtensionPDF(this)">
						@endif
						<a href="" id="3" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
						<br>
						</div>
					</div>
					</div>

					<div class="col-12 form-group">
						<label class="col-lg-4 col-sm-12 verysmall-green-default">
							Scan Gambar Cover *
							<br>
						<small class="text-success font-weight-bold">Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 500 kb</small>

						</label>
						<div class="col-lg-8 col-sm-12">

						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->gambar_cover != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->gambar_cover)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->gambar_cover.'&amp;id='.Request::segment(4).'&amp;column=gambar_cover')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							<input type="file" name="new_gambar_cover" class="file-forum"  data-target="4" required="required"  accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)">
							@endif
						@else
							<input type="file" name="new_gambar_cover" class="file-forum" data-target="4" required="required"  accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)">
						@endif
						<a href="" id="4" target="_blank" class="medium-bold-green" style="display:none">Preview</a>

						<br>
						</div>
					</div>
		
					<div class="col-12 form-group">
						<label class="col-lg-4 col-sm-12 verysmall-green-default">
							Scan Contoh Dokumen Naskah *
							<br>
							<small class="text-success font-weight-bold">Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 500 kb</small>

							<!-- <i class="fa fa-question-circle custom-tooltip" data-toggle="tooltip" title="Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 500 kb"></i> -->
						</label>
						<div class="col-lg-8 col-sm-12">

						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->dokumen_naskah != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->dokumen_naskah)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->dokumen_naskah.'&amp;id='.Request::segment(4).'&amp;column=dokumen_naskah')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							 <input type="file" name="new_dokumen_naskah" class="file-forum"  data-target="5" required="required"  accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)">
							@endif
						@else
							<input type="file" name="new_dokumen_naskah" class="file-forum"  data-target="5" required="required"  accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)" >
						@endif
						<a href="" id="5" target="_blank" class="medium-bold-green" style="display:none">Preview</a>

						<br>

						</div>
					</div> 

					<div class="col-12 form-group">
						<label class="col-lg-4 col-sm-12 verysmall-green-default">
							Apakah Sudah dilakukan Tashih Internal? *
						</label>

						<div class="col-lg-8 col-sm-12">

						</div>
					</div>

					<div class="col-12 form-group">
						<div class="form-check pointer radio-inline" style="padding-top: 10px;padding-bottom: 10px;">
							<input @if($proses_pentashihan->tashih_internal == 'Sudah') checked="" @endif class="form-check-input radio-inline" name="tashih_internal" type="radio" id="sudah" value="Sudah">
							<label for="sudah" class="form-check-label" for="sudah">Sudah</label>
						</div>
						<div class="form-check pointer radio-inline" style="padding-top: 10px;padding-bottom: 10px;">
							<input @if($proses_pentashihan->tashih_internal == 'Belum' || $proses_pentashihan->tashih_internal == '') checked="" @endif class="form-check-input radio-inline" name="tashih_internal" type="radio" id="belum" value="Belum" checked="">
							<label class="form-check-label" for="belum">Belum</label>
						</div>
					</div>

					<div class="col-12 form-group">

						<label id="labelBuktiTashihInternal" class="col-lg-4 col-sm-12 verysmall-green-default">
							Scan Bukti Tashih Internal <br>
						<small class="text-success font-weight-bold">Format file : jpg, jpeg, png, pdf, gif. Ukuran masimal : 500 kb</small>

						</label>

						<div class="col-lg-8 col-sm-12">

						@if(Request::segment('3') == 'ralat')
							@if($proses_pentashihan->bukti_tashih_internal != '')
								<p class="text-muted" style="margin-bottom: 40px;">
									<a href="{{asset($proses_pentashihan->bukti_tashih_internal)}}" target="_blank" class="btn btn-sm btn-primary pull-left" style="margin-right: 10px;">Show</a>
									<a class="btn btn-danger btn-delete btn-sm pull-left" onclick="if(!confirm('Are you sure ?')) return false" href="{{CRUDBooster::mainpath('delete-attachment?image='.$proses_pentashihan->bukti_tashih_internal.'&amp;id='.Request::segment(4).'&amp;column=bukti_tashih_internal')}}" class="pill-left" style="margin-right: 10px;"><i class="fa fa-ban"></i></a>
									<em class="pull-left">*Hapus untuk mengganti file ini</em>
								</p>
							@else
							 <input type="file" name="new_bukti_tashih_internal" class="file-forum"  data-target="6" accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)">
							@endif
						@else
							<input type="file" name="new_bukti_tashih_internal" class="file-forum"  data-target="6" accept="image/*,application/pdf" onchange="ValidateExtensionImage(this)">
						@endif
						<a href="" id="6" target="_blank" class="medium-bold-green" style="display:none">Preview</a>
						
						</div>
					</div>



			</div> <!-- ./box-body -->
			
			<div class="box-footer">
			<br>
												  	
			<div id="divAgreement" style="margin-bottom:50px;" class="col-sm-12">
            <H3><B>
			Pernyataan
			</B>
			</H3> <br>

			Baca dan beri tanda centang pada pernyataan berikut: <br>

			<small class="text-success font-weight-bold">Apabila ceklis pernyataan tidak tampil, silahkan pilih ulang jenis permohonan</small>

			</div>


			
			<div class="form-check">
					<div class="col-lg-1 col-xs-2">
						<input class="form-check-input" type="checkbox" value="" id="agreement" style="margin-left:15px;">
					</div>

					<div class="col-lg-11 col-xs-10" >
						<label class="form-check-label" for="check" id="agreementPendaftaran">
							Saya menyatakan bahwa seluruh data yang terisi dalam formulir pendaftaran mushaf Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.
						</label>
						<label class="form-check-label" for="check" id="agreementPerpanjangan">
							Saya menyatakan bahwa seluruh data yang terisi dalam formulir perpanjangan tanda tashih mushaf Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.
						</label>
						<label class="form-check-label" for="check" id="agreementIzinEdar">
							Saya menyatakan bahwa seluruh data yang terisi dalam formulir izin edar mushaf Al-Qur’an ini adalah benar dan merupakan tanggung jawab saya. Lajnah Pentashihan Mushaf Al-Qur’an Kementerian Agama Republik Indonesia tidak bertanggung jawab atas kesalahan dalam pengisian data oleh saya. Apabila dalam penyampaian informasi dalam permohonan terbukti tidak benar maka saya bersedia dikenakan sanksi sesuai dengan peraturan yang berlaku.
						</label>
					</div>
			</div>
						
				<div class="pull-right">
				<div class="form-group">
					<br>
						<a href="{{CRUDBooster::mainpath('preview-insert')}}" class="btn btn-default">
							<i class="fa fa-chevron-circle-left"></i> Back
						</a>

						<input disabled  id="addBtn" type="submit" name="submit" value="Save" class="btn btn-success">
				</div>
				</div>
			</div> <!-- ./box-footer -->
			<input type="hidden" name="_token" value="{{$token}}">
		</form>
	</div>

</div>


<script type="text/javascript">
	var segment = "{{Request::segment(3)}}";
</script>

@endsection
@push('bottom')


<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script>
		var riwayatSelect = $('select[name=id_m_jenis_mushaf]').select2();
		var countrySelect = $('select[name=id_countries]').select2();
		var mushafLamaSelect = $('select[name=id_mushaf_lama]').select2();
		var kodeMushafLamaSelect = $('select[name=id_proses_pentashihan_ukuran]').select2();
		var jenis_permohonan = '{{$proses_pentashihan->jenis_permohonan}}';

		riwayatSelect.on("change", function (e)
		{
			if(this.value != '')
			{
				$.ajax({
					url: '{{url('api/mushaf_lama')}}',
					data:
					{
						id_m_jenis_mushaf: this.value,
						id_cms_users: {{$id_cms_users}}
					}
				}).then(function (res) {
					// create the option and append to Select2
						var options = []
						res.data.forEach(e => {
							options.push(new Option(e.nomor_registrasi , e.id, true, true))
						})
						mushafLamaSelect.empty().trigger('change');
						mushafLamaSelect.append(options).trigger('change');
						mushafLamaSelect.val('{{$id_mushaf_lama}}').trigger("change");
					});
			}
		})
		
		mushafLamaSelect.on("change", function (e)
		{
			if(this.value != '')
			{
				$.ajax({
					url: '{{url('api/rekap_pentashihan')}}',
					data:
					{
						id_proses_pentashihan: this.value
					}
				}).then(function (res) {
					// create the option and append to Select2
						var options = []
						res.data.forEach(e => {
							options.push(new Option(e.nomor +' | ' + e.kode +' | ' + e.ukuran  , e.id, true, true))
						})
						kodeMushafLamaSelect.empty().trigger('change');
						kodeMushafLamaSelect.append(options).trigger('change');
						kodeMushafLamaSelect.val('{{$id_proses_pentashihan_ukuran}}').trigger('change');
					});
			}
		})

		document.addEventListener('readystatechange', function(event) {
            if (event.target.readyState === "complete") {
				riwayatSelect.val('{{$id_m_jenis_mushaf}}').trigger("change");
				mushafLamaSelect.val('{{$id_mushaf_lama}}').trigger("change");
				kodeMushafLamaSelect.val('{{$id_proses_pentashihan_ukuran}}').trigger("change");
            }
        });
	
	$(document).ready(function() {

        $("#divNoMushafLama").hide();
        $("#divKodeMushafLama").hide();
        $("#divPembaruan").hide();
        $("#divIzinEdar").hide();

        // $("#agreement").hide();

        $("#agreementPendaftaran").hide();
        $("#agreementPerpanjangan").hide();
        $("#agreementIzinEdar").hide();

		showHide(jenis_permohonan);
	});

	function showHide(val)
	{
		if (val == 'Pembaruan/perpanjangan Surat Tanda Tashih') {
				$("#divNoMushafLama").show(200);
				$("#divKodeMushafLama").show(200);
				$("#divPembaruan").show(200);

				$("#divIzinEdar").hide(200);

				$("#agreementIzinEdar").hide(200);
				$("#agreementPendaftaran").hide(200);
				$("#agreementPerpanjangan").show(200);
				
				$("input[name='new_file_surat_pernyataan']").attr("required", "required");
				$("input[name='new_surat_permohonan']").attr("required", "required");
				$("input[name='id_mushaf_lama']").attr("required", "required");


				$("input[name='new_surat_permohonan']").removeAttr("required", "required");
				$("input[name='id_proses_pentashihan_ukuran']").removeAttr("required", "required");
				$("input[name='id_mushaf_lama']").removeAttr("required", "required");
				
				$("input[name='id_countries']").removeAttr("required", "required");
				$("input[name='nama_lembaga_pentashih_asal']").removeAttr("required", "required");
				$("input[name='nama_lembaga_pentashih_asal']").removeAttr("required", "required");
				$("input[name='bukti_tashih_lembaga_pentashih_asal']").removeAttr("required", "required");
			}
			else if(val == 'Surat Izin Edar')
			{
				$("#divIzinEdar").show(200);

				$("#agreementIzinEdar").show(200);
				$("#agreementPerpanjangan").hide(200);
				$("#agreementPendaftaran").hide(200);

				$("input[name='new_surat_permohonan']").removeAttr("required", "required");
				$("input[name='id_proses_pentashihan_ukuran']").removeAttr("required", "required");
				$("input[name='id_mushaf_lama']").removeAttr("required", "required");

				$("input[name='id_countries']").attr("required", "required");
				$("input[name='nama_penerbit_asal']").attr("required", "required");
				$("input[name='nama_lembaga_pentashih_asal']").attr("required", "required");
				$("input[name='bukti_tashih_lembaga_pentashih_asal']").attr("required", "required");
				$("input[name='new_file_surat_pernyataan']").removeAttr("required", "required");
			}
			else if(val == 'Surat Tanda tashih Baru')
			{
				$("#divKodeMushafLama").hide(200);
				$("#divNoMushafLama").hide(200);
				$("#divPembaruan").hide(200);
				$("#divIzinEdar").hide(200);

				$("#agreementIzinEdar").hide(200);
				$("#agreementPerpanjangan").hide(200);
				$("#agreementPendaftaran").show(200);

				$("input[name='new_surat_permohonan']").removeAttr("required", "required");
				$("input[name='id_proses_pentashihan_ukuran']").removeAttr("required", "required");
				$("input[name='id_mushaf_lama']").removeAttr("required", "required");

				$("input[name='id_countries']").removeAttr("required", "required");
				$("input[name='nama_lembaga_pentashih_asal']").removeAttr("required", "required");
				$("input[name='new_file_surat_pernyataan']").removeAttr("required", "required");
				$("input[name='bukti_tashih_lembaga_pentashih_asal']").removeAttr("required", "required");
			}
	}


	$("#agreement").click(function()
	{
		$("#addMoreBtn").attr("disabled", !this.checked);
		$("#addBtn").attr("disabled", !this.checked);
	});

	$(function () {
		$("#jenis_permohonan").change(function () {
			val = $(this).val();
			showHide(val);
		});
	});

	function ValidateExtensionPDF(e) {
		var allowedFiles = [".pdf"];
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
		if (!regex.test($(e).val().toLowerCase())) {
			alert("Please upload files having extensions: " + allowedFiles.join(', ') + " only.");
			$(e).val("");
			$(e).val(null);
			return false;
		}
		return true;
	}
	

	$('.file-forum').on("change", function(event){
			var tmppath = URL.createObjectURL(event.target.files[0]);
			const datatarget = $(this).data('target');
			$('#'+datatarget).attr('href',tmppath);
			$('#'+datatarget).show();
		});

	function ValidateExtensionImage(e) {
		var allowedFiles = [".pdf", ".png", ".jpg", ".jpeg", ".gif"];
		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
		if (!regex.test($(e).val().toLowerCase())) {
			alert("Please upload files having extensions: " + allowedFiles.join(', ') + " only.");
			$(e).val("");
			$(e).val(null);
			return false;
		}
		return true;
	}
</script>
@endpush
