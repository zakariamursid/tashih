<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')

    @if(!empty($isUserAnswer))
    <div class="alert alert-success">
        <h4><i class="icon fa fa-info"></i> Wow, good job...</h4>
        Trima kasih telah memberi kami pendapat anda
    </div>
    @else
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'>Survey Form</div>
        <div class='panel-body'>      
            <form action="{{ url('survey/save') }}" method="post">
            {{csrf_field()}}
            {{method_field('POST')}}
                
                @foreach($result as $x => $xrow)

                    <div class="panel panel-info">
                        <div class="panel-heading">{{ $xrow->nama_pertanyaan }}</div>
                        <div class="panel-body">
                            @if($xrow->type_pertanyaan == 'Pilihan')
                                @if(count($xrow->option) > 0)
                                    @foreach($xrow->option as $d => $data)
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="pertanyaan{{$xrow->survey_pertanyaan_id}}" id="pertanyaan{{$xrow->survey_pertanyaan_id}}" value="{{ $data->pilihan_id }}" required>
                                        <b>{{ $data->opsi }} </b>. 
                                        {{ $data->nama }}
                                        </label>
                                    </div>
                                    @endforeach
                                @else
                                <div class="form-group">
                                    <p class="text-red">pilihan ganda belum ada.</p>
                                </div>
                                @endif
                            @elseif($xrow->type_pertanyaan == 'Essay')
                            <div class="form-group">
                                <textarea name="pertanyaan{{$xrow->survey_pertanyaan_id}}" id="pertanyaan{{$xrow->survey_pertanyaan_id}}" placeholder="isi form .." class="form-control" required cols="30" rows="10"></textarea>
                            </div>
                            @else
                            <div class="form-group">
                                <p class="text-red">undefinied.</p>
                            </div>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="id{{$xrow->survey_pertanyaan_id}}" value="{{$xrow->survey_pertanyaan_id}}">
                    
                @endforeach

                <input type="hidden" name="redirect_link" value="{{ g('redirect_link') }}">
                <input type="hidden" name="type_survey" value="{{ g('type') }}">
                <div class="form-group pull-right">
                    <button type="reset" class="btn btn-sm btn-danger">Reset</button>
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection